/* DO NOT edit this file, your changes will be overwritten and won't take any effect! */
/* Generated from CFLAGS: -DSTDIO_UART_DEV=UART_DEV(1) -DDEVELHELP -Werror -DCPU_FAM_STM32F4 -DSTM32F405xx -DCPU_LINE_STM32F405xx -DSTM32_FLASHSIZE=1048576U -mno-thumb-interwork -mcpu=cortex-m4 -mlittle-endian -mthumb -mfloat-abi=soft -ffunction-sections -fdata-sections -fno-builtin -fshort-enums -ggdb -g3 -Os -DCPU_MODEL_STM32F405RG -DCPU_ARCH_CORTEX_M4F -DBOARD_OLIMEX_STM32_P405="olimex-stm32-p405" -DRIOT_BOARD=BOARD_OLIMEX_STM32_P405 -DCPU_STM32F4="stm32f4" -DRIOT_CPU=CPU_STM32F4 -DMCU_STM32F4="stm32f4" -DRIOT_MCU=MCU_STM32F4 -std=c99 -fno-delete-null-pointer-checks -fdiagnostics-color -Wstrict-prototypes -Wold-style-definition -fno-common -Wall -Wextra -Wformat=2 -Wformat-overflow -Wformat-truncation -Wmissing-include-dirs -DMODULE_AUTO_INIT -DMODULE_AUTO_INIT_CAN -DMODULE_BOARD -DMODULE_CAN -DMODULE_CAN_ISOTP -DMODULE_CAN_MBOX -DMODULE_CAN_PM -DMODULE_CAN_RAW -DMODULE_CAN_TRX -DMODULE_CONN_CAN -DMODULE_CONN_CAN_ISOTP_MULTI -DMODULE_CORE -DMODULE_CORE_MBOX -DMODULE_CORE_MSG -DMODULE_CORTEXM_COMMON -DMODULE_CORTEXM_COMMON_PERIPH -DMODULE_CPU -DMODULE_DIV -DMODULE_GNRC_PKT -DMODULE_GNRC_PKTBUF -DMODULE_GNRC_PKTBUF_STATIC -DMODULE_ISRPIPE -DMODULE_NEWLIB -DMODULE_NEWLIB_NANO -DMODULE_NEWLIB_SYSCALLS_DEFAULT -DMODULE_PERIPH -DMODULE_PERIPH_CAN -DMODULE_PERIPH_COMMON -DMODULE_PERIPH_GPIO -DMODULE_PERIPH_GPIO_IRQ -DMODULE_PERIPH_GPRIO -DMODULE_PERIPH_PM -DMODULE_PERIPH_TIMER -DMODULE_PERIPH_UART -DMODULE_PM_LAYERED -DMODULE_PS -DMODULE_SHELL -DMODULE_SHELL_COMMANDS -DMODULE_STDIO_UART -DMODULE_STM32_COMMON -DMODULE_STM32_COMMON_PERIPH -DMODULE_SYS -DMODULE_TSRB -DMODULE_XTIMER -DRIOT_VERSION="2018.10-devel-986-g19282-wilson-pr/can_stm32" */
#define STDIO_UART_DEV UART_DEV(1)
#define DEVELHELP 1
#define CPU_FAM_STM32F4 1
#define STM32F405xx 1
#define CPU_LINE_STM32F405xx 1
#define STM32_FLASHSIZE 1048576U
#define CPU_MODEL_STM32F405RG 1
#define CPU_ARCH_CORTEX_M4F 1
#define BOARD_OLIMEX_STM32_P405 "olimex-stm32-p405"
#define RIOT_BOARD BOARD_OLIMEX_STM32_P405
#define CPU_STM32F4 "stm32f4"
#define RIOT_CPU CPU_STM32F4
#define MCU_STM32F4 "stm32f4"
#define RIOT_MCU MCU_STM32F4
#define MODULE_AUTO_INIT 1
#define MODULE_AUTO_INIT_CAN 1
#define MODULE_BOARD 1
#define MODULE_CAN 1
#define MODULE_CAN_ISOTP 1
#define MODULE_CAN_MBOX 1
#define MODULE_CAN_PM 1
#define MODULE_CAN_RAW 1
#define MODULE_CAN_TRX 1
#define MODULE_CONN_CAN 1
#define MODULE_CONN_CAN_ISOTP_MULTI 1
#define MODULE_CORE 1
#define MODULE_CORE_MBOX 1
#define MODULE_CORE_MSG 1
#define MODULE_CORTEXM_COMMON 1
#define MODULE_CORTEXM_COMMON_PERIPH 1
#define MODULE_CPU 1
#define MODULE_DIV 1
#define MODULE_GNRC_PKT 1
#define MODULE_GNRC_PKTBUF 1
#define MODULE_GNRC_PKTBUF_STATIC 1
#define MODULE_ISRPIPE 1
#define MODULE_NEWLIB 1
#define MODULE_NEWLIB_NANO 1
#define MODULE_NEWLIB_SYSCALLS_DEFAULT 1
#define MODULE_PERIPH 1
#define MODULE_PERIPH_CAN 1
#define MODULE_PERIPH_COMMON 1
#define MODULE_PERIPH_GPIO 1
#define MODULE_PERIPH_GPIO_IRQ 1
#define MODULE_PERIPH_GPRIO 1
#define MODULE_PERIPH_PM 1
#define MODULE_PERIPH_TIMER 1
#define MODULE_PERIPH_UART 1
#define MODULE_PM_LAYERED 1
#define MODULE_PS 1
#define MODULE_SHELL 1
#define MODULE_SHELL_COMMANDS 1
#define MODULE_STDIO_UART 1
#define MODULE_STM32_COMMON 1
#define MODULE_STM32_COMMON_PERIPH 1
#define MODULE_SYS 1
#define MODULE_TSRB 1
#define MODULE_XTIMER 1
#define RIOT_VERSION "2018.10-devel-986-g19282-wilson-pr/can_stm32"
