var boards_2iotlab_m3_2include_2board_8h =
[
    [ "EXTFLASH_CS", "boards_2iotlab-m3_2include_2board_8h.html#a01928dbdc6fd01222bfa5e5a56bf3571", null ],
    [ "EXTFLASH_HOLD", "boards_2iotlab-m3_2include_2board_8h.html#a334ae2cddb501ed1b6129e8c69545424", null ],
    [ "EXTFLASH_SPI", "boards_2iotlab-m3_2include_2board_8h.html#a4476564e4b7551c96894ae5141987242", null ],
    [ "EXTFLASH_WRITE", "boards_2iotlab-m3_2include_2board_8h.html#a1804a46dffc4c175f8cd32c8b8cac3c1", null ],
    [ "L3G4200D_PARAM_INT1", "boards_2iotlab-m3_2include_2board_8h.html#a4369c98a646dc6b33c4705c226215775", null ],
    [ "L3G4200D_PARAM_INT2", "boards_2iotlab-m3_2include_2board_8h.html#a556c0db52a076976f5b977e3af5afaf8", null ],
    [ "LPS331AP_PARAM_ADDR", "boards_2iotlab-m3_2include_2board_8h.html#a0335ae7c213239fee2a69bfa92832c25", null ],
    [ "LSM303DLHC_PARAM_ACC_PIN", "boards_2iotlab-m3_2include_2board_8h.html#a54667caad387e7efea60599960088d5c", null ],
    [ "LSM303DLHC_PARAM_MAG_PIN", "boards_2iotlab-m3_2include_2board_8h.html#a007cb9a37fae1243f8b064917a9e656f", null ]
];