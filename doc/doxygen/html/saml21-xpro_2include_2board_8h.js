var saml21_xpro_2include_2board_8h =
[
    [ "BTN0_MODE", "saml21-xpro_2include_2board_8h.html#a904884feea8e03c30c9a85ca6d7a8e5c", null ],
    [ "BTN0_PIN", "saml21-xpro_2include_2board_8h.html#aab5c3eca54046333af52593b9e360270", null ],
    [ "BTN0_PORT", "saml21-xpro_2include_2board_8h.html#a9a61388c9e491aec2e44cc03956bb299", null ],
    [ "LED0_MASK", "saml21-xpro_2include_2board_8h.html#abfabde35a3e8b7ab50146a5e223bfead", null ],
    [ "LED0_OFF", "saml21-xpro_2include_2board_8h.html#aef2e39b9fd343d0e437fac0463f3e02d", null ],
    [ "LED0_ON", "saml21-xpro_2include_2board_8h.html#a1e48dc02333bfc2e6ad3e5bd466eecd4", null ],
    [ "LED0_PIN", "saml21-xpro_2include_2board_8h.html#a3fc522fda449d6edde717d6a857429cf", null ],
    [ "LED0_TOGGLE", "saml21-xpro_2include_2board_8h.html#aebc6389533d9fc8dcbe4d2129a4d5a45", null ],
    [ "LED_PORT", "saml21-xpro_2include_2board_8h.html#a663daa01e565aee93c6f20c5845b90b4", null ],
    [ "board_init", "saml21-xpro_2include_2board_8h.html#a916f2adc2080b4fe88034086d107a8dc", null ]
];