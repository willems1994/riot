var group__drivers__can =
[
    [ "CAN device driver interface", "group__drivers__candev.html", "group__drivers__candev" ],
    [ "CAN transceiver interface", "group__drivers__can__trx.html", "group__drivers__can__trx" ],
    [ "STM32 CAN controller", "group__candev__stm32.html", "group__candev__stm32" ],
    [ "SocketCAN driver", "group__drivers__candev__linux.html", "group__drivers__candev__linux" ],
    [ "TJA1042", "group__drivers__tja1042.html", "group__drivers__tja1042" ]
];