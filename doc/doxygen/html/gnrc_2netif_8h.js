var gnrc_2netif_8h =
[
    [ "gnrc_netif_ops_t", "group__net__gnrc__netif.html#gacad0368e2e6e50fe0faecd208a6f6828", null ],
    [ "gnrc_netif_addr_from_str", "group__net__gnrc__netif.html#gaf16e926a8cd64665a2fe889bd0a07f65", null ],
    [ "gnrc_netif_addr_to_str", "group__net__gnrc__netif.html#ga3044f145e443972a3389f0cbe1daa402", null ],
    [ "gnrc_netif_create", "group__net__gnrc__netif.html#ga3cbc950e0ba5203c343161676cc93e8f", null ],
    [ "gnrc_netif_get_by_pid", "group__net__gnrc__netif.html#gae8568cf8c5a1647eceac9209fde86510", null ],
    [ "gnrc_netif_get_from_netdev", "group__net__gnrc__netif.html#gacef1ec847f603c4b55368476fa167018", null ],
    [ "gnrc_netif_ipv6_addr_add", "group__net__gnrc__netif.html#gaf96707a5e322b5fa8458fba45de01837", null ],
    [ "gnrc_netif_ipv6_addr_remove", "group__net__gnrc__netif.html#ga2be202d325400cc2f16158e1bf8df3a7", null ],
    [ "gnrc_netif_ipv6_addrs_get", "group__net__gnrc__netif.html#ga0c2ae9032acd63d1b915e90b923063eb", null ],
    [ "gnrc_netif_ipv6_group_join", "group__net__gnrc__netif.html#gafdd190b08249912f69f10f02c05e453c", null ],
    [ "gnrc_netif_ipv6_group_leave", "group__net__gnrc__netif.html#gac1b4ed4c52eb389d4a1224da55c767ca", null ],
    [ "gnrc_netif_ipv6_groups_get", "group__net__gnrc__netif.html#gad3dd62713036fcfabd6d8ba00acc86f8", null ],
    [ "gnrc_netif_iter", "group__net__gnrc__netif.html#gaa58a468fb9e82d7107e229f0239c4e53", null ],
    [ "gnrc_netif_numof", "group__net__gnrc__netif.html#ga79b293e98896c2831c2ca6750b32c031", null ],
    [ "gnrc_netif_set_from_netdev", "group__net__gnrc__netif.html#ga59885a7baa13748737e354272791ee43", null ]
];