var rcvbuf_8h =
[
    [ "rcvbuf_entry_t", "group__net__gnrc__tcp.html#ga9339adb60ecedee715407f36eb2c1e76", null ],
    [ "rcvbuf_t", "group__net__gnrc__tcp.html#gad2520dd7d6188756f929aed516fc8a4a", null ],
    [ "_rcvbuf_get_buffer", "group__net__gnrc__tcp.html#gaa9e93c6b55e0cf89a4e627a9099ec109", null ],
    [ "_rcvbuf_init", "group__net__gnrc__tcp.html#ga06ec68a92ad2bd571cd685e017926a73", null ],
    [ "_rcvbuf_release_buffer", "group__net__gnrc__tcp.html#ga7392f869837c408c6d74a06bdeb259d0", null ]
];