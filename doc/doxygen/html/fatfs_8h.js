var fatfs_8h =
[
    [ "EPOCH_YEAR_OFFSET", "group__sys__fatfs.html#gac60b25fabfee168b71723862d745cfa1", null ],
    [ "FATFS_MAX_ABS_PATH_SIZE", "group__sys__fatfs.html#gac2705d10dbe276b4b760ead6ef7f20e5", null ],
    [ "FATFS_MAX_VOL_STR_LEN", "group__sys__fatfs.html#ga936f8b2bea013417ca1c0826e8183781", null ],
    [ "FATFS_MOUNT_OPT", "group__sys__fatfs.html#ga7ef4d049a02926687d3e3ff7ec2da937", null ],
    [ "FATFS_YEAR_OFFSET", "group__sys__fatfs.html#ga91e6a88e878df2c8d1ecd3065d92dfa7", null ],
    [ "fatfs_desc_t", "group__sys__fatfs.html#ga55339a4dcebd9f1be581de4638b41670", null ],
    [ "fatfs_file_desc_t", "group__sys__fatfs.html#gaab14929b4866418e088e4283df9a0313", null ],
    [ "fatfs_file_system", "group__sys__fatfs.html#ga9a619a8314ba35fc986b101ddbb973dc", null ]
];