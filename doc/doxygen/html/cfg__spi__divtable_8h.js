var cfg__spi__divtable_8h =
[
    [ "CFG_SPIDIV_100", "cfg__spi__divtable_8h.html#ad113ea730fb956d873dc5f8439594d53", null ],
    [ "CFG_SPIDIV_20", "cfg__spi__divtable_8h.html#a4f3cc0a14e7f75533cff6176944a024f", null ],
    [ "CFG_SPIDIV_30", "cfg__spi__divtable_8h.html#aaf28521a8c30f47382570e20c3b0801a", null ],
    [ "CFG_SPIDIV_32", "cfg__spi__divtable_8h.html#aeefb9e26f70fe5ae911592c1a0aa59dc", null ],
    [ "CFG_SPIDIV_36", "cfg__spi__divtable_8h.html#aa687a1130cfc49bcad411037f82e0f5e", null ],
    [ "CFG_SPIDIV_40", "cfg__spi__divtable_8h.html#a0981f2034532d18565b2bafc89920fb2", null ],
    [ "CFG_SPIDIV_42", "cfg__spi__divtable_8h.html#a5d649be244bbe17d6666a6a76d978b22", null ],
    [ "CFG_SPIDIV_45", "cfg__spi__divtable_8h.html#ac7fde552e67aaac1a50d46922e11c944", null ],
    [ "CFG_SPIDIV_48", "cfg__spi__divtable_8h.html#a3cd5a3b2c4b553e536dc0f692f253061", null ],
    [ "CFG_SPIDIV_50", "cfg__spi__divtable_8h.html#a1c00cb96f2a4fba7d132dcc1965e6624", null ],
    [ "CFG_SPIDIV_60", "cfg__spi__divtable_8h.html#af68f3bfa363beba0a6a2ad4f9e66aaf4", null ],
    [ "CFG_SPIDIV_64", "cfg__spi__divtable_8h.html#a57b114d0c095a219b02da10cae332fcf", null ],
    [ "CFG_SPIDIV_72", "cfg__spi__divtable_8h.html#a7fc8eaaba4e7f3194516b9c064408a7d", null ],
    [ "CFG_SPIDIV_84", "cfg__spi__divtable_8h.html#a5492de721312eab3e521677586754617", null ],
    [ "CFG_SPIDIV_90", "cfg__spi__divtable_8h.html#aa7b17fc4f277a4deb60058d89016564b", null ],
    [ "CFG_SPIDIV_96", "cfg__spi__divtable_8h.html#a42b7b0ba89a37d9cac076fc11ee2b44e", null ],
    [ "spi_divtable", "cfg__spi__divtable_8h.html#ae3b1d06e940a46447dea0987400fdc04", null ]
];