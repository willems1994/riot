var group__drivers__ina220 =
[
    [ "ina220-regs.h", "ina220-regs_8h.html", null ],
    [ "ina220.h", "ina220_8h.html", null ],
    [ "ina220_t", "structina220__t.html", [
      [ "addr", "structina220__t.html#ae84538f8579334b4dafc28d12c4f78a5", null ],
      [ "i2c", "structina220__t.html#a689a4b28e6ddfbc3386254494b8afc12", null ]
    ] ],
    [ "INA220_BUS_VOLTAGE_SHIFT", "group__drivers__ina220.html#gacaf868bdcaa0196ce706cc7b832bbef7", null ],
    [ "INA220_RESET_BIT", "group__drivers__ina220.html#gae9fc61f8db33eb5ac4a184e43f529c8b", null ],
    [ "ina220_badc_t", "group__drivers__ina220.html#ga4ae9d12e0d60c3aba65618301bb76bbe", null ],
    [ "ina220_brng_t", "group__drivers__ina220.html#ga31c5233ee6700fc2d764a6087e8f23f0", null ],
    [ "ina220_mode_t", "group__drivers__ina220.html#ga0f784d230e205ec359463718a27a3786", null ],
    [ "ina220_range_t", "group__drivers__ina220.html#gaa2966bf91bdacbcb09aab32d8d0c1f3a", null ],
    [ "ina220_sadc_t", "group__drivers__ina220.html#ga523b5ccd39186a3f02ec318a79b93741", null ],
    [ "ina220_badc", "group__drivers__ina220.html#gaeddbbbbeed3dbfa5ce57cff3b4bbd56d", [
      [ "INA220_BADC_9BIT", "group__drivers__ina220.html#ggaeddbbbbeed3dbfa5ce57cff3b4bbd56da5ac48e8214811601e78f00abb0b44ce9", null ],
      [ "INA220_BADC_10BIT", "group__drivers__ina220.html#ggaeddbbbbeed3dbfa5ce57cff3b4bbd56da8be02d516a1f03e0a43055a5f54f0b1b", null ],
      [ "INA220_BADC_11BIT", "group__drivers__ina220.html#ggaeddbbbbeed3dbfa5ce57cff3b4bbd56da711d7f0a750cea4523bbf6f10f738aee", null ],
      [ "INA220_BADC_12BIT", "group__drivers__ina220.html#ggaeddbbbbeed3dbfa5ce57cff3b4bbd56da85279237fd8ede7270d040f1eb90306a", null ],
      [ "INA220_BADC_AVG_1_SAMPLE", "group__drivers__ina220.html#ggaeddbbbbeed3dbfa5ce57cff3b4bbd56da4031e64147f88aeed68fdee6279a4959", null ],
      [ "INA220_BADC_AVG_2_SAMPLES", "group__drivers__ina220.html#ggaeddbbbbeed3dbfa5ce57cff3b4bbd56dad1fdfc0b0dd35e9ce7ee12a53c05ede6", null ],
      [ "INA220_BADC_AVG_4_SAMPLES", "group__drivers__ina220.html#ggaeddbbbbeed3dbfa5ce57cff3b4bbd56da9a96b9d50f82ebed7dc31b3a60a85316", null ],
      [ "INA220_BADC_AVG_8_SAMPLES", "group__drivers__ina220.html#ggaeddbbbbeed3dbfa5ce57cff3b4bbd56dac7e9074bcc5fd0941dc248b35064c7a8", null ],
      [ "INA220_BADC_AVG_16_SAMPLES", "group__drivers__ina220.html#ggaeddbbbbeed3dbfa5ce57cff3b4bbd56da181070ec85760b373ece0ade10191eeb", null ],
      [ "INA220_BADC_AVG_32_SAMPLES", "group__drivers__ina220.html#ggaeddbbbbeed3dbfa5ce57cff3b4bbd56dad38155744f84acd2cef33d5bba28336a", null ],
      [ "INA220_BADC_AVG_64_SAMPLES", "group__drivers__ina220.html#ggaeddbbbbeed3dbfa5ce57cff3b4bbd56da748d483dd3651f31a81f126f6e783dc5", null ],
      [ "INA220_BADC_AVG_128_SAMPLES", "group__drivers__ina220.html#ggaeddbbbbeed3dbfa5ce57cff3b4bbd56daaf02ce121c6a1a9fabe6598b5b2497ed", null ]
    ] ],
    [ "ina220_brng", "group__drivers__ina220.html#gac8c5fd669f24a1a9dda168e3ba22e381", [
      [ "INA220_BRNG_16V_FSR", "group__drivers__ina220.html#ggac8c5fd669f24a1a9dda168e3ba22e381a62c77fc547eb98810942a7d96d193dee", null ],
      [ "INA220_BRNG_32V_FSR", "group__drivers__ina220.html#ggac8c5fd669f24a1a9dda168e3ba22e381a3a2d1a8c403639ef435f02f14690a865", null ]
    ] ],
    [ "ina220_mode", "group__drivers__ina220.html#gad5693b3444ca1c43b709359ed2be4987", [
      [ "INA220_MODE_POWERDOWN", "group__drivers__ina220.html#ggad5693b3444ca1c43b709359ed2be4987ab9b07fff4dcf0a2e1aeb95b71d14aa01", null ],
      [ "INA220_MODE_TRIGGER_SHUNT_ONLY", "group__drivers__ina220.html#ggad5693b3444ca1c43b709359ed2be4987a311b5dcedbce3b3eb40ce4eeacd65562", null ],
      [ "INA220_MODE_TRIGGER_BUS_ONLY", "group__drivers__ina220.html#ggad5693b3444ca1c43b709359ed2be4987ab2aeb1ce4a929c8ec0890409a78c0ab8", null ],
      [ "INA220_MODE_TRIGGER_SHUNT_BUS", "group__drivers__ina220.html#ggad5693b3444ca1c43b709359ed2be4987ab54b16c5deb95d45326877cea60a0915", null ],
      [ "INA220_MODE_ADC_DISABLE", "group__drivers__ina220.html#ggad5693b3444ca1c43b709359ed2be4987a198981d8843112b84aed5f0aef9c1a49", null ],
      [ "INA220_MODE_CONTINUOUS_SHUNT_ONLY", "group__drivers__ina220.html#ggad5693b3444ca1c43b709359ed2be4987ac15eb16c57262218c0c6387c54000df1", null ],
      [ "INA220_MODE_CONTINUOUS_BUS_ONLY", "group__drivers__ina220.html#ggad5693b3444ca1c43b709359ed2be4987a560c4c6e69262d3701af3f6e797ec7f6", null ],
      [ "INA220_MODE_CONTINUOUS_SHUNT_BUS", "group__drivers__ina220.html#ggad5693b3444ca1c43b709359ed2be4987a0d6f095d967e90b01ec5d0fb82d2da25", null ]
    ] ],
    [ "ina220_range", "group__drivers__ina220.html#gac66d989204fc4df1cf1fe266f6c9a5d7", [
      [ "INA220_RANGE_40MV", "group__drivers__ina220.html#ggac66d989204fc4df1cf1fe266f6c9a5d7a25bfb86122d653fd671280116077ec2b", null ],
      [ "INA220_RANGE_80MV", "group__drivers__ina220.html#ggac66d989204fc4df1cf1fe266f6c9a5d7a8040ba04dd92f7a4c6420cb04bd0da9a", null ],
      [ "INA220_RANGE_160MV", "group__drivers__ina220.html#ggac66d989204fc4df1cf1fe266f6c9a5d7ac7c82baa078f1a745011bf2d95d186f9", null ],
      [ "INA220_RANGE_320MV", "group__drivers__ina220.html#ggac66d989204fc4df1cf1fe266f6c9a5d7a3b732773141a62e78280912374e29ee9", null ]
    ] ],
    [ "ina220_sadc", "group__drivers__ina220.html#gab16425153d99ad1d435779032c24fc35", [
      [ "INA220_SADC_9BIT", "group__drivers__ina220.html#ggab16425153d99ad1d435779032c24fc35ad5c253dd13ec800b488af507dea558e1", null ],
      [ "INA220_SADC_10BIT", "group__drivers__ina220.html#ggab16425153d99ad1d435779032c24fc35ad51fe0b0797fbcd7fc4f27d20e315f2e", null ],
      [ "INA220_SADC_11BIT", "group__drivers__ina220.html#ggab16425153d99ad1d435779032c24fc35ac49eed5bd6953d96bc9287eaa14e9a1f", null ],
      [ "INA220_SADC_12BIT", "group__drivers__ina220.html#ggab16425153d99ad1d435779032c24fc35a0e78b94583fc9e95b686a2cce78006a2", null ],
      [ "INA220_SADC_AVG_1_SAMPLE", "group__drivers__ina220.html#ggab16425153d99ad1d435779032c24fc35a62058ddbedaae70ee90efeb0f1524e3b", null ],
      [ "INA220_SADC_AVG_2_SAMPLES", "group__drivers__ina220.html#ggab16425153d99ad1d435779032c24fc35ae0848986f0978a850c5323d8ed823606", null ],
      [ "INA220_SADC_AVG_4_SAMPLES", "group__drivers__ina220.html#ggab16425153d99ad1d435779032c24fc35a8ecb86aaf2a98f432b6ddd26321db839", null ],
      [ "INA220_SADC_AVG_8_SAMPLES", "group__drivers__ina220.html#ggab16425153d99ad1d435779032c24fc35a743809bab43d493f6da229cba8e5d47b", null ],
      [ "INA220_SADC_AVG_16_SAMPLES", "group__drivers__ina220.html#ggab16425153d99ad1d435779032c24fc35aac61e71d2da5700ec1bd048746957898", null ],
      [ "INA220_SADC_AVG_32_SAMPLES", "group__drivers__ina220.html#ggab16425153d99ad1d435779032c24fc35aee94bcdd74202e158dfd3c329ab20463", null ],
      [ "INA220_SADC_AVG_64_SAMPLES", "group__drivers__ina220.html#ggab16425153d99ad1d435779032c24fc35add4b84130a1bffe0610c674821a98ccb", null ],
      [ "INA220_SADC_AVG_128_SAMPLES", "group__drivers__ina220.html#ggab16425153d99ad1d435779032c24fc35af5a54533af3decc86d2d0c961349ac89", null ]
    ] ],
    [ "ina220_init", "group__drivers__ina220.html#ga9aa45a881863f15979917aa243e12e2f", null ],
    [ "ina220_read_bus", "group__drivers__ina220.html#ga0f45f0947e9f3b9b2f8047eaf7084b3f", null ],
    [ "ina220_read_current", "group__drivers__ina220.html#gadcc9d77cff3b26e65a1c2fb5ebd7ae08", null ],
    [ "ina220_read_power", "group__drivers__ina220.html#gae37cffe77bfc77aa13ee51bef8b56549", null ],
    [ "ina220_read_shunt", "group__drivers__ina220.html#gae9623f3db4e72ef9361ce831edfd41d1", null ],
    [ "ina220_set_calibration", "group__drivers__ina220.html#gac8ce46a5df4c72f71f7f027ae86cd7ac", null ],
    [ "ina220_set_config", "group__drivers__ina220.html#ga5fd1c445033fd41152b071a092e69a1c", null ]
];