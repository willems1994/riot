var tcs37727_8h =
[
    [ "TCS37727_ATIME_DEFAULT", "group__drivers__tcs37727.html#gae4a361cc65a30fa70ac0ef55d3b1a47b", null ],
    [ "TCS37727_I2C_ADDRESS", "group__drivers__tcs37727.html#gada0d3fdf50833fe6d8e1968f6115ecbf", null ],
    [ "TCS37727_OK", "group__drivers__tcs37727.html#gga425be5f49e9c31d8d13d53190a3e7bc2ae101db5f3ce17167bd9fa3f7dfff005b", null ],
    [ "TCS37727_NOBUS", "group__drivers__tcs37727.html#gga425be5f49e9c31d8d13d53190a3e7bc2aa70bcd24cb8c91daa68a46442414e59a", null ],
    [ "TCS37727_NODEV", "group__drivers__tcs37727.html#gga425be5f49e9c31d8d13d53190a3e7bc2a00aefd14902957a35b209a5fefec8397", null ],
    [ "tcs37727_init", "group__drivers__tcs37727.html#gaff5677adb0695a8a6aa93c73cfdcfc42", null ],
    [ "tcs37727_read", "group__drivers__tcs37727.html#ga674e4fef2b342cba720b2fdf35b5b297", null ],
    [ "tcs37727_set_rgbc_active", "group__drivers__tcs37727.html#ga3e444464184971a6779ba33fba3f4e93", null ],
    [ "tcs37727_set_rgbc_standby", "group__drivers__tcs37727.html#ga1734e0d4171a9233e589178c61af3619", null ]
];