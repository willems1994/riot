var structmsp__usci__spi__t =
[
    [ "BR0", "structmsp__usci__spi__t.html#a3ed83c22c4e2d82ba863d7d5d863bcc9", null ],
    [ "BR1", "structmsp__usci__spi__t.html#a11d31b496379606350d3d36215745b8f", null ],
    [ "CTL0", "structmsp__usci__spi__t.html#aeebb1e406dd1c1a5d5c9941041df6b13", null ],
    [ "CTL1", "structmsp__usci__spi__t.html#ae2bc37324ba16e7994ce9e7a11f7ddec", null ],
    [ "reserved", "structmsp__usci__spi__t.html#ae667fb622118d5020b8de4a9f353cbd5", null ],
    [ "RXBUF", "structmsp__usci__spi__t.html#aac5ce20056e43db786401626be1bf23d", null ],
    [ "STAT", "structmsp__usci__spi__t.html#ae0a1cebcaed397086c6dd4bfdd70a911", null ],
    [ "TXBUF", "structmsp__usci__spi__t.html#a222d2ae88f605650ab4b1ef24ad1b88a", null ]
];