var mac_2types_8h =
[
    [ "gnrc_mac_rx_t", "structgnrc__mac__rx__t.html", "structgnrc__mac__rx__t" ],
    [ "gnrc_mac_tx_neighbor_t", "structgnrc__mac__tx__neighbor__t.html", "structgnrc__mac__tx__neighbor__t" ],
    [ "gnrc_mac_tx_t", "structgnrc__mac__tx__t.html", "structgnrc__mac__tx__t" ],
    [ "GNRC_MAC_PHASE_MAX", "mac_2types_8h.html#ac5a090cd703d88d35ec4ee636743219e", null ],
    [ "GNRC_MAC_PHASE_UNINITIALIZED", "mac_2types_8h.html#a4a4a00050b79e20a9e06c4c800e3fa20", null ],
    [ "GNRC_MAC_RX_INIT", "mac_2types_8h.html#a14ee01b66d562949573ced3772f98568", null ],
    [ "GNRC_MAC_TX_FEEDBACK_INIT", "mac_2types_8h.html#a62cecbebd01945c1b630513b92974ed0", null ],
    [ "GNRC_MAC_TX_INIT", "mac_2types_8h.html#a072cb3514417c9d849eddb248e339c17", null ],
    [ "GNRC_MAC_TX_NEIGHBOR_INIT", "mac_2types_8h.html#a79c63c83d8703b0a8a4cad17694cd032", null ],
    [ "GNRC_MAC_TYPE_GET_DUTYCYCLE", "mac_2types_8h.html#a47523cb42aa2e76a04f3689c4912abcc", null ],
    [ "gnrc_mac_tx_feedback_t", "mac_2types_8h.html#ac191f4e04e6b3a5dce2d9a53b5f2e00f", [
      [ "TX_FEEDBACK_UNDEF", "mac_2types_8h.html#ac191f4e04e6b3a5dce2d9a53b5f2e00fa8d4c02a6af9eb71425f51894e1307326", null ],
      [ "TX_FEEDBACK_SUCCESS", "mac_2types_8h.html#ac191f4e04e6b3a5dce2d9a53b5f2e00fa47dae4e7403b4a8e729e45749f6a37d3", null ],
      [ "TX_FEEDBACK_NOACK", "mac_2types_8h.html#ac191f4e04e6b3a5dce2d9a53b5f2e00fa78a1c3e23b3d8ad590fb1fc634085476", null ],
      [ "TX_FEEDBACK_BUSY", "mac_2types_8h.html#ac191f4e04e6b3a5dce2d9a53b5f2e00fa263f20fa17ed15bc3222d60f856397b8", null ]
    ] ]
];