var stm32l476g_disco_2include_2board_8h =
[
    [ "BTN0_MODE", "group__boards__stm32l476g-disco.html#ga904884feea8e03c30c9a85ca6d7a8e5c", null ],
    [ "BTN0_PIN", "group__boards__stm32l476g-disco.html#gaab5c3eca54046333af52593b9e360270", null ],
    [ "BTN1_MODE", "group__boards__stm32l476g-disco.html#gac792994d9b2d95b9761a05597ddea744", null ],
    [ "BTN1_PIN", "group__boards__stm32l476g-disco.html#gaeb7f5e51d2c7c61ec0a5d5c29591e9b3", null ],
    [ "BTN2_MODE", "group__boards__stm32l476g-disco.html#ga9d5d22198a4a00d13b8edf33ce180fa8", null ],
    [ "BTN2_PIN", "group__boards__stm32l476g-disco.html#ga614337fc407f6d0c9d86efa4e1272ac7", null ],
    [ "BTN3_MODE", "group__boards__stm32l476g-disco.html#ga93cd806af899da82845d3f1817ccc77e", null ],
    [ "BTN3_PIN", "group__boards__stm32l476g-disco.html#ga692acc36358602b709156eb5c312f199", null ],
    [ "BTN4_MODE", "group__boards__stm32l476g-disco.html#gafbebad985c9283bf2a55628d0aed2bd2", null ],
    [ "BTN4_PIN", "group__boards__stm32l476g-disco.html#gad3d0cf6abea314b9e8e97e87f3b6ab46", null ],
    [ "LED0_MASK", "group__boards__stm32l476g-disco.html#gabfabde35a3e8b7ab50146a5e223bfead", null ],
    [ "LED0_OFF", "group__boards__stm32l476g-disco.html#gaef2e39b9fd343d0e437fac0463f3e02d", null ],
    [ "LED0_ON", "group__boards__stm32l476g-disco.html#ga1e48dc02333bfc2e6ad3e5bd466eecd4", null ],
    [ "LED0_PIN", "group__boards__stm32l476g-disco.html#ga3fc522fda449d6edde717d6a857429cf", null ],
    [ "LED0_TOGGLE", "group__boards__stm32l476g-disco.html#gaebc6389533d9fc8dcbe4d2129a4d5a45", null ],
    [ "LED1_MASK", "group__boards__stm32l476g-disco.html#ga669ed6e073140d069b30442bf4c08842", null ],
    [ "LED1_OFF", "group__boards__stm32l476g-disco.html#ga343fd2d3ce61b84f88ddfaea32c67c58", null ],
    [ "LED1_ON", "group__boards__stm32l476g-disco.html#gaadd4c7ae0cd4e9bbb17f7055dd51fe08", null ],
    [ "LED1_PIN", "group__boards__stm32l476g-disco.html#ga318aa17e5d40e2132d2c7f6269ce7f51", null ],
    [ "LED1_TOGGLE", "group__boards__stm32l476g-disco.html#ga267fdbba1d750146b73da35c1731fd17", null ],
    [ "board_init", "group__boards__stm32l476g-disco.html#ga916f2adc2080b4fe88034086d107a8dc", null ]
];