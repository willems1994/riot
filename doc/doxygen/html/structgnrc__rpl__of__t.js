var structgnrc__rpl__of__t =
[
    [ "calc_rank", "structgnrc__rpl__of__t.html#a129dcb914a2c9b7cf0e971972104e71b", null ],
    [ "init", "structgnrc__rpl__of__t.html#aedbb50ac8d866510d63b689655b59f96", null ],
    [ "ocp", "structgnrc__rpl__of__t.html#a42c2f57ff6094da5db37aba767afe8da", null ],
    [ "parent_cmp", "structgnrc__rpl__of__t.html#a76547adffbfc6297dc1760970bae3c46", null ],
    [ "parent_state_callback", "structgnrc__rpl__of__t.html#ae0de9020fb7ae2bff08e8996bf137068", null ],
    [ "process_dio", "structgnrc__rpl__of__t.html#a9362f077bb83ee0281b355727b2a50b4", null ],
    [ "reset", "structgnrc__rpl__of__t.html#a04de86ca94016d4c411500e289c224ca", null ],
    [ "which_dodag", "structgnrc__rpl__of__t.html#ace2b66cbe16c9434145883934d35639d", null ],
    [ "which_parent", "structgnrc__rpl__of__t.html#ab70d7eca0d359688019a053556403612", null ]
];