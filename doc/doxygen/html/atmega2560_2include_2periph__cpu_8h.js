var atmega2560_2include_2periph__cpu_8h =
[
    [ "CPU_ATMEGA_EXT_INTS", "atmega2560_2include_2periph__cpu_8h.html#a33b21f2a315e57318668814f00727415", null ],
    [ "EEPROM_SIZE", "atmega2560_2include_2periph__cpu_8h.html#ae3ef7bba113f663df6996f286b632a3f", null ],
    [ "I2C_PIN_MASK", "atmega2560_2include_2periph__cpu_8h.html#a00d0bff01337d7e458f49061c4e8bb98", null ],
    [ "I2C_PORT_REG", "atmega2560_2include_2periph__cpu_8h.html#aa3fa75e27eb4ad89cba2748dc486ca86", null ],
    [ "PORT_A", "atmega2560_2include_2periph__cpu_8h.html#a61dadd085c1777f559549e05962b2c9eaeb6782d9dfedf3c6a78ffdb1624fa454", null ],
    [ "PORT_B", "atmega2560_2include_2periph__cpu_8h.html#a61dadd085c1777f559549e05962b2c9ea16ada472d473fbd0207b99e9e4d68f4a", null ],
    [ "PORT_C", "atmega2560_2include_2periph__cpu_8h.html#a61dadd085c1777f559549e05962b2c9ea627cc690c37f97527dd2f07aa22092d9", null ],
    [ "PORT_D", "atmega2560_2include_2periph__cpu_8h.html#a61dadd085c1777f559549e05962b2c9eaf7242fe75227a46a190645663f91ce69", null ],
    [ "PORT_E", "atmega2560_2include_2periph__cpu_8h.html#a61dadd085c1777f559549e05962b2c9eabad63f022d1fa37a66f87dc31a78f6a9", null ],
    [ "PORT_F", "atmega2560_2include_2periph__cpu_8h.html#a61dadd085c1777f559549e05962b2c9eaa3760b302740c7d09c93ec7a634f837c", null ],
    [ "PORT_G", "atmega2560_2include_2periph__cpu_8h.html#a61dadd085c1777f559549e05962b2c9ea48afb424254d52e7d97a7c1428f5aafa", null ],
    [ "PORT_H", "atmega2560_2include_2periph__cpu_8h.html#a61dadd085c1777f559549e05962b2c9ea31d1ae08c10668d936d1c2c6426c1c47", null ],
    [ "PORT_J", "atmega2560_2include_2periph__cpu_8h.html#a61dadd085c1777f559549e05962b2c9ea3eea70262d530701087c2a3a09c475fe", null ],
    [ "PORT_K", "atmega2560_2include_2periph__cpu_8h.html#a61dadd085c1777f559549e05962b2c9ea23bae6dc45cdca7c90be62437df0f454", null ],
    [ "PORT_L", "atmega2560_2include_2periph__cpu_8h.html#a61dadd085c1777f559549e05962b2c9ea9e83da163100e922da1c8065b5cf43c2", null ]
];