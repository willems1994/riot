var boards_2blackpill_2include_2board_8h =
[
    [ "LED0_MASK", "group__boards__blackpill.html#gabfabde35a3e8b7ab50146a5e223bfead", null ],
    [ "LED0_OFF", "group__boards__blackpill.html#gaef2e39b9fd343d0e437fac0463f3e02d", null ],
    [ "LED0_ON", "group__boards__blackpill.html#ga1e48dc02333bfc2e6ad3e5bd466eecd4", null ],
    [ "LED0_PIN", "group__boards__blackpill.html#ga3fc522fda449d6edde717d6a857429cf", null ],
    [ "LED0_PORT", "group__boards__blackpill.html#ga76914453bb5cda4ebca204e091e8f55c", null ],
    [ "LED0_TOGGLE", "group__boards__blackpill.html#gaebc6389533d9fc8dcbe4d2129a4d5a45", null ],
    [ "STDIO_UART_DEV", "group__boards__blackpill.html#ga81935d479349dc2ce0a416bcb0e6beda", null ],
    [ "XTIMER_BACKOFF", "group__boards__blackpill.html#ga370b9e9a079c4cb4f54fd947b67b9f41", null ],
    [ "XTIMER_WIDTH", "group__boards__blackpill.html#gafea1be2406d45b8fbb1dca1a318ac2dc", null ],
    [ "board_init", "group__boards__blackpill.html#ga916f2adc2080b4fe88034086d107a8dc", null ]
];