var structlsm6dsl__params__t =
[
    [ "acc_decimation", "structlsm6dsl__params__t.html#ac02fbc3804ffc5df60c21adf4656d94f", null ],
    [ "acc_fs", "structlsm6dsl__params__t.html#a850a07a1a89e5fdd803fdccfd609dac1", null ],
    [ "acc_odr", "structlsm6dsl__params__t.html#aab0d3e44f67056ca59e00431bad9a0c2", null ],
    [ "addr", "structlsm6dsl__params__t.html#ac3a48eb13f1425e421d7973511bc0d9b", null ],
    [ "gyro_decimation", "structlsm6dsl__params__t.html#a71cd12e8a3c79faac2d898c1f1d5e71f", null ],
    [ "gyro_fs", "structlsm6dsl__params__t.html#a6af69c602cce85fefb13f315ea364973", null ],
    [ "gyro_odr", "structlsm6dsl__params__t.html#ac5a407c2eb20fb0664bffa55bc8ce2d6", null ],
    [ "i2c", "structlsm6dsl__params__t.html#aae2f2c729661f508d425612b62131cbd", null ]
];