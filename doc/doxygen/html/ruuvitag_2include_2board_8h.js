var ruuvitag_2include_2board_8h =
[
    [ "BMX280_PARAM_CS", "ruuvitag_2include_2board_8h.html#ad0185c3b7ae6f736dffa8e86b972625f", null ],
    [ "BTN0_MODE", "ruuvitag_2include_2board_8h.html#a904884feea8e03c30c9a85ca6d7a8e5c", null ],
    [ "BTN0_PIN", "ruuvitag_2include_2board_8h.html#aab5c3eca54046333af52593b9e360270", null ],
    [ "CS_PIN_MASK", "ruuvitag_2include_2board_8h.html#aaaed7fd5d188f662cf651f6efc9b472f", null ],
    [ "LED0_MASK", "ruuvitag_2include_2board_8h.html#abfabde35a3e8b7ab50146a5e223bfead", null ],
    [ "LED0_OFF", "ruuvitag_2include_2board_8h.html#aef2e39b9fd343d0e437fac0463f3e02d", null ],
    [ "LED0_ON", "ruuvitag_2include_2board_8h.html#a1e48dc02333bfc2e6ad3e5bd466eecd4", null ],
    [ "LED0_PIN", "ruuvitag_2include_2board_8h.html#a3fc522fda449d6edde717d6a857429cf", null ],
    [ "LED0_TOGGLE", "ruuvitag_2include_2board_8h.html#aebc6389533d9fc8dcbe4d2129a4d5a45", null ],
    [ "LED1_MASK", "ruuvitag_2include_2board_8h.html#a669ed6e073140d069b30442bf4c08842", null ],
    [ "LED1_OFF", "ruuvitag_2include_2board_8h.html#a343fd2d3ce61b84f88ddfaea32c67c58", null ],
    [ "LED1_ON", "ruuvitag_2include_2board_8h.html#aadd4c7ae0cd4e9bbb17f7055dd51fe08", null ],
    [ "LED1_PIN", "ruuvitag_2include_2board_8h.html#a318aa17e5d40e2132d2c7f6269ce7f51", null ],
    [ "LED1_TOGGLE", "ruuvitag_2include_2board_8h.html#a267fdbba1d750146b73da35c1731fd17", null ],
    [ "LED_MASK", "ruuvitag_2include_2board_8h.html#ad0d0bdd5242aaa7aa76f00d47799d0ae", null ],
    [ "LED_PORT", "ruuvitag_2include_2board_8h.html#a663daa01e565aee93c6f20c5845b90b4", null ],
    [ "LIS2DH12_PARAM_CS", "ruuvitag_2include_2board_8h.html#a08f249dcc1ccdb466445e23ac72be1c7", null ],
    [ "LIS2DH12_PARAM_INT1", "ruuvitag_2include_2board_8h.html#a0f3945d2265fbbb673bc170ddaf5ce3c", null ],
    [ "LIS2DH12_PARAM_INT2", "ruuvitag_2include_2board_8h.html#ae622d6e16fdd518b06b19bf42308c534", null ],
    [ "board_init", "ruuvitag_2include_2board_8h.html#a916f2adc2080b4fe88034086d107a8dc", null ]
];