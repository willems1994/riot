var mbox_8h =
[
    [ "MBOX_INIT", "group__core__mbox.html#ga4da35f775c764c17eddc3fe700d59705", null ],
    [ "NON_BLOCKING", "group__core__mbox.html#ggadf764cbdea00d65edcd07bb9953ad2b7a7f2834532c17968dfdcc1505e4bf5376", null ],
    [ "BLOCKING", "group__core__mbox.html#ggadf764cbdea00d65edcd07bb9953ad2b7a854a1cd6e3a98db9e290dddea29725e7", null ],
    [ "_mbox_get", "group__core__mbox.html#gacb4620a29324d15bf6bd7e58198a32b6", null ],
    [ "_mbox_put", "group__core__mbox.html#gaf21f5e201eb96b2507e2d0d590d04bc8", null ],
    [ "mbox_get", "group__core__mbox.html#ga27097c9c9ef5fd4b5bff912580525126", null ],
    [ "mbox_init", "group__core__mbox.html#ga33c89a9d8e639a4d220be184faf5ac85", null ],
    [ "mbox_put", "group__core__mbox.html#gabfb23792f625efb692729cf0ad555349", null ],
    [ "mbox_try_get", "group__core__mbox.html#gafd3d1b114d4db9befc6564750f711d6e", null ],
    [ "mbox_try_put", "group__core__mbox.html#gabed66ce7a0c6cbe474f5accd7658e4a7", null ]
];