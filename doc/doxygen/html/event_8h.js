var event_8h =
[
    [ "EVENT_QUEUE_INIT", "group__sys__event.html#gafbc0919266554541d5723bed79b1e3ed", null ],
    [ "THREAD_FLAG_EVENT", "group__sys__event.html#gaafa9b96ad0e2d68d166b6ba394660a4b", null ],
    [ "event_handler_t", "group__sys__event.html#ga60c9f34f565c834ed82a585d154d0010", null ],
    [ "event_t", "group__sys__event.html#gad0c066ffb009d3286186a124d37a0c2d", null ],
    [ "event_cancel", "group__sys__event.html#ga696280baa5719e3c5ef75fec41e81b3a", null ],
    [ "event_get", "group__sys__event.html#gae5b6336946a171046f7626f476fba292", null ],
    [ "event_loop", "group__sys__event.html#ga7142d0aa584d1174bf15e5dbf36372e9", null ],
    [ "event_post", "group__sys__event.html#ga2053ce1facf709fffd024bee9e2383ba", null ],
    [ "event_queue_init", "group__sys__event.html#ga51eb9a1c10b91fffe4cade8e521cec60", null ],
    [ "event_wait", "group__sys__event.html#ga434b72eb98d39688bef9d0d485a5989c", null ]
];