var slwstk6220a_2include_2board_8h =
[
    [ "BC_PIN", "slwstk6220a_2include_2board_8h.html#a68576b8b2674e98cc147af8367fb58fe", null ],
    [ "HW_TIMER", "slwstk6220a_2include_2board_8h.html#a04121d28390e5fd131f40ae4a1c27412", null ],
    [ "LED0_OFF", "slwstk6220a_2include_2board_8h.html#aef2e39b9fd343d0e437fac0463f3e02d", null ],
    [ "LED0_ON", "slwstk6220a_2include_2board_8h.html#a1e48dc02333bfc2e6ad3e5bd466eecd4", null ],
    [ "LED0_PIN", "slwstk6220a_2include_2board_8h.html#a3fc522fda449d6edde717d6a857429cf", null ],
    [ "LED0_TOGGLE", "slwstk6220a_2include_2board_8h.html#aebc6389533d9fc8dcbe4d2129a4d5a45", null ],
    [ "LED1_OFF", "slwstk6220a_2include_2board_8h.html#a343fd2d3ce61b84f88ddfaea32c67c58", null ],
    [ "LED1_ON", "slwstk6220a_2include_2board_8h.html#aadd4c7ae0cd4e9bbb17f7055dd51fe08", null ],
    [ "LED1_PIN", "slwstk6220a_2include_2board_8h.html#a318aa17e5d40e2132d2c7f6269ce7f51", null ],
    [ "LED1_TOGGLE", "slwstk6220a_2include_2board_8h.html#a267fdbba1d750146b73da35c1731fd17", null ],
    [ "PB0_PIN", "slwstk6220a_2include_2board_8h.html#aea39132b18ce668ed815d13c3153323b", null ],
    [ "PB1_PIN", "slwstk6220a_2include_2board_8h.html#aa33c970f051ff2f38a217f39f00599c1", null ],
    [ "SI7021_ADDR", "slwstk6220a_2include_2board_8h.html#ae2f3ab58a249649a0460ffe3826f8d60", null ],
    [ "SI7021_EN_PIN", "slwstk6220a_2include_2board_8h.html#ae0104fa9895a5df77c7cdbc5579cc012", null ],
    [ "SI7021_I2C", "slwstk6220a_2include_2board_8h.html#a00809eb1e55d1fa68d0e97b9ab2aad07", null ],
    [ "board_init", "slwstk6220a_2include_2board_8h.html#a916f2adc2080b4fe88034086d107a8dc", null ]
];