var structgcoap__request__memo__t =
[
    [ "data", "structgcoap__request__memo__t.html#a95bda8f546b84ace95cab3ece9998fa4", null ],
    [ "hdr_buf", "structgcoap__request__memo__t.html#ac597bcb404192f262b9ca3f78a2e3ef8", null ],
    [ "msg", "structgcoap__request__memo__t.html#a04c7c64f32175b752d291a7eac83880e", null ],
    [ "remote_ep", "structgcoap__request__memo__t.html#afe34c8d38f2fedc0a380528beba1c41f", null ],
    [ "resp_handler", "structgcoap__request__memo__t.html#ae365dbe3dbe51693e934e5a2c7b80438", null ],
    [ "response_timer", "structgcoap__request__memo__t.html#af42ee0f0c3443015e52d530d9b82ee98", null ],
    [ "send_limit", "structgcoap__request__memo__t.html#a0ca19c56b65029aa64f534f7f77dfd03", null ],
    [ "state", "structgcoap__request__memo__t.html#af9cee750a2c1a2f6669c0a4afad6fe60", null ],
    [ "timeout_msg", "structgcoap__request__memo__t.html#a68991f18a271c41b9621ce30df535bac", null ]
];