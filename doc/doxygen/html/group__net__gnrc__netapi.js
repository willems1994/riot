var group__net__gnrc__netapi =
[
    [ "Callback extension", "group__net__gnrc__netapi__callbacks.html", null ],
    [ "Mailbox IPC extension", "group__net__gnrc__netapi__mbox.html", null ],
    [ "NETAPI test framework", "group__net__gnrc__nettest.html", "group__net__gnrc__nettest" ],
    [ "netapi.h", "netapi_8h.html", null ],
    [ "gnrc_netapi_opt_t", "structgnrc__netapi__opt__t.html", [
      [ "context", "structgnrc__netapi__opt__t.html#abb7d76401195cc1ef437f868444e8afe", null ],
      [ "data", "structgnrc__netapi__opt__t.html#a902a7aa2875398f2f64c6655a9a38346", null ],
      [ "data_len", "structgnrc__netapi__opt__t.html#a7db06ffeef445ddc26b2398adabaa16b", null ],
      [ "opt", "structgnrc__netapi__opt__t.html#afd202f69e03cdc5545fe5b8c7d4a9e5c", null ]
    ] ],
    [ "GNRC_NETAPI_MSG_TYPE_ACK", "group__net__gnrc__netapi.html#ga6edd8d8871394091cff19457fc3da1b5", null ],
    [ "GNRC_NETAPI_MSG_TYPE_GET", "group__net__gnrc__netapi.html#gacc19be168656448df4d274293dc23916", null ],
    [ "GNRC_NETAPI_MSG_TYPE_RCV", "group__net__gnrc__netapi.html#ga57b7e8cf32c12beecc9b84ca2cc073b5", null ],
    [ "GNRC_NETAPI_MSG_TYPE_SET", "group__net__gnrc__netapi.html#ga81e297b6dd96cf0a565a9dc77cc54e11", null ],
    [ "GNRC_NETAPI_MSG_TYPE_SND", "group__net__gnrc__netapi.html#gacf009a7a7aa95ec88848b1030ef08b09", null ],
    [ "gnrc_netapi_dispatch", "group__net__gnrc__netapi.html#ga913d93494730b4c50f58ee48cc4ccdbb", null ],
    [ "gnrc_netapi_dispatch_receive", "group__net__gnrc__netapi.html#ga29426dfcc0b46e451efab9939b41d95e", null ],
    [ "gnrc_netapi_dispatch_send", "group__net__gnrc__netapi.html#ga2b5b0f662061aa332cc1cbb9c145ee07", null ],
    [ "gnrc_netapi_get", "group__net__gnrc__netapi.html#ga0ca19b514ad9303c0c3bc206fbe15b0a", null ],
    [ "gnrc_netapi_receive", "group__net__gnrc__netapi.html#gaf1c1a6dc7ef51fe8aa95b7bac37ee6aa", null ],
    [ "gnrc_netapi_send", "group__net__gnrc__netapi.html#ga379dec2cbff3ba3bdc67e5c4057ea580", null ],
    [ "gnrc_netapi_set", "group__net__gnrc__netapi.html#gabc41a5d60228a68daa7ef14b0bbb6ff4", null ]
];