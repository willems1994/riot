var group__boards__stm32l476g_disco =
[
    [ "stm32l476g-disco/include/board.h", "stm32l476g-disco_2include_2board_8h.html", null ],
    [ "boards/stm32l476g-disco/include/periph_conf.h", "boards_2stm32l476g-disco_2include_2periph__conf_8h.html", null ],
    [ "BTN0_MODE", "group__boards__stm32l476g-disco.html#ga904884feea8e03c30c9a85ca6d7a8e5c", null ],
    [ "BTN0_PIN", "group__boards__stm32l476g-disco.html#gaab5c3eca54046333af52593b9e360270", null ],
    [ "BTN1_MODE", "group__boards__stm32l476g-disco.html#gac792994d9b2d95b9761a05597ddea744", null ],
    [ "BTN1_PIN", "group__boards__stm32l476g-disco.html#gaeb7f5e51d2c7c61ec0a5d5c29591e9b3", null ],
    [ "BTN2_MODE", "group__boards__stm32l476g-disco.html#ga9d5d22198a4a00d13b8edf33ce180fa8", null ],
    [ "BTN2_PIN", "group__boards__stm32l476g-disco.html#ga614337fc407f6d0c9d86efa4e1272ac7", null ],
    [ "BTN3_MODE", "group__boards__stm32l476g-disco.html#ga93cd806af899da82845d3f1817ccc77e", null ],
    [ "BTN3_PIN", "group__boards__stm32l476g-disco.html#ga692acc36358602b709156eb5c312f199", null ],
    [ "BTN4_MODE", "group__boards__stm32l476g-disco.html#gafbebad985c9283bf2a55628d0aed2bd2", null ],
    [ "BTN4_PIN", "group__boards__stm32l476g-disco.html#gad3d0cf6abea314b9e8e97e87f3b6ab46", null ],
    [ "board_init", "group__boards__stm32l476g-disco.html#ga916f2adc2080b4fe88034086d107a8dc", null ]
];