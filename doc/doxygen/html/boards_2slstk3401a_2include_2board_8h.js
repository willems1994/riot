var boards_2slstk3401a_2include_2board_8h =
[
    [ "BC_PIN", "group__boards__slstk3401a.html#ga68576b8b2674e98cc147af8367fb58fe", null ],
    [ "DISP_COM_PIN", "group__boards__slstk3401a.html#ga23a47e4c2974429f9d67a4bd00631db4", null ],
    [ "DISP_CS_PIN", "group__boards__slstk3401a.html#gaca3e980b4e32ed692d75cb4cf0eeee25", null ],
    [ "DISP_EN_PIN", "group__boards__slstk3401a.html#ga723e11a34c62d6398bc14072d0db45f1", null ],
    [ "DISP_SPI", "group__boards__slstk3401a.html#gac4bafa01fb75e4ebc81bf352d05af75c", null ],
    [ "LED0_OFF", "group__boards__slstk3401a.html#gaef2e39b9fd343d0e437fac0463f3e02d", null ],
    [ "LED0_ON", "group__boards__slstk3401a.html#ga1e48dc02333bfc2e6ad3e5bd466eecd4", null ],
    [ "LED0_PIN", "group__boards__slstk3401a.html#ga3fc522fda449d6edde717d6a857429cf", null ],
    [ "LED0_TOGGLE", "group__boards__slstk3401a.html#gaebc6389533d9fc8dcbe4d2129a4d5a45", null ],
    [ "LED1_OFF", "group__boards__slstk3401a.html#ga343fd2d3ce61b84f88ddfaea32c67c58", null ],
    [ "LED1_ON", "group__boards__slstk3401a.html#gaadd4c7ae0cd4e9bbb17f7055dd51fe08", null ],
    [ "LED1_PIN", "group__boards__slstk3401a.html#ga318aa17e5d40e2132d2c7f6269ce7f51", null ],
    [ "LED1_TOGGLE", "group__boards__slstk3401a.html#ga267fdbba1d750146b73da35c1731fd17", null ],
    [ "PB0_PIN", "group__boards__slstk3401a.html#gaea39132b18ce668ed815d13c3153323b", null ],
    [ "PB1_PIN", "group__boards__slstk3401a.html#gaa33c970f051ff2f38a217f39f00599c1", null ],
    [ "SI7021_EN_PIN", "group__boards__slstk3401a.html#gae0104fa9895a5df77c7cdbc5579cc012", null ],
    [ "SI7021_I2C", "group__boards__slstk3401a.html#ga00809eb1e55d1fa68d0e97b9ab2aad07", null ],
    [ "SI70XX_PARAM_I2C_DEV", "group__boards__slstk3401a.html#ga1d95f0f459e7753efd6b1ec0d6b25437", null ],
    [ "XTIMER_HZ", "group__boards__slstk3401a.html#gaf68fde6b7d5b362834e6a8d382c6c0d7", null ],
    [ "XTIMER_WIDTH", "group__boards__slstk3401a.html#gafea1be2406d45b8fbb1dca1a318ac2dc", null ],
    [ "board_init", "group__boards__slstk3401a.html#ga916f2adc2080b4fe88034086d107a8dc", null ]
];