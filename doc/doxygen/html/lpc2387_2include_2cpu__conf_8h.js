var lpc2387_2include_2cpu__conf_8h =
[
    [ "__FILENAME_MAX__", "lpc2387_2include_2cpu__conf_8h.html#a7920ead91d83a9b04b5aba3d1dd0f771", null ],
    [ "__FOPEN_MAX__", "lpc2387_2include_2cpu__conf_8h.html#a1e4129d4deb5c07a48920c25892cea1d", null ],
    [ "CC_CONF_INLINE", "lpc2387_2include_2cpu__conf_8h.html#a68bd1bddb3b473dc93afe7f6a68359c4", null ],
    [ "CC_CONF_NONNULL", "lpc2387_2include_2cpu__conf_8h.html#a22e63a3f30a7ffb209beb546507c9dfa", null ],
    [ "CC_CONF_USED", "lpc2387_2include_2cpu__conf_8h.html#afded25e62f64971781589b28445fbabe", null ],
    [ "CC_CONF_WARN_UNUSED_RESULT", "lpc2387_2include_2cpu__conf_8h.html#a03d2c9da5676ba7825df502349ebcdca", null ],
    [ "THREAD_EXTRA_STACKSIZE_PRINTF", "lpc2387_2include_2cpu__conf_8h.html#af30305c0f413c7da1e7445de2b5ea5a5", null ],
    [ "THREAD_EXTRA_STACKSIZE_PRINTF_FLOAT", "lpc2387_2include_2cpu__conf_8h.html#aef8779ee69990dfa8a5ca5d96420c3ff", null ],
    [ "THREAD_STACKSIZE_DEFAULT", "lpc2387_2include_2cpu__conf_8h.html#a713ebddc00581f4d415095cdbfd8791f", null ],
    [ "THREAD_STACKSIZE_IDLE", "lpc2387_2include_2cpu__conf_8h.html#ac4f62f762a057d594ae0ee4522dd14c2", null ]
];