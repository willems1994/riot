var structgnrc__netif__t =
[
    [ "cur_hl", "structgnrc__netif__t.html#a3352b9009d6b3af888c8351572e8f83c", null ],
    [ "dev", "structgnrc__netif__t.html#ab15db3d5a0a026ffa73487fdd84cbd15", null ],
    [ "device_type", "structgnrc__netif__t.html#a269e7bb2472a008176c8e173cd152600", null ],
    [ "flags", "structgnrc__netif__t.html#a1e33d5441ba953f822ec5f4218b6347a", null ],
    [ "ipv6", "structgnrc__netif__t.html#aaa394a6f526275ae4d3cabfe0ab3e32a", null ],
    [ "mac", "structgnrc__netif__t.html#a646f694b7b5818e864ddafcbae185cad", null ],
    [ "mutex", "structgnrc__netif__t.html#a1c83f4e05deadbbff86d34da096d7392", null ],
    [ "ops", "structgnrc__netif__t.html#a1fb8b3d4e737b508c1cd9a675e6d035f", null ],
    [ "pid", "structgnrc__netif__t.html#a48524d59f708cc987051ff1dafe51fe7", null ],
    [ "sixlo", "structgnrc__netif__t.html#a324b26288c265954ee10a8c1517de6eb", null ]
];