var ads101x_8h =
[
    [ "ADS101X_I2C_ADDRESS", "group__drivers__ads101x.html#ga82a07ebad194496b574d495e7e26f911", null ],
    [ "ads101x_alert_cb_t", "group__drivers__ads101x.html#ga180dcefdb44b260b81f422787f9ace52", null ],
    [ "ads101x_alert_params_t", "group__drivers__ads101x.html#ga7b6c9549b691e3b11e9719fa23117f79", null ],
    [ "ads101x_alert_t", "group__drivers__ads101x.html#ga3554bc5e7298f8c1b841514bb0c67655", null ],
    [ "ads101x_params_t", "group__drivers__ads101x.html#ga61c8a7948074d042793b90fb204a3fdf", null ],
    [ "ads101x_t", "group__drivers__ads101x.html#gadeed1f02fed76c5dbf1fd17917bf78d4", null ],
    [ "ADS101X_OK", "group__drivers__ads101x.html#gga7ee8d0f117a79ca7eb1e0076a9182bcbaf1fb7334c77f6a8509f9ecd455c14afe", null ],
    [ "ADS101X_NOI2C", "group__drivers__ads101x.html#gga7ee8d0f117a79ca7eb1e0076a9182bcbae1c762aa7dbe06bd63c9fb2ef7a1081b", null ],
    [ "ADS101X_NODEV", "group__drivers__ads101x.html#gga7ee8d0f117a79ca7eb1e0076a9182bcbafccf6fe3f9126d3aada5e63be1c8cdf6", null ],
    [ "ADS101X_NODATA", "group__drivers__ads101x.html#gga7ee8d0f117a79ca7eb1e0076a9182bcba672c515a86769de4c0b0d93dbc9c03e7", null ],
    [ "ads101x_alert_init", "group__drivers__ads101x.html#ga9ab02377bf2773eb1973ba6de8f14234", null ],
    [ "ads101x_enable_alert", "group__drivers__ads101x.html#ga75092e0fb959c3fa906f7ce9e933f179", null ],
    [ "ads101x_init", "group__drivers__ads101x.html#ga62914846bd803de8fcaf01f46d38abe9", null ],
    [ "ads101x_read_raw", "group__drivers__ads101x.html#gad8f0aee88b7fa4aa9d6c58c59d9b51a4", null ],
    [ "ads101x_set_alert_parameters", "group__drivers__ads101x.html#ga483dda279848fde59e3d142cf26ee0ff", null ],
    [ "ads101x_set_mux_gain", "group__drivers__ads101x.html#ga15a9598fcd6f96dd7627f04c5490151b", null ]
];