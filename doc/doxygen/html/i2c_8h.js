var i2c_8h =
[
    [ "I2C_10BIT_MAGIC", "group__drivers__periph__i2c.html#ga45fc6df6bf09a50095f509cadc0083a8", null ],
    [ "I2C_DEV", "group__drivers__periph__i2c.html#ga9f14916eda80b19ff41d08e25eee56fb", null ],
    [ "I2C_READ", "group__drivers__periph__i2c.html#gab5c0fbe837494c5f9130a5914854250d", null ],
    [ "I2C_UNDEF", "group__drivers__periph__i2c.html#gad224a6eaa456b1a9682b1ca17c4aacc3", null ],
    [ "i2c_t", "group__drivers__periph__i2c.html#ga8c002f11842c26b1ee7048ef51bdc35b", null ],
    [ "i2c_flags_t", "group__drivers__periph__i2c.html#ga9ed58f160035134076b56c8907cf0c6b", [
      [ "I2C_ADDR10", "group__drivers__periph__i2c.html#gga9ed58f160035134076b56c8907cf0c6ba1d95c3f463d6bbe98c1a8fbcd5d1e1f2", null ],
      [ "I2C_REG16", "group__drivers__periph__i2c.html#gga9ed58f160035134076b56c8907cf0c6ba1dc3546ab9d927d48d26781bdc4ad84e", null ],
      [ "I2C_NOSTOP", "group__drivers__periph__i2c.html#gga9ed58f160035134076b56c8907cf0c6bad24d4523893d22e101e27bfc546234c3", null ],
      [ "I2C_NOSTART", "group__drivers__periph__i2c.html#gga9ed58f160035134076b56c8907cf0c6bad5799291fd9b37b7cc3a381636be9c53", null ]
    ] ],
    [ "i2c_speed_t", "group__drivers__periph__i2c.html#ga6e6a870f98abb8cffa95373b69fb8243", [
      [ "I2C_SPEED_LOW", "group__drivers__periph__i2c.html#gga6e6a870f98abb8cffa95373b69fb8243a6b350d376580872bb53bdfc4ff41d9b0", null ],
      [ "I2C_SPEED_NORMAL", "group__drivers__periph__i2c.html#gga6e6a870f98abb8cffa95373b69fb8243a0826bf5711e82ba26b4ada6104260583", null ],
      [ "I2C_SPEED_FAST", "group__drivers__periph__i2c.html#gga6e6a870f98abb8cffa95373b69fb8243a8dc6cae939d20d56a78d123365caa4ca", null ],
      [ "I2C_SPEED_FAST_PLUS", "group__drivers__periph__i2c.html#gga6e6a870f98abb8cffa95373b69fb8243a42e708fe61f237d88f6cf53f32e17e8e", null ],
      [ "I2C_SPEED_HIGH", "group__drivers__periph__i2c.html#gga6e6a870f98abb8cffa95373b69fb8243a745e792241485f11092fabd600fd6b48", null ],
      [ "I2C_SPEED_LOW", "group__drivers__periph__i2c.html#gga6e6a870f98abb8cffa95373b69fb8243a6b350d376580872bb53bdfc4ff41d9b0", null ],
      [ "I2C_SPEED_NORMAL", "group__drivers__periph__i2c.html#gga6e6a870f98abb8cffa95373b69fb8243a0826bf5711e82ba26b4ada6104260583", null ],
      [ "I2C_SPEED_FAST", "group__drivers__periph__i2c.html#gga6e6a870f98abb8cffa95373b69fb8243a8dc6cae939d20d56a78d123365caa4ca", null ],
      [ "I2C_SPEED_FAST_PLUS", "group__drivers__periph__i2c.html#gga6e6a870f98abb8cffa95373b69fb8243a42e708fe61f237d88f6cf53f32e17e8e", null ],
      [ "I2C_SPEED_HIGH", "group__drivers__periph__i2c.html#gga6e6a870f98abb8cffa95373b69fb8243a745e792241485f11092fabd600fd6b48", null ],
      [ "I2C_SPEED_LOW", "group__drivers__periph__i2c.html#gga6e6a870f98abb8cffa95373b69fb8243a6b350d376580872bb53bdfc4ff41d9b0", null ],
      [ "I2C_SPEED_NORMAL", "group__drivers__periph__i2c.html#gga6e6a870f98abb8cffa95373b69fb8243a0826bf5711e82ba26b4ada6104260583", null ],
      [ "I2C_SPEED_FAST", "group__drivers__periph__i2c.html#gga6e6a870f98abb8cffa95373b69fb8243a8dc6cae939d20d56a78d123365caa4ca", null ],
      [ "I2C_SPEED_FAST_PLUS", "group__drivers__periph__i2c.html#gga6e6a870f98abb8cffa95373b69fb8243a42e708fe61f237d88f6cf53f32e17e8e", null ],
      [ "I2C_SPEED_HIGH", "group__drivers__periph__i2c.html#gga6e6a870f98abb8cffa95373b69fb8243a745e792241485f11092fabd600fd6b48", null ],
      [ "I2C_SPEED_LOW", "group__drivers__periph__i2c.html#gga6e6a870f98abb8cffa95373b69fb8243a6b350d376580872bb53bdfc4ff41d9b0", null ],
      [ "I2C_SPEED_NORMAL", "group__drivers__periph__i2c.html#gga6e6a870f98abb8cffa95373b69fb8243a0826bf5711e82ba26b4ada6104260583", null ],
      [ "I2C_SPEED_FAST", "group__drivers__periph__i2c.html#gga6e6a870f98abb8cffa95373b69fb8243a8dc6cae939d20d56a78d123365caa4ca", null ],
      [ "I2C_SPEED_FAST_PLUS", "group__drivers__periph__i2c.html#gga6e6a870f98abb8cffa95373b69fb8243a42e708fe61f237d88f6cf53f32e17e8e", null ],
      [ "I2C_SPEED_HIGH", "group__drivers__periph__i2c.html#gga6e6a870f98abb8cffa95373b69fb8243a745e792241485f11092fabd600fd6b48", null ],
      [ "I2C_SPEED_NORMAL", "group__drivers__periph__i2c.html#gga6e6a870f98abb8cffa95373b69fb8243a0826bf5711e82ba26b4ada6104260583", null ],
      [ "I2C_SPEED_FAST", "group__drivers__periph__i2c.html#gga6e6a870f98abb8cffa95373b69fb8243a8dc6cae939d20d56a78d123365caa4ca", null ],
      [ "I2C_SPEED_LOW", "group__drivers__periph__i2c.html#gga6e6a870f98abb8cffa95373b69fb8243a6b350d376580872bb53bdfc4ff41d9b0", null ],
      [ "I2C_SPEED_NORMAL", "group__drivers__periph__i2c.html#gga6e6a870f98abb8cffa95373b69fb8243a0826bf5711e82ba26b4ada6104260583", null ],
      [ "I2C_SPEED_FAST", "group__drivers__periph__i2c.html#gga6e6a870f98abb8cffa95373b69fb8243a8dc6cae939d20d56a78d123365caa4ca", null ],
      [ "I2C_SPEED_FAST_PLUS", "group__drivers__periph__i2c.html#gga6e6a870f98abb8cffa95373b69fb8243a42e708fe61f237d88f6cf53f32e17e8e", null ],
      [ "I2C_SPEED_HIGH", "group__drivers__periph__i2c.html#gga6e6a870f98abb8cffa95373b69fb8243a745e792241485f11092fabd600fd6b48", null ]
    ] ],
    [ "i2c_acquire", "group__drivers__periph__i2c.html#ga01056db645f8e561ac8bdab1f531a6db", null ],
    [ "i2c_init", "group__drivers__periph__i2c.html#ga2ce7cc916258e7c3fa0b60cba2e4895f", null ],
    [ "i2c_read_byte", "group__drivers__periph__i2c.html#ga81fed3b6aec0ba0956ba6a1efbc74774", null ],
    [ "i2c_read_bytes", "group__drivers__periph__i2c.html#ga196b4508511c41822eddf6b43b008e90", null ],
    [ "i2c_read_reg", "group__drivers__periph__i2c.html#gac4ab6e73e05b22c2da0c2f178da06d7b", null ],
    [ "i2c_read_regs", "group__drivers__periph__i2c.html#ga5e65efc34a8bd77223795faadc29d304", null ],
    [ "i2c_release", "group__drivers__periph__i2c.html#ga78e5d33695e16ee13d6ed38e6e96e57d", null ],
    [ "i2c_write_byte", "group__drivers__periph__i2c.html#ga062b562b2d2cd123199a2890968ba272", null ],
    [ "i2c_write_bytes", "group__drivers__periph__i2c.html#ga2d4c26cb8d79d7a46764fb832e93ff0b", null ],
    [ "i2c_write_reg", "group__drivers__periph__i2c.html#ga02b8328d5e823d972e1e1234965daab1", null ],
    [ "i2c_write_regs", "group__drivers__periph__i2c.html#gac7e12c6df3bc117d5e46d1e98a5a7f08", null ]
];