var stm32l4_2include_2periph__cpu_8h =
[
    [ "adc_conf_t", "structadc__conf__t.html", "structadc__conf__t" ],
    [ "CPUID_ADDR", "stm32l4_2include_2periph__cpu_8h.html#a0d3fc6a1a30b894ce55d50e7eac993b2", null ],
    [ "PORT_A", "stm32l4_2include_2periph__cpu_8h.html#a4f126a0a9b1d8c6a8f46a051ef8830bbaeb6782d9dfedf3c6a78ffdb1624fa454", null ],
    [ "PORT_B", "stm32l4_2include_2periph__cpu_8h.html#a4f126a0a9b1d8c6a8f46a051ef8830bba16ada472d473fbd0207b99e9e4d68f4a", null ],
    [ "PORT_C", "stm32l4_2include_2periph__cpu_8h.html#a4f126a0a9b1d8c6a8f46a051ef8830bba627cc690c37f97527dd2f07aa22092d9", null ],
    [ "PORT_D", "stm32l4_2include_2periph__cpu_8h.html#a4f126a0a9b1d8c6a8f46a051ef8830bbaf7242fe75227a46a190645663f91ce69", null ],
    [ "PORT_E", "stm32l4_2include_2periph__cpu_8h.html#a4f126a0a9b1d8c6a8f46a051ef8830bbabad63f022d1fa37a66f87dc31a78f6a9", null ],
    [ "PORT_F", "stm32l4_2include_2periph__cpu_8h.html#a4f126a0a9b1d8c6a8f46a051ef8830bbaa3760b302740c7d09c93ec7a634f837c", null ],
    [ "PORT_G", "stm32l4_2include_2periph__cpu_8h.html#a4f126a0a9b1d8c6a8f46a051ef8830bba48afb424254d52e7d97a7c1428f5aafa", null ],
    [ "PORT_H", "stm32l4_2include_2periph__cpu_8h.html#a4f126a0a9b1d8c6a8f46a051ef8830bba31d1ae08c10668d936d1c2c6426c1c47", null ]
];