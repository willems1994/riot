var stm32f0_2include_2cpu__conf_8h =
[
    [ "CPU_DEFAULT_IRQ_PRIO", "group__cpu__stm32f0.html#ga811633719ff60ee247e64b333d4b8675", null ],
    [ "CPU_IRQ_NUMOF", "group__cpu__stm32f0.html#gaf6c13d219504576c5c69399033b0ae39", null ],
    [ "FLASHPAGE_NUMOF", "group__cpu__stm32f0.html#ga7c969ee021b57668fd1965bbe9ca1a0a", null ],
    [ "FLASHPAGE_RAW_ALIGNMENT", "group__cpu__stm32f0.html#gac2d27fbc1ae5ae8cb86479626cd4a3a6", null ],
    [ "FLASHPAGE_RAW_BLOCKSIZE", "group__cpu__stm32f0.html#ga782282047494538d13e2b3913bbc94f8", null ]
];