var group__net__rdcli__simple =
[
    [ "rdcli_simple.h", "rdcli__simple_8h.html", [
      [ "RDCLI_SIMPLE_OK", "group__net__rdcli__simple.html#gga223be06596dd9ff740bf0a09c8632294a19da44121ee5801a4d0ef524827c97ec", null ],
      [ "RDCLI_SIMPLE_NOADDR", "group__net__rdcli__simple.html#gga223be06596dd9ff740bf0a09c8632294ab26b0730c1d72624c95f4f6863efe560", null ],
      [ "RDCLI_SIMPLE_ERROR", "group__net__rdcli__simple.html#gga223be06596dd9ff740bf0a09c8632294a0be8c5dc786183b6056be62d00bb29cd", null ]
    ] ],
    [ "rdcli_simple_register", "group__net__rdcli__simple.html#gae12548d6d42ebf5b84d3054f65ad51f5", null ],
    [ "rdcli_simple_run", "group__net__rdcli__simple.html#gaf5c2d80ec28ba41971e1c5d38cfa344e", null ]
];