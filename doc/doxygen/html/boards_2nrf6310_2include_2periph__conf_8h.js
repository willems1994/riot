var boards_2nrf6310_2include_2periph__conf_8h =
[
    [ "ADC_NUMOF", "boards_2nrf6310_2include_2periph__conf_8h.html#a2f0c741db24aa2ccded869ba53f6a302", null ],
    [ "CLOCK_HFCLK", "boards_2nrf6310_2include_2periph__conf_8h.html#a77c3833b2e5ce5a43ecfdfa6692bbae4", null ],
    [ "CLOCK_LFCLK", "boards_2nrf6310_2include_2periph__conf_8h.html#a0696d5de186400ee40b2b03b475344de", null ],
    [ "RTT_DEV", "boards_2nrf6310_2include_2periph__conf_8h.html#a7f1b3908490d3eb5fa9be2688b8b5c4d", null ],
    [ "RTT_FREQUENCY", "boards_2nrf6310_2include_2periph__conf_8h.html#afec7c948b8c70db3c9394fc3dc145a99", null ],
    [ "RTT_MAX_VALUE", "boards_2nrf6310_2include_2periph__conf_8h.html#a57f384110fe2e8f4b3c4b9ba246517c6", null ],
    [ "RTT_NUMOF", "boards_2nrf6310_2include_2periph__conf_8h.html#ac5c886cfa6263655176d9883cb30f3ab", null ],
    [ "SPI_NUMOF", "boards_2nrf6310_2include_2periph__conf_8h.html#ab35a2b79568128efef74adf1ba1910a8", null ],
    [ "TIMER_0_ISR", "boards_2nrf6310_2include_2periph__conf_8h.html#a4c490d334538c05373718609ca5fe2d4", null ],
    [ "TIMER_NUMOF", "boards_2nrf6310_2include_2periph__conf_8h.html#a6e2f66f5b6f5c835dd11f9766c4ed897", null ],
    [ "UART_0_EN", "boards_2nrf6310_2include_2periph__conf_8h.html#a1daeefb24c97883fb801bee8a72fda3c", null ],
    [ "UART_HWFLOWCTRL", "boards_2nrf6310_2include_2periph__conf_8h.html#aa9b58a758c105baa3ff8b033931b5eb8", null ],
    [ "UART_IRQ_PRIO", "boards_2nrf6310_2include_2periph__conf_8h.html#a167b9436613206825d105fee53d246ee", null ],
    [ "UART_NUMOF", "boards_2nrf6310_2include_2periph__conf_8h.html#a850405f2aaa352ad264346531f0e6230", null ],
    [ "UART_PIN_CTS", "boards_2nrf6310_2include_2periph__conf_8h.html#a027eca199cf6c51cac28e671030d8595", null ],
    [ "UART_PIN_RTS", "boards_2nrf6310_2include_2periph__conf_8h.html#ad19306c5f65f535daafaee3faf545c95", null ],
    [ "UART_PIN_RX", "boards_2nrf6310_2include_2periph__conf_8h.html#a3aa1a02b68043877d2940b641989e8f1", null ],
    [ "UART_PIN_TX", "boards_2nrf6310_2include_2periph__conf_8h.html#a4df62a1f19582066fa34d18207192c8b", null ],
    [ "spi_config", "boards_2nrf6310_2include_2periph__conf_8h.html#a873188d7292e07499dcde9674b1e849c", null ],
    [ "timer_config", "boards_2nrf6310_2include_2periph__conf_8h.html#a2dd41f782d2c67052e4dc7d37cef89b1", null ]
];