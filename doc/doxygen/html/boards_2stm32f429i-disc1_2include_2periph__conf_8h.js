var boards_2stm32f429i_disc1_2include_2periph__conf_8h =
[
    [ "SPI_NUMOF", "boards_2stm32f429i-disc1_2include_2periph__conf_8h.html#ab35a2b79568128efef74adf1ba1910a8", null ],
    [ "TIMER_0_ISR", "boards_2stm32f429i-disc1_2include_2periph__conf_8h.html#a4c490d334538c05373718609ca5fe2d4", null ],
    [ "TIMER_NUMOF", "boards_2stm32f429i-disc1_2include_2periph__conf_8h.html#a6e2f66f5b6f5c835dd11f9766c4ed897", null ],
    [ "UART_0_DMA_ISR", "boards_2stm32f429i-disc1_2include_2periph__conf_8h.html#a639d73a9b4925c2970acf86d88df3d49", null ],
    [ "UART_0_ISR", "boards_2stm32f429i-disc1_2include_2periph__conf_8h.html#a713e03d19734d793baee3d1cc25c2dbb", null ],
    [ "UART_NUMOF", "boards_2stm32f429i-disc1_2include_2periph__conf_8h.html#a850405f2aaa352ad264346531f0e6230", null ],
    [ "spi_config", "boards_2stm32f429i-disc1_2include_2periph__conf_8h.html#a873188d7292e07499dcde9674b1e849c", null ],
    [ "timer_config", "boards_2stm32f429i-disc1_2include_2periph__conf_8h.html#a2dd41f782d2c67052e4dc7d37cef89b1", null ],
    [ "uart_config", "boards_2stm32f429i-disc1_2include_2periph__conf_8h.html#a1643cfc64589407fb96b4cbf908689a5", null ]
];