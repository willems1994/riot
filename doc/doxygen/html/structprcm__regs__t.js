var structprcm__regs__t =
[
    [ "__reserved1", "structprcm__regs__t.html#a03cb89e466e516291fba4941e48a92e9", null ],
    [ "__reserved10", "structprcm__regs__t.html#ad9be7ddfac984b4eeec2c810584481e7", null ],
    [ "__reserved11", "structprcm__regs__t.html#a166d671884429e85e78eafd4544141dc", null ],
    [ "__reserved12", "structprcm__regs__t.html#a82155ebeacd06aacd40b50c73c775bc9", null ],
    [ "__reserved13", "structprcm__regs__t.html#a6aed40484f3f64ced30e4248a6742025", null ],
    [ "__reserved14", "structprcm__regs__t.html#a81fbbd00459d2984ecc7a6d69760df12", null ],
    [ "__reserved2", "structprcm__regs__t.html#a4c85621082efc886a0d73ee9b7850ed2", null ],
    [ "__reserved3", "structprcm__regs__t.html#a939ff9ba7501c273347d3d8f94f94ef7", null ],
    [ "__reserved4", "structprcm__regs__t.html#a4b7ba7ab9f97035f9fc96af43cca075d", null ],
    [ "__reserved5", "structprcm__regs__t.html#ace9668fd1a6fae722789165a3377d72a", null ],
    [ "__reserved6", "structprcm__regs__t.html#a3c75652d36ef909b775f2c5df4f4def3", null ],
    [ "__reserved7", "structprcm__regs__t.html#a71b0e04783fa55affb2768dec7b31559", null ],
    [ "__reserved8", "structprcm__regs__t.html#a85bc8a44de068f07b05253f31934e2cb", null ],
    [ "__reserved9", "structprcm__regs__t.html#a6520c9c6288c847d523e10acccd6f5b4", null ],
    [ "CLKLOADCTL", "structprcm__regs__t.html#af1486ab448bb67791403d9a70357536f", null ],
    [ "CPUCLKDIV", "structprcm__regs__t.html#a36b1023c724991d47aa456f4f7ae5522", null ],
    [ "GPIOCLKGDS", "structprcm__regs__t.html#a5a565e4e431e3cb4f88006d678dfd21e", null ],
    [ "GPIOCLKGR", "structprcm__regs__t.html#ab1e5e31aa4f14e0b77cb0f45555c7698", null ],
    [ "GPIOCLKGS", "structprcm__regs__t.html#aec684a4aa6d63488127a71fda6e0a99d", null ],
    [ "GPTCLKDIV", "structprcm__regs__t.html#af627b503ead1ae5678335c51569cd944", null ],
    [ "GPTCLKGDS", "structprcm__regs__t.html#a177193f14e3f31189629aa92a1c86ad4", null ],
    [ "GPTCLKGR", "structprcm__regs__t.html#a529b44980b3a561911b917c2b8520818", null ],
    [ "GPTCLKGS", "structprcm__regs__t.html#a93280415f871c8fafe60f0fc303c194c", null ],
    [ "I2CCLKGDS", "structprcm__regs__t.html#afe0fe24b7547fa3bedf86437573657d8", null ],
    [ "I2CCLKGR", "structprcm__regs__t.html#a661363423c9ecd5d3e1286b000e5a570", null ],
    [ "I2CCLKGS", "structprcm__regs__t.html#aaa3ed9260dc37bf570b208a5aa8333d5", null ],
    [ "I2SBCLKDIV", "structprcm__regs__t.html#a0283d96bd04f98132670ee2297780239", null ],
    [ "I2SBCLKSEL", "structprcm__regs__t.html#a3662384fc3e08c6d9ec732b2d45a05e6", null ],
    [ "I2SCLKCTL", "structprcm__regs__t.html#a6e2959d4b11826a557f05eb86265b233", null ],
    [ "I2SCLKGDS", "structprcm__regs__t.html#a87e72cc8f7f035d137d166b801f1302c", null ],
    [ "I2SCLKGR", "structprcm__regs__t.html#a369df875ec1d10da9d8fa20e065cb64b", null ],
    [ "I2SCLKGS", "structprcm__regs__t.html#a8316acdbe407c6c2dec593fd01366971", null ],
    [ "I2SMCLKDIV", "structprcm__regs__t.html#a7af58cca190d299893c92a689f78b0f8", null ],
    [ "I2SWCLKDIV", "structprcm__regs__t.html#ad37a72fe311925014bff7a73a7265b8c", null ],
    [ "INFRCLKDIVDS", "structprcm__regs__t.html#adb67725210b3203522249bb70e0d066f", null ],
    [ "INFRCLKDIVR", "structprcm__regs__t.html#a791b8f66ee913ee26e46ad8685bf6fad", null ],
    [ "INFRCLKDIVS", "structprcm__regs__t.html#a3770e6f245f27249ff2cf3618fb5ffcb", null ],
    [ "PDCTL0", "structprcm__regs__t.html#abfc6d13e7a881c0d425a789a217e9b17", null ],
    [ "PDCTL0PERIPH", "structprcm__regs__t.html#ab3e80f5f9caf31ec6d6359c0a15add52", null ],
    [ "PDCTL0RFC", "structprcm__regs__t.html#a89a6caba8511d17e8eccbee1b4b19d29", null ],
    [ "PDCTL0SERIAL", "structprcm__regs__t.html#a916c25f3eaedf55286a93610e0d3979c", null ],
    [ "PDCTL1", "structprcm__regs__t.html#a175ab1a770de9397a8da6a255ee42455", null ],
    [ "PDCTL1CPU", "structprcm__regs__t.html#a6aff6fc1c10aba537d27de8a9b7febf8", null ],
    [ "PDCTL1RFC", "structprcm__regs__t.html#afdb9d4f1bfbd06bd34f28297991f0791", null ],
    [ "PDCTL1VIMS", "structprcm__regs__t.html#a0924b1cb221ee35f72666701c6c45b20", null ],
    [ "PDRETEN", "structprcm__regs__t.html#ab07d6c14c209158e4730460d7e99397a", null ],
    [ "PDSTAT0", "structprcm__regs__t.html#a5d314b089f07bb46e971f111ed358bb7", null ],
    [ "PDSTAT0PERIPH", "structprcm__regs__t.html#ac02e40b3907dcb5b5f6687ecfcdab9be", null ],
    [ "PDSTAT0RFC", "structprcm__regs__t.html#a452bcd721f733db1bd17144d226acb36", null ],
    [ "PDSTAT0SERIAL", "structprcm__regs__t.html#a55e242c84912de210f09d60eb658e2a8", null ],
    [ "PDSTAT1", "structprcm__regs__t.html#ad450395710538928ca7b241d8ff17192", null ],
    [ "PDSTAT1BUS", "structprcm__regs__t.html#a9df6c6eaefd4c15d5a34de49622c7e2e", null ],
    [ "PDSTAT1CPU", "structprcm__regs__t.html#ab7a2b90559f3b9378ac8bb7219c6c8c2", null ],
    [ "PDSTAT1RFC", "structprcm__regs__t.html#ae99b638d551b14037658f94287591ab3", null ],
    [ "PDSTAT1VIMS", "structprcm__regs__t.html#aafc0fe8a8b40cef38a1547de3cc7cf7b", null ],
    [ "RAMHWOPT", "structprcm__regs__t.html#ab3890c820de885c911dab9ce8b10ee1a", null ],
    [ "RAMRETEN", "structprcm__regs__t.html#ad95e382a99e60b86988a498a970cdd4a", null ],
    [ "RFCCLKG", "structprcm__regs__t.html#a6d9d938d1e02c940fe3caac305037adf", null ],
    [ "RFCMODESEL", "structprcm__regs__t.html#af9bc97a3d934897b22e9f64f045d5adc", null ],
    [ "SECDMACLKGDS", "structprcm__regs__t.html#aca386259feb737c9a3e6c5d610649efe", null ],
    [ "SECDMACLKGR", "structprcm__regs__t.html#a5cc186ed9433a262db330cf335bde49c", null ],
    [ "SECDMACLKGS", "structprcm__regs__t.html#af2c3297fc73fd5714ef4f5b797f53b37", null ],
    [ "SSICLKGDS", "structprcm__regs__t.html#af51ec8578167ff8059b4bb67ef2bf1d3", null ],
    [ "SSICLKGR", "structprcm__regs__t.html#a9c57becb44ac289b01a9f575e66f5bbe", null ],
    [ "SSICLKGS", "structprcm__regs__t.html#a621e2410afe7c2350f968b658497ec59", null ],
    [ "SWRESET", "structprcm__regs__t.html#a28b12bc3060c0741f5f9fbf28af730c1", null ],
    [ "UARTCLKGDS", "structprcm__regs__t.html#a27a65e6ba81eb7d85c833529d4e5d07b", null ],
    [ "UARTCLKGR", "structprcm__regs__t.html#ae94156999a374dd0f4bdc4a7e1afe8fc", null ],
    [ "UARTCLKGS", "structprcm__regs__t.html#a5c99018ce00d89e7f5ef64eb466c3da7", null ],
    [ "VDCTL", "structprcm__regs__t.html#a21fb44bb70f192bb09a50c1929601369", null ],
    [ "VIMSCLKG", "structprcm__regs__t.html#a2158d0dfd5ab5b46b8383c97b91bb635", null ],
    [ "WARMRESET", "structprcm__regs__t.html#ab89edde7c14da1d0973d02c919fc6d00", null ]
];