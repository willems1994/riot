var group__drivers__enc28j60 =
[
    [ "enc28j60_regs.h", "enc28j60__regs_8h.html", null ],
    [ "enc28j60.h", "enc28j60_8h.html", null ],
    [ "enc28j60_params_t", "structenc28j60__params__t.html", [
      [ "cs_pin", "structenc28j60__params__t.html#a1ca7cd6a120efe891cfab728f6c7f316", null ],
      [ "int_pin", "structenc28j60__params__t.html#ac8cf51031a819004957fd77a6b8e2e19", null ],
      [ "reset_pin", "structenc28j60__params__t.html#a032dca4b62979572841cb692f82768e8", null ],
      [ "spi", "structenc28j60__params__t.html#a476957c7761dd296c6490dc187b0fdae", null ]
    ] ],
    [ "enc28j60_t", "structenc28j60__t.html", [
      [ "bank", "structenc28j60__t.html#ac13a5f76e35051cdd0e119d936bd54bf", null ],
      [ "cs_pin", "structenc28j60__t.html#a8150521656c845974830765fe5f43d7d", null ],
      [ "devlock", "structenc28j60__t.html#a90775e978401b25ccb149e44d73e6d18", null ],
      [ "int_pin", "structenc28j60__t.html#a2b6736b841afbb59f99588cb55161ff5", null ],
      [ "netdev", "structenc28j60__t.html#af0eb1e2204b768c92b36de0b0f1fe500", null ],
      [ "reset_pin", "structenc28j60__t.html#a02f4f49d10aa6f8201082c234d9ae356", null ],
      [ "spi", "structenc28j60__t.html#a87983666ecd9500fdcb84b5fc67e4121", null ],
      [ "tx_time", "structenc28j60__t.html#a22ec488ccaa22d486378170f939f3983", null ]
    ] ],
    [ "enc28j60_setup", "group__drivers__enc28j60.html#ga03ed6ecdf365accb8036b5bbe723d21c", null ]
];