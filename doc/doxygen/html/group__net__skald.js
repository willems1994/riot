var group__net__skald =
[
    [ "Skald about iBeacon", "group__net__skald__ibeacon.html", "group__net__skald__ibeacon" ],
    [ "Skald meets Eddy", "group__net__skald__eddystone.html", "group__net__skald__eddystone" ],
    [ "skald.h", "skald_8h.html", null ],
    [ "skald_uuid_t", "structskald__uuid__t.html", [
      [ "u8", "structskald__uuid__t.html#a14af4eb9ab4292bae66b19785b2de2ad", null ]
    ] ],
    [ "skald_ctx_t", "structskald__ctx__t.html", [
      [ "cur_chan", "structskald__ctx__t.html#a2726e58f7d1ee2b4a4cbd75c21efed2d", null ],
      [ "last", "structskald__ctx__t.html#a255c9809a2bfa585a94a637411f2f432", null ],
      [ "pkt", "structskald__ctx__t.html#ad820e2d0e94dea14e3fac048ef27e6ca", null ],
      [ "timer", "structskald__ctx__t.html#ab726c7315fdd290424343dc7e74c23cf", null ]
    ] ],
    [ "SKALD_ADV_CHAN", "group__net__skald.html#gaa63f27cc07724ee6f804f725511cd1ba", null ],
    [ "SKALD_INTERVAL", "group__net__skald.html#ga9df6271b1eb332159168a7ebd3b65cdf", null ],
    [ "skald_adv_start", "group__net__skald.html#ga5e8e394c7ccee712fab3f30629a07346", null ],
    [ "skald_adv_stop", "group__net__skald.html#gaf8f70841bd1265f8f5a8d1c858ac3d8e", null ],
    [ "skald_generate_random_addr", "group__net__skald.html#gae33964f370c344557e7ae6e28b55d75b", null ],
    [ "skald_init", "group__net__skald.html#gadf41e167fc8fc8418a7cee9bc14d5c29", null ]
];