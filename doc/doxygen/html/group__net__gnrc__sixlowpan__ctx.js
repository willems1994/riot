var group__net__gnrc__sixlowpan__ctx =
[
    [ "ctx.h", "ctx_8h.html", null ],
    [ "gnrc_sixlowpan_ctx_t", "structgnrc__sixlowpan__ctx__t.html", [
      [ "flags_id", "structgnrc__sixlowpan__ctx__t.html#adf3863fe7da35fea1e0cd4d5b100bd9a", null ],
      [ "ltime", "structgnrc__sixlowpan__ctx__t.html#a8c2d2c3cd3f0927c438b3ad97d8488e0", null ],
      [ "prefix", "structgnrc__sixlowpan__ctx__t.html#abab172a4055236dad606413c46fa2ea8", null ],
      [ "prefix_len", "structgnrc__sixlowpan__ctx__t.html#a530aab57551691fcbd7528f7221f64db", null ]
    ] ],
    [ "GNRC_SIXLOWPAN_CTX_FLAGS_CID_MASK", "group__net__gnrc__sixlowpan__ctx.html#gab5722c21694ca76defb4661dadf13641", null ],
    [ "GNRC_SIXLOWPAN_CTX_FLAGS_COMP", "group__net__gnrc__sixlowpan__ctx.html#ga5b66afef337f4d2b7988a6e67e794d3f", null ],
    [ "GNRC_SIXLOWPAN_CTX_SIZE", "group__net__gnrc__sixlowpan__ctx.html#gac8af15dda87a6f02a55d5c2fa0efa35f", null ],
    [ "gnrc_sixlowpan_ctx_lookup_addr", "group__net__gnrc__sixlowpan__ctx.html#gaf4c50f4668e397f5e3d9b6c91091042d", null ],
    [ "gnrc_sixlowpan_ctx_lookup_id", "group__net__gnrc__sixlowpan__ctx.html#gaee6e585aaf5abb7c34c514fa330445f1", null ],
    [ "gnrc_sixlowpan_ctx_reset", "group__net__gnrc__sixlowpan__ctx.html#ga90dc8d6f9e12ec7766ea70fe17835623", null ],
    [ "gnrc_sixlowpan_ctx_update", "group__net__gnrc__sixlowpan__ctx.html#ga1c4ed509226c4d9938fd796ac184c11e", null ]
];