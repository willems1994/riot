var boards_2arduino_mkr1000_2include_2board_8h =
[
    [ "ARDUINO_LED", "boards_2arduino-mkr1000_2include_2board_8h.html#a2fc8bd5fa2ef7a79a517deb7699f7b8e", null ],
    [ "LED0_MASK", "boards_2arduino-mkr1000_2include_2board_8h.html#abfabde35a3e8b7ab50146a5e223bfead", null ],
    [ "LED0_OFF", "boards_2arduino-mkr1000_2include_2board_8h.html#aef2e39b9fd343d0e437fac0463f3e02d", null ],
    [ "LED0_ON", "boards_2arduino-mkr1000_2include_2board_8h.html#a1e48dc02333bfc2e6ad3e5bd466eecd4", null ],
    [ "LED0_PIN", "boards_2arduino-mkr1000_2include_2board_8h.html#a3fc522fda449d6edde717d6a857429cf", null ],
    [ "LED0_TOGGLE", "boards_2arduino-mkr1000_2include_2board_8h.html#aebc6389533d9fc8dcbe4d2129a4d5a45", null ],
    [ "LED_PORT", "boards_2arduino-mkr1000_2include_2board_8h.html#a663daa01e565aee93c6f20c5845b90b4", null ]
];