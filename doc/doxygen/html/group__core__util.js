var group__core__util =
[
    [ "Stack traceback (only under native)", "group__trace.html", "group__trace" ],
    [ "assert.h", "assert_8h.html", null ],
    [ "bitarithm.h", "bitarithm_8h.html", null ],
    [ "byteorder.h", "byteorder_8h.html", null ],
    [ "cib.h", "cib_8h.html", null ],
    [ "clist.h", "clist_8h.html", null ],
    [ "debug.h", "debug_8h.html", null ],
    [ "kernel_types.h", "kernel__types_8h.html", null ],
    [ "lifo.h", "lifo_8h.html", null ],
    [ "list.h", "list_8h.html", null ],
    [ "log.h", "log_8h.html", null ],
    [ "panic.h", "panic_8h.html", null ],
    [ "priority_queue.h", "priority__queue_8h.html", null ],
    [ "ringbuffer.h", "ringbuffer_8h.html", null ],
    [ "le_uint16_t", "unionle__uint16__t.html", [
      [ "u16", "unionle__uint16__t.html#a777436e2e205ce3f6b73b58f930e38ff", null ],
      [ "u8", "unionle__uint16__t.html#a9fa0573baf7cb3d89bf7f5828c81cb12", null ]
    ] ],
    [ "le_uint32_t", "unionle__uint32__t.html", [
      [ "l16", "unionle__uint32__t.html#a41872ba125e35202bc7887db5affa13a", null ],
      [ "u16", "unionle__uint32__t.html#abfc8b5fabbbdc1335ff0bf2e92a4c94c", null ],
      [ "u32", "unionle__uint32__t.html#a1e0108eba462560f75fdf737f23b872f", null ],
      [ "u8", "unionle__uint32__t.html#a287cde1c369de5f786284bb2d3a9da85", null ]
    ] ],
    [ "le_uint64_t", "unionle__uint64__t.html", [
      [ "l16", "unionle__uint64__t.html#a02d2f4bfe48b71f1bdf66ea763b9cd66", null ],
      [ "l32", "unionle__uint64__t.html#a8ac4c7fb58b81642d97d404c476ea367", null ],
      [ "u16", "unionle__uint64__t.html#ab8ec000194c7421aa1b93aad7a783a84", null ],
      [ "u32", "unionle__uint64__t.html#a9ac2b176f5f5c780bcbb3f1a4ff1734f", null ],
      [ "u64", "unionle__uint64__t.html#ab120a2c98e739197b35bbaa90d0ddd04", null ],
      [ "u8", "unionle__uint64__t.html#aad11f43c6d5b527af2d04b51b8e1dd52", null ]
    ] ],
    [ "be_uint16_t", "unionbe__uint16__t.html", [
      [ "u16", "unionbe__uint16__t.html#aecf2de179dfde105b9d80d7c980c13e9", null ],
      [ "u8", "unionbe__uint16__t.html#a27d390ffd4cdfa81d08984abbb8c7c62", null ]
    ] ],
    [ "be_uint32_t", "unionbe__uint32__t.html", [
      [ "b16", "unionbe__uint32__t.html#a30250291a65401e983cfab012a790939", null ],
      [ "u16", "unionbe__uint32__t.html#a67c798641a086681af9c9714ade071d1", null ],
      [ "u32", "unionbe__uint32__t.html#a47dbab39b3ea361038dce0105de1d3c4", null ],
      [ "u8", "unionbe__uint32__t.html#a73af4e13112544903f05f7428ce3cf3d", null ]
    ] ],
    [ "be_uint64_t", "unionbe__uint64__t.html", [
      [ "b16", "unionbe__uint64__t.html#a4e38acd1944fb3cb5cd17791ada4fb69", null ],
      [ "b32", "unionbe__uint64__t.html#adb3ccc7b1fd81feb6a94f144e5f538f0", null ],
      [ "u16", "unionbe__uint64__t.html#a51984b9830d145364c8d68ee677598e1", null ],
      [ "u32", "unionbe__uint64__t.html#a468c82ee3d7fe0363b331d086487a640", null ],
      [ "u64", "unionbe__uint64__t.html#a5d74808d91bca376df2ac5320d07c96c", null ],
      [ "u8", "unionbe__uint64__t.html#a227933143b18fa9325757354fa099ded", null ]
    ] ],
    [ "cib_t", "structcib__t.html", [
      [ "mask", "structcib__t.html#a3bfa99e5dd0ac112b4d1e36931e8901c", null ],
      [ "read_count", "structcib__t.html#ad67ebdba4a245b819dc11c13661b32a6", null ],
      [ "write_count", "structcib__t.html#a489b269e039a03c6f33d79f89cbd4f65", null ]
    ] ],
    [ "list_node", "structlist__node.html", [
      [ "next", "structlist__node.html#a0b99ca890dbfe832a8475f4bbd72338c", null ]
    ] ],
    [ "priority_queue_node", "structpriority__queue__node.html", [
      [ "data", "structpriority__queue__node.html#a5d5125f6870d6d34e1dc3e8cd0c91212", null ],
      [ "next", "structpriority__queue__node.html#acbbe5b2e4bb1c7d4fd6cf4ff569f683f", null ],
      [ "priority", "structpriority__queue__node.html#ad92629c1d46ea2a9622459539bb5ff94", null ]
    ] ],
    [ "priority_queue_t", "structpriority__queue__t.html", [
      [ "first", "structpriority__queue__t.html#a45d74667c537ab1e45f400ac597c54eb", null ]
    ] ],
    [ "_byteorder_swap", "group__core__util.html#gafb16da7687ec8edb2290c557a4da0643", null ],
    [ "ARCH_32_BIT", "group__core__util.html#ga601c5ec30f17b9c6cc955bb83d04e6be", null ],
    [ "assert", "group__core__util.html#ga3153a272f18d0f805028fce7e4337b53", null ],
    [ "CIB_INIT", "group__core__util.html#ga5d28d9cd55e98ce0e88f858630843e75", null ],
    [ "CLRBIT", "group__core__util.html#gaabb2a68d3bfb382f65c851704ff97240", null ],
    [ "DEBUG", "group__core__util.html#ga96dd473db0b3d10bd43390cdacb00120", null ],
    [ "DEBUG_ASSERT_VERBOSE", "group__core__util.html#ga2200149ba880bf26fed140bdcf318113", null ],
    [ "DEBUG_EXTRA_STACKSIZE", "group__core__util.html#ga388b22f752e93207731195e90c17dc1d", null ],
    [ "DEBUG_FUNC", "group__core__util.html#ga94c324ab9e9283e9ec9324baf52f8a71", null ],
    [ "DEBUG_PRINT", "group__core__util.html#ga88edd2aa4feabff4af21a997d5d8aa23", null ],
    [ "KERNEL_PID_FIRST", "group__core__util.html#gafc10ebb77be0d463f6f04e29f066ec9d", null ],
    [ "KERNEL_PID_LAST", "group__core__util.html#ga8328107b84cbf548fa267bffd8a0496c", null ],
    [ "KERNEL_PID_UNDEF", "group__core__util.html#gab4fb52da047cf9af9626b9c408704384", null ],
    [ "LOG", "group__core__util.html#gacaa1c3fa56061380a29889d703236251", null ],
    [ "LOG_LEVEL", "group__core__util.html#ga0b87e0d3bf5853bcbb0b66a7c48fdc05", null ],
    [ "log_write", "group__core__util.html#gac814d9c3a77713d364c21742d8aa8021", null ],
    [ "MAXTHREADS", "group__core__util.html#gab9eade334fd8a98c128802aec7e9191c", null ],
    [ "PRIkernel_pid", "group__core__util.html#gacbdb91f768b6240701dcb6cfaf0216ac", null ],
    [ "PRIORITY_QUEUE_INIT", "group__core__util.html#ga08b66bc9494298795af656fc1dce8e34", null ],
    [ "PRIORITY_QUEUE_NODE_INIT", "group__core__util.html#gab2ba24911f8c4ecf867a9d4931f068a0", null ],
    [ "SETBIT", "group__core__util.html#ga1c2c77c3c5532c2df0ecc1a8cfbd9c07", null ],
    [ "SSIZE_MAX", "group__core__util.html#ga2d6569aa794c2f23e90691e60d2f3ad2", null ],
    [ "static_assert", "group__core__util.html#ga7314912a3e552e888a578c6bdfc0ce72", null ],
    [ "clist_cmp_func_t", "group__core__util.html#gac0ec7f05102657c5a4b1d5ca7edee4a7", null ],
    [ "clist_node_t", "group__core__util.html#ga6346f09447aabddc705945b64e406f0b", null ],
    [ "kernel_pid_t", "group__core__util.html#ga8375139300d7cbf23bd8bd89ddddbe84", null ],
    [ "list_node_t", "group__core__util.html#gad8b8636e2e25308b521b32071e849c80", null ],
    [ "network_uint16_t", "group__core__util.html#ga639ad79c8926cb896d5a8f12b14d49e3", null ],
    [ "network_uint32_t", "group__core__util.html#ga041efbda03b04a2f6866cf12dde1efea", null ],
    [ "network_uint64_t", "group__core__util.html#gad85448169c2299064ee17b507fba692f", null ],
    [ "priority_queue_node_t", "group__core__util.html#ga64902f2e81b5868ff87c351eba2c44bf", [
      [ "LOG_NONE", "group__core__util.html#gga06fc87d81c62e9abb8790b6e5713c55ba85639df34979de4e5ff6f7b05e4de8f1", null ],
      [ "LOG_ERROR", "group__core__util.html#gga06fc87d81c62e9abb8790b6e5713c55ba230506cce5c68c3bac5a821c42ed3473", null ],
      [ "LOG_WARNING", "group__core__util.html#gga06fc87d81c62e9abb8790b6e5713c55ba8f6fe15bfe15104da6d1b360194a5400", null ],
      [ "LOG_INFO", "group__core__util.html#gga06fc87d81c62e9abb8790b6e5713c55ba6e98ff471e3ce6c4ef2d75c37ee51837", null ],
      [ "LOG_DEBUG", "group__core__util.html#gga06fc87d81c62e9abb8790b6e5713c55bab9f002c6ffbfd511da8090213227454e", null ],
      [ "LOG_ALL", "group__core__util.html#gga06fc87d81c62e9abb8790b6e5713c55ba87cff070224b283d7aace436723245fc", null ]
    ] ],
    [ "core_panic_t", "group__core__util.html#gafc1f32803e42d93e9fb4b9c19fa1b6e6", [
      [ "PANIC_SSP", "group__core__util.html#ggafc1f32803e42d93e9fb4b9c19fa1b6e6a0116a9ac2b87114c6a164333b0ff8ddd", null ]
    ] ],
    [ "_assert_failure", "group__core__util.html#ga81e5112ee70c93087960d7dd508cce2c", null ],
    [ "_clist_sort", "group__core__util.html#ga4c12aba839649e81b17ca6a4dde6c0bc", null ],
    [ "bitarithm_bits_set", "group__core__util.html#ga52c7993a9dedfff1f3c6f4061b9811cb", null ],
    [ "bitarithm_lsb", "group__core__util.html#ga29ff221e39c33a890123d496d9b905b8", null ],
    [ "bitarithm_msb", "group__core__util.html#gae46b870f744cf8a090b92cc0ccdffd57", null ],
    [ "byteorder_bebuftohs", "group__core__util.html#ga99640389c7624713b15fbd84277581a5", null ],
    [ "byteorder_btoll", "group__core__util.html#gab267ddcf30bb6cbacddb4ba5d4b43797", null ],
    [ "byteorder_btolll", "group__core__util.html#ga1e18a877d504245eee1c8dd53f77ee9d", null ],
    [ "byteorder_btols", "group__core__util.html#gab5bf6dfbca0857abd3b59ba4f3156132", null ],
    [ "byteorder_htobebufs", "group__core__util.html#ga0f7b9cb1a075f7e7d7e1f9059b9b2d1a", null ],
    [ "byteorder_htonl", "group__core__util.html#gaf70d2bbc1772b810c5f63929e56e747c", null ],
    [ "byteorder_htonll", "group__core__util.html#ga442cb27fcc61f6238990ecc034325d7c", null ],
    [ "byteorder_htons", "group__core__util.html#ga6b9fe4af075f2d9f3acca84ad167f003", null ],
    [ "byteorder_ltobl", "group__core__util.html#ga3c5b94ec0317462fd54266d2f2b8ed5e", null ],
    [ "byteorder_ltobll", "group__core__util.html#gac35f5bbf9fa42e3e2a21e379af41bc5a", null ],
    [ "byteorder_ltobs", "group__core__util.html#ga88b4f812dd46338cf67ed3444bb12e68", null ],
    [ "byteorder_ntohl", "group__core__util.html#ga0c6db4b53f00e68904e390a521f7df52", null ],
    [ "byteorder_ntohll", "group__core__util.html#ga46fa24d5956766847e01e776e512b6d6", null ],
    [ "byteorder_ntohs", "group__core__util.html#ga8b6508aa369528da61f75cb1a7df0c72", null ],
    [ "byteorder_swapl", "group__core__util.html#gac786fe42f31cea28921553bd040d8f38", null ],
    [ "byteorder_swapll", "group__core__util.html#ga3529983ea215f5922c4264ce32ac445c", null ],
    [ "byteorder_swaps", "group__core__util.html#gaba8648afeb93361bad221f23f0008009", null ],
    [ "cib_avail", "group__core__util.html#gaee69c5b79cbdc608f05f0005041d452c", null ],
    [ "cib_full", "group__core__util.html#ga7ce2b729dceec57505e46429682d9999", null ],
    [ "cib_get", "group__core__util.html#gaa0b21dcbf37f8869733ee2e49acef02b", null ],
    [ "cib_get_unsafe", "group__core__util.html#ga3cf8c45cd451dd2e8a77294e07d531e3", null ],
    [ "cib_init", "group__core__util.html#gabc6b04c82f9c0bbef0f06800f19a5001", null ],
    [ "cib_peek", "group__core__util.html#ga13398162a91a61290ffe7682d2cc81fb", null ],
    [ "cib_put", "group__core__util.html#gab8eeb9fa00fb289ab94aa5e1bcfcc8a2", null ],
    [ "cib_put_unsafe", "group__core__util.html#gaa461ff31d7bef0d7bfb9b4d46fb60a83", null ],
    [ "clist_find", "group__core__util.html#gadd2facdff66c9b123318370ddbf209d0", null ],
    [ "clist_find_before", "group__core__util.html#gad24db9a9f53fdbe439304e8fcfe57665", null ],
    [ "clist_foreach", "group__core__util.html#gadf5cb5bf551dcef8c6c57348a44ff773", null ],
    [ "clist_lpeek", "group__core__util.html#gaa5a4bd8e2cace2e738659f2db5d8da33", null ],
    [ "clist_lpop", "group__core__util.html#gaff94c001a5f62e1c1e994ab8dce57410", null ],
    [ "clist_lpoprpush", "group__core__util.html#gab838c6948a107f303f4134501c4c15d0", null ],
    [ "clist_lpush", "group__core__util.html#ga8e0169f917e3452ba216ddbc498691b5", null ],
    [ "clist_remove", "group__core__util.html#ga1f79f458584bda97371873f9cc86696b", null ],
    [ "clist_rpeek", "group__core__util.html#ga481aeb10c82c4165bd494d6b67e17426", null ],
    [ "clist_rpop", "group__core__util.html#gaae77bae4de0017639aebfce1ed250e5a", null ],
    [ "clist_rpush", "group__core__util.html#ga60b48772afe042a1eb0447e475afb1ba", null ],
    [ "clist_sort", "group__core__util.html#ga329c26d6ab789b930204c27abc7c6956", null ],
    [ "core_panic", "group__core__util.html#ga1f6114cc82aa8f5be5ebc2d601313c2f", null ],
    [ "htonl", "group__core__util.html#ga8c502d56f59415c4d6a909d62c602892", null ],
    [ "htonll", "group__core__util.html#ga6e307076dea862802ba0e49b98e60c47", null ],
    [ "htons", "group__core__util.html#gafc7adb63ff7355740923d4321f4e98b3", null ],
    [ "lifo_empty", "group__core__util.html#ga6b9f4ec069df7fd64e3f35c68449fa5c", null ],
    [ "lifo_get", "group__core__util.html#ga41b2e5ede51055f5b3f56043875aba6d", null ],
    [ "lifo_init", "group__core__util.html#ga1021063b280689780a60f09bf9ce132f", null ],
    [ "lifo_insert", "group__core__util.html#ga7f244179c4e5e4dc09e2c69c1552b5c6", null ],
    [ "list_add", "group__core__util.html#ga9e2bc1427bebc6edd97553c2bf6964f6", null ],
    [ "list_remove", "group__core__util.html#ga69ef1fcc895ffce8cbc3e05e4520d28b", null ],
    [ "list_remove_head", "group__core__util.html#gaaf37b0250681b3d49007e18769efc71c", null ],
    [ "ntohl", "group__core__util.html#gaea3165e59b8ae93892fdcdfbd6b7cd8d", null ],
    [ "ntohll", "group__core__util.html#ga0481689eb39bbc425a2110f48b7861c0", null ],
    [ "ntohs", "group__core__util.html#ga0e3c8704fcb8b1476dedea6c3a817de7", null ],
    [ "panic_arch", "group__core__util.html#ga288f20efe44ebe61a91403ea87a694d8", null ],
    [ "pid_is_valid", "group__core__util.html#ga512e493d9d773ddd21c3370ad14b8aaa", null ],
    [ "priority_queue_add", "group__core__util.html#gadb24278777be0ab8a954235b957af98d", null ],
    [ "priority_queue_init", "group__core__util.html#ga5d3cc838f96d979f5cba9f53102c7585", null ],
    [ "priority_queue_node_init", "group__core__util.html#gae617c3af6aa86b1eed6849d910f9f4ab", null ],
    [ "priority_queue_print", "group__core__util.html#ga48209a807ac146de2adfa4fd195b5c31", null ],
    [ "priority_queue_print_node", "group__core__util.html#ga9681ae80f47c9cc71fe6e0fbf198218f", null ],
    [ "priority_queue_remove", "group__core__util.html#ga98577f89c1d490bd9d635b8a9b69522c", null ],
    [ "priority_queue_remove_head", "group__core__util.html#ga7cea3d557e0bc559b72fcda1c69a5dee", null ],
    [ "assert_crash_message", "group__core__util.html#ga43b75e88e095cbafaff9fe0770f6368f", null ]
];