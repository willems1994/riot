var si70xx__internals_8h =
[
    [ "SI70XX_I2C_ADDRESS", "si70xx__internals_8h.html#ac9fabb5fdc2f5e4d3bce4290e47041b7", null ],
    [ "SI70XX_MEASURE_RH", "si70xx__internals_8h.html#a40a69f6de4794867b0f8bfb3d112ea8b", null ],
    [ "SI70XX_MEASURE_RH_HOLD", "si70xx__internals_8h.html#a211cba3c180d43fa214c689672053bc8", null ],
    [ "SI70XX_MEASURE_TEMP", "si70xx__internals_8h.html#a7b9354cc2cba135f0a25cb8e701e45d3", null ],
    [ "SI70XX_MEASURE_TEMP_HOLD", "si70xx__internals_8h.html#a87c3c247dd75dc0db969c0e2d62ce745", null ],
    [ "SI70XX_MEASURE_TEMP_PREV", "si70xx__internals_8h.html#a8cd6fae937e56d64e829c08b158c6a31", null ],
    [ "SI70XX_READ_HEATER_REG", "si70xx__internals_8h.html#ad3d80cc96e6865be348160ca664a3d3d", null ],
    [ "SI70XX_READ_ID_FIRST_A", "si70xx__internals_8h.html#af1561b1e70fd55469db82e095b2ce69a", null ],
    [ "SI70XX_READ_ID_FIRST_B", "si70xx__internals_8h.html#a00f621a9cfba8a4a8bf0378cc9cfa95c", null ],
    [ "SI70XX_READ_ID_SECOND_A", "si70xx__internals_8h.html#a068ccd6b42b9200a99c7b9e62e35b86f", null ],
    [ "SI70XX_READ_ID_SECOND_B", "si70xx__internals_8h.html#a0e02cef17e0e515286a6076d4a5eadc5", null ],
    [ "SI70XX_READ_REVISION_A", "si70xx__internals_8h.html#a46dd13f1ce5c1674a3c3c710dedb6269", null ],
    [ "SI70XX_READ_REVISION_B", "si70xx__internals_8h.html#a3c5507153691b27eeddbfa3a7c8d6fcc", null ],
    [ "SI70XX_READ_USER_REG", "si70xx__internals_8h.html#a06b83305ba6711c7d1825f272a04a913", null ],
    [ "SI70XX_RESET", "si70xx__internals_8h.html#a36cd08c80d048f9ecb49c4cf34d059e9", null ],
    [ "SI70XX_REVISION_1", "si70xx__internals_8h.html#a4b2f4b20ac44a11b34bea6d5ad30aad6", null ],
    [ "SI70XX_REVISION_2", "si70xx__internals_8h.html#af87b69ac7eb835c70a7a182ff03bcbc8", null ],
    [ "SI70XX_WRITE_HEATER_REG", "si70xx__internals_8h.html#a4d5d3f675e9e9a9495492fe99838c2db", null ],
    [ "SI70XX_WRITE_USER_REG", "si70xx__internals_8h.html#ad20cdb96056ece893b8f8b05b3744171", null ]
];