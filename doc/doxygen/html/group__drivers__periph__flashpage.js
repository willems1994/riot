var group__drivers__periph__flashpage =
[
    [ "flashpage.h", "flashpage_8h.html", null ],
    [ "CPU_FLASH_BASE", "group__drivers__periph__flashpage.html#gad33eb792f7cf98b55b73fea8239c5f45", null ],
    [ "FLASHPAGE_RAW_ALIGNMENT", "group__drivers__periph__flashpage.html#gac2d27fbc1ae5ae8cb86479626cd4a3a6", null ],
    [ "FLASHPAGE_RAW_BLOCKSIZE", "group__drivers__periph__flashpage.html#ga782282047494538d13e2b3913bbc94f8", [
      [ "FLASHPAGE_OK", "group__drivers__periph__flashpage.html#ggabc3975f01f66a098a347a8262ff103e5abbfd3aff1792ce2d3654c849684ee52b", null ],
      [ "FLASHPAGE_NOMATCH", "group__drivers__periph__flashpage.html#ggabc3975f01f66a098a347a8262ff103e5a5b88341d18672e0b3ad3b34e6b9ffd90", null ]
    ] ],
    [ "flashpage_addr", "group__drivers__periph__flashpage.html#ga819f2de7a9889289cd24291d6862a720", null ],
    [ "flashpage_page", "group__drivers__periph__flashpage.html#ga353a9c170ba6b54468d36e173193b52e", null ],
    [ "flashpage_read", "group__drivers__periph__flashpage.html#ga0c54494884ad395b16887f345e13b6c5", null ],
    [ "flashpage_verify", "group__drivers__periph__flashpage.html#ga35d3fafa77af19a6542f2369313b94fa", null ],
    [ "flashpage_write", "group__drivers__periph__flashpage.html#ga8bde2e3285088e799d8de12ec86f04cd", null ],
    [ "flashpage_write_and_verify", "group__drivers__periph__flashpage.html#gaf753e100ab977210a1e3b6e3a9a79c96", null ],
    [ "flashpage_write_raw", "group__drivers__periph__flashpage.html#ga49a6d8187646747e39dc73fdd16fadd5", null ]
];