var sock_2udp_8h =
[
    [ "sock_udp_ep_t", "group__net__sock__udp.html#gaedc829c7973d7870c1ec078f9ffd45a1", null ],
    [ "sock_udp_t", "group__net__sock__udp.html#ga3cb61a4ee66c9c235e4f22860658698c", null ],
    [ "sock_udp_close", "group__net__sock__udp.html#ga9bab7d0998b2c49e66bab0f03367298a", null ],
    [ "sock_udp_create", "group__net__sock__udp.html#ga20fa4b890dff1c97a63075090e6f9d7d", null ],
    [ "sock_udp_get_local", "group__net__sock__udp.html#ga1a0e4c3a85b42cfcdc2f016dd2c96c83", null ],
    [ "sock_udp_get_remote", "group__net__sock__udp.html#ga626661c83a17ad7f4f3e25b67378f7d7", null ],
    [ "sock_udp_recv", "group__net__sock__udp.html#gad1030df9b925878c2bf678487a9c88f7", null ],
    [ "sock_udp_send", "group__net__sock__udp.html#ga89562920ad89fe0e098fc989e3b064ec", null ]
];