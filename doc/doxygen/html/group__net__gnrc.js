var group__net__gnrc =
[
    [ "6LoWPAN", "group__net__gnrc__sixlowpan.html", "group__net__gnrc__sixlowpan" ],
    [ "Common MAC module", "group__net__gnrc__mac.html", "group__net__gnrc__mac" ],
    [ "Dump Network Packets", "group__net__gnrc__pktdump.html", "group__net__gnrc__pktdump" ],
    [ "GNRC communication interface", "group__net__gnrc__netapi.html", "group__net__gnrc__netapi" ],
    [ "GNRC-specific implementation of the sock API", "group__net__gnrc__sock.html", "group__net__gnrc__sock" ],
    [ "GoMacH", "group__net__gnrc__gomach.html", "group__net__gnrc__gomach" ],
    [ "IPv6", "group__net__gnrc__ipv6.html", "group__net__gnrc__ipv6" ],
    [ "LWMAC", "group__net__gnrc__lwmac.html", "group__net__gnrc__lwmac" ],
    [ "Network interface API", "group__net__gnrc__netif.html", "group__net__gnrc__netif" ],
    [ "Network protocol registry", "group__net__gnrc__netreg.html", "group__net__gnrc__netreg" ],
    [ "Packet buffer", "group__net__gnrc__pktbuf.html", "group__net__gnrc__pktbuf" ],
    [ "Packet queue", "group__net__gnrc__pktqueue.html", "group__net__gnrc__pktqueue" ],
    [ "Priority packet queue for GNRC", "group__net__gnrc__priority__pktqueue.html", "group__net__gnrc__priority__pktqueue" ],
    [ "Protocol type", "group__net__gnrc__nettype.html", "group__net__gnrc__nettype" ],
    [ "RPL", "group__net__gnrc__rpl.html", "group__net__gnrc__rpl" ],
    [ "TCP", "group__net__gnrc__tcp.html", "group__net__gnrc__tcp" ],
    [ "TFTP Support Library", "group__net__gnrc__tftp.html", "group__net__gnrc__tftp" ],
    [ "UDP", "group__net__gnrc__udp.html", "group__net__gnrc__udp" ],
    [ "gnrc.h", "gnrc_8h.html", null ]
];