var group__drivers__bmx280 =
[
    [ "bmx280_internals.h", "bmx280__internals_8h.html", null ],
    [ "bmx280.h", "bmx280_8h.html", null ],
    [ "bmx280_calibration_t", "structbmx280__calibration__t.html", [
      [ "dig_H1", "structbmx280__calibration__t.html#a7c3200eef7a04cdf152a179bcb2cefb9", null ],
      [ "dig_H2", "structbmx280__calibration__t.html#a964908a06813687f05b05fc74a106535", null ],
      [ "dig_H3", "structbmx280__calibration__t.html#a7ae7d2d0ae66d02342a0a67c01bfae84", null ],
      [ "dig_H4", "structbmx280__calibration__t.html#adb7601eb7bf375a6c2877d130b219abd", null ],
      [ "dig_H5", "structbmx280__calibration__t.html#ae94315012a19e950b5021265ae188a12", null ],
      [ "dig_H6", "structbmx280__calibration__t.html#a87d75764d94ec2558c34fa7ba9c28252", null ],
      [ "dig_P1", "structbmx280__calibration__t.html#aa23431bd8871cde03284796bc3c310fc", null ],
      [ "dig_P2", "structbmx280__calibration__t.html#abd62f1b20f152934ce9a01678fd7c993", null ],
      [ "dig_P3", "structbmx280__calibration__t.html#af2c7098a20eefc3b59b67beb7a2e883d", null ],
      [ "dig_P4", "structbmx280__calibration__t.html#aa4ffd01cbd460e4a81acb3834521ae3b", null ],
      [ "dig_P5", "structbmx280__calibration__t.html#af3bed3a32d499985d61472b75fcc6158", null ],
      [ "dig_P6", "structbmx280__calibration__t.html#aee5caa47d8b2f4808de8886b23e0fdc2", null ],
      [ "dig_P7", "structbmx280__calibration__t.html#a36b0cfce08e09d5ca40e49e4a1b3759a", null ],
      [ "dig_P8", "structbmx280__calibration__t.html#a134294ed25230051284e2110fa5c8271", null ],
      [ "dig_P9", "structbmx280__calibration__t.html#a26f0e9e86110152b310d1679f83adec4", null ],
      [ "dig_T1", "structbmx280__calibration__t.html#a4aeb072aa0076add90c03c5f52cab59c", null ],
      [ "dig_T2", "structbmx280__calibration__t.html#aa82687ad5e6cb4e6d5308fc931e7c858", null ],
      [ "dig_T3", "structbmx280__calibration__t.html#a4d16483e66366abb7ea84b1c60ebdd8f", null ]
    ] ],
    [ "bmx280_params_t", "structbmx280__params__t.html", [
      [ "filter", "structbmx280__params__t.html#adb43b7a39966025e83deb2d4a2986a90", null ],
      [ "humid_oversample", "structbmx280__params__t.html#aa6568e269f4f3f2645b3115b33782182", null ],
      [ "i2c_addr", "structbmx280__params__t.html#a4e9097aaa216123d033dd79564b0dca3", null ],
      [ "i2c_dev", "structbmx280__params__t.html#a228f55aa1e94d17e3842c7eb38bc80d1", null ],
      [ "press_oversample", "structbmx280__params__t.html#af992b0672a0aeab2f3427e0e620ee82b", null ],
      [ "run_mode", "structbmx280__params__t.html#a555cdb2eef0b60f5664d9841a3c9552f", null ],
      [ "spi3w_en", "structbmx280__params__t.html#acfc86a6c745de5d466307dc914b046f2", null ],
      [ "t_sb", "structbmx280__params__t.html#a7701cc3f3bad3a7d923840ea469af5ce", null ],
      [ "temp_oversample", "structbmx280__params__t.html#a31aaa2084e58dcedecebd48cfa8c0bb7", null ]
    ] ],
    [ "bmx280_t", "structbmx280__t.html", [
      [ "calibration", "structbmx280__t.html#adcdbf17bcd7f475387c95117b3235e52", null ],
      [ "params", "structbmx280__t.html#a9cc26f01a66c6d7b7f21e14f8caed61f", null ],
      [ "BMX280_OK", "group__drivers__bmx280.html#gga2f1398dba5e4a5616b83437528bdb28ead656ef45890e366ab0b62e82c8d8492e", null ],
      [ "BMX280_ERR_NODEV", "group__drivers__bmx280.html#gga2f1398dba5e4a5616b83437528bdb28ea3c2ff1851fa1744f9ac3e7d7a5048974", null ],
      [ "BMX280_ERR_NOCAL", "group__drivers__bmx280.html#gga2f1398dba5e4a5616b83437528bdb28eadaf0d4376743b052987aabb172f0cc83", null ]
    ] ],
    [ "bmx280_filter_t", "group__drivers__bmx280.html#gaf0194620d1e46c8b30f340320121c24a", null ],
    [ "bmx280_mode_t", "group__drivers__bmx280.html#gaac1b0c55f8eddf04811f7c996b6d9dc9", null ],
    [ "bmx280_osrs_t", "group__drivers__bmx280.html#ga34a2512229772c04fdeed5aef28b703b", null ],
    [ "bmx280_t_sb_t", "group__drivers__bmx280.html#gadeee9ddfa4006747352b2df38e5322fe", null ],
    [ "bme280_read_humidity", "group__drivers__bmx280.html#ga070ae9dc86e840bf3ef8906d38ccc9ba", null ],
    [ "bmx280_init", "group__drivers__bmx280.html#gacd5e95c111d850fb9c5a8c52ed15e576", null ],
    [ "bmx280_read_pressure", "group__drivers__bmx280.html#ga2de43bcac4757ac3aadedb6bd4c486ad", null ],
    [ "bmx280_read_temperature", "group__drivers__bmx280.html#gad56c3ca7fb43afc9f7ded726a72a05f7", null ]
];