var periph__common_8h =
[
    [ "CLOCK_CORECLOCK", "periph__common_8h.html#afc465f12242e68f6c3695caa3ba0a169", null ],
    [ "RADIO_IRQ_PRIO", "periph__common_8h.html#abc130ad4aa6c216bc9b810e36d5d63f9", null ],
    [ "TIMER_IRQ_PRIO", "periph__common_8h.html#adbe3eb289332c7b7bf848191d3a65923", null ],
    [ "TIMER_NUMOF", "periph__common_8h.html#a6e2f66f5b6f5c835dd11f9766c4ed897", null ],
    [ "UART_0_ISR", "periph__common_8h.html#a713e03d19734d793baee3d1cc25c2dbb", null ],
    [ "UART_1_ISR", "periph__common_8h.html#af9358264b5cbce69dddad098a8600aae", null ],
    [ "UART_NUMOF", "periph__common_8h.html#a850405f2aaa352ad264346531f0e6230", null ],
    [ "timer_config", "periph__common_8h.html#a2dd41f782d2c67052e4dc7d37cef89b1", null ],
    [ "uart_config", "periph__common_8h.html#a1643cfc64589407fb96b4cbf908689a5", null ]
];