var atmega1281_2include_2periph__cpu_8h =
[
    [ "CPU_ATMEGA_EXT_INTS", "atmega1281_2include_2periph__cpu_8h.html#a33b21f2a315e57318668814f00727415", null ],
    [ "EEPROM_SIZE", "atmega1281_2include_2periph__cpu_8h.html#ae3ef7bba113f663df6996f286b632a3f", null ],
    [ "I2C_PIN_MASK", "atmega1281_2include_2periph__cpu_8h.html#a00d0bff01337d7e458f49061c4e8bb98", null ],
    [ "I2C_PORT_REG", "atmega1281_2include_2periph__cpu_8h.html#aa3fa75e27eb4ad89cba2748dc486ca86", null ],
    [ "PORT_A", "atmega1281_2include_2periph__cpu_8h.html#abc6126af1d45847bc59afa0aa3216b04aeb6782d9dfedf3c6a78ffdb1624fa454", null ],
    [ "PORT_B", "atmega1281_2include_2periph__cpu_8h.html#abc6126af1d45847bc59afa0aa3216b04a16ada472d473fbd0207b99e9e4d68f4a", null ],
    [ "PORT_C", "atmega1281_2include_2periph__cpu_8h.html#abc6126af1d45847bc59afa0aa3216b04a627cc690c37f97527dd2f07aa22092d9", null ],
    [ "PORT_D", "atmega1281_2include_2periph__cpu_8h.html#abc6126af1d45847bc59afa0aa3216b04af7242fe75227a46a190645663f91ce69", null ],
    [ "PORT_E", "atmega1281_2include_2periph__cpu_8h.html#abc6126af1d45847bc59afa0aa3216b04abad63f022d1fa37a66f87dc31a78f6a9", null ],
    [ "PORT_F", "atmega1281_2include_2periph__cpu_8h.html#abc6126af1d45847bc59afa0aa3216b04aa3760b302740c7d09c93ec7a634f837c", null ],
    [ "PORT_G", "atmega1281_2include_2periph__cpu_8h.html#abc6126af1d45847bc59afa0aa3216b04a48afb424254d52e7d97a7c1428f5aafa", null ]
];