var cpu_2native_2include_2periph__conf_8h =
[
    [ "NATIVE_TIMER_MIN_RES", "cpu_2native_2include_2periph__conf_8h.html#ad2b1b919216760a8c9c7c6c0448238c1", null ],
    [ "QDEC_NUMOF", "cpu_2native_2include_2periph__conf_8h.html#a174877c2f07efedd095a1e95bf7e2ddc", null ],
    [ "RANDOM_NUMOF", "cpu_2native_2include_2periph__conf_8h.html#ac63a84fd7c167bc652e6d7adbab7e57a", null ],
    [ "RTC_NUMOF", "cpu_2native_2include_2periph__conf_8h.html#a4201ed1203952946a7b53934fcc5dad6", null ],
    [ "TIMER_0_EN", "cpu_2native_2include_2periph__conf_8h.html#a242efc9e93fc24e15fac27ef4eda1e4e", null ],
    [ "TIMER_NUMOF", "cpu_2native_2include_2periph__conf_8h.html#a6e2f66f5b6f5c835dd11f9766c4ed897", null ],
    [ "XTIMER_BACKOFF", "cpu_2native_2include_2periph__conf_8h.html#a370b9e9a079c4cb4f54fd947b67b9f41", null ],
    [ "XTIMER_ISR_BACKOFF", "cpu_2native_2include_2periph__conf_8h.html#aa1be564fc21297d7c1c8be267cbd36f6", null ],
    [ "XTIMER_OVERHEAD", "cpu_2native_2include_2periph__conf_8h.html#a6692048ab318695696ecf54dc4cc04be", null ]
];