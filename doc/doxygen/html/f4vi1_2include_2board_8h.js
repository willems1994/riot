var f4vi1_2include_2board_8h =
[
    [ "LED0_MASK", "f4vi1_2include_2board_8h.html#abfabde35a3e8b7ab50146a5e223bfead", null ],
    [ "LED0_OFF", "f4vi1_2include_2board_8h.html#aef2e39b9fd343d0e437fac0463f3e02d", null ],
    [ "LED0_ON", "f4vi1_2include_2board_8h.html#a1e48dc02333bfc2e6ad3e5bd466eecd4", null ],
    [ "LED0_PIN", "f4vi1_2include_2board_8h.html#a3fc522fda449d6edde717d6a857429cf", null ],
    [ "LED0_TOGGLE", "f4vi1_2include_2board_8h.html#aebc6389533d9fc8dcbe4d2129a4d5a45", null ],
    [ "LED1_MASK", "f4vi1_2include_2board_8h.html#a669ed6e073140d069b30442bf4c08842", null ],
    [ "LED1_OFF", "f4vi1_2include_2board_8h.html#a343fd2d3ce61b84f88ddfaea32c67c58", null ],
    [ "LED1_ON", "f4vi1_2include_2board_8h.html#aadd4c7ae0cd4e9bbb17f7055dd51fe08", null ],
    [ "LED1_PIN", "f4vi1_2include_2board_8h.html#a318aa17e5d40e2132d2c7f6269ce7f51", null ],
    [ "LED1_TOGGLE", "f4vi1_2include_2board_8h.html#a267fdbba1d750146b73da35c1731fd17", null ],
    [ "LED2_MASK", "f4vi1_2include_2board_8h.html#a40f0f4b5ae7ea50d341105ddc740101e", null ],
    [ "LED2_OFF", "f4vi1_2include_2board_8h.html#ac6468b1df4dfabcca0bb142044d6f976", null ],
    [ "LED2_ON", "f4vi1_2include_2board_8h.html#ab55f588eb2c5177d3f7806e60d379fba", null ],
    [ "LED2_PIN", "f4vi1_2include_2board_8h.html#af6f84078113b55354d20585131b386f7", null ],
    [ "LED2_TOGGLE", "f4vi1_2include_2board_8h.html#acd16785845ce7004334b91a98707f8eb", null ],
    [ "LED_PORT", "f4vi1_2include_2board_8h.html#a663daa01e565aee93c6f20c5845b90b4", null ],
    [ "board_init", "f4vi1_2include_2board_8h.html#a916f2adc2080b4fe88034086d107a8dc", null ]
];