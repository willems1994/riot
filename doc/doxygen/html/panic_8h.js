var panic_8h =
[
    [ "core_panic_t", "group__core__util.html#gafc1f32803e42d93e9fb4b9c19fa1b6e6", [
      [ "PANIC_GENERAL_ERROR", "group__core__util.html#ggafc1f32803e42d93e9fb4b9c19fa1b6e6a57f9f94153ef759494bc40894a5fdcb5", null ],
      [ "PANIC_SOFT_REBOOT", "group__core__util.html#ggafc1f32803e42d93e9fb4b9c19fa1b6e6af3dbd6bee8b08d75f624ebcb71e88b8c", null ],
      [ "PANIC_HARD_REBOOT", "group__core__util.html#ggafc1f32803e42d93e9fb4b9c19fa1b6e6a51ec22c82a838045c053a42312e82901", null ],
      [ "PANIC_ASSERT_FAIL", "group__core__util.html#ggafc1f32803e42d93e9fb4b9c19fa1b6e6aef23e5d4da3ed5e67d59e4e1991bb719", null ],
      [ "PANIC_SSP", "group__core__util.html#ggafc1f32803e42d93e9fb4b9c19fa1b6e6a0116a9ac2b87114c6a164333b0ff8ddd", null ],
      [ "PANIC_UNDEFINED", "group__core__util.html#ggafc1f32803e42d93e9fb4b9c19fa1b6e6aa1835224ad222647d14984eedd771a30", null ]
    ] ],
    [ "core_panic", "group__core__util.html#ga1f6114cc82aa8f5be5ebc2d601313c2f", null ],
    [ "panic_arch", "group__core__util.html#ga288f20efe44ebe61a91403ea87a694d8", null ]
];