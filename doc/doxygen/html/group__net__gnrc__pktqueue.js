var group__net__gnrc__pktqueue =
[
    [ "pktqueue.h", "pktqueue_8h.html", null ],
    [ "gnrc_pktqueue", "structgnrc__pktqueue.html", [
      [ "next", "structgnrc__pktqueue.html#a010d7d50c27e79ab64208c48e5a1dc12", null ],
      [ "pkt", "structgnrc__pktqueue.html#aca2f7b69bf37ceb16d0d1b31c65201a3", null ]
    ] ],
    [ "gnrc_pktqueue_t", "group__net__gnrc__pktqueue.html#ga186ec35fb003d79cf4b5ddea7add2a3f", null ],
    [ "gnrc_pktqueue_add", "group__net__gnrc__pktqueue.html#ga084afe737f9d3528617757b3593585df", null ],
    [ "gnrc_pktqueue_remove", "group__net__gnrc__pktqueue.html#gab4b2c4cb2ff85bed72fc5b32ab479cba", null ],
    [ "gnrc_pktqueue_remove_head", "group__net__gnrc__pktqueue.html#ga80f8a0d877b285e356f9291e15236620", null ]
];