var structaux__aiodio__regs__t =
[
    [ "GPIODIE", "structaux__aiodio__regs__t.html#a5e3ac97b9ca8be53faa2540b4bccde92", null ],
    [ "GPIODIN", "structaux__aiodio__regs__t.html#ac89841a276b73cf06b649d1d089c5311", null ],
    [ "GPIODOUT", "structaux__aiodio__regs__t.html#ac4b35559042c9ecf9e2e5bdd5a5dc185", null ],
    [ "GPIODOUTCLR", "structaux__aiodio__regs__t.html#ad81b8d12b47d740b8e0859b316300d09", null ],
    [ "GPIODOUTSET", "structaux__aiodio__regs__t.html#a06848916492981696975465c925184b7", null ],
    [ "GPIODOUTTGL", "structaux__aiodio__regs__t.html#a2b758e7fc747d31a36735a4eed8aa31f", null ],
    [ "IOMODE", "structaux__aiodio__regs__t.html#ad393b1fa06b94f7c6953553948457b0a", null ]
];