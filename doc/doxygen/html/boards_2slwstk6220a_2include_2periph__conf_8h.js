var boards_2slwstk6220a_2include_2periph__conf_8h =
[
    [ "CLOCK_CORECLOCK", "boards_2slwstk6220a_2include_2periph__conf_8h.html#afc465f12242e68f6c3695caa3ba0a169", null ],
    [ "CLOCK_HFCORECLKDIV", "boards_2slwstk6220a_2include_2periph__conf_8h.html#a0da57e659cd334495e718247ae8bb0e4", null ],
    [ "CLOCK_HFPERCLK", "boards_2slwstk6220a_2include_2periph__conf_8h.html#af0299963ec37fb00fa99b193af954e62", null ],
    [ "CLOCK_HFPERCLKDIV", "boards_2slwstk6220a_2include_2periph__conf_8h.html#a46491287f5669997691f41332aa1a4e5", null ],
    [ "CLOCK_HFXO", "boards_2slwstk6220a_2include_2periph__conf_8h.html#a61386ee5e291b1289483a68d0c3c4cd4", null ],
    [ "CLOCK_RCOSC", "boards_2slwstk6220a_2include_2periph__conf_8h.html#abf541ca00403986cb6271ce79166a8ec", null ],
    [ "TIMER_0_ISR", "boards_2slwstk6220a_2include_2periph__conf_8h.html#a4c490d334538c05373718609ca5fe2d4", null ],
    [ "TIMER_0_MAX_VALUE", "boards_2slwstk6220a_2include_2periph__conf_8h.html#ab43cbb6f07f99fddc0e0f6a710160654", null ],
    [ "TIMER_NUMOF", "boards_2slwstk6220a_2include_2periph__conf_8h.html#a6e2f66f5b6f5c835dd11f9766c4ed897", null ],
    [ "UART_0_ISR_RX", "boards_2slwstk6220a_2include_2periph__conf_8h.html#a8e13c32456487d198aa5f98d0611c0d7", null ],
    [ "UART_NUMOF", "boards_2slwstk6220a_2include_2periph__conf_8h.html#a850405f2aaa352ad264346531f0e6230", null ],
    [ "timer_config", "boards_2slwstk6220a_2include_2periph__conf_8h.html#a2dd41f782d2c67052e4dc7d37cef89b1", null ],
    [ "uart_config", "boards_2slwstk6220a_2include_2periph__conf_8h.html#a1643cfc64589407fb96b4cbf908689a5", null ]
];