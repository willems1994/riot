var b_l475e_iot01a_2include_2board_8h =
[
    [ "BTN_B1_PIN", "b-l475e-iot01a_2include_2board_8h.html#aa18831bd3375b44db9902639ce2c1c2b", null ],
    [ "HTS221_PARAM_I2C", "b-l475e-iot01a_2include_2board_8h.html#adeaf926ed41209e21fb5a97cef8fe8b7", null ],
    [ "LED0_MASK", "b-l475e-iot01a_2include_2board_8h.html#abfabde35a3e8b7ab50146a5e223bfead", null ],
    [ "LED0_OFF", "b-l475e-iot01a_2include_2board_8h.html#aef2e39b9fd343d0e437fac0463f3e02d", null ],
    [ "LED0_ON", "b-l475e-iot01a_2include_2board_8h.html#a1e48dc02333bfc2e6ad3e5bd466eecd4", null ],
    [ "LED0_PIN", "b-l475e-iot01a_2include_2board_8h.html#a3fc522fda449d6edde717d6a857429cf", null ],
    [ "LED0_TOGGLE", "b-l475e-iot01a_2include_2board_8h.html#aebc6389533d9fc8dcbe4d2129a4d5a45", null ],
    [ "LED1_MASK", "b-l475e-iot01a_2include_2board_8h.html#a669ed6e073140d069b30442bf4c08842", null ],
    [ "LED1_OFF", "b-l475e-iot01a_2include_2board_8h.html#a343fd2d3ce61b84f88ddfaea32c67c58", null ],
    [ "LED1_ON", "b-l475e-iot01a_2include_2board_8h.html#aadd4c7ae0cd4e9bbb17f7055dd51fe08", null ],
    [ "LED1_PIN", "b-l475e-iot01a_2include_2board_8h.html#a318aa17e5d40e2132d2c7f6269ce7f51", null ],
    [ "LED1_TOGGLE", "b-l475e-iot01a_2include_2board_8h.html#a267fdbba1d750146b73da35c1731fd17", null ],
    [ "LIS3MDL_PARAM_I2C", "b-l475e-iot01a_2include_2board_8h.html#ae08100c1f25ff6956142cd8b128390fc", null ],
    [ "LSM6DSL_PARAM_ADDR", "b-l475e-iot01a_2include_2board_8h.html#ad38496588bff754500d05f9fe085213e", null ],
    [ "LSM6DSL_PARAM_I2C", "b-l475e-iot01a_2include_2board_8h.html#a7709e562c157525e280d9879b69f7ea1", null ],
    [ "board_init", "b-l475e-iot01a_2include_2board_8h.html#a916f2adc2080b4fe88034086d107a8dc", null ]
];