var cc26x0__vims_8h =
[
    [ "flash_regs_t", "structflash__regs__t.html", "structflash__regs__t" ],
    [ "vims_regs_t", "structvims__regs__t.html", "structvims__regs__t" ],
    [ "FLASH", "cc26x0__vims_8h.html#a844ea28ba1e0a5a0e497f16b61ea306b", null ],
    [ "FLASH_BASEADDR", "group__cpu__specific__peripheral__memory__map.html#gaf6863e7094aca292453684fa2af16686", null ],
    [ "VIMS", "cc26x0__vims_8h.html#a2097ced0314f7cdd8a3ce52603bd32bd", null ],
    [ "VIMS_BASE", "group__cpu__specific__peripheral__memory__map.html#gad43ff60bcb712a7544e8f4ad483a5649", null ],
    [ "VIMS_CTL_ARB_CFG", "cc26x0__vims_8h.html#a878145fd49cadab88a80b5a8e4546ccb", null ],
    [ "VIMS_CTL_ARB_CFG_m", "cc26x0__vims_8h.html#a4efab9cfaffbbff580a5347fea3b63a4", null ],
    [ "VIMS_CTL_DYN_CG_EN", "cc26x0__vims_8h.html#a1ada49f8ecca2020f910ea0bacd0fd45", null ],
    [ "VIMS_CTL_DYN_CG_EN_m", "cc26x0__vims_8h.html#a66ac92d6028e6418ebe42f3edfcb9d58", null ],
    [ "VIMS_CTL_IDCODE_LB_DIS", "cc26x0__vims_8h.html#af938d9f32a6ede54993f8d4e944cf154", null ],
    [ "VIMS_CTL_IDCODE_LB_DIS_m", "cc26x0__vims_8h.html#a8fa8f08593c4914591b971b0f40336b9", null ],
    [ "VIMS_CTL_MODE_CACHE", "cc26x0__vims_8h.html#a814f39e98b6a8d95686ba4183bac9dd7", null ],
    [ "VIMS_CTL_MODE_GPRAM", "cc26x0__vims_8h.html#ad7d7906be803075128c956960fefca88", null ],
    [ "VIMS_CTL_MODE_m", "cc26x0__vims_8h.html#a6dbb36f29f9aaa769e6be394ab9c24b8", null ],
    [ "VIMS_CTL_MODE_OFF", "cc26x0__vims_8h.html#aaed167d0be5c72ce8845a1d067390ccb", null ],
    [ "VIMS_CTL_MODE_SPLIT", "cc26x0__vims_8h.html#a6793955d26ff5e760c809e902785e9e9", null ],
    [ "VIMS_CTL_PREF_EN", "cc26x0__vims_8h.html#abbb7e8cebe7b6f5dc72bfcf91a798495", null ],
    [ "VIMS_CTL_PREF_EN_m", "cc26x0__vims_8h.html#af5e2d8b08869842574ac83ebd7d20017", null ],
    [ "VIMS_CTL_STATS_CLR", "cc26x0__vims_8h.html#a70c663efb14c21c81ce802976c3b41dc", null ],
    [ "VIMS_CTL_STATS_CLR_m", "cc26x0__vims_8h.html#a927b2ca8cd85857b4813cb6bdc119a53", null ],
    [ "VIMS_CTL_STATS_EN", "cc26x0__vims_8h.html#acf269769e08c5158f38944b312ea0ca9", null ],
    [ "VIMS_CTL_STATS_EN_m", "cc26x0__vims_8h.html#a5679ad635d55a1a9b607fcaa2df6d02e", null ],
    [ "VIMS_CTL_SYSBUS_LB_DIS", "cc26x0__vims_8h.html#a048a769b77432947f6450cce5c46fac5", null ],
    [ "VIMS_CTL_SYSBUS_LB_DIS_m", "cc26x0__vims_8h.html#ae95de3c55538b14a4c0d715598ebd53b", null ]
];