var structtimer__conf__t =
[
    [ "bitmode", "structtimer__conf__t.html#a576c90d916a10dffb6de9d2418b9acfc", null ],
    [ "bus", "structtimer__conf__t.html#a2f1f33d278924f405112f206d02284ef", null ],
    [ "cfg", "structtimer__conf__t.html#a42e40b6d083eaaf9e15cd410f6b206ce", null ],
    [ "channels", "structtimer__conf__t.html#a56938b7d01c759f7bd0932464e8d7d87", null ],
    [ "chn", "structtimer__conf__t.html#ab2e2e1c3805e2ac8df68c93dd169897d", null ],
    [ "dev", "structtimer__conf__t.html#a744b07be733984f7772499a53319a963", null ],
    [ "dev", "structtimer__conf__t.html#a27fa19007d9745c56f2676b5de85d7c0", null ],
    [ "dev", "structtimer__conf__t.html#ad4af21466b36319186de7af6833c8eac", null ],
    [ "id_ch0", "structtimer__conf__t.html#a3c1ae42b0958d942879aa0496b648c57", null ],
    [ "irq", "structtimer__conf__t.html#a57f2559bafd81e9fa5a341f20e65f728", null ],
    [ "irqn", "structtimer__conf__t.html#a2a2737c68edbf5f98b9735fca5148daa", null ],
    [ "max", "structtimer__conf__t.html#adab46f5d98f25c33980aed852d5431b1", null ],
    [ "pre_cmu", "structtimer__conf__t.html#ad479e450e46be368e3f8dfc755c68587", null ],
    [ "prescaler", "structtimer__conf__t.html#a197e86ca65a21032786612ea74076419", null ],
    [ "prescaler", "structtimer__conf__t.html#a61d705f7045030f595e7bfccae8a30b8", null ],
    [ "rcc_mask", "structtimer__conf__t.html#a3c5e51478a993f8ff6fea8127f75ecd2", null ],
    [ "timer", "structtimer__conf__t.html#a892174e2656256807767d444366edc42", null ],
    [ "timer", "structtimer__conf__t.html#a9bc1d8274fadb013f29e83c5424175b5", null ]
];