var structfcfg__regs__t =
[
    [ "__reserved1", "structfcfg__regs__t.html#a131caded0c07bd55f8ae816120014541", null ],
    [ "__reserved10", "structfcfg__regs__t.html#a3cd745566c1d1ab64092b8cd0a3631ad", null ],
    [ "__reserved11", "structfcfg__regs__t.html#a7af1be2e9d71e237e5f162b829d8524a", null ],
    [ "__reserved12", "structfcfg__regs__t.html#a4663606fddf914eafd2ebfa676216d82", null ],
    [ "__reserved13", "structfcfg__regs__t.html#a62e85fb7e23de3fc260ec3c84f1cc11f", null ],
    [ "__reserved14", "structfcfg__regs__t.html#a75b18eff78e6ca7b27c0f369a6c98864", null ],
    [ "__reserved15", "structfcfg__regs__t.html#a7a435738859bf4bdebf2f4596783a040", null ],
    [ "__reserved2", "structfcfg__regs__t.html#a9735787964eab3062688ccbdce9a59bb", null ],
    [ "__reserved3", "structfcfg__regs__t.html#aa79e781ff3563b50de3fde3aebba4990", null ],
    [ "__reserved4", "structfcfg__regs__t.html#aba684fbc3670fa7c18a620e29e6d6601", null ],
    [ "__reserved5", "structfcfg__regs__t.html#ab633278f8aad9ffe562c499dcc8b6d0e", null ],
    [ "__reserved6", "structfcfg__regs__t.html#a22c98123e020ee793d820613fdb01308", null ],
    [ "__reserved7", "structfcfg__regs__t.html#a0496e5a7d280bc0d4f0cdc0d65e0c2d2", null ],
    [ "__reserved8", "structfcfg__regs__t.html#a44ab4be3642e7425830c9af57f7d03a9", null ],
    [ "__reserved9", "structfcfg__regs__t.html#a9b70e2e60f0f08d385a39d9004f2c233", null ],
    [ "AMPCOMP_CTRL1", "structfcfg__regs__t.html#a5affdaa501a02899b2c1a68372df27b8", null ],
    [ "AMPCOMP_TH1", "structfcfg__regs__t.html#a86d54a100ecd95de5c11038db6057326", null ],
    [ "AMPCOMP_TH2", "structfcfg__regs__t.html#a23f889d8fa541ff76e81baad273c5e65", null ],
    [ "ANA2_TRIM", "structfcfg__regs__t.html#add191088c7baf3dd88893a24372069ca", null ],
    [ "ANABYPASS_VALUE2", "structfcfg__regs__t.html#a3f5b948137d3d330c0dd444a3951e65e", null ],
    [ "CAP_TRIM", "structfcfg__regs__t.html#afdf49550ed714afa3e15ef98e5ae3cf6", null ],
    [ "CONFIG_IF_ADC", "structfcfg__regs__t.html#aa118759e5795fad148fa9752e98da688", null ],
    [ "CONFIG_MISC_ADC", "structfcfg__regs__t.html#afbd3cd0ad941bcf4f3f3730c7b11cd26", null ],
    [ "CONFIG_MISC_ADC_DIV10", "structfcfg__regs__t.html#a16f47750f4bbdcbf98679b25ba10e0b8", null ],
    [ "CONFIG_MISC_ADC_DIV12", "structfcfg__regs__t.html#a3b88ad85842586b6a93d939c74009906", null ],
    [ "CONFIG_MISC_ADC_DIV15", "structfcfg__regs__t.html#a8401f017634798cecd5ce872b7dec59c", null ],
    [ "CONFIG_MISC_ADC_DIV30", "structfcfg__regs__t.html#a9c91dc7bfa1efb4571afd9891709cfea", null ],
    [ "CONFIG_MISC_ADC_DIV5", "structfcfg__regs__t.html#a9497872da02c32283e37b4003996353c", null ],
    [ "CONFIG_MISC_ADC_DIV6", "structfcfg__regs__t.html#adc403fd8ff6cc5bbfd740089220e6b4c", null ],
    [ "CONFIG_OSC_TOP", "structfcfg__regs__t.html#a8dad2af8a03c6bd6447e5598f40e9e24", null ],
    [ "CONFIG_RF_FRONTEND", "structfcfg__regs__t.html#a1af51c79cffae8f66cfcbed3c9fed7a6", null ],
    [ "CONFIG_RF_FRONTEND_DIV10", "structfcfg__regs__t.html#a96e718a95c9942dcee0f8920a1d7c8aa", null ],
    [ "CONFIG_RF_FRONTEND_DIV12", "structfcfg__regs__t.html#a80d356cb4e5de9b5d4446a3449599efb", null ],
    [ "CONFIG_RF_FRONTEND_DIV15", "structfcfg__regs__t.html#ad5f056195a5ebc4e202c9b9d716bf8ff", null ],
    [ "CONFIG_RF_FRONTEND_DIV30", "structfcfg__regs__t.html#a42202928aadc3dce783139badafc1043", null ],
    [ "CONFIG_RF_FRONTEND_DIV5", "structfcfg__regs__t.html#accfb20ddb0c63007adbc661ac36220ec", null ],
    [ "CONFIG_RF_FRONTEND_DIV6", "structfcfg__regs__t.html#a6eec129a04d29b25a0afdb56c5eeb1e9", null ],
    [ "CONFIG_SYNTH", "structfcfg__regs__t.html#aed4a72295f21f084fbd158d191ced9ea", null ],
    [ "CONFIG_SYNTH_DIV10", "structfcfg__regs__t.html#a114668fcbc93e6dff367eb97df9a4434", null ],
    [ "CONFIG_SYNTH_DIV12", "structfcfg__regs__t.html#a22c01e7700e851fe4ea53f241a7df3b0", null ],
    [ "CONFIG_SYNTH_DIV15", "structfcfg__regs__t.html#ac43cd1054cf9f3e5e1a5c73fff144c4e", null ],
    [ "CONFIG_SYNTH_DIV30", "structfcfg__regs__t.html#a50fcd74ca36561efdc7ec4ffb4fe495e", null ],
    [ "CONFIG_SYNTH_DIV5", "structfcfg__regs__t.html#acebf523dd0baefb7ec9c6828bc2fb0a2", null ],
    [ "CONFIG_SYNTH_DIV6", "structfcfg__regs__t.html#af8c88154c4ac5d0f12b0716e60c92d32", null ],
    [ "FCFG1_REVISION", "structfcfg__regs__t.html#a4863c241d8fc93d3b167ab6e963b4256", null ],
    [ "FLASH_C_E_P_R", "structfcfg__regs__t.html#a6ec3763504974947bd7dce2f2c92ff9c", null ],
    [ "FLASH_COORDINATE", "structfcfg__regs__t.html#a9c076b68fed51f258669c9c0c3311bfa", null ],
    [ "FLASH_E_P", "structfcfg__regs__t.html#ad10e3e608799493df2881cbe719be8e8", null ],
    [ "FLASH_EH_SEQ", "structfcfg__regs__t.html#ace765d3cbe05398e88954870a22b7a54", null ],
    [ "FLASH_ERA_PW", "structfcfg__regs__t.html#a3789c7c0e7e20402df03d26ca494aa68", null ],
    [ "FLASH_NUMBER", "structfcfg__regs__t.html#aef60fa9830b36c800cff18d0ae2aefa4", null ],
    [ "FLASH_OTP_DATA3", "structfcfg__regs__t.html#afad0243cc586ab6ce76d5579da5bee4c", null ],
    [ "FLASH_OTP_DATA4", "structfcfg__regs__t.html#ac3024459fb93b763e9edd9ea7bea8a68", null ],
    [ "FLASH_P_R_PV", "structfcfg__regs__t.html#a3a77dd15c789136d6b192440d38670d1", null ],
    [ "FLASH_PP", "structfcfg__regs__t.html#a407a547cf9dfa6576bf83fea17e03eef", null ],
    [ "FLASH_PROG_EP", "structfcfg__regs__t.html#a34cbe3d03da6d52e967fb08428ab7fa8", null ],
    [ "FLASH_V", "structfcfg__regs__t.html#aec48995c709b5aa4ff3fa5f7b0f7b0ae", null ],
    [ "FLASH_VHV", "structfcfg__regs__t.html#a2cbc52dc5ac280507e400c2cc451f376", null ],
    [ "FLASH_VHV_E", "structfcfg__regs__t.html#aa7f9ead4e2929c228e74c589f884d5ea", null ],
    [ "FLASH_VHV_PV", "structfcfg__regs__t.html#a9f618e0100fcda4277c0c6d50bda2120", null ],
    [ "ICEPICK_DEVICE_ID", "structfcfg__regs__t.html#a28dd0cd8887bd6798dd7f2030e1204ec", null ],
    [ "IOCONF", "structfcfg__regs__t.html#a40e3223da62acc1e1f179cada0314da0", null ],
    [ "LDO_TRIM", "structfcfg__regs__t.html#ac6533febd4d00316723acbef717d5f3f", null ],
    [ "MAC_15_4_0", "structfcfg__regs__t.html#a3d5dce7d9b03a928e4cc909c29653d93", null ],
    [ "MAC_15_4_1", "structfcfg__regs__t.html#a5185ef95913c65c0e4453ad791aba0b9", null ],
    [ "MAC_BLE_0", "structfcfg__regs__t.html#a7f5c747d5fbb9f5fb7e2088ff19dacf6", null ],
    [ "MAC_BLE_1", "structfcfg__regs__t.html#acb0126995eb23ed5e81368d355cee0b8", null ],
    [ "MISC_CONF_1", "structfcfg__regs__t.html#a26936f58014c51e1c1f178938a2315ce", null ],
    [ "MISC_OTP_DATA", "structfcfg__regs__t.html#a90f5a14fdfe33041f9cc16a6f09bcf9d", null ],
    [ "MISC_OTP_DATA_1", "structfcfg__regs__t.html#a7e2f29c0a5ea0bdf54d6006776573131", null ],
    [ "MISC_TRIM", "structfcfg__regs__t.html#a2f44090daae43af58c105a96c0a70b00", null ],
    [ "OSC_CONF", "structfcfg__regs__t.html#a4219e69386053754320d09845091e6fb", null ],
    [ "PWD_CURR_110C", "structfcfg__regs__t.html#acfef16244fcd55592e30bec037cbaf07", null ],
    [ "PWD_CURR_125C", "structfcfg__regs__t.html#a04ce3bc5e564a8d0c143f49544ec466d", null ],
    [ "PWD_CURR_20C", "structfcfg__regs__t.html#a99f8a2b454093b5e5dd02441ce48f1c7", null ],
    [ "PWD_CURR_35C", "structfcfg__regs__t.html#a486208f024f007e906a9bbe2bf43c110", null ],
    [ "PWD_CURR_50C", "structfcfg__regs__t.html#a6784e3c2440f0d15827b73b2fd621d96", null ],
    [ "PWD_CURR_65C", "structfcfg__regs__t.html#a7f3c500d014b9a9b2b4b11191bd0b781", null ],
    [ "PWD_CURR_80C", "structfcfg__regs__t.html#a3f34672f5eb3d27e5c7d4f2ebb0d9178", null ],
    [ "PWD_CURR_95C", "structfcfg__regs__t.html#aad3be5d32116798a49abad8b2dd28caa", null ],
    [ "RCOSC_HF_TEMPCOMP", "structfcfg__regs__t.html#ab9017bcaca6655ff1f14a750e8d2eeef", null ],
    [ "SHDW_ANA_TRIM", "structfcfg__regs__t.html#a322ab9ac2dfe7c6e050c2b458b2254b5", null ],
    [ "SHDW_DIE_ID_0", "structfcfg__regs__t.html#a8495d450e9b3c2ae9ae632fb6cbfd149", null ],
    [ "SHDW_DIE_ID_1", "structfcfg__regs__t.html#a6ae1208749fc11d6ebea7c1b86d576f4", null ],
    [ "SHDW_DIE_ID_2", "structfcfg__regs__t.html#a917c639009acdd69fb562818ccc0e46b", null ],
    [ "SHDW_DIE_ID_3", "structfcfg__regs__t.html#acb1cff512b7e50ae6c3d44662282d7af", null ],
    [ "SHDW_OSC_BIAS_LDO_TRIM", "structfcfg__regs__t.html#afcfc3a9b03b5acf8cec632f08010c8b4", null ],
    [ "SOC_ADC_ABS_GAIN", "structfcfg__regs__t.html#a79cab3229c67dc73df42277474f12924", null ],
    [ "SOC_ADC_OFFSET_INT", "structfcfg__regs__t.html#ac7b485f72b5cd3ed47a8183dd1c6d868", null ],
    [ "SOC_ADC_REF_TRIM_AND_OFFSET_EXT", "structfcfg__regs__t.html#a770d207dd3c48293d2728ff1e636ab93", null ],
    [ "SOC_ADC_REL_GAIN", "structfcfg__regs__t.html#a5d7d9d5c96c3dd3eeb5406b9bd84cd31", null ],
    [ "USER_ID", "structfcfg__regs__t.html#a0e6360ede9c435a9fefe7a9081affabb", null ],
    [ "VOLT_TRIM", "structfcfg__regs__t.html#a16af5545c82806df1be43649497cb870", null ]
];