var bh1750fvi__internal_8h =
[
    [ "DELAY_HMODE", "bh1750fvi__internal_8h.html#a72f2116781e0a2066243c1648beca47e", null ],
    [ "DELAY_LMODE", "bh1750fvi__internal_8h.html#a2dd86ba1165e42eb0b99d45ec4f9c403", null ],
    [ "OP_CHANGE_TIME_H_MASK", "bh1750fvi__internal_8h.html#afce7904427de97a126dd83d2862b4820", null ],
    [ "OP_CHANGE_TIME_L_MASK", "bh1750fvi__internal_8h.html#a504bb3a3dba3c047019a8fce03661d92", null ],
    [ "OP_CONT_HRES1", "bh1750fvi__internal_8h.html#a818d9d7ede9d8e1e96aea25627cb946c", null ],
    [ "OP_CONT_HRES2", "bh1750fvi__internal_8h.html#a63c2e33b19d24de9f6164e4bf63406fc", null ],
    [ "OP_CONT_LRES", "bh1750fvi__internal_8h.html#a74e3974447d85ec74a5b4daea2a97fe0", null ],
    [ "OP_POWER_DOWN", "bh1750fvi__internal_8h.html#a6609c982269d2cb3999cefee9fd25919", null ],
    [ "OP_POWER_ON", "bh1750fvi__internal_8h.html#af459bd8496165e95e5610ec555ba8c36", null ],
    [ "OP_RESET", "bh1750fvi__internal_8h.html#ace4998415109013208595f14bdecb1e2", null ],
    [ "OP_SINGLE_HRES1", "bh1750fvi__internal_8h.html#aabccac314ca6fc542042397c5b606b73", null ],
    [ "OP_SINGLE_HRES2", "bh1750fvi__internal_8h.html#a9ccf6c9565ea7b655c61a89bfc238944", null ],
    [ "OP_SINGLE_LRES", "bh1750fvi__internal_8h.html#acf38695c7c89d3ffdc9aaf549191e0ce", null ],
    [ "RES_DIV", "bh1750fvi__internal_8h.html#a47132a147c7939554d85b46cde62a73b", null ]
];