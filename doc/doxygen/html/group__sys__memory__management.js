var group__sys__memory__management =
[
    [ "Oneway malloc", "group__oneway__malloc.html", "group__oneway__malloc" ],
    [ "TLSF-based malloc.", "group__pkg__tlsf__malloc.html", "group__pkg__tlsf__malloc" ],
    [ "memory array allocator", "group__sys__memarray.html", "group__sys__memarray" ]
];