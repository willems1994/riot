var group__net__gnrc__udp =
[
    [ "gnrc/udp.h", "gnrc_2udp_8h.html", null ],
    [ "GNRC_UDP_MSG_QUEUE_SIZE", "group__net__gnrc__udp.html#ga8414691b8da07ad07af65277a110cade", null ],
    [ "GNRC_UDP_PRIO", "group__net__gnrc__udp.html#gab866a5d747e44b76046b4344ddbd1a6f", null ],
    [ "GNRC_UDP_STACK_SIZE", "group__net__gnrc__udp.html#ga1e3d7b199d86ed40fad92ebf785ef1a2", null ],
    [ "gnrc_udp_calc_csum", "group__net__gnrc__udp.html#ga50f60f90f3d91c9ec1e18a5acb474adf", null ],
    [ "gnrc_udp_hdr_build", "group__net__gnrc__udp.html#ga262e6ffc6333255900f79329d9e445b9", null ],
    [ "gnrc_udp_init", "group__net__gnrc__udp.html#ga58042bdb9954ecec46aa0011705223ee", null ]
];