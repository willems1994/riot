var mtd_8h =
[
    [ "mtd_desc_t", "group__drivers__mtd.html#ga016431acc3cc8d6f5a40149225dabc05", null ],
    [ "mtd_power_state", "group__drivers__mtd.html#ga2e588df22ed1a520ba154adbd93b1455", [
      [ "MTD_POWER_UP", "group__drivers__mtd.html#gga2e588df22ed1a520ba154adbd93b1455a860be5ff4a3cfb933b479391d2538939", null ],
      [ "MTD_POWER_DOWN", "group__drivers__mtd.html#gga2e588df22ed1a520ba154adbd93b1455a81eb56a249bbf2eebd5cfbd8ef2c7ce8", null ]
    ] ],
    [ "mtd_erase", "group__drivers__mtd.html#gaabab7f7b0b1cfcdbe2b32d2e26bbf4d7", null ],
    [ "mtd_init", "group__drivers__mtd.html#ga6bcf582eaf56330ea31c1162ea535076", null ],
    [ "mtd_power", "group__drivers__mtd.html#gaf44f35470c2180ebd4b772f3b25d31fc", null ],
    [ "mtd_read", "group__drivers__mtd.html#ga74ffca1da0436aada58ef3f18f469e36", null ],
    [ "mtd_write", "group__drivers__mtd.html#ga5f36fb25e0daada2e9a873475c532d94", null ],
    [ "mtd_vfs_ops", "group__drivers__mtd.html#gac0a5658e078c937a88369f695baf88d8", null ]
];