var limifrog_v1_2include_2board_8h =
[
    [ "LED0_MASK", "limifrog-v1_2include_2board_8h.html#abfabde35a3e8b7ab50146a5e223bfead", null ],
    [ "LED0_OFF", "limifrog-v1_2include_2board_8h.html#aef2e39b9fd343d0e437fac0463f3e02d", null ],
    [ "LED0_ON", "limifrog-v1_2include_2board_8h.html#a1e48dc02333bfc2e6ad3e5bd466eecd4", null ],
    [ "LED0_PIN", "limifrog-v1_2include_2board_8h.html#a3fc522fda449d6edde717d6a857429cf", null ],
    [ "LED0_PORT", "limifrog-v1_2include_2board_8h.html#a76914453bb5cda4ebca204e091e8f55c", null ],
    [ "LED0_TOGGLE", "limifrog-v1_2include_2board_8h.html#aebc6389533d9fc8dcbe4d2129a4d5a45", null ],
    [ "LIS3MDL_PARAM_ADDR", "limifrog-v1_2include_2board_8h.html#a6807ca71896578b3923c2962775d95d9", null ],
    [ "LIS3MDL_PARAM_I2C", "limifrog-v1_2include_2board_8h.html#ae08100c1f25ff6956142cd8b128390fc", null ],
    [ "XTIMER_WIDTH", "limifrog-v1_2include_2board_8h.html#afea1be2406d45b8fbb1dca1a318ac2dc", null ],
    [ "board_init", "limifrog-v1_2include_2board_8h.html#a916f2adc2080b4fe88034086d107a8dc", null ]
];