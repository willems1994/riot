var boards_2common_2wsn430_2include_2periph__conf_8h =
[
    [ "CLOCK_CMCLK", "boards_2common_2wsn430_2include_2periph__conf_8h.html#a39d82d87dd1eaba834e00b113782dbac", null ],
    [ "CLOCK_CORECLOCK", "boards_2common_2wsn430_2include_2periph__conf_8h.html#afc465f12242e68f6c3695caa3ba0a169", null ],
    [ "CLOCK_MODE", "boards_2common_2wsn430_2include_2periph__conf_8h.html#aa5ebc1eafe0896afbcbd6b0eff1ae6e6", null ],
    [ "SPI_BASE", "boards_2common_2wsn430_2include_2periph__conf_8h.html#a4a3757b6bf87a9402b4cc6ff355dd015", null ],
    [ "SPI_IE", "boards_2common_2wsn430_2include_2periph__conf_8h.html#aad3cd491353e7c161fe9aba57a15e476", null ],
    [ "SPI_IE_RX_BIT", "boards_2common_2wsn430_2include_2periph__conf_8h.html#a9176f99df50fc11ac17cf9ab2780987d", null ],
    [ "SPI_IE_TX_BIT", "boards_2common_2wsn430_2include_2periph__conf_8h.html#ab039889dbe51d3c2931ccc46baa73c96", null ],
    [ "SPI_IF", "boards_2common_2wsn430_2include_2periph__conf_8h.html#a28bf28c03f2c224b6fdfee6f155cf90a", null ],
    [ "SPI_ME", "boards_2common_2wsn430_2include_2periph__conf_8h.html#a716afb7fb7d595f0ec9e762a398f8970", null ],
    [ "SPI_ME_BIT", "boards_2common_2wsn430_2include_2periph__conf_8h.html#a0d4f84143a5bb34dc7840277fe898d9b", null ],
    [ "SPI_NUMOF", "boards_2common_2wsn430_2include_2periph__conf_8h.html#ab35a2b79568128efef74adf1ba1910a8", null ],
    [ "SPI_PIN_CLK", "boards_2common_2wsn430_2include_2periph__conf_8h.html#af1d80bebb2ac6612ef0708dd63c50711", null ],
    [ "SPI_PIN_MISO", "boards_2common_2wsn430_2include_2periph__conf_8h.html#a8c324b7fda3d7ab955ab2e1c14e9bba1", null ],
    [ "SPI_PIN_MOSI", "boards_2common_2wsn430_2include_2periph__conf_8h.html#ae92d87a045a62c81ba03225f826d0d66", null ],
    [ "TIMER_BASE", "boards_2common_2wsn430_2include_2periph__conf_8h.html#a251f8c6600afee0dddf950c7a41d4723", null ],
    [ "TIMER_CHAN", "boards_2common_2wsn430_2include_2periph__conf_8h.html#ad4a3ac30513665bbbd9f08fb2d7ec7ec", null ],
    [ "TIMER_ISR_CC0", "boards_2common_2wsn430_2include_2periph__conf_8h.html#a1cbd7df3aa2f7db358a0fb67bff00fbd", null ],
    [ "TIMER_ISR_CCX", "boards_2common_2wsn430_2include_2periph__conf_8h.html#a1d7726ae1504b45722115283ab648dca", null ],
    [ "TIMER_NUMOF", "boards_2common_2wsn430_2include_2periph__conf_8h.html#a6e2f66f5b6f5c835dd11f9766c4ed897", null ],
    [ "UART_0_EN", "boards_2common_2wsn430_2include_2periph__conf_8h.html#a1daeefb24c97883fb801bee8a72fda3c", null ],
    [ "UART_BASE", "boards_2common_2wsn430_2include_2periph__conf_8h.html#aa501d94aad5260161a3f0b89ec827e92", null ],
    [ "UART_IE", "boards_2common_2wsn430_2include_2periph__conf_8h.html#a2a0ef571f37749d940482dab02335032", null ],
    [ "UART_IE_RX_BIT", "boards_2common_2wsn430_2include_2periph__conf_8h.html#a213aea478c72d6bf5b5cd9a24ae5764f", null ],
    [ "UART_IE_TX_BIT", "boards_2common_2wsn430_2include_2periph__conf_8h.html#ac6102e2daf176a51ed1955083a6a1169", null ],
    [ "UART_IF", "boards_2common_2wsn430_2include_2periph__conf_8h.html#ac3d5817cd8e644811cbcca527d805a8a", null ],
    [ "UART_ME", "boards_2common_2wsn430_2include_2periph__conf_8h.html#aa61cd0e464129b138229bdb3b65760d4", null ],
    [ "UART_ME_BITS", "boards_2common_2wsn430_2include_2periph__conf_8h.html#a286e5ce147f3a07ba2f6cef01d49df18", null ],
    [ "UART_NUMOF", "boards_2common_2wsn430_2include_2periph__conf_8h.html#a850405f2aaa352ad264346531f0e6230", null ],
    [ "UART_PORT", "boards_2common_2wsn430_2include_2periph__conf_8h.html#aa7a5950d998e44d87137de0780bd0af3", null ],
    [ "UART_RX_ISR", "boards_2common_2wsn430_2include_2periph__conf_8h.html#ae1dfef48b71adc7009ef42bc81fce100", null ],
    [ "UART_RX_PIN", "boards_2common_2wsn430_2include_2periph__conf_8h.html#aa37eca6fe61f6938d925975dff27c8fd", null ],
    [ "UART_TX_ISR", "boards_2common_2wsn430_2include_2periph__conf_8h.html#a16fdef101f5d8e7e157d1f9632adac51", null ],
    [ "UART_TX_PIN", "boards_2common_2wsn430_2include_2periph__conf_8h.html#a6c82a6844388a551e8f06d249598989e", null ]
];