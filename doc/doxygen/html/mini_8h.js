var mini_8h =
[
    [ "MINI_MATRIX_COLS", "mini_8h.html#a96d3ae3fc666f0295af8ba22a7eb10ef", null ],
    [ "MINI_MATRIX_ROWS", "mini_8h.html#adca16ff330683a673f63c6c804bd8599", null ],
    [ "mini_matrix_init", "mini_8h.html#a9a884a93844b6e815a2fb4cc90d17a95", null ],
    [ "mini_matrix_off", "mini_8h.html#a5bfbcd0e7fbf0d0da696a27e762831c7", null ],
    [ "mini_matrix_on", "mini_8h.html#a0565b1a24ed033aec27b373cedca1807", null ],
    [ "mini_matrix_set_char", "mini_8h.html#ad12dfbdf9a5a1671c145411d42c8ea0e", null ],
    [ "mini_matrix_set_raw", "mini_8h.html#ac1ce7a22901fe6124c63a1fc31c1c2b8", null ],
    [ "mini_matrix_shift_str", "mini_8h.html#a70ac38ebdaa50b91615f08a1568968fa", null ]
];