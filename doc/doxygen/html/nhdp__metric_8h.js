var nhdp__metric_8h =
[
    [ "DAT_CONSTANT", "nhdp__metric_8h.html#a9a76ecfdc59a24ae3f41defad5cebcf2", null ],
    [ "DAT_HELLO_TIMEOUT_FACTOR", "nhdp__metric_8h.html#a83fb741709f53d17efc9091e33be744c", null ],
    [ "DAT_MAXIMUM_LOSS", "nhdp__metric_8h.html#aa9159f77b06078e04a83864934026097", null ],
    [ "DAT_MEMORY_LENGTH", "nhdp__metric_8h.html#aa4cedad48287b417986cec1012394ab1", null ],
    [ "DAT_MINIMUM_BITRATE", "nhdp__metric_8h.html#a7bb07f3e752dbb228c4ab61e2afa05b8", null ],
    [ "DAT_REFRESH_INTERVAL", "nhdp__metric_8h.html#ab9b5853bc29917156a554c3d70c5a9bc", null ],
    [ "NHDP_KD_LM_INC", "nhdp__metric_8h.html#ab1bd6b758651a54695c3826679a7aa3b", null ],
    [ "NHDP_KD_LM_OUT", "nhdp__metric_8h.html#a896e8b22860355831096f333ed794be4", null ],
    [ "NHDP_KD_NM_INC", "nhdp__metric_8h.html#a05ea983585b562ca34cc53e11ada236c", null ],
    [ "NHDP_KD_NM_OUT", "nhdp__metric_8h.html#a84e25854f4a83f56144df72d5effb20b", null ],
    [ "NHDP_LMT_DAT", "nhdp__metric_8h.html#a54302d3db219da6efff427ccdd0c571f", null ],
    [ "NHDP_LMT_HOP_COUNT", "nhdp__metric_8h.html#aa182d31fd0fc428b82cc3928c705faa4", null ],
    [ "NHDP_METRIC", "nhdp__metric_8h.html#aa9b77b766c4c1fff6551913a0ead9bb0", null ],
    [ "NHDP_METRIC_MAXIMUM", "nhdp__metric_8h.html#a84b8b58604126ebec184a9ad006b47f3", null ],
    [ "NHDP_METRIC_MINIMUM", "nhdp__metric_8h.html#a6acd44ee51112a1db1b47cc5db7a11da", null ],
    [ "NHDP_METRIC_NEEDS_TIMER", "nhdp__metric_8h.html#a649788fb22d0473ee496eb4a5ea02c44", null ],
    [ "NHDP_METRIC_TIMER", "nhdp__metric_8h.html#aed8134b6ee283c55fb033fd8cc4bddb1", null ],
    [ "NHDP_METRIC_UNKNOWN", "nhdp__metric_8h.html#a9ff5e864a9caac19c923ca1298d7800f", null ],
    [ "NHDP_Q_MEM_LENGTH", "nhdp__metric_8h.html#a1d088dc98f42bc6492fc6cfd23436fbe", null ],
    [ "NHDP_SEQNO_RESTART_DETECT", "nhdp__metric_8h.html#a1457cc4c8a68ba731b7cfa3e1fcaef9b", null ]
];