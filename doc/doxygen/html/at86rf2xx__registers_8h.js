var at86rf2xx__registers_8h =
[
    [ "AT86RF212B_PARTNUM", "at86rf2xx__registers_8h.html#a0d31d880f9d3aa56b7bf60cf6a004b74", null ],
    [ "AT86RF231_PARTNUM", "at86rf2xx__registers_8h.html#ad5a2d3d8d0d031a05fa61814b6f4f39b", null ],
    [ "AT86RF232_PARTNUM", "at86rf2xx__registers_8h.html#a59d390ca7784a7ae635746f65436a53b", null ],
    [ "AT86RF233_PARTNUM", "at86rf2xx__registers_8h.html#a33f9a6cf6a8664c122721a0759869e7a", null ],
    [ "AT86RF2XX_ACCESS_FB", "at86rf2xx__registers_8h.html#a44b4c7dd76b31468ef9dd394414c6e22", null ],
    [ "AT86RF2XX_ACCESS_READ", "at86rf2xx__registers_8h.html#a25d047d047a64bd824736a1d39334826", null ],
    [ "AT86RF2XX_ACCESS_REG", "at86rf2xx__registers_8h.html#a955d128af9e8f7b157da24890bf0e869", null ],
    [ "AT86RF2XX_ACCESS_SRAM", "at86rf2xx__registers_8h.html#a6da3f88f145064b7cdc91fba2c00e59d", null ],
    [ "AT86RF2XX_ACCESS_WRITE", "at86rf2xx__registers_8h.html#ac544f94a30ffd03d019a096607d2a870", null ],
    [ "AT86RF2XX_CCA_THRES_MASK__CCA_ED_THRES", "at86rf2xx__registers_8h.html#a055abb171cfd4a7eee557f1485373297", null ],
    [ "AT86RF2XX_CCA_THRES_MASK__RSVD_HI_NIBBLE", "at86rf2xx__registers_8h.html#a76833525f714aaeac89d71ad8aaeb8ce", null ],
    [ "AT86RF2XX_CSMA_SEED_1__AACK_DIS_ACK", "at86rf2xx__registers_8h.html#afa60c6e527aea9c9cda8fffa631f5840", null ],
    [ "AT86RF2XX_CSMA_SEED_1__AACK_I_AM_COORD", "at86rf2xx__registers_8h.html#a33f169abc5e7fdee97160a9bf904ce18", null ],
    [ "AT86RF2XX_CSMA_SEED_1__AACK_SET_PD", "at86rf2xx__registers_8h.html#a7d3fcbca88e09a37e21fb6369f33b754", null ],
    [ "AT86RF2XX_CSMA_SEED_1__CSMA_SEED_1", "at86rf2xx__registers_8h.html#a221926a1bf3f803ccfed4540dc5444d9", null ],
    [ "AT86RF2XX_IRQ_STATUS_MASK__AMI", "at86rf2xx__registers_8h.html#a5a01032114642f85e888169a1bec0c8e", null ],
    [ "AT86RF2XX_IRQ_STATUS_MASK__BAT_LOW", "at86rf2xx__registers_8h.html#ab89b06dba52204c3bd5bf92072aacd51", null ],
    [ "AT86RF2XX_IRQ_STATUS_MASK__CCA_ED_DONE", "at86rf2xx__registers_8h.html#a42815d186765bf708966af40a44abf16", null ],
    [ "AT86RF2XX_IRQ_STATUS_MASK__PLL_LOCK", "at86rf2xx__registers_8h.html#a58fc3c77caa0522ae744c9a4ed10019b", null ],
    [ "AT86RF2XX_IRQ_STATUS_MASK__PLL_UNLOCK", "at86rf2xx__registers_8h.html#af465d48b1a8b29b76dda4508cb0f5e07", null ],
    [ "AT86RF2XX_IRQ_STATUS_MASK__RX_START", "at86rf2xx__registers_8h.html#ad771f619e00c72dc3542686f1184f9ca", null ],
    [ "AT86RF2XX_IRQ_STATUS_MASK__TRX_END", "at86rf2xx__registers_8h.html#a0fcf6b2b8da0e1823a0efeaa9ee88c5d", null ],
    [ "AT86RF2XX_IRQ_STATUS_MASK__TRX_UR", "at86rf2xx__registers_8h.html#a64aee4178747017eca2a3e47c73c9f94", null ],
    [ "AT86RF2XX_PARTNUM", "at86rf2xx__registers_8h.html#a434f0a1d59450cf702021d3bd8ce195b", null ],
    [ "AT86RF2XX_PHY_CC_CCA_DEFAULT__CCA_MODE", "at86rf2xx__registers_8h.html#af3dd483a84aac08217a99b194bbce429", null ],
    [ "AT86RF2XX_PHY_CC_CCA_MASK__CCA_MODE", "at86rf2xx__registers_8h.html#a3b822ab7928d7655278456d1a1538d39", null ],
    [ "AT86RF2XX_PHY_CC_CCA_MASK__CCA_REQUEST", "at86rf2xx__registers_8h.html#a09b367405373384e810b6f3c0fffc760", null ],
    [ "AT86RF2XX_PHY_CC_CCA_MASK__CHANNEL", "at86rf2xx__registers_8h.html#aaf2f44be92fee480b30a90480cf2be0e", null ],
    [ "AT86RF2XX_PHY_RSSI_MASK__RND_VALUE", "at86rf2xx__registers_8h.html#a2981f46fc827f56b9c0746059de90c3b", null ],
    [ "AT86RF2XX_PHY_RSSI_MASK__RSSI", "at86rf2xx__registers_8h.html#a454f704f3f6c54924dc1d61105446d7b", null ],
    [ "AT86RF2XX_PHY_RSSI_MASK__RX_CRC_VALID", "at86rf2xx__registers_8h.html#a16d71f21ca407132b27520fef7b74f66", null ],
    [ "AT86RF2XX_PHY_TX_PWR_DEFAULT__PA_BUF_LT", "at86rf2xx__registers_8h.html#ab226c30bf24c9d6ccd75ddbb0b2d6418", null ],
    [ "AT86RF2XX_PHY_TX_PWR_DEFAULT__PA_LT", "at86rf2xx__registers_8h.html#a0c82729fd98f01440dddd15aa830bfa8", null ],
    [ "AT86RF2XX_PHY_TX_PWR_DEFAULT__TX_PWR", "at86rf2xx__registers_8h.html#afc4fd44536dfed86e95b0b9a3a3f48f0", null ],
    [ "AT86RF2XX_PHY_TX_PWR_MASK__TX_PWR", "at86rf2xx__registers_8h.html#a98100a851aa12d909f8b7d3c2894f307", null ],
    [ "AT86RF2XX_REG__ANT_DIV", "at86rf2xx__registers_8h.html#a672ee40c28c2c7b7e903aaee897b4eae", null ],
    [ "AT86RF2XX_REG__BATMON", "at86rf2xx__registers_8h.html#ab086e5bf3b746da1419b79e8a6ae212e", null ],
    [ "AT86RF2XX_REG__CC_CTRL_1", "at86rf2xx__registers_8h.html#a53cc908746462df6a46dc3355f9cbbae", null ],
    [ "AT86RF2XX_REG__CCA_THRES", "at86rf2xx__registers_8h.html#a3c63742dabfb69c48998005c35bfb27d", null ],
    [ "AT86RF2XX_REG__CSMA_BE", "at86rf2xx__registers_8h.html#ac844ad2250e44d851913f691df0ddda7", null ],
    [ "AT86RF2XX_REG__CSMA_SEED_0", "at86rf2xx__registers_8h.html#ad61ddb9f0a6345fa6dde36942e174693", null ],
    [ "AT86RF2XX_REG__CSMA_SEED_1", "at86rf2xx__registers_8h.html#a2a0cda3865f1a921f85cc230a16e010a", null ],
    [ "AT86RF2XX_REG__FTN_CTRL", "at86rf2xx__registers_8h.html#a1c93f250f1a534ce50eb45d9a8dc1291", null ],
    [ "AT86RF2XX_REG__IEEE_ADDR_0", "at86rf2xx__registers_8h.html#a1a44458095040a5371286c9769164ecc", null ],
    [ "AT86RF2XX_REG__IEEE_ADDR_1", "at86rf2xx__registers_8h.html#aaeadb63cb1990a156a2fa068e942d40d", null ],
    [ "AT86RF2XX_REG__IEEE_ADDR_2", "at86rf2xx__registers_8h.html#a0c3040c816272fc117230fcb1c0b773b", null ],
    [ "AT86RF2XX_REG__IEEE_ADDR_3", "at86rf2xx__registers_8h.html#a33fe7229194f51abd352fc727d19eb71", null ],
    [ "AT86RF2XX_REG__IEEE_ADDR_4", "at86rf2xx__registers_8h.html#a34ea5a8c6d94310797dc65f9be372abb", null ],
    [ "AT86RF2XX_REG__IEEE_ADDR_5", "at86rf2xx__registers_8h.html#a56fca88c4a6670ef084c007deb244fd8", null ],
    [ "AT86RF2XX_REG__IEEE_ADDR_6", "at86rf2xx__registers_8h.html#a6686fbe0832c318e182e28fb7479f842", null ],
    [ "AT86RF2XX_REG__IEEE_ADDR_7", "at86rf2xx__registers_8h.html#a778ec44518466e8c69e9716a54a06c43", null ],
    [ "AT86RF2XX_REG__IRQ_MASK", "at86rf2xx__registers_8h.html#af7cc743546a63a2b63168696a6a94caa", null ],
    [ "AT86RF2XX_REG__IRQ_STATUS", "at86rf2xx__registers_8h.html#a520ef48dc8e3c5a29ce95fe975f559cb", null ],
    [ "AT86RF2XX_REG__MAN_ID_0", "at86rf2xx__registers_8h.html#a0b837f23389e1c6003a2015a964ec829", null ],
    [ "AT86RF2XX_REG__MAN_ID_1", "at86rf2xx__registers_8h.html#a9ca3634313becc5939d8ca921d1c9086", null ],
    [ "AT86RF2XX_REG__PAN_ID_0", "at86rf2xx__registers_8h.html#ae39adf1e30120dd2f7df81a7c3c698c7", null ],
    [ "AT86RF2XX_REG__PAN_ID_1", "at86rf2xx__registers_8h.html#a7e88e71a668bb726c4c2feb6d362d96c", null ],
    [ "AT86RF2XX_REG__PART_NUM", "at86rf2xx__registers_8h.html#a6d5af873d0b2616402164a110b6ab445", null ],
    [ "AT86RF2XX_REG__PHY_CC_CCA", "at86rf2xx__registers_8h.html#a3b1c45c354a6c897208fc5290ae7ae77", null ],
    [ "AT86RF2XX_REG__PHY_ED_LEVEL", "at86rf2xx__registers_8h.html#afb95106f61a8e0c3dd4cb0bbde641d0e", null ],
    [ "AT86RF2XX_REG__PHY_RSSI", "at86rf2xx__registers_8h.html#a35c680db6741133597ca316561942666", null ],
    [ "AT86RF2XX_REG__PHY_TX_PWR", "at86rf2xx__registers_8h.html#a69fec1c37294fc43b4c8bcaabfa86e09", null ],
    [ "AT86RF2XX_REG__PLL_CF", "at86rf2xx__registers_8h.html#ae883756f0fda4b1c2f65647da4ba1edb", null ],
    [ "AT86RF2XX_REG__PLL_DCU", "at86rf2xx__registers_8h.html#a30eec1db9f37fe80f52f892e02d7b35e", null ],
    [ "AT86RF2XX_REG__RX_CTRL", "at86rf2xx__registers_8h.html#a6a0f4583683a848c8ed5015681ca93f6", null ],
    [ "AT86RF2XX_REG__RX_SYN", "at86rf2xx__registers_8h.html#ac7e82626349739df85f4fe3e7991346d", null ],
    [ "AT86RF2XX_REG__SFD_VALUE", "at86rf2xx__registers_8h.html#ac849e22e357ef9626342bbdf5528676f", null ],
    [ "AT86RF2XX_REG__SHORT_ADDR_0", "at86rf2xx__registers_8h.html#a8fa8861c4d584d771077a16e70fac517", null ],
    [ "AT86RF2XX_REG__SHORT_ADDR_1", "at86rf2xx__registers_8h.html#a0672c8a67bd9d67aefedec72b7f78e85", null ],
    [ "AT86RF2XX_REG__TRX_CTRL_0", "at86rf2xx__registers_8h.html#a390fe4cb8b94254cf2956e2695609526", null ],
    [ "AT86RF2XX_REG__TRX_CTRL_1", "at86rf2xx__registers_8h.html#a84e0d8a352892e4efe1daf06900ec8ec", null ],
    [ "AT86RF2XX_REG__TRX_CTRL_2", "at86rf2xx__registers_8h.html#a358cedfbe05123c90e01ec7dd7326cdd", null ],
    [ "AT86RF2XX_REG__TRX_STATE", "at86rf2xx__registers_8h.html#a52f1d8878b55a706a84d84acf3651a68", null ],
    [ "AT86RF2XX_REG__TRX_STATUS", "at86rf2xx__registers_8h.html#abc5bebeb25cccc54e6d9357b4801c82e", null ],
    [ "AT86RF2XX_REG__TST_CTRL_DIGI", "at86rf2xx__registers_8h.html#ab9c01e2a0be853ec704e0f401bf46121", null ],
    [ "AT86RF2XX_REG__VERSION_NUM", "at86rf2xx__registers_8h.html#a884b33f167ca955c69fafa17246ae6ff", null ],
    [ "AT86RF2XX_REG__VREG_CTRL", "at86rf2xx__registers_8h.html#af9713a08bdeeac7cdffa74ab2824fee6", null ],
    [ "AT86RF2XX_REG__XAH_CTRL_0", "at86rf2xx__registers_8h.html#a46e6daf0a1c7f2652f0d2cefac1aed63", null ],
    [ "AT86RF2XX_REG__XAH_CTRL_1", "at86rf2xx__registers_8h.html#a1ec4bf5c885d2b4f376aa864d2e65db6", null ],
    [ "AT86RF2XX_REG__XOSC_CTRL", "at86rf2xx__registers_8h.html#af4bdbd7f7ae34702e4593319c97ab2ac", null ],
    [ "AT86RF2XX_RX_SYN__RX_OVERRIDE", "at86rf2xx__registers_8h.html#a481cf046cb622db17a33e1a921f49fe7", null ],
    [ "AT86RF2XX_RX_SYN__RX_PDT_DIS", "at86rf2xx__registers_8h.html#a9b7bb7ea4d62dbc14f9e75f0484ff6ef", null ],
    [ "AT86RF2XX_RX_SYN__RX_PDT_LEVEL", "at86rf2xx__registers_8h.html#a1218231bab463127839f35c3ea2581bc", null ],
    [ "AT86RF2XX_TIMING__PLL_ON_TO_BUSY_TX", "at86rf2xx__registers_8h.html#a34d80d821ff641f5ae7c7ef3c38ecb4f", null ],
    [ "AT86RF2XX_TIMING__RESET", "at86rf2xx__registers_8h.html#a508812930ec5854fe9de6010b48a51f9", null ],
    [ "AT86RF2XX_TIMING__RESET_TO_TRX_OFF", "at86rf2xx__registers_8h.html#abe3cd307d9bd928906c6acc3f5f59be2", null ],
    [ "AT86RF2XX_TIMING__SLEEP_TO_TRX_OFF", "at86rf2xx__registers_8h.html#aa50bced729765fcab3ad9b7d963ec8b0", null ],
    [ "AT86RF2XX_TIMING__TRX_OFF_TO_PLL_ON", "at86rf2xx__registers_8h.html#a0709aac23eda55d2c6334fa4a96eee74", null ],
    [ "AT86RF2XX_TIMING__TRX_OFF_TO_RX_ON", "at86rf2xx__registers_8h.html#aa0922daead7a361044f3d5e19eb229b6", null ],
    [ "AT86RF2XX_TIMING__VCC_TO_P_ON", "at86rf2xx__registers_8h.html#a45f7b7bc7ae75f5dec258db2ddf04fe5", null ],
    [ "AT86RF2XX_TRX_CTRL_0_CLKM_CTRL__16MHz", "at86rf2xx__registers_8h.html#a855353ddbc72fda35e7f56dec4df3ea7", null ],
    [ "AT86RF2XX_TRX_CTRL_0_CLKM_CTRL__1MHz", "at86rf2xx__registers_8h.html#acc71a6ce0c80359e9fbfb6e0476dd5f8", null ],
    [ "AT86RF2XX_TRX_CTRL_0_CLKM_CTRL__250kHz", "at86rf2xx__registers_8h.html#a444da553d840486adb83a54cdab8fe5c", null ],
    [ "AT86RF2XX_TRX_CTRL_0_CLKM_CTRL__2MHz", "at86rf2xx__registers_8h.html#a8327428d520691ed6610cc834e6550e0", null ],
    [ "AT86RF2XX_TRX_CTRL_0_CLKM_CTRL__4MHz", "at86rf2xx__registers_8h.html#aa4397cd8c0720b2e3bd686486770f0e3", null ],
    [ "AT86RF2XX_TRX_CTRL_0_CLKM_CTRL__62_5kHz", "at86rf2xx__registers_8h.html#a87a602de6a04fbd9d2bc817f1e7eed05", null ],
    [ "AT86RF2XX_TRX_CTRL_0_CLKM_CTRL__8MHz", "at86rf2xx__registers_8h.html#a06f39872385b79551859754b1208c56a", null ],
    [ "AT86RF2XX_TRX_CTRL_0_CLKM_CTRL__OFF", "at86rf2xx__registers_8h.html#a256e0f1817e95ee968f7a3e8af69ce5a", null ],
    [ "AT86RF2XX_TRX_CTRL_0_DEFAULT__CLKM_CTRL", "at86rf2xx__registers_8h.html#af39e182ec6e4ef53206b8c3d68655218", null ],
    [ "AT86RF2XX_TRX_CTRL_0_DEFAULT__CLKM_SHA_SEL", "at86rf2xx__registers_8h.html#a2ecdb9e6d0eab31a2e22023c800ed227", null ],
    [ "AT86RF2XX_TRX_CTRL_0_DEFAULT__PAD_IO", "at86rf2xx__registers_8h.html#a3b9beda7a1f2e41513be83ec392b4811", null ],
    [ "AT86RF2XX_TRX_CTRL_0_DEFAULT__PAD_IO_CLKM", "at86rf2xx__registers_8h.html#a2d167d438d8fcf08d8dedd57e7fae29a", null ],
    [ "AT86RF2XX_TRX_CTRL_0_MASK__CLKM_CTRL", "at86rf2xx__registers_8h.html#a14231da57be8a404978798eeff6a0d0e", null ],
    [ "AT86RF2XX_TRX_CTRL_0_MASK__CLKM_SHA_SEL", "at86rf2xx__registers_8h.html#a1d64ef85f7fab463e9fea40171582fb1", null ],
    [ "AT86RF2XX_TRX_CTRL_0_MASK__PAD_IO", "at86rf2xx__registers_8h.html#a82794556ee585a26c6bd43d08e36d23f", null ],
    [ "AT86RF2XX_TRX_CTRL_0_MASK__PAD_IO_CLKM", "at86rf2xx__registers_8h.html#ab0139b7459feaf80dabe1db4c688cf75", null ],
    [ "AT86RF2XX_TRX_CTRL_1_MASK__IRQ_2_EXT_EN", "at86rf2xx__registers_8h.html#a02df1dcde88b2969d3461ce8c42ae955", null ],
    [ "AT86RF2XX_TRX_CTRL_1_MASK__IRQ_MASK_MODE", "at86rf2xx__registers_8h.html#a4724609ca5e80e2acd62f091ba3e1627", null ],
    [ "AT86RF2XX_TRX_CTRL_1_MASK__IRQ_POLARITY", "at86rf2xx__registers_8h.html#a56b8e238cfbe9541c2b0fdc1b8f1773d", null ],
    [ "AT86RF2XX_TRX_CTRL_1_MASK__PA_EXT_EN", "at86rf2xx__registers_8h.html#ae620e94609a7d4d010e18932153ac234", null ],
    [ "AT86RF2XX_TRX_CTRL_1_MASK__RX_BL_CTRL", "at86rf2xx__registers_8h.html#a88c930b0e26409ea4407d9f79e9ad780", null ],
    [ "AT86RF2XX_TRX_CTRL_1_MASK__SPI_CMD_MODE", "at86rf2xx__registers_8h.html#a9a5f3ac64eb8f59f15b7b2b1cd1e4d9e", null ],
    [ "AT86RF2XX_TRX_CTRL_1_MASK__TX_AUTO_CRC_ON", "at86rf2xx__registers_8h.html#a6511d17c3baaeacb4f1bc3a14eacfcd5", null ],
    [ "AT86RF2XX_TRX_CTRL_2_MASK__ALT_SPECTRUM", "at86rf2xx__registers_8h.html#ac979d73f173df82ddfdc9cdbe1917748", null ],
    [ "AT86RF2XX_TRX_CTRL_2_MASK__BPSK_OQPSK", "at86rf2xx__registers_8h.html#ad78f609c13c9e9cb6806c55dd34581ad", null ],
    [ "AT86RF2XX_TRX_CTRL_2_MASK__FREQ_MODE", "at86rf2xx__registers_8h.html#a9825b12946013714f5d2c840917e76eb", null ],
    [ "AT86RF2XX_TRX_CTRL_2_MASK__OQPSK_DATA_RATE", "at86rf2xx__registers_8h.html#afcdf2338858ea2c6a1e940089c25f1a2", null ],
    [ "AT86RF2XX_TRX_CTRL_2_MASK__OQPSK_SCRAM_EN", "at86rf2xx__registers_8h.html#ad52f198d6f7c52cf152d78c746efe98e", null ],
    [ "AT86RF2XX_TRX_CTRL_2_MASK__RX_SAFE_MODE", "at86rf2xx__registers_8h.html#ad8d3543808ba7a8058ea032add7fd8a6", null ],
    [ "AT86RF2XX_TRX_CTRL_2_MASK__SUB_MODE", "at86rf2xx__registers_8h.html#a9f8b7500784af68ddb8c739fbb3ec130", null ],
    [ "AT86RF2XX_TRX_CTRL_2_MASK__TRX_OFF_AVDD_EN", "at86rf2xx__registers_8h.html#a576f085312d5722b3a84992b69759ba7", null ],
    [ "AT86RF2XX_TRX_STATE__FORCE_PLL_ON", "at86rf2xx__registers_8h.html#a4e1f0b08579d6b87210715c9fc2b15ad", null ],
    [ "AT86RF2XX_TRX_STATE__FORCE_TRX_OFF", "at86rf2xx__registers_8h.html#a0863082ba36476a101d4f931aff8ee27", null ],
    [ "AT86RF2XX_TRX_STATE__NOP", "at86rf2xx__registers_8h.html#a6d4fcb05711d62813a90af18de283d1b", null ],
    [ "AT86RF2XX_TRX_STATE__PLL_ON", "at86rf2xx__registers_8h.html#a695474a4831735598fb971618014bbbe", null ],
    [ "AT86RF2XX_TRX_STATE__RX_AACK_ON", "at86rf2xx__registers_8h.html#a0e615d1d56422a667d509142f5e25258", null ],
    [ "AT86RF2XX_TRX_STATE__RX_ON", "at86rf2xx__registers_8h.html#ac22e5db71825811d0c4424e8f58a1d6c", null ],
    [ "AT86RF2XX_TRX_STATE__TRAC_CHANNEL_ACCESS_FAILURE", "at86rf2xx__registers_8h.html#a4e508c9886d5429cf2a32eed131ad628", null ],
    [ "AT86RF2XX_TRX_STATE__TRAC_INVALID", "at86rf2xx__registers_8h.html#a3c249fdfc9d9fe8ab6b478df99a48f58", null ],
    [ "AT86RF2XX_TRX_STATE__TRAC_NO_ACK", "at86rf2xx__registers_8h.html#ab88c538ed8997f375fda2b441a652b7f", null ],
    [ "AT86RF2XX_TRX_STATE__TRAC_SUCCESS", "at86rf2xx__registers_8h.html#a707506dc754c0a94e8a3a16ea30b7e37", null ],
    [ "AT86RF2XX_TRX_STATE__TRAC_SUCCESS_DATA_PENDING", "at86rf2xx__registers_8h.html#a0460a7c8a034cdd57cdc1ea8b146ac7e", null ],
    [ "AT86RF2XX_TRX_STATE__TRAC_SUCCESS_WAIT_FOR_ACK", "at86rf2xx__registers_8h.html#a55bc6ba6627cf90e95fafc9fa9b5f0fd", null ],
    [ "AT86RF2XX_TRX_STATE__TRX_OFF", "at86rf2xx__registers_8h.html#ab73422023935373b6f8ef4db8113d747", null ],
    [ "AT86RF2XX_TRX_STATE__TX_ARET_ON", "at86rf2xx__registers_8h.html#a315fd71162e6fda3343bbba51dfee88e", null ],
    [ "AT86RF2XX_TRX_STATE__TX_START", "at86rf2xx__registers_8h.html#ad32358598293ad2ffc6c51366130ff7a", null ],
    [ "AT86RF2XX_TRX_STATE_MASK__TRAC", "at86rf2xx__registers_8h.html#a785f780fb14acbfc3db7f7c85831adbc", null ],
    [ "AT86RF2XX_TRX_STATUS__BUSY_RX", "at86rf2xx__registers_8h.html#a1f96c05c701168244408ae96663a639d", null ],
    [ "AT86RF2XX_TRX_STATUS__BUSY_RX_AACK", "at86rf2xx__registers_8h.html#a9fd5d2020f7230a138ecc3d070427aa6", null ],
    [ "AT86RF2XX_TRX_STATUS__BUSY_RX_AACK_NOCLK", "at86rf2xx__registers_8h.html#a4cba00f9b8a53c0bbd25e780293f8f61", null ],
    [ "AT86RF2XX_TRX_STATUS__BUSY_TX", "at86rf2xx__registers_8h.html#afb8a19c6b08a7215a3561871bf65a83c", null ],
    [ "AT86RF2XX_TRX_STATUS__BUSY_TX_ARET", "at86rf2xx__registers_8h.html#a2ba08c276d04e98c9611b5edb7523f47", null ],
    [ "AT86RF2XX_TRX_STATUS__P_ON", "at86rf2xx__registers_8h.html#a8e486930d8f3d0b204968fc0d98b30d8", null ],
    [ "AT86RF2XX_TRX_STATUS__PLL_ON", "at86rf2xx__registers_8h.html#acab29c5a2fbe6893882ee10d501ff986", null ],
    [ "AT86RF2XX_TRX_STATUS__RX_AACK_ON", "at86rf2xx__registers_8h.html#aa2e05b55988ea7cda91a61fee5c84856", null ],
    [ "AT86RF2XX_TRX_STATUS__RX_AACK_ON_NOCLK", "at86rf2xx__registers_8h.html#adeaaaff469850b64a6ba008f3ea955f1", null ],
    [ "AT86RF2XX_TRX_STATUS__RX_ON", "at86rf2xx__registers_8h.html#add774fce0a5927036b56998dcea039c4", null ],
    [ "AT86RF2XX_TRX_STATUS__RX_ON_NOCLK", "at86rf2xx__registers_8h.html#a30f79882c7becca8651f9e60dc34c1b0", null ],
    [ "AT86RF2XX_TRX_STATUS__SLEEP", "at86rf2xx__registers_8h.html#ae74dbfcacb4b768cdd4a47f652ae47c4", null ],
    [ "AT86RF2XX_TRX_STATUS__STATE_TRANSITION_IN_PROGRESS", "at86rf2xx__registers_8h.html#ad6570f48ddcf1a5e982c3a62780237c3", null ],
    [ "AT86RF2XX_TRX_STATUS__TRX_OFF", "at86rf2xx__registers_8h.html#a796adffeb9341df01914dfc32bec8c3f", null ],
    [ "AT86RF2XX_TRX_STATUS__TX_ARET_ON", "at86rf2xx__registers_8h.html#a38b11ca5a6ac31d19e8a81a8207eecb8", null ],
    [ "AT86RF2XX_TRX_STATUS_MASK__CCA_DONE", "at86rf2xx__registers_8h.html#ab372725b913919daf1fb682a64d3d5d3", null ],
    [ "AT86RF2XX_TRX_STATUS_MASK__CCA_STATUS", "at86rf2xx__registers_8h.html#ac1da6520394b1f9ff9a7a2bcbdf2c011", null ],
    [ "AT86RF2XX_TRX_STATUS_MASK__TRX_STATUS", "at86rf2xx__registers_8h.html#a218f240ad1d87ec15660b4df1a28e006", null ],
    [ "AT86RF2XX_XAH_CTRL_0__MAX_CSMA_RETRIES", "at86rf2xx__registers_8h.html#a63cc97d9aa170730f5d6bcb9264ea183", null ],
    [ "AT86RF2XX_XAH_CTRL_0__MAX_FRAME_RETRIES", "at86rf2xx__registers_8h.html#a7cb04b895d8ec8dd60e42e68843ed43e", null ],
    [ "AT86RF2XX_XAH_CTRL_0__SLOTTED_OPERATION", "at86rf2xx__registers_8h.html#a5aa33e2c563bc998657b8a13cb794bbd", null ],
    [ "AT86RF2XX_XAH_CTRL_1__AACK_ACK_TIME", "at86rf2xx__registers_8h.html#a7bcfa6c26aec9dfae9a78d30579d6cc2", null ],
    [ "AT86RF2XX_XAH_CTRL_1__AACK_FLTR_RES_FT", "at86rf2xx__registers_8h.html#afbb1585351644b73532eab4e458cae32", null ],
    [ "AT86RF2XX_XAH_CTRL_1__AACK_PROM_MODE", "at86rf2xx__registers_8h.html#ae570b429299f540b688f0635fb30209c", null ],
    [ "AT86RF2XX_XAH_CTRL_1__AACK_UPLD_RES_FT", "at86rf2xx__registers_8h.html#abfb4c368281062ed77e46b722cc58c56", null ],
    [ "AT86RF2XX_XOSC_CTRL__XTAL_MODE_CRYSTAL", "at86rf2xx__registers_8h.html#ad571b5e05ada03fb3e705c46621178b6", null ],
    [ "AT86RF2XX_XOSC_CTRL__XTAL_MODE_EXTERNAL", "at86rf2xx__registers_8h.html#aa4ac5b7f2c8436341bd31ddec0b46229", null ]
];