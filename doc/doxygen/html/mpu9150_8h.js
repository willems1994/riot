var mpu9150_8h =
[
    [ "MPU9150_BYPASS_SLEEP_US", "group__drivers__mpu9150.html#ga5f10d809e5cac87903643aedf6758b83", null ],
    [ "MPU9150_COMP_FUSE_ROM", "group__drivers__mpu9150.html#ga25f83599636b9b935afc706d61326249", null ],
    [ "MPU9150_COMP_MODE_SLEEP_US", "group__drivers__mpu9150.html#ga34e878022f3f1cf99a5141cc2b551d90", null ],
    [ "MPU9150_COMP_POWER_DOWN", "group__drivers__mpu9150.html#ga623b04b2b3e6d440014509332828198d", null ],
    [ "MPU9150_COMP_SELF_TEST", "group__drivers__mpu9150.html#gab10e25dd0a4ccc0644a3b93649e758e1", null ],
    [ "MPU9150_COMP_SINGLE_MEASURE", "group__drivers__mpu9150.html#gabf28bfa399884930f4523ce3aa008680", null ],
    [ "MPU9150_COMP_WHOAMI_ANSWER", "group__drivers__mpu9150.html#ga7e00b732d15f3f42e4481ce090394844", null ],
    [ "MPU9150_DEFAULT_SAMPLE_RATE", "group__drivers__mpu9150.html#gaa2bab68afee636944ec1e1166e28b95b", null ],
    [ "MPU9150_MAX_COMP_SMPL_RATE", "group__drivers__mpu9150.html#gac487d2f1fa13f25aa36fc76d4d5db34a", null ],
    [ "MPU9150_MAX_SAMPLE_RATE", "group__drivers__mpu9150.html#ga88c58e351110d4ce797f10fafc710427", null ],
    [ "MPU9150_MIN_COMP_SMPL_RATE", "group__drivers__mpu9150.html#gac422ac997b801491398736cc97c42fe6", null ],
    [ "MPU9150_MIN_SAMPLE_RATE", "group__drivers__mpu9150.html#gaff9342eacc8884ac19554b9010fc0f01", null ],
    [ "MPU9150_PWR_ACCEL", "group__drivers__mpu9150.html#ga34bb7cbc820adbe859eb8e6abcfa4179", null ],
    [ "MPU9150_PWR_CHANGE_SLEEP_US", "group__drivers__mpu9150.html#ga429f6556f9646c284ea86eb5626ae33f", null ],
    [ "MPU9150_PWR_GYRO", "group__drivers__mpu9150.html#ga692bc1d62f4e30df880b7a8c250c3c8d", null ],
    [ "MPU9150_PWR_PLL", "group__drivers__mpu9150.html#ga2424e7b380da61eefff93a74db6bb383", null ],
    [ "MPU9150_PWR_RESET", "group__drivers__mpu9150.html#gacba6a180248b230be09b5cd22bc352fc", null ],
    [ "MPU9150_PWR_WAKEUP", "group__drivers__mpu9150.html#gaaafc21e267d7a0bde1b0e62118c3aa7e", null ],
    [ "MPU9150_RESET_SLEEP_US", "group__drivers__mpu9150.html#ga44a180ed48717fdb9f9cf93cf253c9b7", null ],
    [ "mpu9150_accel_ranges_t", "group__drivers__mpu9150.html#ga3228c7e770a4be305737017cb4b3f980", [
      [ "MPU9150_ACCEL_FSR_2G", "group__drivers__mpu9150.html#gga3228c7e770a4be305737017cb4b3f980a86966ce91598c49be4ae25134920d135", null ],
      [ "MPU9150_ACCEL_FSR_4G", "group__drivers__mpu9150.html#gga3228c7e770a4be305737017cb4b3f980ad752f3c072f40e793a06f2c7dd1a8d65", null ],
      [ "MPU9150_ACCEL_FSR_8G", "group__drivers__mpu9150.html#gga3228c7e770a4be305737017cb4b3f980a21894579a0c1edcfff6136de9a1a4787", null ],
      [ "MPU9150_ACCEL_FSR_16G", "group__drivers__mpu9150.html#gga3228c7e770a4be305737017cb4b3f980a139abaa2637928e1c9731344b4efe190", null ]
    ] ],
    [ "mpu9150_comp_addr_t", "group__drivers__mpu9150.html#gafdab809031cd22c53300b381bb0207bf", [
      [ "MPU9150_COMP_ADDR_HEX_0C", "group__drivers__mpu9150.html#ggafdab809031cd22c53300b381bb0207bfad711da38a9b16054d9362ca4a66b4a18", null ],
      [ "MPU9150_COMP_ADDR_HEX_0D", "group__drivers__mpu9150.html#ggafdab809031cd22c53300b381bb0207bfafbabc35dba4b9b0dba2a3857835dad4c", null ],
      [ "MPU9150_COMP_ADDR_HEX_0E", "group__drivers__mpu9150.html#ggafdab809031cd22c53300b381bb0207bfa5dbb32ed4f00c7eab007431104bfa4c3", null ],
      [ "MPU9150_COMP_ADDR_HEX_0F", "group__drivers__mpu9150.html#ggafdab809031cd22c53300b381bb0207bfa710e3ecef0233bc5909777f501182a91", null ]
    ] ],
    [ "mpu9150_gyro_ranges_t", "group__drivers__mpu9150.html#gaa126660dd8db65f72bb4249994f9bef6", [
      [ "MPU9150_GYRO_FSR_250DPS", "group__drivers__mpu9150.html#ggaa126660dd8db65f72bb4249994f9bef6af07ab47067c496030f933be1acc3a73d", null ],
      [ "MPU9150_GYRO_FSR_500DPS", "group__drivers__mpu9150.html#ggaa126660dd8db65f72bb4249994f9bef6ab8b1f24835abdcb6d0a179ee8a76de6e", null ],
      [ "MPU9150_GYRO_FSR_1000DPS", "group__drivers__mpu9150.html#ggaa126660dd8db65f72bb4249994f9bef6a7a37b49c72c8a4e033ebf017ca1fcc7e", null ],
      [ "MPU9150_GYRO_FSR_2000DPS", "group__drivers__mpu9150.html#ggaa126660dd8db65f72bb4249994f9bef6a4f8ced21e58afef782daed5900c72a45", null ]
    ] ],
    [ "mpu9150_hw_addr_t", "group__drivers__mpu9150.html#ga5d8b814ba537a1952c7e411831f77da3", [
      [ "MPU9150_HW_ADDR_HEX_68", "group__drivers__mpu9150.html#gga5d8b814ba537a1952c7e411831f77da3ab9f778cbd7a16ec85301eb901224bd11", null ],
      [ "MPU9150_HW_ADDR_HEX_69", "group__drivers__mpu9150.html#gga5d8b814ba537a1952c7e411831f77da3a06ce16ac31efd181fc46db6da3dad7d5", null ]
    ] ],
    [ "mpu9150_lpf_t", "group__drivers__mpu9150.html#gae24e2383fe0ea85f30dd1b5e0bfd635a", [
      [ "MPU9150_FILTER_188HZ", "group__drivers__mpu9150.html#ggae24e2383fe0ea85f30dd1b5e0bfd635aa0cf642a89330801bd296dc101a952022", null ],
      [ "MPU9150_FILTER_98HZ", "group__drivers__mpu9150.html#ggae24e2383fe0ea85f30dd1b5e0bfd635aaf4ce44c14d8445804f4a1cea10161033", null ],
      [ "MPU9150_FILTER_42HZ", "group__drivers__mpu9150.html#ggae24e2383fe0ea85f30dd1b5e0bfd635aaacb7ff3626822d26e7efb20fff486976", null ],
      [ "MPU9150_FILTER_20HZ", "group__drivers__mpu9150.html#ggae24e2383fe0ea85f30dd1b5e0bfd635aa6763efdaf13066f6c287e6c79d247647", null ],
      [ "MPU9150_FILTER_10HZ", "group__drivers__mpu9150.html#ggae24e2383fe0ea85f30dd1b5e0bfd635aaa9a1fb5d879f1bf010c3c9b32e8868fa", null ],
      [ "MPU9150_FILTER_5HZ", "group__drivers__mpu9150.html#ggae24e2383fe0ea85f30dd1b5e0bfd635aa294d1e1f51a341e40516e67a29549b4c", null ]
    ] ],
    [ "mpu9150_pwr_t", "group__drivers__mpu9150.html#ga18617e03d41c9988a8b99e00ef00c2db", [
      [ "MPU9150_SENSOR_PWR_OFF", "group__drivers__mpu9150.html#gga18617e03d41c9988a8b99e00ef00c2dbadce009ddb9c1ca0b39dab16235628f4e", null ],
      [ "MPU9150_SENSOR_PWR_ON", "group__drivers__mpu9150.html#gga18617e03d41c9988a8b99e00ef00c2dba958ee8b47f33e683b02fce2e99c9ea3d", null ]
    ] ],
    [ "mpu9150_init", "group__drivers__mpu9150.html#ga3371054a3fea11f1ff21bd5aa83d5ee4", null ],
    [ "mpu9150_read_accel", "group__drivers__mpu9150.html#ga5f14cd279af09aad146784163c15d619", null ],
    [ "mpu9150_read_compass", "group__drivers__mpu9150.html#ga6622adce0784b030c8e0128b92203235", null ],
    [ "mpu9150_read_gyro", "group__drivers__mpu9150.html#gac4e00e5a9a00df1be43f0b685d8b1e3f", null ],
    [ "mpu9150_read_temperature", "group__drivers__mpu9150.html#gaaf275e3dbbf2ef6cfdfbf088c4d80c12", null ],
    [ "mpu9150_set_accel_fsr", "group__drivers__mpu9150.html#ga63e6b870737a5da617a3205a3c32d140", null ],
    [ "mpu9150_set_accel_power", "group__drivers__mpu9150.html#ga0926a4f72d87c8fdd1a8ae160b921659", null ],
    [ "mpu9150_set_compass_power", "group__drivers__mpu9150.html#ga045c51d7aee16b73cd25690c64da3c82", null ],
    [ "mpu9150_set_compass_sample_rate", "group__drivers__mpu9150.html#gacc20b320489bfb259ccfbb21c6ad0d0a", null ],
    [ "mpu9150_set_gyro_fsr", "group__drivers__mpu9150.html#ga5367266d93931d47db685961a03647e9", null ],
    [ "mpu9150_set_gyro_power", "group__drivers__mpu9150.html#ga8dc34d39ec7920398dcbf18fadf9acea", null ],
    [ "mpu9150_set_sample_rate", "group__drivers__mpu9150.html#ga70460ec1f5411afe5f1e170c44a2e0bc", null ]
];