var group__posix =
[
    [ "POSIX semaphores", "group__posix__semaphore.html", "group__posix__semaphore" ],
    [ "POSIX sockets", "group__posix__sockets.html", "group__posix__sockets" ],
    [ "POSIX threads", "group__pthread.html", "group__pthread" ],
    [ "uio.h", "uio_8h.html", null ],
    [ "fcntl.h", "fcntl_8h.html", null ],
    [ "iovec", "structiovec.html", [
      [ "iov_base", "structiovec.html#a07aeddeccf80f14520fdd7ef0759442b", null ],
      [ "iov_len", "structiovec.html#a17b5ac2078fd1adfabb262a95808a07d", null ]
    ] ]
];