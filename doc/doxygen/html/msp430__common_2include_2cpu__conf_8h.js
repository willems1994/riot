var msp430__common_2include_2cpu__conf_8h =
[
    [ "FLASHPAGE_SIZE", "msp430__common_2include_2cpu__conf_8h.html#afce96cb577e50c76434ba92363ca20e8", null ],
    [ "GNRC_IPV6_STACK_SIZE", "msp430__common_2include_2cpu__conf_8h.html#a2326b53a03c6e2ddc9036bf020b11b99", null ],
    [ "GNRC_PKTBUF_SIZE", "msp430__common_2include_2cpu__conf_8h.html#a83a14b0588bb8adc93b565c4ebc5ec56", null ],
    [ "ISR_STACKSIZE", "msp430__common_2include_2cpu__conf_8h.html#aa9a78f0a8d308d2d145cfe671c36ec8e", null ],
    [ "THREAD_EXTRA_STACKSIZE_PRINTF", "msp430__common_2include_2cpu__conf_8h.html#af30305c0f413c7da1e7445de2b5ea5a5", null ],
    [ "THREAD_STACKSIZE_DEFAULT", "msp430__common_2include_2cpu__conf_8h.html#a713ebddc00581f4d415095cdbfd8791f", null ],
    [ "THREAD_STACKSIZE_IDLE", "msp430__common_2include_2cpu__conf_8h.html#ac4f62f762a057d594ae0ee4522dd14c2", null ]
];