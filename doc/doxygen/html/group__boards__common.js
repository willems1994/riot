var group__boards__common =
[
    [ "Arduino Due Common", "group__boards__common__arduino__due.html", "group__boards__common__arduino__due" ],
    [ "Arduino MKR Common", "group__boards__common__arduino-mkr.html", "group__boards__common__arduino-mkr" ],
    [ "ESP8266 Common", "group__boards__common__esp8266.html", "group__boards__common__esp8266" ],
    [ "IoTlab Common", "group__boards__common__iotlab.html", "group__boards__common__iotlab" ],
    [ "MSB-430 common", "group__boards__common__msb-430.html", "group__boards__common__msb-430" ],
    [ "MSB-A2 common", "group__boards__common__msba2.html", "group__boards__common__msba2" ],
    [ "NRF52 common", "group__boards__common__nrf52.html", "group__boards__common__nrf52" ],
    [ "NXP FRDM Common", "group__boards__common__frdm.html", null ],
    [ "STM32 Configuration Snippets", "group__boards__common__stm32.html", "group__boards__common__stm32" ],
    [ "STM32 Nucleo Common", "group__boards__common__nucleo.html", "group__boards__common__nucleo" ],
    [ "STM32F103C8 based boards common", "group__boards__common__stm32f103c8.html", "group__boards__common__stm32f103c8" ],
    [ "Shared WSN430 files", "group__boards__common__wsn430.html", "group__boards__common__wsn430" ],
    [ "Silicon Labs Common", "group__boards__common__silabs.html", "group__boards__common__silabs" ],
    [ "Zolertia Re-Mote common", "group__boards__common__remote.html", "group__boards__common__remote" ]
];