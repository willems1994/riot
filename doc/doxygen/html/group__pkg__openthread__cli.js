var group__pkg__openthread__cli =
[
    [ "ot.h", "ot_8h.html", null ],
    [ "serial_msg_t", "structserial__msg__t.html", [
      [ "buf", "structserial__msg__t.html#afb88dd6ffbf2bb9544cdb695b4a330c2", null ],
      [ "len", "structserial__msg__t.html#ab988652c4aff64975ce9e3c52d803229", null ]
    ] ],
    [ "ot_job_t", "structot__job__t.html", [
      [ "answer", "structot__job__t.html#ad505535ec426a888446bdd53cac1fa62", null ],
      [ "arg", "structot__job__t.html#a43d3cb4bf0ee2745b1e95233a101f237", null ],
      [ "command", "structot__job__t.html#a885a468b09fb44084dbf572c58c8bb9e", null ]
    ] ],
    [ "OPENTHREAD_JOB_MSG_TYPE_EVENT", "group__pkg__openthread__cli.html#gaa9a23fb62a13a66c0c30156a9400bc16", null ],
    [ "OPENTHREAD_MSG_TYPE_RECV", "group__pkg__openthread__cli.html#ga3c25a9fffe6b903255a9fb30b704f082", null ],
    [ "OPENTHREAD_NETDEV_MSG_TYPE_EVENT", "group__pkg__openthread__cli.html#ga01d823dfed3516ae9956c131476a325c", null ],
    [ "OPENTHREAD_SERIAL_MSG_TYPE_EVENT", "group__pkg__openthread__cli.html#ga8e8fd20861bbb3bd9b459ca5e19ede2d", null ],
    [ "OPENTHREAD_XTIMER_MSG_TYPE_EVENT", "group__pkg__openthread__cli.html#ga59f79c284a963284048f720b54332965", null ],
    [ "openthread_bootstrap", "group__pkg__openthread__cli.html#ga963c57aa6bb15734ab10566bee94dc6c", null ],
    [ "openthread_get_pid", "group__pkg__openthread__cli.html#ga9271773f1e41c8d19c37690d2a18162b", null ],
    [ "openthread_netdev_init", "group__pkg__openthread__cli.html#gad2afaf2ae5b311fd512511bdc564facb", null ],
    [ "openthread_radio_init", "group__pkg__openthread__cli.html#gaba109f63056026e2684665c1e8065b2a", null ],
    [ "openthread_uart_run", "group__pkg__openthread__cli.html#ga181bff7ce10f7bb4879c193f673dfd59", null ],
    [ "ot_call_command", "group__pkg__openthread__cli.html#ga07a09ae021a8e316aeacd4472c3269ce", null ],
    [ "ot_exec_command", "group__pkg__openthread__cli.html#ga03dce7ac079be61c316c1beff0cfcfe8", null ],
    [ "ot_random_init", "group__pkg__openthread__cli.html#ga98a74e75b4e2a956751f57dda897d36b", null ],
    [ "recv_pkt", "group__pkg__openthread__cli.html#gae51cf07d6e60984548e8e4e800354903", null ],
    [ "send_pkt", "group__pkg__openthread__cli.html#ga8c24ba8801614d7839291212853172a5", null ]
];