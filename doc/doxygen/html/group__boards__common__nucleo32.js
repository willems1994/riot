var group__boards__common__nucleo32 =
[
    [ "STM32 Nucleo-F031K6", "group__boards__nucleo-f031k6.html", "group__boards__nucleo-f031k6" ],
    [ "STM32 Nucleo-F042K6", "group__boards__nucleo-f042k6.html", "group__boards__nucleo-f042k6" ],
    [ "STM32 Nucleo-F303K8", "group__boards__nucleo-f303k8.html", "group__boards__nucleo-f303k8" ],
    [ "STM32 Nucleo-L031K6", "group__boards__nucleo-l031k6.html", "group__boards__nucleo-l031k6" ],
    [ "STM32 Nucleo-L432KC", "group__boards__nucleo-l432kc.html", "group__boards__nucleo-l432kc" ],
    [ "common/nucleo32/include/arduino_board.h", "common_2nucleo32_2include_2arduino__board_8h.html", null ],
    [ "common/nucleo32/include/arduino_pinmap.h", "common_2nucleo32_2include_2arduino__pinmap_8h.html", null ],
    [ "common/nucleo32/include/board.h", "common_2nucleo32_2include_2board_8h.html", null ]
];