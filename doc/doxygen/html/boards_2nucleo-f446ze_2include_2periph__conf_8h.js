var boards_2nucleo_f446ze_2include_2periph__conf_8h =
[
    [ "ADC_NUMOF", "group__boards__nucleo-f446ze.html#ga2f0c741db24aa2ccded869ba53f6a302", null ],
    [ "I2C_0_ISR", "group__boards__nucleo-f446ze.html#ga5655b75a493bbd17b560958d66369152", null ],
    [ "I2C_NUMOF", "group__boards__nucleo-f446ze.html#gabce62e16a6e3b3205801fed93c51692d", null ],
    [ "PWM_NUMOF", "group__boards__nucleo-f446ze.html#ga44adbd579bb180f3cfe8ec78932eb7a1", null ],
    [ "SPI_NUMOF", "group__boards__nucleo-f446ze.html#gab35a2b79568128efef74adf1ba1910a8", null ],
    [ "TIMER_0_ISR", "group__boards__nucleo-f446ze.html#ga4c490d334538c05373718609ca5fe2d4", null ],
    [ "TIMER_NUMOF", "group__boards__nucleo-f446ze.html#ga6e2f66f5b6f5c835dd11f9766c4ed897", null ],
    [ "UART_0_DMA_ISR", "group__boards__nucleo-f446ze.html#ga639d73a9b4925c2970acf86d88df3d49", null ],
    [ "UART_0_ISR", "group__boards__nucleo-f446ze.html#ga713e03d19734d793baee3d1cc25c2dbb", null ],
    [ "UART_1_DMA_ISR", "group__boards__nucleo-f446ze.html#gadf387a613ea902458db462f67f43dfcb", null ],
    [ "UART_1_ISR", "group__boards__nucleo-f446ze.html#gaf9358264b5cbce69dddad098a8600aae", null ],
    [ "UART_2_DMA_ISR", "group__boards__nucleo-f446ze.html#gaeb88de936556ee08e00c78860eadfd0f", null ],
    [ "UART_2_ISR", "group__boards__nucleo-f446ze.html#gafdfcd079b784102f3ceb94a04d9c9250", null ],
    [ "UART_NUMOF", "group__boards__nucleo-f446ze.html#ga850405f2aaa352ad264346531f0e6230", null ],
    [ "i2c_config", "group__boards__nucleo-f446ze.html#gaa9dcbfbe7aa5baf027d834e5bca62a47", null ],
    [ "pwm_config", "group__boards__nucleo-f446ze.html#ga5177e7e865ac2ae63f7d56c8b3078707", null ],
    [ "spi_config", "group__boards__nucleo-f446ze.html#ga873188d7292e07499dcde9674b1e849c", null ],
    [ "timer_config", "group__boards__nucleo-f446ze.html#ga2dd41f782d2c67052e4dc7d37cef89b1", null ],
    [ "uart_config", "group__boards__nucleo-f446ze.html#ga1643cfc64589407fb96b4cbf908689a5", null ]
];