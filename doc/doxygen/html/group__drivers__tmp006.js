var group__drivers__tmp006 =
[
    [ "tmp006.h", "tmp006_8h.html", null ],
    [ "tmp006_regs.h", "tmp006__regs_8h.html", null ],
    [ "tmp006_params_t", "structtmp006__params__t.html", [
      [ "addr", "structtmp006__params__t.html#ace4dbe2a6f8a73449300f44159aa130e", null ],
      [ "i2c", "structtmp006__params__t.html#ab158494af46b8f8fb5396196a6a8e9cb", null ],
      [ "rate", "structtmp006__params__t.html#a7f9ee2924cbf29237ddd24585c8756d2", null ]
    ] ],
    [ "tmp006_t", "structtmp006__t.html", [
      [ "p", "structtmp006__t.html#a7e543dcf51765eff02d7f311f7245a54", null ]
    ] ],
    [ "TMP006_CCONST_A1", "group__drivers__tmp006.html#ga537c362b7ec307c6d0288a2a67af3b85", null ],
    [ "TMP006_CCONST_A2", "group__drivers__tmp006.html#ga91768fe2ef399634c1a3d4cb7b8c32b6", null ],
    [ "TMP006_CCONST_B0", "group__drivers__tmp006.html#gaffa981361b02d8d036407cb74e86892c", null ],
    [ "TMP006_CCONST_B1", "group__drivers__tmp006.html#ga02fdb9ccdef00fe0b0a62d98205c17a2", null ],
    [ "TMP006_CCONST_B2", "group__drivers__tmp006.html#ga2a9921b2bee9bad2701f3098a8e1466e", null ],
    [ "TMP006_CCONST_C2", "group__drivers__tmp006.html#ga362d966fa0e630e77a619e52d715d0b8", null ],
    [ "TMP006_CCONST_LSB_SIZE", "group__drivers__tmp006.html#ga3ba2f20e8e48e2788bd71f12caf189a0", null ],
    [ "TMP006_CCONST_S0", "group__drivers__tmp006.html#gab50cf68a5ecd186361aa95a9e2918e76", null ],
    [ "TMP006_CCONST_TREF", "group__drivers__tmp006.html#ga5d93acd7900016a9b344f1b9f14a1449", null ],
    [ "TMP006_CONFIG_CR_AS1", "group__drivers__tmp006.html#gac5b0396043dfd61e36314189803b7c0e", null ],
    [ "TMP006_CONFIG_CR_AS16", "group__drivers__tmp006.html#ga98290bd210761b83331fb596e8a18a35", null ],
    [ "TMP006_CONFIG_CR_AS2", "group__drivers__tmp006.html#ga5882962a58189aadcc1a0a84fd511758", null ],
    [ "TMP006_CONFIG_CR_AS4", "group__drivers__tmp006.html#ga82786c3e68dfab4fbb503a9253086ff1", null ],
    [ "TMP006_CONFIG_CR_AS8", "group__drivers__tmp006.html#ga8ebe7b983ab16fd5bc49599ed3818439", null ],
    [ "TMP006_CONFIG_CR_DEF", "group__drivers__tmp006.html#gaf9558acde66ebf0c8c95b4fb491908e2", null ],
    [ "TMP006_CONVERSION_TIME", "group__drivers__tmp006.html#ga3deeb9a2d264989ccbe6c3da6d2075d8", null ],
    [ "TMP006_I2C_ADDRESS", "group__drivers__tmp006.html#ga68eb21120307413be3e6688b01a52714", null ],
    [ "TMP006_USE_LOW_POWER", "group__drivers__tmp006.html#gabd099dc7c9c2f82394066b57120b39c8", null ],
    [ "TMP006_USE_RAW_VALUES", "group__drivers__tmp006.html#ga6a620a8d569f7ef247163bd516b5ff8d", [
      [ "TMP006_OK", "group__drivers__tmp006.html#gga6739788b75165c40c5fa298fbd9c18bdae6ee0ca6b4d1d27ca97595b2cf887aaa", null ],
      [ "TMP006_ERROR_BUS", "group__drivers__tmp006.html#gga6739788b75165c40c5fa298fbd9c18bda6293116a164555f9f0b7675960ee5917", null ],
      [ "TMP006_ERROR_DEV", "group__drivers__tmp006.html#gga6739788b75165c40c5fa298fbd9c18bdabdc5fb24cee42bc322908c1098c23021", null ],
      [ "TMP006_ERROR_CONF", "group__drivers__tmp006.html#gga6739788b75165c40c5fa298fbd9c18bda7a5ba3f7e051d1f510d32d1348437209", null ],
      [ "TMP006_ERROR", "group__drivers__tmp006.html#gga6739788b75165c40c5fa298fbd9c18bdaf41cf0cebde1294b9186a4c11c650201", null ]
    ] ],
    [ "tmp006_convert", "group__drivers__tmp006.html#ga4d1263c542b7110c1f54388e606a83fb", null ],
    [ "tmp006_init", "group__drivers__tmp006.html#gaf090e1dd80221391d8fcf40b696154b4", null ],
    [ "tmp006_read", "group__drivers__tmp006.html#gac33d3971f5e8aa2f6579df9bfd719d8d", null ],
    [ "tmp006_read_temperature", "group__drivers__tmp006.html#gaba352a746cac29f5494a25635f46debe", null ],
    [ "tmp006_reset", "group__drivers__tmp006.html#gad78b28e9d5512b9df67a2008611d6c06", null ],
    [ "tmp006_set_active", "group__drivers__tmp006.html#gacb5dee9ebe500edf37dd62ff36670247", null ],
    [ "tmp006_set_standby", "group__drivers__tmp006.html#ga142935a42aaae1829525238378ded568", null ]
];