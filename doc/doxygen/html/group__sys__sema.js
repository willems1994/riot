var group__sys__sema =
[
    [ "sema.h", "sema_8h.html", null ],
    [ "sema_t", "structsema__t.html", [
      [ "mutex", "structsema__t.html#ab75db51ac45ccea6f6794766d7a315ba", null ],
      [ "state", "structsema__t.html#ab874ea29817acba1427a12cbe01fc490", null ],
      [ "value", "structsema__t.html#add06f5e375e701eb2b79d7cf2bff0ea3", null ]
    ] ],
    [ "SEMA_CREATE", "group__sys__sema.html#ga645eadee6b055f1f42ecb04b24f9b566", null ],
    [ "SEMA_CREATE_LOCKED", "group__sys__sema.html#ga76d77845ad407609a3f945e88ff98a5c", null ],
    [ "sema_state_t", "group__sys__sema.html#ga3eb616a930f4a20204840b005c0593c2", null ],
    [ "_sema_wait", "group__sys__sema.html#gaf55a03fc006f25a5fe859daf26780ce5", null ],
    [ "sema_create", "group__sys__sema.html#ga06edca1366345c396aaf97fc91111788", null ],
    [ "sema_destroy", "group__sys__sema.html#ga29f1cc0af024caaa05ff2127e1543b2e", null ],
    [ "sema_post", "group__sys__sema.html#gac4f16d7add49ff68ab91045201d6cc98", null ],
    [ "sema_try_wait", "group__sys__sema.html#gac2909449a3c922433e84fe762ba68a56", null ],
    [ "sema_wait", "group__sys__sema.html#ga13b854f43a6b768b89cef120cdb1480b", null ],
    [ "sema_wait_timed", "group__sys__sema.html#gaa69089013308d2a98109052116296ede", null ]
];