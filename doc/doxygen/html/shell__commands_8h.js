var shell__commands_8h =
[
    [ "DISK_GET_BLOCK_SIZE", "group__sys__shell__commands.html#gab7b00645a78e8ea10e60cd80c5d222dc", null ],
    [ "DISK_GET_SECTOR_COUNT", "group__sys__shell__commands.html#gaddbef1c98b58d9f428a76a6f9d8cb51c", null ],
    [ "DISK_GET_SECTOR_SIZE", "group__sys__shell__commands.html#ga5385b8c497eb027cba701a2e1e7c0618", null ],
    [ "DISK_READ_BYTES_CMD", "group__sys__shell__commands.html#ga5276bfdc784192f787abae448d45dff3", null ],
    [ "DISK_READ_SECTOR_CMD", "group__sys__shell__commands.html#ga99d05b979e697b839ff84043a0f75df1", null ],
    [ "_shell_command_list", "group__sys__shell__commands.html#ga27944915543b06cbe26ea00e50f1492e", null ]
];