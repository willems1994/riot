var group__sys__shell =
[
    [ "shell.h", "shell_8h.html", null ],
    [ "shell_command_t", "structshell__command__t.html", [
      [ "desc", "structshell__command__t.html#ae42a6658af9f41c405936655f629510e", null ],
      [ "handler", "structshell__command__t.html#ab9e626b70c34603cf06cc0d82ee25e6b", null ],
      [ "name", "structshell__command__t.html#aca72232a2f1848131bd0238991478df3", null ]
    ] ],
    [ "SHELL_DEFAULT_BUFSIZE", "group__sys__shell.html#gac6ce9a12c394d101eb934286106ed2ee", null ],
    [ "shell_command_handler_t", "group__sys__shell.html#ga6cf265eee7d4bf93b186d6aaadd659a5", null ],
    [ "shell_command_t", "group__sys__shell.html#ga169f80d22f314444b0a8b8efa6bc5fb9", null ],
    [ "shell_run", "group__sys__shell.html#gaa9f533c4a192e32f0b59be0491f00bdf", null ]
];