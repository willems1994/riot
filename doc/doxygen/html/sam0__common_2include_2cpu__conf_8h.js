var sam0__common_2include_2cpu__conf_8h =
[
    [ "CPU_DEFAULT_IRQ_PRIO", "sam0__common_2include_2cpu__conf_8h.html#a811633719ff60ee247e64b333d4b8675", null ],
    [ "CPU_FLASH_BASE", "sam0__common_2include_2cpu__conf_8h.html#ad33eb792f7cf98b55b73fea8239c5f45", null ],
    [ "CPU_IRQ_NUMOF", "sam0__common_2include_2cpu__conf_8h.html#af6c13d219504576c5c69399033b0ae39", null ],
    [ "FLASHPAGE_NUMOF", "sam0__common_2include_2cpu__conf_8h.html#a7c969ee021b57668fd1965bbe9ca1a0a", null ],
    [ "FLASHPAGE_RAW_ALIGNMENT", "sam0__common_2include_2cpu__conf_8h.html#ac2d27fbc1ae5ae8cb86479626cd4a3a6", null ],
    [ "FLASHPAGE_RAW_BLOCKSIZE", "sam0__common_2include_2cpu__conf_8h.html#a782282047494538d13e2b3913bbc94f8", null ],
    [ "FLASHPAGE_SIZE", "sam0__common_2include_2cpu__conf_8h.html#afce96cb577e50c76434ba92363ca20e8", null ]
];