var group__sys__tsrb =
[
    [ "tsrb.h", "tsrb_8h.html", null ],
    [ "tsrb", "structtsrb.html", [
      [ "buf", "structtsrb.html#adaf786a21d5c789333314258be4c3ab0", null ],
      [ "reads", "structtsrb.html#a9b279d7f91ac38184d3cbe2ada2051d8", null ],
      [ "size", "structtsrb.html#af7c14acd3b2f78b2f0b30ee7038731a0", null ],
      [ "writes", "structtsrb.html#adcc1fcb5cf1a6911ac6bfbb5ba5ded2b", null ]
    ] ],
    [ "TSRB_INIT", "group__sys__tsrb.html#gacdfccc0e546ce7c53b1058f64a7ba6cc", null ],
    [ "tsrb_t", "group__sys__tsrb.html#gabedbf79d8e24fa03a34da67387fe67f5", null ],
    [ "tsrb_add", "group__sys__tsrb.html#ga6025084e490e583e67370192d9123396", null ],
    [ "tsrb_add_one", "group__sys__tsrb.html#ga2754ab41461487ec595855d5215dbd94", null ],
    [ "tsrb_avail", "group__sys__tsrb.html#ga84672d08611d40b66635e495a712e8b9", null ],
    [ "tsrb_drop", "group__sys__tsrb.html#ga43d290de08163c75841ac1d0ae040579", null ],
    [ "tsrb_empty", "group__sys__tsrb.html#gaff1ac63e9384bbaf8ac7cc7c6b031a44", null ],
    [ "tsrb_free", "group__sys__tsrb.html#ga78852ef6f10c21c849caf4e5e8a8bb5a", null ],
    [ "tsrb_full", "group__sys__tsrb.html#gaf7a98b968396a27cf0d1f6ca4d3bd24a", null ],
    [ "tsrb_get", "group__sys__tsrb.html#gaaca15758b529202af0c027e3225c9ce9", null ],
    [ "tsrb_get_one", "group__sys__tsrb.html#ga9433f8aa0248805b30691d26c4de6839", null ],
    [ "tsrb_init", "group__sys__tsrb.html#ga198732c767000a5e08b6d7de269466f8", null ]
];