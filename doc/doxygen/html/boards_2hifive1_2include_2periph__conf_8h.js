var boards_2hifive1_2include_2periph__conf_8h =
[
    [ "CLOCK_CORECLOCK", "boards_2hifive1_2include_2periph__conf_8h.html#afc465f12242e68f6c3695caa3ba0a169", null ],
    [ "GPIO_INTR_PRIORITY", "boards_2hifive1_2include_2periph__conf_8h.html#ad2e7681a6549b57ca930007848841a1d", null ],
    [ "PWM_NUMOF", "boards_2hifive1_2include_2periph__conf_8h.html#a44adbd579bb180f3cfe8ec78932eb7a1", null ],
    [ "RTC_NUMOF", "boards_2hifive1_2include_2periph__conf_8h.html#a4201ed1203952946a7b53934fcc5dad6", null ],
    [ "RTT_FREQUENCY", "boards_2hifive1_2include_2periph__conf_8h.html#afec7c948b8c70db3c9394fc3dc145a99", null ],
    [ "RTT_INTR_PRIORITY", "boards_2hifive1_2include_2periph__conf_8h.html#ac91b29bf80f58030decd7a89d9fa6e8d", null ],
    [ "RTT_MAX_VALUE", "boards_2hifive1_2include_2periph__conf_8h.html#a57f384110fe2e8f4b3c4b9ba246517c6", null ],
    [ "RTT_NUMOF", "boards_2hifive1_2include_2periph__conf_8h.html#ac5c886cfa6263655176d9883cb30f3ab", null ],
    [ "TIMER_NUMOF", "boards_2hifive1_2include_2periph__conf_8h.html#a6e2f66f5b6f5c835dd11f9766c4ed897", null ],
    [ "UART0_RX_INTR_PRIORITY", "boards_2hifive1_2include_2periph__conf_8h.html#a05d73ad2b41518916ba22eeb40227b69", null ],
    [ "UART1_RX_INTR_PRIORITY", "boards_2hifive1_2include_2periph__conf_8h.html#a30409c9ccc002d3190a0a60bc00915c8", null ],
    [ "UART_NUMOF", "boards_2hifive1_2include_2periph__conf_8h.html#a850405f2aaa352ad264346531f0e6230", null ],
    [ "XTIMER_CHAN", "boards_2hifive1_2include_2periph__conf_8h.html#a8b747b85d4d5f2e1be910cdbc72a01de", null ],
    [ "XTIMER_DEV", "boards_2hifive1_2include_2periph__conf_8h.html#a5e48bb301c732e044b08f336fb851d5e", null ],
    [ "XTIMER_HZ", "boards_2hifive1_2include_2periph__conf_8h.html#af68fde6b7d5b362834e6a8d382c6c0d7", null ],
    [ "XTIMER_WIDTH", "boards_2hifive1_2include_2periph__conf_8h.html#afea1be2406d45b8fbb1dca1a318ac2dc", null ]
];