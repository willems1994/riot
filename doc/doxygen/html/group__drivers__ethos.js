var group__drivers__ethos =
[
    [ "ethos.h", "ethos_8h.html", null ],
    [ "ethos_t", "structethos__t.html", [
      [ "framesize", "structethos__t.html#a65b6c9a33a2f4968c9a37a9c8b8f5546", null ],
      [ "frametype", "structethos__t.html#a5e87a24716d8235930ab211d10f1dec9", null ],
      [ "inbuf", "structethos__t.html#a126fafcdccea6410c086a9a4cb3af3dd", null ],
      [ "last_framesize", "structethos__t.html#a308e4b1796d69ccf5ee40f805d03eb4f", null ],
      [ "mac_addr", "structethos__t.html#a661bb3a792175f8ccbaa702557814841", null ],
      [ "netdev", "structethos__t.html#aa19e014d50b17153a37b4f569e850816", null ],
      [ "out_mutex", "structethos__t.html#a40cdc851c2db31ea04b3ce8e34bc9474", null ],
      [ "remote_mac_addr", "structethos__t.html#a60a4822f8d39e3d2008df73ca45fa297", null ],
      [ "state", "structethos__t.html#add056b1c4bc907d6012a5cd780397116", null ],
      [ "uart", "structethos__t.html#afd058f8f2efb04dec945d7d1f8b1177c", null ]
    ] ],
    [ "ethos_params_t", "structethos__params__t.html", [
      [ "baudrate", "structethos__params__t.html#a2774f92f32a5498f68bdd89775d3c431", null ],
      [ "buf", "structethos__params__t.html#abcb474777c542105c7531975a0f26842", null ],
      [ "bufsize", "structethos__params__t.html#a13105fcb2c843346d9f636f73d4925ec", null ],
      [ "uart", "structethos__params__t.html#a8374cf7f3a8c6264f0422d9b244ffe40", null ]
    ] ],
    [ "line_state_t", "group__drivers__ethos.html#ga55f4253a9885dff9bf3ab4e3131b5e45", null ],
    [ "ethos_send_frame", "group__drivers__ethos.html#gadb9bc46ab8a39cc605210402c12fa2b8", null ],
    [ "ethos_setup", "group__drivers__ethos.html#ga5e025d81f8e7863ca7e7d009f1a0eb07", null ]
];