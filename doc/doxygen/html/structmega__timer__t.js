var structmega__timer__t =
[
    [ "CNT", "structmega__timer__t.html#a15c537569384e867c010d461bd7f20f0", null ],
    [ "CRA", "structmega__timer__t.html#a7defb9f4137bf76f9bb35aef4269a55b", null ],
    [ "CRB", "structmega__timer__t.html#ab723c892816b294224aedd2f6c5d02f8", null ],
    [ "CRC", "structmega__timer__t.html#aa5575cb7cebf530c9487146f03f13392", null ],
    [ "ICR", "structmega__timer__t.html#ac6dd2820724cd5b6848abe5058a170f6", null ],
    [ "OCR", "structmega__timer__t.html#a6346c3487ea56907b83a93ebcb5e1302", null ],
    [ "reserved", "structmega__timer__t.html#a4dfbbef9c69121fb96fffc05db2b238c", null ]
];