var nhdp_8h =
[
    [ "ENABLE_DEBUG", "group__nhdp.html#ga432138093c53d7580af9ec5c5dca387f", null ],
    [ "MANET_PORT", "group__nhdp.html#ga6c8b9f2e5a0505b5223a2132c49fb004", null ],
    [ "NHDP_DEFAULT_HELLO_INT_MS", "group__nhdp.html#ga964ea5e125114f0afedf64247964d2f9", null ],
    [ "NHDP_DEFAULT_HOLD_TIME_MS", "group__nhdp.html#ga06c47a5caffbc1a8820b814e1c3b8283", null ],
    [ "NHDP_HP_MAXJITTER_MS", "group__nhdp.html#gab45172eb730a42984f9fec8c9c3bfcfb", null ],
    [ "NHDP_I_HOLD_TIME_MS", "group__nhdp.html#gaf049994e27bafd80efa33cda01119f3e", null ],
    [ "NHDP_INITIAL_PENDING", "group__nhdp.html#ga1b94818db3f61bf738fedf30dea84d58", null ],
    [ "NHDP_L_HOLD_TIME_MS", "group__nhdp.html#gafb87e4270f03e93f08b7594826bf8a41", null ],
    [ "NHDP_MAX_RFC5444_PACKET_SZ", "group__nhdp.html#ga93848cfb2f309557dd7f3a6104218acd", null ],
    [ "NHDP_MSG_QUEUE_SIZE", "group__nhdp.html#ga20d608875eeafed7beae1d8bc4f624d9", null ],
    [ "NHDP_N_HOLD_TIME_MS", "group__nhdp.html#ga302a676f2662b8891b1b71a628e6eb65", null ],
    [ "NHDP_STACK_SIZE", "group__nhdp.html#ga74d8c39ef0c166da64667aeaff5372fc", null ],
    [ "NHDP_WR_MSG_BUF_SIZE", "group__nhdp.html#ga062b252586d5c9da755679134c71a062", null ],
    [ "NHDP_WR_TLV_BUF_SIZE", "group__nhdp.html#gaf43428f4545b7871ea7c650971e1c97b", null ],
    [ "nhdp_address_type_t", "group__nhdp.html#gacb1446487edd24385defb5013e9fc93c", [
      [ "AF_CC110X", "group__nhdp.html#ggacb1446487edd24385defb5013e9fc93cac83d5a33c6d2cce6c0b852e613890654", null ]
    ] ],
    [ "nhdp_add_address", "group__nhdp.html#gae3d5f83d9edc8b0846eb00cfa3afb477", null ],
    [ "nhdp_init", "group__nhdp.html#gade8419dc2e8aed4c9246b8c729aca913", null ],
    [ "nhdp_register_if", "group__nhdp.html#ga34b67b18b806ace21dc87303ec4b9ab8", null ],
    [ "nhdp_register_if_default", "group__nhdp.html#ga99d258f640c224d284429dc289d11b8f", null ],
    [ "nhdp_register_non_manet_if", "group__nhdp.html#ga72d318af1652c146bf1d01c83150a4b4", null ],
    [ "nhdp_start", "group__nhdp.html#ga657c25174d9d2284e10c755576f22375", null ]
];