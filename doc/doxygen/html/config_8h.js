var config_8h =
[
    [ "GNRC_TCP_CONNECTION_TIMEOUT_DURATION", "group__net__gnrc__tcp.html#ga29fe90ebedfd775c3329158454824518", null ],
    [ "GNRC_TCP_DEFAULT_WINDOW", "group__net__gnrc__tcp.html#ga1dc817ad4b1f01bb3c13e6eb6b57a997", null ],
    [ "GNRC_TCP_MSL", "group__net__gnrc__tcp.html#gaea312503e0828362edcb13366ef41d38", null ],
    [ "GNRC_TCP_MSS", "group__net__gnrc__tcp.html#gaa7e536cb887b9abd216607f286dffd9c", null ],
    [ "GNRC_TCP_MSS_MULTIPLICATOR", "group__net__gnrc__tcp.html#gafe7a8ca6efccadfe0e564c2c12e84221", null ],
    [ "GNRC_TCP_PROBE_LOWER_BOUND", "group__net__gnrc__tcp.html#ga3abc405205651d95da7a663870527735", null ],
    [ "GNRC_TCP_PROBE_UPPER_BOUND", "group__net__gnrc__tcp.html#ga1d3caca610135c0fe63526eca28733ac", null ],
    [ "GNRC_TCP_RCV_BUF_SIZE", "group__net__gnrc__tcp.html#gad718860ca97c59cb129d0baf89220f18", null ],
    [ "GNRC_TCP_RCV_BUFFERS", "group__net__gnrc__tcp.html#ga6ed45c578803e903d616cfc9af13afa5", null ],
    [ "GNRC_TCP_RTO_A_DIV", "group__net__gnrc__tcp.html#gaee9d9c41d5d5bdf6e27098522e976f9e", null ],
    [ "GNRC_TCP_RTO_B_DIV", "group__net__gnrc__tcp.html#gaa810c1994167c49e29fd63c7cf899c60", null ],
    [ "GNRC_TCP_RTO_GRANULARITY", "group__net__gnrc__tcp.html#ga91e8504870ecfa53cea5d13e47d942ad", null ],
    [ "GNRC_TCP_RTO_K", "group__net__gnrc__tcp.html#ga77f79b83cad3cf4a416da0c487cc00f6", null ],
    [ "GNRC_TCP_RTO_LOWER_BOUND", "group__net__gnrc__tcp.html#ga8f6090d2714b7de5af52a77b3b2d01eb", null ],
    [ "GNRC_TCP_RTO_UPPER_BOUND", "group__net__gnrc__tcp.html#ga23a2c25d713fa6056ddcaccfa9ee065c", null ]
];