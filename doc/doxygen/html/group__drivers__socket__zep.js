var group__drivers__socket__zep =
[
    [ "socket_zep.h", "socket__zep_8h.html", null ],
    [ "socket_zep_t", "structsocket__zep__t.html", [
      [ "chksum_buf", "structsocket__zep__t.html#a4cde7e094c9f83cfade6bde5b4120ac4", null ],
      [ "last_event", "structsocket__zep__t.html#a9bf616333bbf184221bdc2565e10ca22", null ],
      [ "netdev", "structsocket__zep__t.html#a11e00a2d0e36c4a3e7bab9879692fbd7", null ],
      [ "rcv_buf", "structsocket__zep__t.html#adcdaf276e4a1715af243a19388c3c03c", null ],
      [ "seq", "structsocket__zep__t.html#a231112b98d3582ad28065d4f85d96ed6", null ],
      [ "snd_hdr_buf", "structsocket__zep__t.html#a52f310cf8a5d2bec15abf318b8254e19", null ],
      [ "sock_fd", "structsocket__zep__t.html#a6671ce84793e3ed58ba78e0302a11edd", null ]
    ] ],
    [ "socket_zep_params_t", "structsocket__zep__params__t.html", [
      [ "local_addr", "structsocket__zep__params__t.html#ab17f2388be84025473b4bbeb5f345efd", null ],
      [ "local_port", "structsocket__zep__params__t.html#a75d849a6f746123a777f48c50fb0e76c", null ],
      [ "remote_addr", "structsocket__zep__params__t.html#a2bc3e86c52ca867a6ffe9ab29450ec2e", null ],
      [ "remote_port", "structsocket__zep__params__t.html#a3e4d0b6e82fb3d02fc22d51e7350ecc7", null ]
    ] ],
    [ "SOCKET_ZEP_FRAME_PAYLOAD_LEN", "group__drivers__socket__zep.html#gab6cfda813e492b5c3a6c979d016fb8d4", null ],
    [ "socket_zep_cleanup", "group__drivers__socket__zep.html#ga65823b7cd584ca501e7c2c5cad1874ba", null ],
    [ "socket_zep_setup", "group__drivers__socket__zep.html#ga923938308545d6cf32ec9e2f9c7d24e0", null ]
];