var timex_8h =
[
    [ "CS_PER_SEC", "group__sys__timex.html#ga54bc7a7ddcef9c01170abc3cfd734daa", null ],
    [ "MS_PER_SEC", "group__sys__timex.html#ga98c842b52ffe344288b6e38b12417baa", null ],
    [ "NS_PER_US", "group__sys__timex.html#gad3fdd4302891bc3c724c798e033e3425", null ],
    [ "SEC_PER_MIN", "group__sys__timex.html#gac47b302f1b8d2a7a9c035c417247be76", null ],
    [ "TIMEX_MAX_STR_LEN", "group__sys__timex.html#ga4ac3f83d591b7b6dddb1dd7eec97b3c2", null ],
    [ "US_PER_MS", "group__sys__timex.html#ga31ae5fdfe8827f4d8def045948d3986e", null ],
    [ "US_PER_SEC", "group__sys__timex.html#ga63a377676c26adf4e7f641f98c78fc13", null ],
    [ "timex_add", "group__sys__timex.html#ga1e9d5cbb82fbe42e8c0f8b1c450ddd5e", null ],
    [ "timex_cmp", "group__sys__timex.html#ga4f39d89e25bb9ad8dc732d280cdcf34a", null ],
    [ "timex_from_uint64", "group__sys__timex.html#gaf4d7c63297ac6720ab3ca4490d9e5c8c", null ],
    [ "timex_isnormalized", "group__sys__timex.html#ga7728e254a400bc3b68c7550480f1f8c4", null ],
    [ "timex_normalize", "group__sys__timex.html#ga6ba2c35a0ce3eb5c834c99a927e68e84", null ],
    [ "timex_set", "group__sys__timex.html#gafef6baf60e2b381b6c17b2350276dbd2", null ],
    [ "timex_sub", "group__sys__timex.html#gaa86a4ecaf454d7369647327c9e3910cf", null ],
    [ "timex_to_str", "group__sys__timex.html#ga7597b2c5c463a64ab80e1a07d43a454f", null ],
    [ "timex_uint64", "group__sys__timex.html#gaba7ae3494893a929a99b6f59cc0f5d2b", null ]
];