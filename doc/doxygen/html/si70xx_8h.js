var si70xx_8h =
[
    [ "SI70XX_OK", "group__drivers__si70xx.html#gga8feb5a39a55448c86e0dfc20f53a6c1daff94a792ecda64c98b250450c6b6187d", null ],
    [ "SI70XX_ERR_NODEV", "group__drivers__si70xx.html#gga8feb5a39a55448c86e0dfc20f53a6c1da52ca5295c4a91bae14f2174295b59263", null ],
    [ "SI70XX_ERR_I2C", "group__drivers__si70xx.html#gga8feb5a39a55448c86e0dfc20f53a6c1dac992464a9264917af1a9244952ae0b75", null ],
    [ "si70xx_get_both", "group__drivers__si70xx.html#ga31261c190e8f388406be1a565c289166", null ],
    [ "si70xx_get_id", "group__drivers__si70xx.html#gaa7aeb4778005673b6654b8d99aa222ef", null ],
    [ "si70xx_get_relative_humidity", "group__drivers__si70xx.html#gaaaba79dd3a2dd359b4ff75e44e22663b", null ],
    [ "si70xx_get_revision", "group__drivers__si70xx.html#ga13402e5826cfda97ddb13f020adbedbf", null ],
    [ "si70xx_get_serial", "group__drivers__si70xx.html#gab1d5a8c57bd2da5b4a4f3d3222a60276", null ],
    [ "si70xx_get_temperature", "group__drivers__si70xx.html#ga65292eb1062e420ec52adf68bf3d586a", null ],
    [ "si70xx_init", "group__drivers__si70xx.html#gaa760c8b1bea2835c6a02a84fb0393f37", null ]
];