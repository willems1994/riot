var boards_2chronos_2include_2board_8h =
[
    [ "__CC430F6137__", "boards_2chronos_2include_2board_8h.html#a0c5b6c0026f5ddf49d8de06f5616df5a", null ],
    [ "F_CPU", "boards_2chronos_2include_2board_8h.html#a43bafb28b29491ec7f871319b5a3b2f8", null ],
    [ "F_RC_OSCILLATOR", "boards_2chronos_2include_2board_8h.html#ac842f33954353e7364121068e2805ff2", null ],
    [ "MSP430_HAS_DCOR", "boards_2chronos_2include_2board_8h.html#a3d9611258f0542c7933f06e3a407040c", null ],
    [ "MSP430_HAS_EXTERNAL_CRYSTAL", "boards_2chronos_2include_2board_8h.html#a3142e89bed881feff6b2eccaf3d0c3fc", null ],
    [ "MSP430_INITIAL_CPU_SPEED", "boards_2chronos_2include_2board_8h.html#a95ebe9dd4378d815f81e2609c69b0d5d", null ],
    [ "XTIMER_WIDTH", "boards_2chronos_2include_2board_8h.html#afea1be2406d45b8fbb1dca1a318ac2dc", null ]
];