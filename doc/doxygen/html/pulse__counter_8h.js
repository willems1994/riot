var pulse__counter_8h =
[
    [ "pulse_counter_init", "group__drivers__pulse__counter.html#ga54733af1995a24a49a6419ae7a6b7d7a", null ],
    [ "pulse_counter_read_with_reset", "group__drivers__pulse__counter.html#gadd8f5dab5fbf97293ff29b837afeb8f6", null ],
    [ "pulse_counter_read_without_reset", "group__drivers__pulse__counter.html#ga8d87506be159027aac70556c1dc50c63", null ],
    [ "pulse_counter_reset", "group__drivers__pulse__counter.html#gaaad1f05228696abcb6fad40528183d3b", null ]
];