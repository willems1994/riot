var irq_8h =
[
    [ "irq_disable", "group__core__irq.html#gae2c42997fe73f8d5cc4890c66955b21d", null ],
    [ "irq_enable", "group__core__irq.html#gac5652462dfaa82609d3f88f6b09519ae", null ],
    [ "irq_is_in", "group__core__irq.html#ga1694ef0b2f2e27ed79c84818d830495b", null ],
    [ "irq_restore", "group__core__irq.html#ga3c458468c67b69b914c7376d4cefeb67", null ]
];