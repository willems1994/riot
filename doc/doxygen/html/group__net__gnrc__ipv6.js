var group__net__gnrc__ipv6 =
[
    [ "ICMPv6", "group__net__gnrc__icmpv6.html", "group__net__gnrc__icmpv6" ],
    [ "IPv6 address blacklist", "group__net__gnrc__ipv6__blacklist.html", "group__net__gnrc__ipv6__blacklist" ],
    [ "IPv6 address whitelist", "group__net__gnrc__ipv6__whitelist.html", "group__net__gnrc__ipv6__whitelist" ],
    [ "IPv6 extension headers.", "group__net__gnrc__ipv6__ext.html", "group__net__gnrc__ipv6__ext" ],
    [ "IPv6 header defintions", "group__net__gnrc__ipv6__hdr.html", "group__net__gnrc__ipv6__hdr" ],
    [ "IPv6 neighbor discovery (v2)", "group__net__gnrc__ndp.html", "group__net__gnrc__ndp" ],
    [ "Neighbor Information Base for IPv6", "group__net__gnrc__ipv6__nib.html", "group__net__gnrc__ipv6__nib" ],
    [ "gnrc/ipv6.h", "gnrc_2ipv6_8h.html", null ],
    [ "GNRC_IPV6_MSG_QUEUE_SIZE", "group__net__gnrc__ipv6.html#ga3c24988d18687541689ef97671675844", null ],
    [ "GNRC_IPV6_PRIO", "group__net__gnrc__ipv6.html#ga8f22df988032dbd4e6684e63bcdd90e1", null ],
    [ "GNRC_IPV6_STACK_SIZE", "group__net__gnrc__ipv6.html#ga2326b53a03c6e2ddc9036bf020b11b99", null ],
    [ "GNRC_IPV6_STATIC_LLADDR", "group__net__gnrc__ipv6.html#gaf9a41f698d7e8d827fe35712e3f14b64", null ],
    [ "gnrc_ipv6_demux", "group__net__gnrc__ipv6.html#ga6ae9b4ce12a4c52d2ea153a8b0b67f4b", null ],
    [ "gnrc_ipv6_get_header", "group__net__gnrc__ipv6.html#gae22a5a0f301577af0e2e07d482446fc2", null ],
    [ "gnrc_ipv6_init", "group__net__gnrc__ipv6.html#ga3350f54d8afb8203f4ebdffa40c9b917", null ],
    [ "gnrc_ipv6_pid", "group__net__gnrc__ipv6.html#gaf0719b09dd3ad0ff774e142d05301f51", null ]
];