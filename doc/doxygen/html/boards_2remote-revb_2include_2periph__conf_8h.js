var boards_2remote_revb_2include_2periph__conf_8h =
[
    [ "ADC_NUMOF", "boards_2remote-revb_2include_2periph__conf_8h.html#a2f0c741db24aa2ccded869ba53f6a302", null ],
    [ "I2C_IRQ_PRIO", "boards_2remote-revb_2include_2periph__conf_8h.html#a7a0bc389843ed85946f608482ee17929", null ],
    [ "I2C_NUMOF", "boards_2remote-revb_2include_2periph__conf_8h.html#abce62e16a6e3b3205801fed93c51692d", null ],
    [ "SOC_ADC_ADCCON3_EREF", "boards_2remote-revb_2include_2periph__conf_8h.html#a8622110f4312bd12718d9969714501e8", null ],
    [ "SPI_NUMOF", "boards_2remote-revb_2include_2periph__conf_8h.html#ab35a2b79568128efef74adf1ba1910a8", null ],
    [ "adc_config", "boards_2remote-revb_2include_2periph__conf_8h.html#a160c7b1c3bc13c7cb5ac4ed375f4e21d", null ],
    [ "i2c_config", "boards_2remote-revb_2include_2periph__conf_8h.html#aa9dcbfbe7aa5baf027d834e5bca62a47", null ],
    [ "spi_config", "boards_2remote-revb_2include_2periph__conf_8h.html#a873188d7292e07499dcde9674b1e849c", null ]
];