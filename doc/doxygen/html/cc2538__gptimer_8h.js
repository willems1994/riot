var cc2538__gptimer_8h =
[
    [ "GPTIMER_ONE_SHOT_MODE", "group__cpu__cc2538__gptimer.html#ggabc5c98fcc1211af2b80116dd6e0a035da6e2ca3e065c80f374c2e4da3067d600c", null ],
    [ "GPTIMER_PERIODIC_MODE", "group__cpu__cc2538__gptimer.html#ggabc5c98fcc1211af2b80116dd6e0a035da7cd5053cfbd937714c6e67f14579cc1a", null ],
    [ "GPTIMER_CAPTURE_MODE", "group__cpu__cc2538__gptimer.html#ggabc5c98fcc1211af2b80116dd6e0a035da0d70b2c180ce88bed3760eee738b4e60", null ],
    [ "GPTMCFG_32_BIT_TIMER", "group__cpu__cc2538__gptimer.html#ggac36f475ca5b446f4fde4c9b90bec77c8a27a79d5177f0469357570728d5ce0175", null ],
    [ "GPTMCFG_32_BIT_REAL_TIME_CLOCK", "group__cpu__cc2538__gptimer.html#ggac36f475ca5b446f4fde4c9b90bec77c8a5828d7986df125ec3268cada1e8ef3d8", null ],
    [ "GPTMCFG_16_BIT_TIMER", "group__cpu__cc2538__gptimer.html#ggac36f475ca5b446f4fde4c9b90bec77c8a84db720f677023b4a575c633e8c3c072", null ]
];