var ina220_regs_8h =
[
    [ "ina220_reg_t", "ina220-regs_8h.html#ac54c253e27c5769bed489e180a430ac5", null ],
    [ "ina220_reg", "ina220-regs_8h.html#a711d5cabafb4d03df0070e9fa7786eca", [
      [ "INA220_REG_CONFIGURATION", "ina220-regs_8h.html#a711d5cabafb4d03df0070e9fa7786ecaa56136106b8eec5fc6e2246bc3ad57c82", null ],
      [ "INA220_REG_SHUNT_VOLTAGE", "ina220-regs_8h.html#a711d5cabafb4d03df0070e9fa7786ecaa625f67c0bc58ebf642f37ae45cc16ddc", null ],
      [ "INA220_REG_BUS_VOLTAGE", "ina220-regs_8h.html#a711d5cabafb4d03df0070e9fa7786ecaaf03eb06a77ffaf985df73c0b860855c1", null ],
      [ "INA220_REG_POWER", "ina220-regs_8h.html#a711d5cabafb4d03df0070e9fa7786ecaa974fb5ee84a85446f31338ce9bcebdb2", null ],
      [ "INA220_REG_CURRENT", "ina220-regs_8h.html#a711d5cabafb4d03df0070e9fa7786ecaa53c1cd3dbf2b1e002743d2955a345719", null ],
      [ "INA220_REG_CALIBRATION", "ina220-regs_8h.html#a711d5cabafb4d03df0070e9fa7786ecaafc6695e1bed7bbb9e899cc05ad659901", null ]
    ] ]
];