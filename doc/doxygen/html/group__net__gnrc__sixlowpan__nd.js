var group__net__gnrc__sixlowpan__nd =
[
    [ "Border router part of 6LoWPAN-ND", "group__net__gnrc__sixlowpan__nd__border__router.html", "group__net__gnrc__sixlowpan__nd__border__router" ],
    [ "gnrc/sixlowpan/nd.h", "gnrc_2sixlowpan_2nd_8h.html", null ],
    [ "GNRC_SIXLOWPAN_ND_AR_LTIME", "group__net__gnrc__sixlowpan__nd.html#gad338720b1dc04fcfa24e9d8aedbb038b", null ],
    [ "gnrc_sixlowpan_nd_opt_6ctx_build", "group__net__gnrc__sixlowpan__nd.html#ga146c8a8696fedf2c6e998a53ab76a7e8", null ],
    [ "gnrc_sixlowpan_nd_opt_abr_build", "group__net__gnrc__sixlowpan__nd.html#gacabb73149cb9d7a144d2e035d8d81141", null ],
    [ "gnrc_sixlowpan_nd_opt_ar_build", "group__net__gnrc__sixlowpan__nd.html#ga71fdb0bf9e367133538fdd8e9297e0d5", null ]
];