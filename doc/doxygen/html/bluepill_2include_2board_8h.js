var bluepill_2include_2board_8h =
[
    [ "LED0_MASK", "bluepill_2include_2board_8h.html#abfabde35a3e8b7ab50146a5e223bfead", null ],
    [ "LED0_OFF", "bluepill_2include_2board_8h.html#aef2e39b9fd343d0e437fac0463f3e02d", null ],
    [ "LED0_ON", "bluepill_2include_2board_8h.html#a1e48dc02333bfc2e6ad3e5bd466eecd4", null ],
    [ "LED0_PIN", "bluepill_2include_2board_8h.html#a3fc522fda449d6edde717d6a857429cf", null ],
    [ "LED0_PORT", "bluepill_2include_2board_8h.html#a76914453bb5cda4ebca204e091e8f55c", null ],
    [ "LED0_TOGGLE", "bluepill_2include_2board_8h.html#aebc6389533d9fc8dcbe4d2129a4d5a45", null ],
    [ "STDIO_UART_DEV", "bluepill_2include_2board_8h.html#a81935d479349dc2ce0a416bcb0e6beda", null ],
    [ "XTIMER_BACKOFF", "bluepill_2include_2board_8h.html#a370b9e9a079c4cb4f54fd947b67b9f41", null ],
    [ "XTIMER_WIDTH", "bluepill_2include_2board_8h.html#afea1be2406d45b8fbb1dca1a318ac2dc", null ],
    [ "board_init", "bluepill_2include_2board_8h.html#a916f2adc2080b4fe88034086d107a8dc", null ]
];