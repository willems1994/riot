var boards_2calliope_mini_2include_2board_8h =
[
    [ "BTN0_MODE", "boards_2calliope-mini_2include_2board_8h.html#a904884feea8e03c30c9a85ca6d7a8e5c", null ],
    [ "BTN0_PIN", "boards_2calliope-mini_2include_2board_8h.html#aab5c3eca54046333af52593b9e360270", null ],
    [ "BTN1_MODE", "boards_2calliope-mini_2include_2board_8h.html#ac792994d9b2d95b9761a05597ddea744", null ],
    [ "BTN1_PIN", "boards_2calliope-mini_2include_2board_8h.html#aeb7f5e51d2c7c61ec0a5d5c29591e9b3", null ],
    [ "MINI_LED_COL1", "boards_2calliope-mini_2include_2board_8h.html#a2db772fcaf0238887e5662bdc1ebd187", null ],
    [ "MINI_LED_COL2", "boards_2calliope-mini_2include_2board_8h.html#add0cb43816ad07ca1bc0fdc23efcad53", null ],
    [ "MINI_LED_COL3", "boards_2calliope-mini_2include_2board_8h.html#aa2f2437aaaff524db9bf32d0813a0ede", null ],
    [ "MINI_LED_COL4", "boards_2calliope-mini_2include_2board_8h.html#aed887379e45cf38f48c5ea27677748ba", null ],
    [ "MINI_LED_COL5", "boards_2calliope-mini_2include_2board_8h.html#a50f542bac3a4e6172399ba3e3ffa2609", null ],
    [ "MINI_LED_COL6", "boards_2calliope-mini_2include_2board_8h.html#ad95a54f80d5a42b5630739ea6669cb66", null ],
    [ "MINI_LED_COL7", "boards_2calliope-mini_2include_2board_8h.html#abecb64be52fae71bb327eb97e32d4341", null ],
    [ "MINI_LED_COL8", "boards_2calliope-mini_2include_2board_8h.html#a84afc75a8376ab5280b650af5b21eeff", null ],
    [ "MINI_LED_COL9", "boards_2calliope-mini_2include_2board_8h.html#a3778196ccfbe0390388f4ffc58e6c605", null ],
    [ "MINI_LED_ROW1", "boards_2calliope-mini_2include_2board_8h.html#a80b2d0d58b4fb7dfc7918aa2f44e8f5c", null ],
    [ "MINI_LED_ROW2", "boards_2calliope-mini_2include_2board_8h.html#ac8e7f80bd0cd416544b99c90e040f299", null ],
    [ "MINI_LED_ROW3", "boards_2calliope-mini_2include_2board_8h.html#af4fe3d4f546bb16c54c45538d11e821f", null ],
    [ "XTIMER_BACKOFF", "boards_2calliope-mini_2include_2board_8h.html#a370b9e9a079c4cb4f54fd947b67b9f41", null ],
    [ "XTIMER_WIDTH", "boards_2calliope-mini_2include_2board_8h.html#afea1be2406d45b8fbb1dca1a318ac2dc", null ],
    [ "board_init", "boards_2calliope-mini_2include_2board_8h.html#a916f2adc2080b4fe88034086d107a8dc", null ]
];