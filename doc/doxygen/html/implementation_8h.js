var implementation_8h =
[
    [ "MSG_XTIMER", "implementation_8h.html#af81d42d4ae766913ab8b622245cadeb7", null ],
    [ "XTIMER_MIN_SPIN", "implementation_8h.html#ae5f2a3ca927ab2ebafe3bbbdd350ee3d", null ],
    [ "_xtimer_lltimer_mask", "implementation_8h.html#a7e0261bfb3d19e1343fb62eb72a6b420", null ],
    [ "_xtimer_lltimer_now", "implementation_8h.html#aed3f6836c0e5831ae17854aa6377a61e", null ],
    [ "_xtimer_msg_receive_timeout", "implementation_8h.html#acbf6bd2f95faff610feecf3d258b9341", null ],
    [ "_xtimer_msg_receive_timeout64", "implementation_8h.html#a35a32ec007ee13aa8a6416ad3c42a25d", null ],
    [ "_xtimer_now64", "implementation_8h.html#a98d710faa381fce4f3804b54ac967ae5", null ],
    [ "_xtimer_periodic_wakeup", "implementation_8h.html#a8c7c25a33106253bc17b46fcc6865d35", null ],
    [ "_xtimer_set", "implementation_8h.html#a78f3ad70ff363d8deb4444e90b885d17", null ],
    [ "_xtimer_set64", "implementation_8h.html#a4806d8b428cd208fde0bbd4476df233a", null ],
    [ "_xtimer_set_absolute", "implementation_8h.html#a4382bd0dc3e67b9a834503c08b57b27c", null ],
    [ "_xtimer_set_msg", "implementation_8h.html#a79b896e567d59d58c8d9ce3c7e9e21d5", null ],
    [ "_xtimer_set_msg64", "implementation_8h.html#a037da692e3a4d5b22ced065ab8387207", null ],
    [ "_xtimer_set_wakeup", "implementation_8h.html#a0422979f7c736782098b62c73bb3b46c", null ],
    [ "_xtimer_set_wakeup64", "implementation_8h.html#a07d728c3730f1214ecb38224e3d2dd95", null ],
    [ "_xtimer_tsleep", "implementation_8h.html#ab3afc291e845821191d3171bba74a89d", null ]
];