var ikea_tradfri_2include_2board_8h =
[
    [ "LED0_OFF", "ikea-tradfri_2include_2board_8h.html#aef2e39b9fd343d0e437fac0463f3e02d", null ],
    [ "LED0_ON", "ikea-tradfri_2include_2board_8h.html#a1e48dc02333bfc2e6ad3e5bd466eecd4", null ],
    [ "LED0_PIN", "ikea-tradfri_2include_2board_8h.html#a3fc522fda449d6edde717d6a857429cf", null ],
    [ "LED0_TOGGLE", "ikea-tradfri_2include_2board_8h.html#aebc6389533d9fc8dcbe4d2129a4d5a45", null ],
    [ "LED1_OFF", "ikea-tradfri_2include_2board_8h.html#a343fd2d3ce61b84f88ddfaea32c67c58", null ],
    [ "LED1_ON", "ikea-tradfri_2include_2board_8h.html#aadd4c7ae0cd4e9bbb17f7055dd51fe08", null ],
    [ "LED1_PIN", "ikea-tradfri_2include_2board_8h.html#a318aa17e5d40e2132d2c7f6269ce7f51", null ],
    [ "LED1_TOGGLE", "ikea-tradfri_2include_2board_8h.html#a267fdbba1d750146b73da35c1731fd17", null ],
    [ "XTIMER_HZ", "ikea-tradfri_2include_2board_8h.html#af68fde6b7d5b362834e6a8d382c6c0d7", null ],
    [ "XTIMER_WIDTH", "ikea-tradfri_2include_2board_8h.html#afea1be2406d45b8fbb1dca1a318ac2dc", null ],
    [ "board_init", "ikea-tradfri_2include_2board_8h.html#a916f2adc2080b4fe88034086d107a8dc", null ]
];