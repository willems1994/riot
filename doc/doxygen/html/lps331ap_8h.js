var lps331ap_8h =
[
    [ "LPS331AP_DEFAULT_ADDRESS", "group__drivers__lps331ap.html#ga933445f4a8b7dcbf611393f7a7b9164e", null ],
    [ "lps331ap_rate_t", "group__drivers__lps331ap.html#ga53568b8059f4d358c859da78096e9660", [
      [ "LPS331AP_RATE_1HZ", "group__drivers__lps331ap.html#gga53568b8059f4d358c859da78096e9660aeef86718646d956b2d0c424c90689e80", null ],
      [ "LPS331AP_RATE_7HZ", "group__drivers__lps331ap.html#gga53568b8059f4d358c859da78096e9660aeefc224cd62c531499332d5972fd5953", null ],
      [ "LPS331AP_RATE_12HZ5", "group__drivers__lps331ap.html#gga53568b8059f4d358c859da78096e9660a1944a2c49987d6d213c9727708a0ca5f", null ],
      [ "LPS331AP_RATE_25HZ", "group__drivers__lps331ap.html#gga53568b8059f4d358c859da78096e9660ad77694b711b33e89fbf4b0d2408e2ccd", null ]
    ] ],
    [ "lps331ap_disable", "group__drivers__lps331ap.html#gaf07d580ff96df1bdc8aea4bd17131240", null ],
    [ "lps331ap_enable", "group__drivers__lps331ap.html#ga051c8b80df9683b1ee43686e0a732ffe", null ],
    [ "lps331ap_init", "group__drivers__lps331ap.html#ga8a73cfd81a1e6db8e4d0020de73e4944", null ],
    [ "lps331ap_read_pres", "group__drivers__lps331ap.html#ga5a7bede9ee2c117ba4c456ea2139a41c", null ],
    [ "lps331ap_read_temp", "group__drivers__lps331ap.html#ga91fe8fe1cbd81de865a096e6cf28e9a2", null ]
];