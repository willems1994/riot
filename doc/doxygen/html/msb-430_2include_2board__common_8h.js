var msb_430_2include_2board__common_8h =
[
    [ "INFOMEM", "group__boards__common__msb-430.html#gaf7c03cea4b136269f027b8782207ba38", null ],
    [ "LED0_MASK", "group__boards__common__msb-430.html#gabfabde35a3e8b7ab50146a5e223bfead", null ],
    [ "LED0_OFF", "group__boards__common__msb-430.html#gaef2e39b9fd343d0e437fac0463f3e02d", null ],
    [ "LED0_ON", "group__boards__common__msb-430.html#ga1e48dc02333bfc2e6ad3e5bd466eecd4", null ],
    [ "LED0_PIN", "group__boards__common__msb-430.html#ga3fc522fda449d6edde717d6a857429cf", null ],
    [ "LED0_TOGGLE", "group__boards__common__msb-430.html#gaebc6389533d9fc8dcbe4d2129a4d5a45", null ],
    [ "LED_OUT_REG", "group__boards__common__msb-430.html#ga7705441336d4aa7fb18dddcd5dc24d00", null ],
    [ "XTIMER_BACKOFF", "group__boards__common__msb-430.html#ga370b9e9a079c4cb4f54fd947b67b9f41", null ],
    [ "XTIMER_WIDTH", "group__boards__common__msb-430.html#gafea1be2406d45b8fbb1dca1a318ac2dc", null ]
];