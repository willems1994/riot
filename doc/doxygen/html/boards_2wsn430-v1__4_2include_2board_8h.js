var boards_2wsn430_v1__4_2include_2board_8h =
[
    [ "CC2420_PARAMS_PIN_CCA", "group__boards__wsn430-v1__4.html#ga36d58609c19c02fd72096669886c8069", null ],
    [ "CC2420_PARAMS_PIN_CS", "group__boards__wsn430-v1__4.html#ga0e0723332df2cd16dc6120c6576881c1", null ],
    [ "CC2420_PARAMS_PIN_FIFO", "group__boards__wsn430-v1__4.html#ga2da1cd17922036b21ad46448292c8e8c", null ],
    [ "CC2420_PARAMS_PIN_FIFOP", "group__boards__wsn430-v1__4.html#gae529b9fd8838641994b56950eaae95c2", null ],
    [ "CC2420_PARAMS_PIN_RESET", "group__boards__wsn430-v1__4.html#ga6f66e632e0fac96378528105f491f4c0", null ],
    [ "CC2420_PARAMS_PIN_SFD", "group__boards__wsn430-v1__4.html#ga2ab46cb5820f213d7da270062422ecc4", null ],
    [ "CC2420_PARAMS_PIN_VREFEN", "group__boards__wsn430-v1__4.html#ga44b40de13887e3db2c01b6b634238731", null ]
];