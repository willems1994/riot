var group__drivers__netdev__api =
[
    [ "802.15.4 radio drivers", "group__drivers__netdev__ieee802154.html", "group__drivers__netdev__ieee802154" ],
    [ "Ethernet drivers", "group__drivers__netdev__eth.html", "group__drivers__netdev__eth" ],
    [ "netdev BLE mode", "group__drivers__netdev__ble.html", "group__drivers__netdev__ble" ],
    [ "layer.h", "layer_8h.html", null ],
    [ "netdev.h", "netdev_8h.html", null ],
    [ "netdev_radio_rx_info", "structnetdev__radio__rx__info.html", [
      [ "lqi", "structnetdev__radio__rx__info.html#a526abb867e84092d098523a924d7dffc", null ],
      [ "rssi", "structnetdev__radio__rx__info.html#a90256333057acd53b5152f14bb6900be", null ]
    ] ],
    [ "netdev", "structnetdev.html", [
      [ "context", "structnetdev.html#aaacac29a8aa7891d7dc1882b5eb89773", null ],
      [ "driver", "structnetdev.html#a54bb633f6794c67d66685ecfe21aa3d2", null ],
      [ "event_callback", "structnetdev.html#af91155f0bff62279c2b4c12fcc909d16", null ]
    ] ],
    [ "netdev_driver", "structnetdev__driver.html", [
      [ "get", "structnetdev__driver.html#a0f7bbd084d7a51756fba665903884317", null ],
      [ "init", "structnetdev__driver.html#ae9c8aa7e8b6ef4b7b4acfcb0ae84209a", null ],
      [ "isr", "structnetdev__driver.html#aef446b76f05e6b5cfef52ab7460f3cd6", null ],
      [ "recv", "structnetdev__driver.html#ae2c8cad80067e3b1f9979931ddb3cc8b", null ],
      [ "send", "structnetdev__driver.html#a39585137953a92c1c58b5e0ad3262096", null ],
      [ "set", "structnetdev__driver.html#ad5c8c409276a702877ad50ad09e87a58", null ]
    ] ],
    [ "netdev_driver_t", "group__drivers__netdev__api.html#ga4cb47b21edc35315143091dfa97652e4", null ],
    [ "netdev_event_cb_t", "group__drivers__netdev__api.html#gad323355be0487ba68c16dea102740275", null ],
    [ "netdev_t", "group__drivers__netdev__api.html#ga14012f723b7591ad2fa42ace34601ac4", null ],
    [ "netdev_event_t", "group__drivers__netdev__api.html#gaef91a5201cb4a25d1c3ef41b783a395b", [
      [ "NETDEV_EVENT_ISR", "group__drivers__netdev__api.html#ggaef91a5201cb4a25d1c3ef41b783a395ba168994cf166acb43cd2a84d3d6ffb3b2", null ],
      [ "NETDEV_EVENT_RX_STARTED", "group__drivers__netdev__api.html#ggaef91a5201cb4a25d1c3ef41b783a395ba89aecdc8f6cad116c5cc960d000f9674", null ],
      [ "NETDEV_EVENT_RX_COMPLETE", "group__drivers__netdev__api.html#ggaef91a5201cb4a25d1c3ef41b783a395ba4cd3a85ba967f4d91beccf6afc76af00", null ],
      [ "NETDEV_EVENT_TX_STARTED", "group__drivers__netdev__api.html#ggaef91a5201cb4a25d1c3ef41b783a395ba0eb0b2e773afcfd02dd27e862df2d9dc", null ],
      [ "NETDEV_EVENT_TX_COMPLETE", "group__drivers__netdev__api.html#ggaef91a5201cb4a25d1c3ef41b783a395babc159414d6cb9e811df35258dc2d7262", null ],
      [ "NETDEV_EVENT_TX_COMPLETE_DATA_PENDING", "group__drivers__netdev__api.html#ggaef91a5201cb4a25d1c3ef41b783a395ba8fe79b16b45d236cbcf0e601c1ce61fd", null ],
      [ "NETDEV_EVENT_TX_NOACK", "group__drivers__netdev__api.html#ggaef91a5201cb4a25d1c3ef41b783a395ba3f0c14b10f18a82b0a57dbcfe98466a4", null ],
      [ "NETDEV_EVENT_TX_MEDIUM_BUSY", "group__drivers__netdev__api.html#ggaef91a5201cb4a25d1c3ef41b783a395ba668af8115ed5d90be0f30edc21b30f8a", null ],
      [ "NETDEV_EVENT_LINK_UP", "group__drivers__netdev__api.html#ggaef91a5201cb4a25d1c3ef41b783a395ba1770509acec86cdb1b65d04646c7f2b6", null ],
      [ "NETDEV_EVENT_LINK_DOWN", "group__drivers__netdev__api.html#ggaef91a5201cb4a25d1c3ef41b783a395bae146cd5423f6be146373fe286e8b1466", null ],
      [ "NETDEV_EVENT_TX_TIMEOUT", "group__drivers__netdev__api.html#ggaef91a5201cb4a25d1c3ef41b783a395ba775056d3ef94f184a2757fd31871c422", null ],
      [ "NETDEV_EVENT_RX_TIMEOUT", "group__drivers__netdev__api.html#ggaef91a5201cb4a25d1c3ef41b783a395bac2f0fa7a22b3cb312da6b7f0c773367b", null ],
      [ "NETDEV_EVENT_CRC_ERROR", "group__drivers__netdev__api.html#ggaef91a5201cb4a25d1c3ef41b783a395ba59ef2a7f5c5d6da023953b43a1b8c4a6", null ],
      [ "NETDEV_EVENT_FHSS_CHANGE_CHANNEL", "group__drivers__netdev__api.html#ggaef91a5201cb4a25d1c3ef41b783a395ba78ad65eb223df7bc239fa7b8802611c3", null ],
      [ "NETDEV_EVENT_CAD_DONE", "group__drivers__netdev__api.html#ggaef91a5201cb4a25d1c3ef41b783a395bafc1fd5cc76d7fb9a42eb45042dbdeca0", null ]
    ] ]
];