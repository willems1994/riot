var group__sys__memarray =
[
    [ "memarray_t", "structmemarray__t.html", [
      [ "free_data", "structmemarray__t.html#a0d79965bb7617fd2386faf0dcc48fa49", null ],
      [ "num", "structmemarray__t.html#a4bbec294884157c9b07fe23802133052", null ],
      [ "size", "structmemarray__t.html#a548499923fe28b111e8fb8fbe3aa66b2", null ]
    ] ],
    [ "memarray_alloc", "group__sys__memarray.html#ga955a7096484acba855812b622ff7c3bc", null ],
    [ "memarray_free", "group__sys__memarray.html#ga5d01c87550334a57fc783f7014e5e3d7", null ],
    [ "memarray_init", "group__sys__memarray.html#ga909c3b3b5ae072abb2bb538ead44128d", null ]
];