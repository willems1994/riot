var periph_8h =
[
    [ "saul_gpio_params_t", "structsaul__gpio__params__t.html", "structsaul__gpio__params__t" ],
    [ "saul_adc_params_t", "structsaul__adc__params__t.html", "structsaul__adc__params__t" ],
    [ "saul_gpio_flags_t", "periph_8h.html#a67c936922c075feb1c954a9da4e42410", [
      [ "SAUL_GPIO_INVERTED", "periph_8h.html#a67c936922c075feb1c954a9da4e42410ad464bd2c885625ccb8cbd12d3fce794e", null ],
      [ "SAUL_GPIO_INIT_CLEAR", "periph_8h.html#a67c936922c075feb1c954a9da4e42410a4f182cc8475d948dc49b35b9b6dccd34", null ],
      [ "SAUL_GPIO_INIT_SET", "periph_8h.html#a67c936922c075feb1c954a9da4e42410a983d0cd7eb0981f86677d828c2fb476a", null ]
    ] ]
];