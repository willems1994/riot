var common_2arduino_due_2include_2board_8h =
[
    [ "LED0_OFF", "group__boards__common__arduino__due.html#gaef2e39b9fd343d0e437fac0463f3e02d", null ],
    [ "LED0_ON", "group__boards__common__arduino__due.html#ga1e48dc02333bfc2e6ad3e5bd466eecd4", null ],
    [ "LED0_PIN", "group__boards__common__arduino__due.html#ga3fc522fda449d6edde717d6a857429cf", null ],
    [ "LED0_TOGGLE", "group__boards__common__arduino__due.html#gaebc6389533d9fc8dcbe4d2129a4d5a45", null ],
    [ "board_init", "group__boards__common__arduino__due.html#ga916f2adc2080b4fe88034086d107a8dc", null ]
];