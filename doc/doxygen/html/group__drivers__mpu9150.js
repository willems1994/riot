var group__drivers__mpu9150 =
[
    [ "mpu9150.h", "mpu9150_8h.html", null ],
    [ "mpu9150-regs.h", "mpu9150-regs_8h.html", null ],
    [ "mpu9150_results_t", "structmpu9150__results__t.html", [
      [ "x_axis", "structmpu9150__results__t.html#a74c6ffe244eed461d0417cec1b1a532b", null ],
      [ "y_axis", "structmpu9150__results__t.html#a7b0f0ebd651c5201c721400b688e20da", null ],
      [ "z_axis", "structmpu9150__results__t.html#a7c200266e930e15601d70af7c23849d7", null ]
    ] ],
    [ "mpu9150_status_t", "structmpu9150__status__t.html", [
      [ "accel_fsr", "structmpu9150__status__t.html#a0980109b62cc51d5c21aa8f2c11e4efd", null ],
      [ "accel_pwr", "structmpu9150__status__t.html#a387a950658575416221b3712e6812a6b", null ],
      [ "compass_pwr", "structmpu9150__status__t.html#a7a5e5f3a7cd00023f7392bafdec964e9", null ],
      [ "compass_sample_rate", "structmpu9150__status__t.html#acacd71649695ba7b53bb14a56b1a0885", null ],
      [ "compass_x_adj", "structmpu9150__status__t.html#a111af747e66d8b74e9054c1c3baa2ba0", null ],
      [ "compass_y_adj", "structmpu9150__status__t.html#aaada6637f943a99562b456d93102157d", null ],
      [ "compass_z_adj", "structmpu9150__status__t.html#ad2992988a2fca1bc859c6cac259f047a", null ],
      [ "gyro_fsr", "structmpu9150__status__t.html#a9fd45a2971360608e70d42c922313744", null ],
      [ "gyro_pwr", "structmpu9150__status__t.html#a2586413256f26656d162af2a4cb5e059", null ],
      [ "sample_rate", "structmpu9150__status__t.html#a03296b5e23206559087776877f41fb30", null ]
    ] ],
    [ "mpu9150_params_t", "structmpu9150__params__t.html", [
      [ "addr", "structmpu9150__params__t.html#a7c2d84e030e45df2c4c39910266221fc", null ],
      [ "comp_addr", "structmpu9150__params__t.html#a97749a90d4327f008e2c3288fc82e46d", null ],
      [ "i2c", "structmpu9150__params__t.html#ae14f61ddbe0ec8909c0f2d91a49bc904", null ],
      [ "sample_rate", "structmpu9150__params__t.html#a123ea339af89285646aa8df947999026", null ]
    ] ],
    [ "mpu9150_t", "structmpu9150__t.html", [
      [ "conf", "structmpu9150__t.html#aab1bc6f24e203ce74f218563d492d785", null ],
      [ "params", "structmpu9150__t.html#a343f352af10f5bf585606ee89beecf4d", null ]
    ] ],
    [ "mpu9150_accel_ranges_t", "group__drivers__mpu9150.html#ga3228c7e770a4be305737017cb4b3f980", null ],
    [ "mpu9150_comp_addr_t", "group__drivers__mpu9150.html#gafdab809031cd22c53300b381bb0207bf", null ],
    [ "mpu9150_gyro_ranges_t", "group__drivers__mpu9150.html#gaa126660dd8db65f72bb4249994f9bef6", null ],
    [ "mpu9150_hw_addr_t", "group__drivers__mpu9150.html#ga5d8b814ba537a1952c7e411831f77da3", null ],
    [ "mpu9150_lpf_t", "group__drivers__mpu9150.html#gae24e2383fe0ea85f30dd1b5e0bfd635a", null ],
    [ "mpu9150_pwr_t", "group__drivers__mpu9150.html#ga18617e03d41c9988a8b99e00ef00c2db", null ],
    [ "mpu9150_init", "group__drivers__mpu9150.html#ga3371054a3fea11f1ff21bd5aa83d5ee4", null ],
    [ "mpu9150_read_accel", "group__drivers__mpu9150.html#ga5f14cd279af09aad146784163c15d619", null ],
    [ "mpu9150_read_compass", "group__drivers__mpu9150.html#ga6622adce0784b030c8e0128b92203235", null ],
    [ "mpu9150_read_gyro", "group__drivers__mpu9150.html#gac4e00e5a9a00df1be43f0b685d8b1e3f", null ],
    [ "mpu9150_read_temperature", "group__drivers__mpu9150.html#gaaf275e3dbbf2ef6cfdfbf088c4d80c12", null ],
    [ "mpu9150_set_accel_fsr", "group__drivers__mpu9150.html#ga63e6b870737a5da617a3205a3c32d140", null ],
    [ "mpu9150_set_accel_power", "group__drivers__mpu9150.html#ga0926a4f72d87c8fdd1a8ae160b921659", null ],
    [ "mpu9150_set_compass_power", "group__drivers__mpu9150.html#ga045c51d7aee16b73cd25690c64da3c82", null ],
    [ "mpu9150_set_compass_sample_rate", "group__drivers__mpu9150.html#gacc20b320489bfb259ccfbb21c6ad0d0a", null ],
    [ "mpu9150_set_gyro_fsr", "group__drivers__mpu9150.html#ga5367266d93931d47db685961a03647e9", null ],
    [ "mpu9150_set_gyro_power", "group__drivers__mpu9150.html#ga8dc34d39ec7920398dcbf18fadf9acea", null ],
    [ "mpu9150_set_sample_rate", "group__drivers__mpu9150.html#ga70460ec1f5411afe5f1e170c44a2e0bc", null ]
];