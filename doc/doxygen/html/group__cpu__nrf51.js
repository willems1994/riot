var group__cpu__nrf51 =
[
    [ "nrf51/include/cpu_conf.h", "nrf51_2include_2cpu__conf_8h.html", null ],
    [ "nrf51/include/periph_cpu.h", "nrf51_2include_2periph__cpu_8h.html", null ],
    [ "CPU_DEFAULT_IRQ_PRIO", "group__cpu__nrf51.html#ga811633719ff60ee247e64b333d4b8675", null ],
    [ "FLASHPAGE_SIZE", "group__cpu__nrf51.html#gafce96cb577e50c76434ba92363ca20e8", null ],
    [ "GNRC_PKTBUF_SIZE", "group__cpu__nrf51.html#ga83a14b0588bb8adc93b565c4ebc5ec56", null ],
    [ "PWM_GPIOTE_CH", "group__cpu__nrf51.html#gad7beb462c4f6fc530157c5a4be0747ca", null ]
];