var group__net__netif =
[
    [ "netif.h", "netif_8h.html", null ],
    [ "netif_types.h", "netif__types_8h.html", null ],
    [ "NETIF_NAMELENMAX", "group__net__netif.html#ga3b975d610514bfd8f31bb49da894ecce", null ],
    [ "netif_get_by_name", "group__net__netif.html#gaa66f6d91a485ff5dfe37cab6d6d1dd9d", null ],
    [ "netif_get_name", "group__net__netif.html#ga3e1a19cc9bf3b43d5a489995c360cf26", null ],
    [ "netif_get_opt", "group__net__netif.html#gad7fad00d41d57c61d7b14db7800f94d7", null ],
    [ "netif_iter", "group__net__netif.html#ga60a511479a27e74a37447452fbd3e71c", null ],
    [ "netif_set_opt", "group__net__netif.html#gadbcec9b9fce0c0d0ac3d30c9e3042418", null ]
];