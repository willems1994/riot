var searchData=
[
  ['kinetis_5fclock_5fmcgirclk_5fen',['KINETIS_CLOCK_MCGIRCLK_EN',['../kinetis_2include_2periph__cpu_8h.html#a5a6ede8decd16d38c03c66c19963f141a0fd1970b3ccb4addc8be1608528372e7',1,'periph_cpu.h']]],
  ['kinetis_5fclock_5fmcgirclk_5fstop_5fen',['KINETIS_CLOCK_MCGIRCLK_STOP_EN',['../kinetis_2include_2periph__cpu_8h.html#a5a6ede8decd16d38c03c66c19963f141a21bea2177f3f1070c65e6914c6c6f9d6',1,'periph_cpu.h']]],
  ['kinetis_5fclock_5fosc0_5fen',['KINETIS_CLOCK_OSC0_EN',['../kinetis_2include_2periph__cpu_8h.html#a5a6ede8decd16d38c03c66c19963f141a4fef598820431439d77fd0c7f41b5bf6',1,'periph_cpu.h']]],
  ['kinetis_5fclock_5frtcosc_5fen',['KINETIS_CLOCK_RTCOSC_EN',['../kinetis_2include_2periph__cpu_8h.html#a5a6ede8decd16d38c03c66c19963f141accf3c4c58ef90df54ee316d78a123070',1,'periph_cpu.h']]],
  ['kinetis_5fclock_5fuse_5ffast_5firc',['KINETIS_CLOCK_USE_FAST_IRC',['../kinetis_2include_2periph__cpu_8h.html#a5a6ede8decd16d38c03c66c19963f141a07d1ff52d4067cb70a6913b1a1a2b933',1,'periph_cpu.h']]],
  ['kinetis_5flpuart',['KINETIS_LPUART',['../kinetis_2include_2periph__cpu_8h.html#a5df130ef5152041e30a95957a5e4ca64a8784937474269f0e7b8a3115c63925cc',1,'periph_cpu.h']]],
  ['kinetis_5fmcg_5ferc_5frange_5fhigh',['KINETIS_MCG_ERC_RANGE_HIGH',['../kinetis_2include_2periph__cpu_8h.html#ad76b9fe264273b921a2991c97261e86ea8c827b89841edabda430a93eef212f16',1,'periph_cpu.h']]],
  ['kinetis_5fmcg_5ferc_5frange_5flow',['KINETIS_MCG_ERC_RANGE_LOW',['../kinetis_2include_2periph__cpu_8h.html#ad76b9fe264273b921a2991c97261e86ea53f934def188a5098bc3233e3c5a0cff',1,'periph_cpu.h']]],
  ['kinetis_5fmcg_5ferc_5frange_5fvery_5fhigh',['KINETIS_MCG_ERC_RANGE_VERY_HIGH',['../kinetis_2include_2periph__cpu_8h.html#ad76b9fe264273b921a2991c97261e86ea1ae484f789a72e32a266022bfe79fdb9',1,'periph_cpu.h']]],
  ['kinetis_5fmcg_5ffll_5ffactor_5f1280',['KINETIS_MCG_FLL_FACTOR_1280',['../kinetis_2include_2periph__cpu_8h.html#ac3e51d9033dcb58cc37b1a4bb9e69557a365f0905266de485a5ac4472a89be8e8',1,'periph_cpu.h']]],
  ['kinetis_5fmcg_5ffll_5ffactor_5f1464',['KINETIS_MCG_FLL_FACTOR_1464',['../kinetis_2include_2periph__cpu_8h.html#ac3e51d9033dcb58cc37b1a4bb9e69557a06f810051ab6ea02d00026122cd0ae7c',1,'periph_cpu.h']]],
  ['kinetis_5fmcg_5ffll_5ffactor_5f1920',['KINETIS_MCG_FLL_FACTOR_1920',['../kinetis_2include_2periph__cpu_8h.html#ac3e51d9033dcb58cc37b1a4bb9e69557a081a63132b84fe10749bae2ca6105e73',1,'periph_cpu.h']]],
  ['kinetis_5fmcg_5ffll_5ffactor_5f2197',['KINETIS_MCG_FLL_FACTOR_2197',['../kinetis_2include_2periph__cpu_8h.html#ac3e51d9033dcb58cc37b1a4bb9e69557a11ed6e4f1a32601fc69311be0516ea09',1,'periph_cpu.h']]],
  ['kinetis_5fmcg_5ffll_5ffactor_5f2560',['KINETIS_MCG_FLL_FACTOR_2560',['../kinetis_2include_2periph__cpu_8h.html#ac3e51d9033dcb58cc37b1a4bb9e69557a2e3e5531edf8f3881374e883acee124c',1,'periph_cpu.h']]],
  ['kinetis_5fmcg_5ffll_5ffactor_5f2929',['KINETIS_MCG_FLL_FACTOR_2929',['../kinetis_2include_2periph__cpu_8h.html#ac3e51d9033dcb58cc37b1a4bb9e69557ab5f1f295b2312eb2ae601bae88bedf89',1,'periph_cpu.h']]],
  ['kinetis_5fmcg_5ffll_5ffactor_5f640',['KINETIS_MCG_FLL_FACTOR_640',['../kinetis_2include_2periph__cpu_8h.html#ac3e51d9033dcb58cc37b1a4bb9e69557adc550377c98e81493c124af2dfb6aa9a',1,'periph_cpu.h']]],
  ['kinetis_5fmcg_5ffll_5ffactor_5f732',['KINETIS_MCG_FLL_FACTOR_732',['../kinetis_2include_2periph__cpu_8h.html#ac3e51d9033dcb58cc37b1a4bb9e69557a69ca1bb73c75b1bdf0b26ea56a0adf18',1,'periph_cpu.h']]],
  ['kinetis_5fmcg_5fmode_5fblpe',['KINETIS_MCG_MODE_BLPE',['../kinetis_2include_2periph__cpu_8h.html#aefa7dcc2d6801b96454728846d81971bade6bbcd553699b6a7a5bbdb5a8d68e30',1,'periph_cpu.h']]],
  ['kinetis_5fmcg_5fmode_5fblpi',['KINETIS_MCG_MODE_BLPI',['../kinetis_2include_2periph__cpu_8h.html#aefa7dcc2d6801b96454728846d81971ba28d337200770ee1884d7b9e9c22021c7',1,'periph_cpu.h']]],
  ['kinetis_5fmcg_5fmode_5ffbe',['KINETIS_MCG_MODE_FBE',['../kinetis_2include_2periph__cpu_8h.html#aefa7dcc2d6801b96454728846d81971ba02acb64ab51f47a7bd149701eaa23652',1,'periph_cpu.h']]],
  ['kinetis_5fmcg_5fmode_5ffbi',['KINETIS_MCG_MODE_FBI',['../kinetis_2include_2periph__cpu_8h.html#aefa7dcc2d6801b96454728846d81971bacf4fae8358eb59b8b70d46fd3a63e089',1,'periph_cpu.h']]],
  ['kinetis_5fmcg_5fmode_5ffee',['KINETIS_MCG_MODE_FEE',['../kinetis_2include_2periph__cpu_8h.html#aefa7dcc2d6801b96454728846d81971baed07b410ef6592ee63941ff58b6f76ed',1,'periph_cpu.h']]],
  ['kinetis_5fmcg_5fmode_5ffei',['KINETIS_MCG_MODE_FEI',['../kinetis_2include_2periph__cpu_8h.html#aefa7dcc2d6801b96454728846d81971ba97220df4189d20fad5eda4b93ead2493',1,'periph_cpu.h']]],
  ['kinetis_5fmcg_5fmode_5fnumof',['KINETIS_MCG_MODE_NUMOF',['../kinetis_2include_2periph__cpu_8h.html#aefa7dcc2d6801b96454728846d81971bae2a99d76c0b47b00f91bdb3f610c8d2c',1,'periph_cpu.h']]],
  ['kinetis_5fmcg_5fmode_5fpbe',['KINETIS_MCG_MODE_PBE',['../kinetis_2include_2periph__cpu_8h.html#aefa7dcc2d6801b96454728846d81971ba527ed845be9e1794469b47134aec9295',1,'periph_cpu.h']]],
  ['kinetis_5fmcg_5fmode_5fpee',['KINETIS_MCG_MODE_PEE',['../kinetis_2include_2periph__cpu_8h.html#aefa7dcc2d6801b96454728846d81971ba3e454aad2fe8fec0526fea53ff91f0b6',1,'periph_cpu.h']]],
  ['kinetis_5fuart',['KINETIS_UART',['../kinetis_2include_2periph__cpu_8h.html#a5df130ef5152041e30a95957a5e4ca64ab8ed8126c384baba9b983d7864f0a704',1,'periph_cpu.h']]]
];
