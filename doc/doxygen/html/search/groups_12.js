var searchData=
[
  ['power_2dand_2dinterrupt_20driver',['Power-and-interrupt driver',['../group__boards__common__silabs__drivers__pic.html',1,'']]],
  ['phywave_2dkw22_20board',['phyWAVE-KW22 Board',['../group__boards__pba-d-01-kw2x.html',1,'']]],
  ['pcd8544_20lcd_20driver',['PCD8544 LCD driver',['../group__drivers__pcd8544.html',1,'']]],
  ['peripheral_20driver_20interface',['Peripheral Driver Interface',['../group__drivers__periph.html',1,'']]],
  ['power_20management',['Power Management',['../group__drivers__periph__pm.html',1,'']]],
  ['pwm',['PWM',['../group__drivers__periph__pwm.html',1,'']]],
  ['pir_20motion_20sensor',['PIR Motion Sensor',['../group__drivers__pir.html',1,'']]],
  ['pn532_20nfc_20reader',['PN532 NFC Reader',['../group__drivers__pn532.html',1,'']]],
  ['pulse_20counter',['Pulse counter',['../group__drivers__pulse__counter.html',1,'']]],
  ['prefix_20list',['Prefix list',['../group__net__gnrc__ipv6__nib__pl.html',1,'']]],
  ['protocol_20type',['Protocol type',['../group__net__gnrc__nettype.html',1,'']]],
  ['packet',['Packet',['../group__net__gnrc__pkt.html',1,'']]],
  ['packet_20buffer',['Packet buffer',['../group__net__gnrc__pktbuf.html',1,'']]],
  ['packet_20queue',['Packet queue',['../group__net__gnrc__pktqueue.html',1,'']]],
  ['priority_20packet_20queue_20for_20gnrc',['Priority packet queue for GNRC',['../group__net__gnrc__priority__pktqueue.html',1,'']]],
  ['packet_20statistics_20per_20module',['Packet statistics per module',['../group__net__netstats.html',1,'']]],
  ['packet_20statistics_20for_20rpl',['Packet statistics for RPL',['../group__net__netstats__rpl.html',1,'']]],
  ['packet_20interface_20on_20device_20level',['Packet interface on device level',['../group__net__packet.html',1,'']]],
  ['point_2dto_2dpoint_20protocol_20_28ppp_29_20data_20link_20layer',['Point-to-Point Protocol (PPP) Data Link Layer',['../group__net__ppp.html',1,'']]],
  ['point_2dto_2dpoint_20protocol_20header',['Point-to-Point Protocol Header',['../group__net__ppphdr.html',1,'']]],
  ['protocol_20numbers',['Protocol Numbers',['../group__net__protnum.html',1,'']]],
  ['packages',['Packages',['../group__pkg.html',1,'']]],
  ['posix_20wrapper_20for_20riot',['POSIX wrapper for RIOT',['../group__posix.html',1,'']]],
  ['posix_20semaphores',['POSIX semaphores',['../group__posix__semaphore.html',1,'']]],
  ['posix_20sockets',['POSIX sockets',['../group__posix__sockets.html',1,'']]],
  ['posix_20threads',['POSIX threads',['../group__pthread.html',1,'']]],
  ['puts_20log_20module',['puts log module',['../group__sys__log__printfnoformat.html',1,'']]],
  ['phydat',['Phydat',['../group__sys__phydat.html',1,'']]],
  ['pipe_20ipc',['Pipe IPC',['../group__sys__pipe.html',1,'']]],
  ['ps',['PS',['../group__sys__ps.html',1,'']]]
];
