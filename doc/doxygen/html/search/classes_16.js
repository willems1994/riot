var searchData=
[
  ['veml6070_5fparams_5ft',['veml6070_params_t',['../structveml6070__params__t.html',1,'']]],
  ['veml6070_5ft',['veml6070_t',['../structveml6070__t.html',1,'']]],
  ['vfs_5fdir',['vfs_DIR',['../structvfs__DIR.html',1,'']]],
  ['vfs_5fdir_5fops',['vfs_dir_ops',['../structvfs__dir__ops.html',1,'']]],
  ['vfs_5fdirent_5ft',['vfs_dirent_t',['../structvfs__dirent__t.html',1,'']]],
  ['vfs_5ffile_5fops',['vfs_file_ops',['../structvfs__file__ops.html',1,'']]],
  ['vfs_5ffile_5fsystem_5fops',['vfs_file_system_ops',['../structvfs__file__system__ops.html',1,'']]],
  ['vfs_5ffile_5fsystem_5ft',['vfs_file_system_t',['../structvfs__file__system__t.html',1,'']]],
  ['vfs_5ffile_5ft',['vfs_file_t',['../structvfs__file__t.html',1,'']]],
  ['vfs_5fmount_5fstruct',['vfs_mount_struct',['../structvfs__mount__struct.html',1,'']]],
  ['vims_5fregs_5ft',['vims_regs_t',['../structvims__regs__t.html',1,'']]]
];
