var searchData=
[
  ['sa_5ffamily_5ft',['sa_family_t',['../group__posix__sockets.html#ga27a82860cef19f4a53f68516e7b2ee0e',1,'socket.h']]],
  ['saul_5fread_5ft',['saul_read_t',['../group__drivers__saul.html#gac3882b034e8e751cfb95742a9100ecf2',1,'saul.h']]],
  ['saul_5freg_5ft',['saul_reg_t',['../group__sys__saul__reg.html#gace9930d1aa84c5856a998c4e68f3f791',1,'saul_reg.h']]],
  ['saul_5fwrite_5ft',['saul_write_t',['../group__drivers__saul.html#ga20135aa2a618d391a3cda5f89f307ab9',1,'saul.h']]],
  ['sem_5ft',['sem_t',['../group__posix__semaphore.html#ga634a03f9d2c7240d7cdecc9effa28e15',1,'semaphore.h']]],
  ['seq16_5ft',['seq16_t',['../seq_8h.html#a14673568b996d69ea3db7d1949a00369',1,'seq.h']]],
  ['seq32_5ft',['seq32_t',['../seq_8h.html#a6e046fe393488b105cc781f760ce428a',1,'seq.h']]],
  ['seq64_5ft',['seq64_t',['../seq_8h.html#aab2ab0e31ee5d318147fef8dc6b6cb76',1,'seq.h']]],
  ['seq8_5ft',['seq8_t',['../seq_8h.html#abfe669af3acf6763e654913d5afa3c22',1,'seq.h']]],
  ['shell_5fcommand_5fhandler_5ft',['shell_command_handler_t',['../group__sys__shell.html#ga6cf265eee7d4bf93b186d6aaadd659a5',1,'shell.h']]],
  ['shell_5fcommand_5ft',['shell_command_t',['../group__sys__shell.html#ga169f80d22f314444b0a8b8efa6bc5fb9',1,'shell.h']]],
  ['size_5ft',['size_t',['../msp430__types_8h.html#a7c94ea6f8948649f8d181ae55911eeaf',1,'msp430_types.h']]],
  ['sock_5fip_5ft',['sock_ip_t',['../group__net__sock__ip.html#ga439f68352d1339555536747a64b5232e',1,'ip.h']]],
  ['sock_5ftcp_5fep_5ft',['sock_tcp_ep_t',['../group__net__sock__tcp.html#ga882b5e7467eb5820579921241858e912',1,'tcp.h']]],
  ['sock_5ftcp_5fqueue_5ft',['sock_tcp_queue_t',['../group__net__sock__tcp.html#ga233763551e481f547f1cb07e7e31f981',1,'tcp.h']]],
  ['sock_5ftcp_5ft',['sock_tcp_t',['../group__net__sock__tcp.html#ga0144778bc074e291fcb0d43bd35c0179',1,'tcp.h']]],
  ['sock_5fudp_5fep_5ft',['sock_udp_ep_t',['../group__net__sock__udp.html#gaedc829c7973d7870c1ec078f9ffd45a1',1,'udp.h']]],
  ['sock_5fudp_5ft',['sock_udp_t',['../group__net__sock__udp.html#ga3cb61a4ee66c9c235e4f22860658698c',1,'udp.h']]],
  ['socklen_5ft',['socklen_t',['../bytes_8h.html#acf0ed818b0a3c85ff6a9206679d6d91a',1,'bytes.h']]],
  ['soft_5fspi_5fcs_5ft',['soft_spi_cs_t',['../group__drivers__soft__spi.html#ga6d93b1294961ea929e354f99b976d1c1',1,'soft_spi.h']]],
  ['soft_5fspi_5ft',['soft_spi_t',['../group__drivers__soft__spi.html#ga3673409c1743be03fbd6ad8ac8bdad2c',1,'soft_spi.h']]],
  ['spi_5fcs_5ft',['spi_cs_t',['../group__drivers__periph__spi.html#gaf5fbaf43946646c588c9372e8906c99e',1,'spi.h']]],
  ['spi_5ft',['spi_t',['../group__drivers__periph__spi.html#gac33db64063315b2b98b535051813ccea',1,'spi.h']]],
  ['spiffs_5fdesc_5ft',['spiffs_desc_t',['../group__sys__spiffs.html#gacb13b7b0bdecb7709499d1410cf37aa9',1,'spiffs_fs.h']]],
  ['ssize_5ft',['ssize_t',['../msp430__types_8h.html#af02a2e09430a9ed80bedb598467ce8d1',1,'msp430_types.h']]],
  ['suseconds_5ft',['suseconds_t',['../msp430__types_8h.html#af28078005859af6cd1418116b137ea5d',1,'msp430_types.h']]],
  ['sx127x_5fdio_5firq_5fhandler_5ft',['sx127x_dio_irq_handler_t',['../group__drivers__sx127x.html#ga5546e2d96cce3e0fb21f9beb0701c40a',1,'sx127x.h']]],
  ['sx127x_5fflags_5ft',['sx127x_flags_t',['../group__drivers__sx127x.html#ga9346021be4d9ac0dfd27f3307e19509f',1,'sx127x.h']]]
];
