var searchData=
[
  ['re_2dmote_20prototype_20a',['Re-Mote Prototype A',['../group__boards__remote-pa.html',1,'']]],
  ['re_2dmote_20revision_20a',['RE-Mote Revision A',['../group__boards__remote-reva.html',1,'']]],
  ['re_2dmote_20revision_20b',['RE-Mote Revision B',['../group__boards__remote-revb.html',1,'']]],
  ['ruuvitag',['RuuviTag',['../group__boards__ruuvitag.html',1,'']]],
  ['rtc',['RTC',['../group__drivers__periph__rtc.html',1,'']]],
  ['rtt',['RTT',['../group__drivers__periph__rtt.html',1,'']]],
  ['rgb_2dled_20driver',['RGB-LED driver',['../group__drivers__rgbled.html',1,'']]],
  ['rn2483_2frn2903_20lora_20module_20driver',['RN2483/RN2903 LoRa module driver',['../group__drivers__rn2xx3.html',1,'']]],
  ['rpl',['RPL',['../group__net__gnrc__rpl.html',1,'']]],
  ['reactive_20discovery_20of_20p2p_20routes_20in_20llns',['Reactive Discovery of P2P Routes in LLNs',['../group__net__gnrc__rpl__p2p.html',1,'']]],
  ['rpl_20source_20routing_20header_20extension',['RPL source routing header extension',['../group__net__gnrc__rpl__srh.html',1,'']]],
  ['raw_20ipv4_2fipv6_20sock_20api',['Raw IPv4/IPv6 sock API',['../group__net__sock__ip.html',1,'']]],
  ['relic_20toolkit_20for_20riot',['Relic toolkit for RIOT',['../group__pkg__relic.html',1,'']]],
  ['rotating',['Rotating',['../group__sys__hashes__rotating.html',1,'']]],
  ['random',['Random',['../group__sys__random.html',1,'']]],
  ['rfc_204122_20compliant_20uuid_27s',['RFC 4122 compliant UUID&apos;s',['../group__sys__uuid.html',1,'']]]
];
