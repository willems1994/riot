var searchData=
[
  ['boards',['Boards',['../group__boards.html',1,'']]],
  ['black_20pill',['Black pill',['../group__boards__blackpill.html',1,'']]],
  ['bluepill_20board',['Bluepill board',['../group__boards__bluepill.html',1,'']]],
  ['board_20communication_20controller',['Board communication controller',['../group__boards__common__silabs__drivers__bc.html',1,'']]],
  ['bbc_20micro_3abit',['BBC micro:bit',['../group__boards__microbit.html',1,'']]],
  ['bh1750fvi_20light_20sensor',['BH1750FVI Light Sensor',['../group__drivers__bh1750fvi.html',1,'']]],
  ['bmp180_20temperature_20and_20pressure_20sensor',['BMP180 temperature and pressure sensor',['../group__drivers__bmp180.html',1,'']]],
  ['bmx055_209_2daxis_20sensor',['BMX055 9-axis sensor',['../group__drivers__bmx055.html',1,'']]],
  ['bmp280_2fbme280_20temperature_2c_20pressure_20and_20humidity_20sensor',['BMP280/BME280 temperature, pressure and humidity sensor',['../group__drivers__bmx280.html',1,'']]],
  ['ble_20defines',['BLE defines',['../group__net__ble.html',1,'']]],
  ['border_20router_20part_20of_206lowpan_2dnd',['Border router part of 6LoWPAN-ND',['../group__net__gnrc__sixlowpan__nd__border__router.html',1,'']]],
  ['bluetooth_20low_20energy_20drivers',['Bluetooth Low Energy drivers',['../group__nrf52832-ble.html',1,'']]],
  ['base64_20encoder_20decoder',['base64 encoder decoder',['../group__sys__base64.html',1,'']]],
  ['binary_20coded_20decimal',['Binary coded decimal',['../group__sys__bcd.html',1,'']]],
  ['benchmark',['Benchmark',['../group__sys__benchmark.html',1,'']]],
  ['bitfields',['Bitfields',['../group__sys__bitfield.html',1,'']]],
  ['bloom_20filter',['Bloom filter',['../group__sys__bloom.html',1,'']]],
  ['bernstein_20hash_20djb2',['Bernstein hash djb2',['../group__sys__hashes__djb2.html',1,'']]]
];
