var searchData=
[
  ['grove_20ledbar',['Grove ledbar',['../group__drivers__grove__ledbar.html',1,'']]],
  ['gnrc_20adapter_20for_20nrfmin',['GNRC adapter for nrfmin',['../group__drivers__nrf5x__nrfmin__gnrc.html',1,'']]],
  ['gpio',['GPIO',['../group__drivers__periph__gpio.html',1,'']]],
  ['generic_20_28gnrc_29_20network_20stack',['Generic (GNRC) network stack',['../group__net__gnrc.html',1,'']]],
  ['gomach',['GoMacH',['../group__net__gnrc__gomach.html',1,'']]],
  ['gnrc_20communication_20interface',['GNRC communication interface',['../group__net__gnrc__netapi.html',1,'']]],
  ['generic_20network_20interface_20header',['Generic network interface header',['../group__net__gnrc__netif__hdr.html',1,'']]],
  ['gnrc_2dspecific_20implementation_20of_20the_20sock_20api',['GNRC-specific implementation of the sock API',['../group__net__gnrc__sock.html',1,'']]],
  ['gps_20parser_20library',['GPS parser library',['../group__pkg__minmea.html',1,'']]],
  ['gnrc_20netif_20drivers_20auto_2dinitialization',['GNRC netif drivers auto-initialization',['../group__sys__auto__init__gnrc__netif.html',1,'']]]
];
