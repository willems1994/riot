var searchData=
[
  ['saul_5fgpio_5fflags_5ft',['saul_gpio_flags_t',['../periph_8h.html#a67c936922c075feb1c954a9da4e42410',1,'periph.h']]],
  ['sd_5frw_5fresponse_5ft',['sd_rw_response_t',['../group__drivers__sdcard__spi.html#ga80c23a4db4f9950391ae7718a49a5172',1,'sdcard_spi.h']]],
  ['sd_5fversion_5ft',['sd_version_t',['../group__drivers__sdcard__spi.html#ga0fc206d1dcbc265c9672c6ec0b24ee14',1,'sdcard_spi.h']]],
  ['sema_5fstate_5ft',['sema_state_t',['../group__sys__sema.html#ga3eb616a930f4a20204840b005c0593c2',1,'sema.h']]],
  ['serialformat',['SerialFormat',['../serialport_8hpp.html#a0b7d38b35e62513d1ad5b1529a788b52',1,'serialport.hpp']]],
  ['sht1x_5fconf_5ft',['sht1x_conf_t',['../group__drivers__sht1x.html#gac676807c37fb5930cca9089950f5aac5',1,'sht1x.h']]],
  ['sht1x_5fvdd_5ft',['sht1x_vdd_t',['../group__drivers__sht1x.html#gad2fe11cd213e7957f8ec9356d3fbeb61',1,'sht1x.h']]],
  ['si114x_5fled_5fcurrent_5ft',['si114x_led_current_t',['../group__drivers__si114x.html#ga67bcb332da5eaf760068d925b4706d68',1,'si114x.h']]],
  ['si114x_5fret_5fcode_5ft',['si114x_ret_code_t',['../group__drivers__si114x.html#ga33b32493572c95dd647d0bdf0b7da5c4',1,'si114x.h']]],
  ['soft_5fspi_5fclk_5ft',['soft_spi_clk_t',['../group__drivers__soft__spi.html#gab891c834ce3f8f77ce8daacef7a2498e',1,'soft_spi.h']]],
  ['soft_5fspi_5fmode_5ft',['soft_spi_mode_t',['../group__drivers__soft__spi.html#ga0067f31877b2a3787c4de2a0413bac65',1,'soft_spi.h']]],
  ['spi_5fclk_5ft',['spi_clk_t',['../atmega__common_2include_2periph__cpu__common_8h.html#ae81cec9f03084065c25089e514a57337',1,'spi_clk_t():&#160;periph_cpu_common.h'],['../cc2538_2include_2periph__cpu_8h.html#ae81cec9f03084065c25089e514a57337',1,'spi_clk_t():&#160;periph_cpu.h'],['../lm4f120_2include_2periph__cpu_8h.html#ae81cec9f03084065c25089e514a57337',1,'spi_clk_t():&#160;periph_cpu.h'],['../lpc2387_2include_2periph__cpu_8h.html#ae81cec9f03084065c25089e514a57337',1,'spi_clk_t():&#160;periph_cpu.h'],['../nrf5x__common_2include_2periph__cpu__common_8h.html#ae81cec9f03084065c25089e514a57337',1,'spi_clk_t():&#160;periph_cpu_common.h'],['../sam0__common_2include_2periph__cpu__common_8h.html#ae81cec9f03084065c25089e514a57337',1,'spi_clk_t():&#160;periph_cpu_common.h'],['../sam3_2include_2periph__cpu_8h.html#ae81cec9f03084065c25089e514a57337',1,'spi_clk_t():&#160;periph_cpu.h'],['../group__drivers__periph__spi.html#gae81cec9f03084065c25089e514a57337',1,'spi_clk_t():&#160;spi.h']]],
  ['spi_5fmisopad_5ft',['spi_misopad_t',['../sam0__common_2include_2periph__cpu__common_8h.html#a5f3aefc5ac5917dd4a5c71ca3b624b29',1,'periph_cpu_common.h']]],
  ['spi_5fmode_5ft',['spi_mode_t',['../atmega__common_2include_2periph__cpu__common_8h.html#ac4b206a51636d91c5cffcbcee458c3cb',1,'spi_mode_t():&#160;periph_cpu_common.h'],['../cc2538_2include_2periph__cpu_8h.html#ac4b206a51636d91c5cffcbcee458c3cb',1,'spi_mode_t():&#160;periph_cpu.h'],['../lm4f120_2include_2periph__cpu_8h.html#ac4b206a51636d91c5cffcbcee458c3cb',1,'spi_mode_t():&#160;periph_cpu.h'],['../nrf5x__common_2include_2periph__cpu__common_8h.html#ac4b206a51636d91c5cffcbcee458c3cb',1,'spi_mode_t():&#160;periph_cpu_common.h'],['../sam0__common_2include_2periph__cpu__common_8h.html#ac4b206a51636d91c5cffcbcee458c3cb',1,'spi_mode_t():&#160;periph_cpu_common.h'],['../sam3_2include_2periph__cpu_8h.html#ac4b206a51636d91c5cffcbcee458c3cb',1,'spi_mode_t():&#160;periph_cpu.h'],['../group__drivers__periph__spi.html#gac4b206a51636d91c5cffcbcee458c3cb',1,'spi_mode_t():&#160;spi.h']]],
  ['spi_5fmosipad_5ft',['spi_mosipad_t',['../sam0__common_2include_2periph__cpu__common_8h.html#aecd093a68bc8af5fd8df392bc9e7d598',1,'periph_cpu_common.h']]],
  ['srf02_5fmode_5ft',['srf02_mode_t',['../group__drivers__srf02.html#ga057812ccdb4358a6b6c69cc73a8c7207',1,'srf02.h']]],
  ['srf08_5fmode_5ft',['srf08_mode_t',['../group__drivers__srf08.html#gad50064fa217e2d965e11cf4ae10bf070',1,'srf08.h']]]
];
