var searchData=
[
  ['dac_5fnumof',['DAC_NUMOF',['../sam3_2include_2periph__cpu_8h.html#a2d6ae6694d0a51952fb26d994de93d12',1,'periph_cpu.h']]],
  ['dat_5fconstant',['DAT_CONSTANT',['../nhdp__metric_8h.html#a9a76ecfdc59a24ae3f41defad5cebcf2',1,'nhdp_metric.h']]],
  ['dat_5fhello_5ftimeout_5ffactor',['DAT_HELLO_TIMEOUT_FACTOR',['../nhdp__metric_8h.html#a83fb741709f53d17efc9091e33be744c',1,'nhdp_metric.h']]],
  ['dat_5fmaximum_5floss',['DAT_MAXIMUM_LOSS',['../nhdp__metric_8h.html#aa9159f77b06078e04a83864934026097',1,'nhdp_metric.h']]],
  ['dat_5fmemory_5flength',['DAT_MEMORY_LENGTH',['../nhdp__metric_8h.html#aa4cedad48287b417986cec1012394ab1',1,'nhdp_metric.h']]],
  ['dat_5fminimum_5fbitrate',['DAT_MINIMUM_BITRATE',['../nhdp__metric_8h.html#a7bb07f3e752dbb228c4ab61e2afa05b8',1,'nhdp_metric.h']]],
  ['dat_5frefresh_5finterval',['DAT_REFRESH_INTERVAL',['../nhdp__metric_8h.html#ab9b5853bc29917156a554c3d70c5a9bc',1,'nhdp_metric.h']]],
  ['ddi_5f0_5fosc',['DDI_0_OSC',['../cc26x0__prcm_8h.html#a3a79f997a72da9fb5b6acc7265db515d',1,'cc26x0_prcm.h']]],
  ['ddi_5f0_5fosc_5fctl0_5fsclk_5fhf_5fsrc_5fsel_5frcosc',['DDI_0_OSC_CTL0_SCLK_HF_SRC_SEL_RCOSC',['../cc26x0__prcm_8h.html#a2131fa39eaadc8ff91d8df61b71e8a17',1,'cc26x0_prcm.h']]],
  ['delay_5fhmode',['DELAY_HMODE',['../bh1750fvi__internal_8h.html#a72f2116781e0a2066243c1648beca47e',1,'bh1750fvi_internal.h']]],
  ['delay_5flmode',['DELAY_LMODE',['../bh1750fvi__internal_8h.html#a2dd86ba1165e42eb0b99d45ec4f9c403',1,'bh1750fvi_internal.h']]],
  ['disable_5fwdog',['DISABLE_WDOG',['../mulle_2include_2board_8h.html#a93c87c5f0e67ed3de661632f8b54d803',1,'DISABLE_WDOG():&#160;board.h'],['../teensy31_2include_2board_8h.html#a93c87c5f0e67ed3de661632f8b54d803',1,'DISABLE_WDOG():&#160;board.h']]],
  ['div_5fh_5finv_5f15625_5f32',['DIV_H_INV_15625_32',['../div_8h.html#aa582d82bfc335a1472e82066337fdbab',1,'div.h']]],
  ['div_5fh_5finv_5f15625_5f64',['DIV_H_INV_15625_64',['../div_8h.html#aa06fcc99e39d277ba341277596fc372c',1,'div.h']]],
  ['div_5fh_5finv_5f15625_5fshift',['DIV_H_INV_15625_SHIFT',['../div_8h.html#ae70d0ba5db3a7b5d438068b53789d7f7',1,'div.h']]],
  ['ds1307_5fdow_5foffset',['DS1307_DOW_OFFSET',['../ds1307__internal_8h.html#a92a7172be068740b565e126ce4309160',1,'ds1307_internal.h']]],
  ['ds1307_5fmon_5foffset',['DS1307_MON_OFFSET',['../ds1307__internal_8h.html#a453e36a7a7ebc7314b23da45d96f2975',1,'ds1307_internal.h']]],
  ['ds1307_5freg_5fdom',['DS1307_REG_DOM',['../ds1307__internal_8h.html#a98ab0d27cac4971a07eee0433b2b2ed3',1,'ds1307_internal.h']]],
  ['ds1307_5freg_5fdom_5fmask',['DS1307_REG_DOM_MASK',['../ds1307__internal_8h.html#a848ce30d3e0351ba74a65ddf11bf243c',1,'ds1307_internal.h']]],
  ['ds1307_5freg_5fdow',['DS1307_REG_DOW',['../ds1307__internal_8h.html#a299dfffcd2f7070ca4fb1fecd2ee8b60',1,'ds1307_internal.h']]],
  ['ds1307_5freg_5fdow_5fmask',['DS1307_REG_DOW_MASK',['../ds1307__internal_8h.html#ad06c57d64b1bb9ed8f15f283821cf595',1,'ds1307_internal.h']]],
  ['ds1307_5freg_5fhour',['DS1307_REG_HOUR',['../ds1307__internal_8h.html#a287744f29c1140d06d855fb640e017bd',1,'ds1307_internal.h']]],
  ['ds1307_5freg_5fhour_5f12h',['DS1307_REG_HOUR_12H',['../ds1307__internal_8h.html#a18f0dd7aac0394d2c4e4ebef1ea5e428',1,'ds1307_internal.h']]],
  ['ds1307_5freg_5fhour_5f12h_5fmask',['DS1307_REG_HOUR_12H_MASK',['../ds1307__internal_8h.html#ae97bd29f96795a73aadc87130cc72a63',1,'ds1307_internal.h']]],
  ['ds1307_5freg_5fhour_5f24h_5fmask',['DS1307_REG_HOUR_24H_MASK',['../ds1307__internal_8h.html#a48fd0154cc57e2c32cf034f5622dec97',1,'ds1307_internal.h']]],
  ['ds1307_5freg_5fhour_5fpm',['DS1307_REG_HOUR_PM',['../ds1307__internal_8h.html#a670f3a669d727239365b7edeb689e935',1,'ds1307_internal.h']]],
  ['ds1307_5freg_5fmin',['DS1307_REG_MIN',['../ds1307__internal_8h.html#a93b075c88ca0a1b8913a14e7cd13873b',1,'ds1307_internal.h']]],
  ['ds1307_5freg_5fmin_5fmask',['DS1307_REG_MIN_MASK',['../ds1307__internal_8h.html#aaeabec92aad3fe4c46dcef740a75ac47',1,'ds1307_internal.h']]],
  ['ds1307_5freg_5fmon',['DS1307_REG_MON',['../ds1307__internal_8h.html#a95a63d281eb6731b1d2df8d6757ac7c3',1,'ds1307_internal.h']]],
  ['ds1307_5freg_5fmon_5fmask',['DS1307_REG_MON_MASK',['../ds1307__internal_8h.html#a108a10e30a34d23bfca850bd5781a7ba',1,'ds1307_internal.h']]],
  ['ds1307_5freg_5fram_5ffirst',['DS1307_REG_RAM_FIRST',['../ds1307__internal_8h.html#a906ce21566f73dac47f225889f3dd164',1,'ds1307_internal.h']]],
  ['ds1307_5freg_5fram_5flast',['DS1307_REG_RAM_LAST',['../ds1307__internal_8h.html#a0da0363df720e61c7d8da171b7a00e80',1,'ds1307_internal.h']]],
  ['ds1307_5freg_5fsec',['DS1307_REG_SEC',['../ds1307__internal_8h.html#ae6387456a4f32f6875f657e0ae01e441',1,'ds1307_internal.h']]],
  ['ds1307_5freg_5fsec_5fch',['DS1307_REG_SEC_CH',['../ds1307__internal_8h.html#aa4c57c09b61bccddb4d2192a670d96d9',1,'ds1307_internal.h']]],
  ['ds1307_5freg_5fsec_5fmask',['DS1307_REG_SEC_MASK',['../ds1307__internal_8h.html#a9f0246e451628e17f6e36d8ee2cb5bdc',1,'ds1307_internal.h']]],
  ['ds1307_5freg_5fsqw_5fctl',['DS1307_REG_SQW_CTL',['../ds1307__internal_8h.html#aad17b31fc9ba66a8bed3f7c07b1e78c0',1,'ds1307_internal.h']]],
  ['ds1307_5freg_5fyear',['DS1307_REG_YEAR',['../ds1307__internal_8h.html#a56f5f0ab147a2a589b50886b4553bec9',1,'ds1307_internal.h']]],
  ['ds1307_5fyear_5foffset',['DS1307_YEAR_OFFSET',['../ds1307__internal_8h.html#a0669eb0d5cd9dd34fdd66a4a5c050f21',1,'ds1307_internal.h']]],
  ['dxl_5fdir_5fpin',['DXL_DIR_PIN',['../opencm904_2include_2board_8h.html#a96c1b1f174eb046793a44d2246563e5a',1,'board.h']]]
];
