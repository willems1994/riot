var searchData=
[
  ['rbuf_5fint_5ft',['rbuf_int_t',['../rbuf_8h.html#a64bfc189d27ec817951a4d0591d52ba3',1,'rbuf.h']]],
  ['rcvbuf_5fentry_5ft',['rcvbuf_entry_t',['../group__net__gnrc__tcp.html#ga9339adb60ecedee715407f36eb2c1e76',1,'rcvbuf.h']]],
  ['rcvbuf_5ft',['rcvbuf_t',['../group__net__gnrc__tcp.html#gad2520dd7d6188756f929aed516fc8a4a',1,'rcvbuf.h']]],
  ['rmutex_5ft',['rmutex_t',['../rmutex_8h.html#aeee295d75891dbbe73a3641b3dfb13ae',1,'rmutex.h']]],
  ['rtc_5falarm_5fcb_5ft',['rtc_alarm_cb_t',['../group__drivers__periph__rtc.html#ga236005f42cd58827ca616cc5e808dc12',1,'rtc.h']]],
  ['rtt_5fcb_5ft',['rtt_cb_t',['../group__drivers__periph__rtt.html#ga6dbaceff1e27e05d49a9c32c57834771',1,'rtt.h']]]
];
