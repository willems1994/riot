var bmx280_8h =
[
    [ "BMX280_OK", "group__drivers__bmx280.html#gga2f1398dba5e4a5616b83437528bdb28ead656ef45890e366ab0b62e82c8d8492e", null ],
    [ "BMX280_ERR_NODEV", "group__drivers__bmx280.html#gga2f1398dba5e4a5616b83437528bdb28ea3c2ff1851fa1744f9ac3e7d7a5048974", null ],
    [ "BMX280_ERR_NOCAL", "group__drivers__bmx280.html#gga2f1398dba5e4a5616b83437528bdb28eadaf0d4376743b052987aabb172f0cc83", null ],
    [ "bmx280_filter_t", "group__drivers__bmx280.html#gaf0194620d1e46c8b30f340320121c24a", [
      [ "BMX280_FILTER_OFF", "group__drivers__bmx280.html#ggaf0194620d1e46c8b30f340320121c24aa1ac6832acfe74b3bbb0b5fbb7d68b960", null ],
      [ "BMX280_FILTER_2", "group__drivers__bmx280.html#ggaf0194620d1e46c8b30f340320121c24aa4ecc86a1268882245fae18f91e0ced10", null ],
      [ "BMX280_FILTER_4", "group__drivers__bmx280.html#ggaf0194620d1e46c8b30f340320121c24aac96a0fd9db6dac7dd463190bfb5c1c3c", null ],
      [ "BMX280_FILTER_8", "group__drivers__bmx280.html#ggaf0194620d1e46c8b30f340320121c24aa773d15b387f27a50112b5abfdc7ddd30", null ],
      [ "BMX280_FILTER_16", "group__drivers__bmx280.html#ggaf0194620d1e46c8b30f340320121c24aaaa97969f9091b51bf7f6d28af0177088", null ]
    ] ],
    [ "bmx280_mode_t", "group__drivers__bmx280.html#gaac1b0c55f8eddf04811f7c996b6d9dc9", [
      [ "BMX280_MODE_SLEEP", "group__drivers__bmx280.html#ggaac1b0c55f8eddf04811f7c996b6d9dc9a374ff07be81e4107adfc0794b3170890", null ],
      [ "BMX280_MODE_FORCED", "group__drivers__bmx280.html#ggaac1b0c55f8eddf04811f7c996b6d9dc9a09ae3804eb3dbc9611026e2222289878", null ],
      [ "BMX280_MODE_FORCED2", "group__drivers__bmx280.html#ggaac1b0c55f8eddf04811f7c996b6d9dc9a531296222cb6e0fc310a1754be81cde4", null ],
      [ "BMX280_MODE_NORMAL", "group__drivers__bmx280.html#ggaac1b0c55f8eddf04811f7c996b6d9dc9a24e73b7e0b7eefd515ed21399bf30060", null ]
    ] ],
    [ "bmx280_osrs_t", "group__drivers__bmx280.html#ga34a2512229772c04fdeed5aef28b703b", [
      [ "BMX280_OSRS_SKIPPED", "group__drivers__bmx280.html#gga34a2512229772c04fdeed5aef28b703ba38a8194a8eb1ad6f12e44e6c1ffcd87d", null ],
      [ "BMX280_OSRS_X1", "group__drivers__bmx280.html#gga34a2512229772c04fdeed5aef28b703baa7a8b6a9c521eca01fb540341a1bf94b", null ],
      [ "BMX280_OSRS_X2", "group__drivers__bmx280.html#gga34a2512229772c04fdeed5aef28b703bace17cc54515ee05fdd80f1aac73ae306", null ],
      [ "BMX280_OSRS_X4", "group__drivers__bmx280.html#gga34a2512229772c04fdeed5aef28b703ba92d0e666d326322759d8221ea3ff49d4", null ],
      [ "BMX280_OSRS_X8", "group__drivers__bmx280.html#gga34a2512229772c04fdeed5aef28b703ba674d546f510124cd181d08d1d043f3ce", null ],
      [ "BMX280_OSRS_X16", "group__drivers__bmx280.html#gga34a2512229772c04fdeed5aef28b703baa49292646dc8a7c1b946c8b014d55449", null ]
    ] ],
    [ "bmx280_t_sb_t", "group__drivers__bmx280.html#gadeee9ddfa4006747352b2df38e5322fe", [
      [ "BMX280_SB_0_5", "group__drivers__bmx280.html#ggadeee9ddfa4006747352b2df38e5322fea2e461f34a9be130ae2c5291e8fc4db13", null ],
      [ "BMX280_SB_62_5", "group__drivers__bmx280.html#ggadeee9ddfa4006747352b2df38e5322fea8e324a835a7eaf3946bc233a22a16b30", null ],
      [ "BMX280_SB_125", "group__drivers__bmx280.html#ggadeee9ddfa4006747352b2df38e5322fea3a2d23eb3b04ff79baea47d83713e2af", null ],
      [ "BMX280_SB_250", "group__drivers__bmx280.html#ggadeee9ddfa4006747352b2df38e5322fea8400bb4b8d55c64a6774596a85663cc5", null ],
      [ "BMX280_SB_500", "group__drivers__bmx280.html#ggadeee9ddfa4006747352b2df38e5322feaa3b7d8e8b817ef9c3099fdd9679ad5fe", null ],
      [ "BMX280_SB_1000", "group__drivers__bmx280.html#ggadeee9ddfa4006747352b2df38e5322feab524eede5b45421c279c092842945225", null ],
      [ "BMX280_SB_10", "group__drivers__bmx280.html#ggadeee9ddfa4006747352b2df38e5322fea968905a7ac53bcb2766d67b60fd3ad27", null ],
      [ "BMX280_SB_20", "group__drivers__bmx280.html#ggadeee9ddfa4006747352b2df38e5322fea1f1e0b6c2cb18732070b8756af17beff", null ]
    ] ],
    [ "bme280_read_humidity", "group__drivers__bmx280.html#ga070ae9dc86e840bf3ef8906d38ccc9ba", null ],
    [ "bmx280_init", "group__drivers__bmx280.html#gacd5e95c111d850fb9c5a8c52ed15e576", null ],
    [ "bmx280_read_pressure", "group__drivers__bmx280.html#ga2de43bcac4757ac3aadedb6bd4c486ad", null ],
    [ "bmx280_read_temperature", "group__drivers__bmx280.html#gad56c3ca7fb43afc9f7ded726a72a05f7", null ]
];