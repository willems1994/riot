var group__net__gnrc__sixlowpan =
[
    [ "6LoWPAN Fragmentation", "group__net__gnrc__sixlowpan__frag.html", "group__net__gnrc__sixlowpan__frag" ],
    [ "6LoWPAN neighbor discovery", "group__net__gnrc__sixlowpan__nd.html", "group__net__gnrc__sixlowpan__nd" ],
    [ "Contexts for 6LoWPAN address compression", "group__net__gnrc__sixlowpan__ctx.html", "group__net__gnrc__sixlowpan__ctx" ],
    [ "IPv6 header compression (IPHC)", "group__net__gnrc__sixlowpan__iphc.html", "group__net__gnrc__sixlowpan__iphc" ],
    [ "sixlowpan/internal.h", "sixlowpan_2internal_8h.html", null ],
    [ "gnrc/sixlowpan.h", "gnrc_2sixlowpan_8h.html", null ],
    [ "GNRC_SIXLOWPAN_MSG_QUEUE_SIZE", "group__net__gnrc__sixlowpan.html#ga7823a7ea8ee8c89c95f67df6cf0aa347", null ],
    [ "GNRC_SIXLOWPAN_PRIO", "group__net__gnrc__sixlowpan.html#ga394abb39f199270051899ec29ec1a3da", null ],
    [ "GNRC_SIXLOWPAN_STACK_SIZE", "group__net__gnrc__sixlowpan.html#gafda0f30679301e03fd3035e24497b05c", null ],
    [ "gnrc_sixlowpan_init", "group__net__gnrc__sixlowpan.html#gacd05c49a69c32d29720bb87758eac003", null ]
];