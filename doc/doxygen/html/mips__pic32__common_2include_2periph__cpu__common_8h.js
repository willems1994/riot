var mips__pic32__common_2include_2periph__cpu__common_8h =
[
    [ "CPUID_LEN", "mips__pic32__common_2include_2periph__cpu__common_8h.html#a1943715eaeaa63e28b7b4e207f655fca", null ],
    [ "GPIO_PIN", "mips__pic32__common_2include_2periph__cpu__common_8h.html#ae29846b3ecd19a0b7c44ff80a37ae7c1", null ],
    [ "PERIPH_TIMER_PROVIDES_SET", "mips__pic32__common_2include_2periph__cpu__common_8h.html#afd04a76b55e9fef358e904400cde4db7", null ],
    [ "PROVIDES_PM_SET_LOWEST", "mips__pic32__common_2include_2periph__cpu__common_8h.html#a04211208eb7863ce1c522440a91c745c", null ],
    [ "PORT_A", "mips__pic32__common_2include_2periph__cpu__common_8h.html#a7859c0a3efa8b1c360f5c2376baf051eaeb6782d9dfedf3c6a78ffdb1624fa454", null ],
    [ "PORT_B", "mips__pic32__common_2include_2periph__cpu__common_8h.html#a7859c0a3efa8b1c360f5c2376baf051ea16ada472d473fbd0207b99e9e4d68f4a", null ],
    [ "PORT_C", "mips__pic32__common_2include_2periph__cpu__common_8h.html#a7859c0a3efa8b1c360f5c2376baf051ea627cc690c37f97527dd2f07aa22092d9", null ],
    [ "PORT_D", "mips__pic32__common_2include_2periph__cpu__common_8h.html#a7859c0a3efa8b1c360f5c2376baf051eaf7242fe75227a46a190645663f91ce69", null ],
    [ "PORT_E", "mips__pic32__common_2include_2periph__cpu__common_8h.html#a7859c0a3efa8b1c360f5c2376baf051eabad63f022d1fa37a66f87dc31a78f6a9", null ],
    [ "PORT_F", "mips__pic32__common_2include_2periph__cpu__common_8h.html#a7859c0a3efa8b1c360f5c2376baf051eaa3760b302740c7d09c93ec7a634f837c", null ],
    [ "PORT_G", "mips__pic32__common_2include_2periph__cpu__common_8h.html#a7859c0a3efa8b1c360f5c2376baf051ea48afb424254d52e7d97a7c1428f5aafa", null ]
];