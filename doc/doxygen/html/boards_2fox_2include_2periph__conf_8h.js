var boards_2fox_2include_2periph__conf_8h =
[
    [ "ADC_NUMOF", "boards_2fox_2include_2periph__conf_8h.html#a2f0c741db24aa2ccded869ba53f6a302", null ],
    [ "CLOCK_AHB", "boards_2fox_2include_2periph__conf_8h.html#aad59105b6bb2f74e0ca52f5d2be8b8e1", null ],
    [ "CLOCK_AHB_DIV", "boards_2fox_2include_2periph__conf_8h.html#a11f66224742678d401efba36fb4d9164", null ],
    [ "CLOCK_APB1", "boards_2fox_2include_2periph__conf_8h.html#a7c07f079a96c4bf2c1c0727cc73a8efd", null ],
    [ "CLOCK_APB1_DIV", "boards_2fox_2include_2periph__conf_8h.html#a2cad8e54e6cdecca5c8a58a411ef5a93", null ],
    [ "CLOCK_APB2", "boards_2fox_2include_2periph__conf_8h.html#ab9db63572275f73c0933ab6733daf159", null ],
    [ "CLOCK_APB2_DIV", "boards_2fox_2include_2periph__conf_8h.html#a34259b3a8aae08bd77bab9cecdf1398e", null ],
    [ "CLOCK_CORECLOCK", "boards_2fox_2include_2periph__conf_8h.html#afc465f12242e68f6c3695caa3ba0a169", null ],
    [ "CLOCK_HSE", "boards_2fox_2include_2periph__conf_8h.html#a19d32ef5403d838f9398b9706618cb40", null ],
    [ "CLOCK_LSE", "boards_2fox_2include_2periph__conf_8h.html#a727373fed6afe243f41b211f7e66b285", null ],
    [ "CLOCK_PLL_DIV", "boards_2fox_2include_2periph__conf_8h.html#ad7c20feb5ba29cd3c6479fa74db3bbb0", null ],
    [ "CLOCK_PLL_MUL", "boards_2fox_2include_2periph__conf_8h.html#a902a38d6dfe9bc80271132ace0d1ca69", null ],
    [ "I2C_0_ISR", "boards_2fox_2include_2periph__conf_8h.html#a5655b75a493bbd17b560958d66369152", null ],
    [ "I2C_NUMOF", "boards_2fox_2include_2periph__conf_8h.html#abce62e16a6e3b3205801fed93c51692d", null ],
    [ "RTT_DEV", "boards_2fox_2include_2periph__conf_8h.html#a7f1b3908490d3eb5fa9be2688b8b5c4d", null ],
    [ "RTT_FREQUENCY", "boards_2fox_2include_2periph__conf_8h.html#afec7c948b8c70db3c9394fc3dc145a99", null ],
    [ "RTT_IRQ", "boards_2fox_2include_2periph__conf_8h.html#af260dd94dbc8753c0514345c83e7398d", null ],
    [ "RTT_IRQ_PRIO", "boards_2fox_2include_2periph__conf_8h.html#a79096369a15694b9330f70630e61c8c1", null ],
    [ "RTT_ISR", "boards_2fox_2include_2periph__conf_8h.html#a4cf1b90e7fa30c5aa12e04408214a74d", null ],
    [ "RTT_MAX_VALUE", "boards_2fox_2include_2periph__conf_8h.html#a57f384110fe2e8f4b3c4b9ba246517c6", null ],
    [ "RTT_NUMOF", "boards_2fox_2include_2periph__conf_8h.html#ac5c886cfa6263655176d9883cb30f3ab", null ],
    [ "RTT_PRESCALER", "boards_2fox_2include_2periph__conf_8h.html#a5a7a43a91632e478dc2685ff168efa71", null ],
    [ "SPI_NUMOF", "boards_2fox_2include_2periph__conf_8h.html#ab35a2b79568128efef74adf1ba1910a8", null ],
    [ "TIMER_0_ISR", "boards_2fox_2include_2periph__conf_8h.html#a4c490d334538c05373718609ca5fe2d4", null ],
    [ "TIMER_1_ISR", "boards_2fox_2include_2periph__conf_8h.html#ab1f8037bcb60d4669f508c471f92bc17", null ],
    [ "TIMER_NUMOF", "boards_2fox_2include_2periph__conf_8h.html#a6e2f66f5b6f5c835dd11f9766c4ed897", null ],
    [ "UART_0_ISR", "boards_2fox_2include_2periph__conf_8h.html#a713e03d19734d793baee3d1cc25c2dbb", null ],
    [ "UART_1_ISR", "boards_2fox_2include_2periph__conf_8h.html#af9358264b5cbce69dddad098a8600aae", null ],
    [ "UART_NUMOF", "boards_2fox_2include_2periph__conf_8h.html#a850405f2aaa352ad264346531f0e6230", null ],
    [ "i2c_config", "boards_2fox_2include_2periph__conf_8h.html#aa9dcbfbe7aa5baf027d834e5bca62a47", null ],
    [ "spi_config", "boards_2fox_2include_2periph__conf_8h.html#a873188d7292e07499dcde9674b1e849c", null ],
    [ "spi_divtable", "boards_2fox_2include_2periph__conf_8h.html#ae3b1d06e940a46447dea0987400fdc04", null ],
    [ "timer_config", "boards_2fox_2include_2periph__conf_8h.html#a2dd41f782d2c67052e4dc7d37cef89b1", null ],
    [ "uart_config", "boards_2fox_2include_2periph__conf_8h.html#a1643cfc64589407fb96b4cbf908689a5", null ]
];