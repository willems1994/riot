var structmega__uart__t =
[
    [ "BRR", "structmega__uart__t.html#a522e2c3c23632a7d2d609776e9095095", null ],
    [ "CSRA", "structmega__uart__t.html#adf3e54eaed04426126c92353ea15fbce", null ],
    [ "CSRB", "structmega__uart__t.html#a6fdb84303d4d9d5def8be2c76486cf00", null ],
    [ "CSRC", "structmega__uart__t.html#abd6353243a0c28a2c2fcf86589cfbd7a", null ],
    [ "DR", "structmega__uart__t.html#a81089026837368fcc61af808428f8dbe", null ],
    [ "reserved", "structmega__uart__t.html#af99182c0d1651b4371d8c76b2013455d", null ]
];