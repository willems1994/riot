var util_8h =
[
    [ "SOCK_HOSTPORT_MAXLEN", "group__net__sock__util.html#ga3c4db31fc99db24346aaea453c1a8997", null ],
    [ "SOCK_SCHEME_MAXLEN", "group__net__sock__util.html#gaf957713c3602434f9fca2d6e2645984b", null ],
    [ "SOCK_URLPATH_MAXLEN", "group__net__sock__util.html#gaee0f967f44d2acbd25d810fb5023805f", null ],
    [ "sock_udp_ep_equal", "group__net__sock__util.html#gaec9a883fa84c600daa5cccbe60eacd9b", null ],
    [ "sock_udp_ep_fmt", "group__net__sock__util.html#ga33670209cd5f843ac7f07d3bf39193f5", null ],
    [ "sock_udp_str2ep", "group__net__sock__util.html#gac1f524a66e0ff64589ef36dd53b817ee", null ],
    [ "sock_urlsplit", "group__net__sock__util.html#ga8aaf86cf785fe7b51ea90c495ba354a6", null ]
];