var ipv4_2addr_8h =
[
    [ "IPV4_ADDR_MAX_STR_LEN", "group__net__ipv4__addr.html#gaf174a4416a48d74279b85cc4f6fce97e", null ],
    [ "ipv4_addr_equal", "group__net__ipv4__addr.html#ga1152bca72cd6d63dc6bd4fc6b7d7074c", null ],
    [ "ipv4_addr_from_str", "group__net__ipv4__addr.html#gaddd4d56dd883cf318502e83041a48f4d", null ],
    [ "ipv4_addr_to_str", "group__net__ipv4__addr.html#ga746a395627e22a3a0c57940d0cd13f36", null ]
];