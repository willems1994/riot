var group__net__sock__ip =
[
    [ "ip.h", "ip_8h.html", null ],
    [ "sock_ip_t", "group__net__sock__ip.html#ga439f68352d1339555536747a64b5232e", null ],
    [ "sock_ip_close", "group__net__sock__ip.html#ga59aea36d7e415e533686e37bf69d3ec4", null ],
    [ "sock_ip_create", "group__net__sock__ip.html#ga0b416e35dd100962d88bdd675e7f6b18", null ],
    [ "sock_ip_get_local", "group__net__sock__ip.html#gae01afa2e9d65199732f37d0ea927b581", null ],
    [ "sock_ip_get_remote", "group__net__sock__ip.html#ga51463dba508be8f9a114486a75f9fdbe", null ],
    [ "sock_ip_recv", "group__net__sock__ip.html#ga2e003369faf135e84ad4a499949eaeed", null ],
    [ "sock_ip_send", "group__net__sock__ip.html#ga08bc8991ef0ac5c19081fb51ad151ba3", null ]
];