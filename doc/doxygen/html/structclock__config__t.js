var structclock__config__t =
[
    [ "clkdiv1", "structclock__config__t.html#ac499b4ad0def262ed003ad2c92b54dbc", null ],
    [ "clock_flags", "structclock__config__t.html#a1118fc9ad0c26c0129edb53ac1c88ef6", null ],
    [ "default_mode", "structclock__config__t.html#aa782826cef424ab9d97593fc946def29", null ],
    [ "erc_range", "structclock__config__t.html#a00024ddc2f54a0bfe380ecbc89466052", null ],
    [ "fcrdiv", "structclock__config__t.html#a89da89d1467aee49ae584e1d3f90168a", null ],
    [ "fll_factor_fee", "structclock__config__t.html#af960ec5a861e4e4bdaa8dd10d1ad8f59", null ],
    [ "fll_factor_fei", "structclock__config__t.html#ae892700e834aa2cfe71c135d36cdf730", null ],
    [ "fll_frdiv", "structclock__config__t.html#a148d255859db3bfcc22bdf616642a46a", null ],
    [ "osc32ksel", "structclock__config__t.html#a1712278e5974f0ce70aa92ece0438489", null ],
    [ "osc_clc", "structclock__config__t.html#a57aae6088e95ccefa55ab64a91e1c2b1", null ],
    [ "oscsel", "structclock__config__t.html#a9c1e839c292e909c221f753a61134ce4", null ],
    [ "pll_prdiv", "structclock__config__t.html#a707035b79208c0e0e596b478d05b1ff1", null ],
    [ "pll_vdiv", "structclock__config__t.html#a8ebbd05cbcd145eb01694817234bd62a", null ],
    [ "rtc_clc", "structclock__config__t.html#a880ceb1e415a1c821af28d66dfd30a05", null ]
];