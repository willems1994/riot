var nrf51_2include_2cpu__conf_8h =
[
    [ "CPU_DEFAULT_IRQ_PRIO", "group__cpu__nrf51.html#ga811633719ff60ee247e64b333d4b8675", null ],
    [ "CPU_FLASH_BASE", "group__cpu__nrf51.html#gad33eb792f7cf98b55b73fea8239c5f45", null ],
    [ "CPU_IRQ_NUMOF", "group__cpu__nrf51.html#gaf6c13d219504576c5c69399033b0ae39", null ],
    [ "FLASHPAGE_SIZE", "group__cpu__nrf51.html#gafce96cb577e50c76434ba92363ca20e8", null ],
    [ "GNRC_PKTBUF_SIZE", "group__cpu__nrf51.html#ga83a14b0588bb8adc93b565c4ebc5ec56", null ],
    [ "PWM_GPIOTE_CH", "group__cpu__nrf51.html#gad7beb462c4f6fc530157c5a4be0747ca", null ],
    [ "PWM_PPI_A", "group__cpu__nrf51.html#gadd0086c0b205e3e464008fd4db71d65e", null ],
    [ "PWM_PPI_B", "group__cpu__nrf51.html#ga3c56028c77b94a179aed082d53e37ca7", null ]
];