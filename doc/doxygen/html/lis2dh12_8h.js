var lis2dh12_8h =
[
    [ "LIS2DH12_OK", "group__drivers__lis2dh12.html#ggafb24d298ddd4bc4ff61aa333f07a574aad14af1cd22e260e7512b548ec67e8a10", null ],
    [ "LIS2DH12_NOBUS", "group__drivers__lis2dh12.html#ggafb24d298ddd4bc4ff61aa333f07a574aa85cee82a2fe71cee4a6226f364de0c3c", null ],
    [ "LIS2DH12_NODEV", "group__drivers__lis2dh12.html#ggafb24d298ddd4bc4ff61aa333f07a574aa5734e496fe9d61c899ff01f12a0c38bb", null ],
    [ "lis2dh12_rate_t", "group__drivers__lis2dh12.html#gaab3a9480859693923073b2465d8157f5", [
      [ "LIS2DH12_RATE_1HZ", "group__drivers__lis2dh12.html#ggaab3a9480859693923073b2465d8157f5abbfa7f75d93d3311237f3d66066b2a8a", null ],
      [ "LIS2DH12_RATE_10HZ", "group__drivers__lis2dh12.html#ggaab3a9480859693923073b2465d8157f5ac25ca3c055cea83035b5dc8448dd2a95", null ],
      [ "LIS2DH12_RATE_25HZ", "group__drivers__lis2dh12.html#ggaab3a9480859693923073b2465d8157f5a8d0ab7f19bfac556c78efb8764c4429f", null ],
      [ "LIS2DH12_RATE_50HZ", "group__drivers__lis2dh12.html#ggaab3a9480859693923073b2465d8157f5a87b57a0a50684364292301acfa742964", null ],
      [ "LIS2DH12_RATE_100HZ", "group__drivers__lis2dh12.html#ggaab3a9480859693923073b2465d8157f5ad356ca37e85b58982ce2fdbdb273b0ac", null ],
      [ "LIS2DH12_RATE_200HZ", "group__drivers__lis2dh12.html#ggaab3a9480859693923073b2465d8157f5a18ce6813ebb75ecc44b125c7cfed5620", null ],
      [ "LIS2DH12_RATE_400HZ", "group__drivers__lis2dh12.html#ggaab3a9480859693923073b2465d8157f5a179cfcbe10277e02e206174e001731b4", null ]
    ] ],
    [ "lis2dh12_scale_t", "group__drivers__lis2dh12.html#ga06364327a9b994b352adeb91fccf3965", [
      [ "LIS2DH12_SCALE_2G", "group__drivers__lis2dh12.html#gga06364327a9b994b352adeb91fccf3965a7c92d829c97c3a50cb4baf732a6208a3", null ],
      [ "LIS2DH12_SCALE_4G", "group__drivers__lis2dh12.html#gga06364327a9b994b352adeb91fccf3965acb21fe06366d23dd29d52de69fba3a8d", null ],
      [ "LIS2DH12_SCALE_8G", "group__drivers__lis2dh12.html#gga06364327a9b994b352adeb91fccf3965a693d602d3d8ec0cb91a2b2f6b84248e7", null ],
      [ "LIS2DH12_SCALE_16G", "group__drivers__lis2dh12.html#gga06364327a9b994b352adeb91fccf3965abd4fc4de9132238a234fab7120904e6a", null ]
    ] ],
    [ "lis2dh12_init", "group__drivers__lis2dh12.html#gaf2e0a538321e117e492f0baa10962b22", null ],
    [ "lis2dh12_poweroff", "group__drivers__lis2dh12.html#gaceaf1d635398314c792e855eba702981", null ],
    [ "lis2dh12_poweron", "group__drivers__lis2dh12.html#gab84da5729fea172e106c9a725cb8a6a5", null ],
    [ "lis2dh12_read", "group__drivers__lis2dh12.html#ga2f653cd1b9f81f0f5284652d44847d61", null ],
    [ "lis2dh12_saul_driver", "group__drivers__lis2dh12.html#ga9f91c4ca7a1e6aa4927ac94f06a2e7ba", null ]
];