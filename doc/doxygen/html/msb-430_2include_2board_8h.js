var msb_430_2include_2board_8h =
[
    [ "__MSP430F1612__", "msb-430_2include_2board_8h.html#adb9620d04a71cd066af767a507f87bce", null ],
    [ "F_CPU", "msb-430_2include_2board_8h.html#a43bafb28b29491ec7f871319b5a3b2f8", null ],
    [ "F_RC_OSCILLATOR", "msb-430_2include_2board_8h.html#ac842f33954353e7364121068e2805ff2", null ],
    [ "MSP430_HAS_DCOR", "msb-430_2include_2board_8h.html#a3d9611258f0542c7933f06e3a407040c", null ],
    [ "MSP430_HAS_EXTERNAL_CRYSTAL", "msb-430_2include_2board_8h.html#a3142e89bed881feff6b2eccaf3d0c3fc", null ],
    [ "MSP430_INITIAL_CPU_SPEED", "msb-430_2include_2board_8h.html#a95ebe9dd4378d815f81e2609c69b0d5d", null ],
    [ "SHT1X_PARAM_CLK", "msb-430_2include_2board_8h.html#aacb9c55450037c4b22608e750826a0e9", null ],
    [ "SHT1X_PARAM_DATA", "msb-430_2include_2board_8h.html#a4e39355a1b088aa9ed75276d6d948f06", null ]
];