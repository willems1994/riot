var group__core__internal =
[
    [ "kernel_defines.h", "kernel__defines_8h.html", null ],
    [ "kernel_init.h", "kernel__init_8h.html", null ],
    [ "ALIGN_OF", "group__core__internal.html#ga81d77270f081f16d85b0652acc4e2089", null ],
    [ "BUILD_BUG_ON", "group__core__internal.html#ga0fc08f11f6c83dbc94a75ca49d29fae8", null ],
    [ "CONST", "group__core__internal.html#ga0c33b494a68ce28497e7ce8e5e95feff", null ],
    [ "container_of", "group__core__internal.html#gaa06910d90372b14a0756bf14cdc64811", null ],
    [ "NORETURN", "group__core__internal.html#gaa1728270d73c5d1598de1fd691762eb1", null ],
    [ "PURE", "group__core__internal.html#gacd42770aecb025cfac170d4d3ace4544", null ],
    [ "UNREACHABLE", "group__core__internal.html#ga0bc63b24b654ca433be7b97a3edde132", null ],
    [ "kernel_init", "group__core__internal.html#ga0dbe43a64cbf994bf9189e7767f93342", null ]
];