var structgnrc__netif__ops =
[
    [ "get", "structgnrc__netif__ops.html#ae07c67f21356167679f13a1abd04f541", null ],
    [ "init", "structgnrc__netif__ops.html#ac8d556a20a3a2d7121934b624a7a0a21", null ],
    [ "msg_handler", "structgnrc__netif__ops.html#ad30ddbef3e34b6f4a51cff51a5af410d", null ],
    [ "recv", "structgnrc__netif__ops.html#af4a9b66278b6d7d7352ecfd4394fb861", null ],
    [ "send", "structgnrc__netif__ops.html#a86afb0a1108b6bbacf281c4aba13824c", null ],
    [ "set", "structgnrc__netif__ops.html#a3301d3571916757ee943a87945baac93", null ]
];