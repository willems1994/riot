var group__drivers__mtd =
[
    [ "Native MTD", "group__drivers__mtd__native.html", "group__drivers__mtd__native" ],
    [ "mtd.h", "mtd_8h.html", null ],
    [ "mtd_dev_t", "structmtd__dev__t.html", [
      [ "driver", "structmtd__dev__t.html#a5d72e335ca2807fcb1badb94a7d62608", null ],
      [ "page_size", "structmtd__dev__t.html#afbb923a3f218ad7a7cb83ccd7a379493", null ],
      [ "pages_per_sector", "structmtd__dev__t.html#a072137ea8598a509cc7d367e6a35ce10", null ],
      [ "sector_count", "structmtd__dev__t.html#a3bb7b38359fdbf4a5c7b37afe85baee2", null ]
    ] ],
    [ "mtd_desc", "structmtd__desc.html", [
      [ "erase", "structmtd__desc.html#a5ceed1da5bce9427950a429f3aa2ee75", null ],
      [ "init", "structmtd__desc.html#aed886992d42b8d4b46f847e82586ae7b", null ],
      [ "power", "structmtd__desc.html#aae0381c0b324a5d4f047b5afc2f98484", null ],
      [ "read", "structmtd__desc.html#a0e9e45b783e97805755a5ab49e0f92cc", null ],
      [ "write", "structmtd__desc.html#afa93f3e1afa9cbd577762bc83de9808d", null ]
    ] ],
    [ "mtd_desc_t", "group__drivers__mtd.html#ga016431acc3cc8d6f5a40149225dabc05", null ],
    [ "mtd_power_state", "group__drivers__mtd.html#ga2e588df22ed1a520ba154adbd93b1455", [
      [ "MTD_POWER_UP", "group__drivers__mtd.html#gga2e588df22ed1a520ba154adbd93b1455a860be5ff4a3cfb933b479391d2538939", null ],
      [ "MTD_POWER_DOWN", "group__drivers__mtd.html#gga2e588df22ed1a520ba154adbd93b1455a81eb56a249bbf2eebd5cfbd8ef2c7ce8", null ]
    ] ],
    [ "mtd_erase", "group__drivers__mtd.html#gaabab7f7b0b1cfcdbe2b32d2e26bbf4d7", null ],
    [ "mtd_init", "group__drivers__mtd.html#ga6bcf582eaf56330ea31c1162ea535076", null ],
    [ "mtd_power", "group__drivers__mtd.html#gaf44f35470c2180ebd4b772f3b25d31fc", null ],
    [ "mtd_read", "group__drivers__mtd.html#ga74ffca1da0436aada58ef3f18f469e36", null ],
    [ "mtd_write", "group__drivers__mtd.html#ga5f36fb25e0daada2e9a873475c532d94", null ],
    [ "mtd_vfs_ops", "group__drivers__mtd.html#gac0a5658e078c937a88369f695baf88d8", null ]
];