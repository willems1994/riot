var boards_2ikea_tradfri_2include_2periph__conf_8h =
[
    [ "CLOCK_CORE_DIV", "boards_2ikea-tradfri_2include_2periph__conf_8h.html#a72c545f836f5baef7095780b4f98c8b7", null ],
    [ "CLOCK_HF", "boards_2ikea-tradfri_2include_2periph__conf_8h.html#abd65074c67a2e5c8da24150ebf61a185", null ],
    [ "CLOCK_LFA", "boards_2ikea-tradfri_2include_2periph__conf_8h.html#afb10892a5c3c55c67bec30df7121334a", null ],
    [ "CLOCK_LFB", "boards_2ikea-tradfri_2include_2periph__conf_8h.html#a9c3e02ef7f90cbb1c701e8d930b0c92b", null ],
    [ "CLOCK_LFE", "boards_2ikea-tradfri_2include_2periph__conf_8h.html#a479fb1a474a4aab73a8e12cc11881d2a", null ],
    [ "PERIPH_NUMOF", "boards_2ikea-tradfri_2include_2periph__conf_8h.html#aeb6b11ac01a3de01d1be83a681f6cc28", null ],
    [ "RTC_NUMOF", "boards_2ikea-tradfri_2include_2periph__conf_8h.html#a4201ed1203952946a7b53934fcc5dad6", null ],
    [ "RTT_FREQUENCY", "boards_2ikea-tradfri_2include_2periph__conf_8h.html#afec7c948b8c70db3c9394fc3dc145a99", null ],
    [ "RTT_MAX_VALUE", "boards_2ikea-tradfri_2include_2periph__conf_8h.html#a57f384110fe2e8f4b3c4b9ba246517c6", null ],
    [ "RTT_NUMOF", "boards_2ikea-tradfri_2include_2periph__conf_8h.html#ac5c886cfa6263655176d9883cb30f3ab", null ],
    [ "SPI_NUMOF", "boards_2ikea-tradfri_2include_2periph__conf_8h.html#ab35a2b79568128efef74adf1ba1910a8", null ],
    [ "TIMER_0_ISR", "boards_2ikea-tradfri_2include_2periph__conf_8h.html#a4c490d334538c05373718609ca5fe2d4", null ],
    [ "TIMER_NUMOF", "boards_2ikea-tradfri_2include_2periph__conf_8h.html#a6e2f66f5b6f5c835dd11f9766c4ed897", null ],
    [ "UART_0_ISR_RX", "boards_2ikea-tradfri_2include_2periph__conf_8h.html#a8e13c32456487d198aa5f98d0611c0d7", null ],
    [ "UART_NUMOF", "boards_2ikea-tradfri_2include_2periph__conf_8h.html#a850405f2aaa352ad264346531f0e6230", null ],
    [ "spi_config", "boards_2ikea-tradfri_2include_2periph__conf_8h.html#a7285f045531445b15aa331cd5f27e6e1", null ],
    [ "timer_config", "boards_2ikea-tradfri_2include_2periph__conf_8h.html#a2dd41f782d2c67052e4dc7d37cef89b1", null ],
    [ "uart_config", "boards_2ikea-tradfri_2include_2periph__conf_8h.html#a1643cfc64589407fb96b4cbf908689a5", null ]
];