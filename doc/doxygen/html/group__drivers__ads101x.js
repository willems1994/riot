var group__drivers__ads101x =
[
    [ "ads101x_regs.h", "ads101x__regs_8h.html", null ],
    [ "ads101x.h", "ads101x_8h.html", null ],
    [ "ads101x_params", "structads101x__params.html", [
      [ "addr", "structads101x__params.html#a808404eaabcf9f265e885a26c2b144aa", null ],
      [ "i2c", "structads101x__params.html#a84037b0cdc7e87bfef7f2e24f37306ae", null ],
      [ "mux_gain", "structads101x__params.html#a631a94c09f7eb2a7e053166c07254dc4", null ]
    ] ],
    [ "ads101x_alert_params", "structads101x__alert__params.html", [
      [ "addr", "structads101x__alert__params.html#ad9b43dc795efe76a32686067abc38eb4", null ],
      [ "alert_pin", "structads101x__alert__params.html#ac4f5bb6cea371ff3cc6481ab0d2fc587", null ],
      [ "high_limit", "structads101x__alert__params.html#a9cc80157dd0970387d909ffb64c7be15", null ],
      [ "i2c", "structads101x__alert__params.html#a13ea9d4ee9ee0f1dcf47b8f040a8622e", null ],
      [ "low_limit", "structads101x__alert__params.html#ac156b980afa85d600a3fd4893d0c0f60", null ]
    ] ],
    [ "ads101x", "structads101x.html", [
      [ "params", "structads101x.html#a690cd563b5ab1a12458a2b90b14a5842", null ]
    ] ],
    [ "ads101x_alert", "structads101x__alert.html", [
      [ "arg", "structads101x__alert.html#a2aec0a3d583af8dd5516a93aea2fb9a2", null ],
      [ "cb", "structads101x__alert.html#a238830fe512b1532353296e9cb66fb3a", null ],
      [ "params", "structads101x__alert.html#afb75539811baa2c1c72c8c5049a5c38a", null ]
    ] ],
    [ "ADS101X_I2C_ADDRESS", "group__drivers__ads101x.html#ga82a07ebad194496b574d495e7e26f911", null ],
    [ "ads101x_alert_cb_t", "group__drivers__ads101x.html#ga180dcefdb44b260b81f422787f9ace52", null ],
    [ "ads101x_alert_params_t", "group__drivers__ads101x.html#ga7b6c9549b691e3b11e9719fa23117f79", null ],
    [ "ads101x_alert_t", "group__drivers__ads101x.html#ga3554bc5e7298f8c1b841514bb0c67655", null ],
    [ "ads101x_params_t", "group__drivers__ads101x.html#ga61c8a7948074d042793b90fb204a3fdf", null ],
    [ "ads101x_t", "group__drivers__ads101x.html#gadeed1f02fed76c5dbf1fd17917bf78d4", [
      [ "ADS101X_OK", "group__drivers__ads101x.html#gga7ee8d0f117a79ca7eb1e0076a9182bcbaf1fb7334c77f6a8509f9ecd455c14afe", null ],
      [ "ADS101X_NOI2C", "group__drivers__ads101x.html#gga7ee8d0f117a79ca7eb1e0076a9182bcbae1c762aa7dbe06bd63c9fb2ef7a1081b", null ],
      [ "ADS101X_NODEV", "group__drivers__ads101x.html#gga7ee8d0f117a79ca7eb1e0076a9182bcbafccf6fe3f9126d3aada5e63be1c8cdf6", null ],
      [ "ADS101X_NODATA", "group__drivers__ads101x.html#gga7ee8d0f117a79ca7eb1e0076a9182bcba672c515a86769de4c0b0d93dbc9c03e7", null ]
    ] ],
    [ "ads101x_alert_init", "group__drivers__ads101x.html#ga9ab02377bf2773eb1973ba6de8f14234", null ],
    [ "ads101x_enable_alert", "group__drivers__ads101x.html#ga75092e0fb959c3fa906f7ce9e933f179", null ],
    [ "ads101x_init", "group__drivers__ads101x.html#ga62914846bd803de8fcaf01f46d38abe9", null ],
    [ "ads101x_read_raw", "group__drivers__ads101x.html#gad8f0aee88b7fa4aa9d6c58c59d9b51a4", null ],
    [ "ads101x_set_alert_parameters", "group__drivers__ads101x.html#ga483dda279848fde59e3d142cf26ee0ff", null ],
    [ "ads101x_set_mux_gain", "group__drivers__ads101x.html#ga15a9598fcd6f96dd7627f04c5490151b", null ]
];