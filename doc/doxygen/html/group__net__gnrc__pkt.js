var group__net__gnrc__pkt =
[
    [ "include/net/gnrc/pkt.h", "include_2net_2gnrc_2pkt_8h.html", null ],
    [ "gnrc_pktsnip", "structgnrc__pktsnip.html", [
      [ "data", "structgnrc__pktsnip.html#a04ca47171b70901e55f4a5c67399f070", null ],
      [ "next", "structgnrc__pktsnip.html#ad6d823f68b2ee82429861342bd86c0a9", null ],
      [ "size", "structgnrc__pktsnip.html#a0718f2cde3411a4833f82c10f8ea8d71", null ],
      [ "type", "structgnrc__pktsnip.html#a8362d0e56cfabedcf51459377c58db59", null ],
      [ "users", "structgnrc__pktsnip.html#a4ccc7be825c7f5c449825676606921b3", null ]
    ] ],
    [ "gnrc_pktsnip_t", "group__net__gnrc__pkt.html#ga278e783e56a5ee6f1bd7b81077ed82a7", null ],
    [ "gnrc_pkt_count", "group__net__gnrc__pkt.html#ga64bff5436035b3d3f4fb231c72fd9d98", null ],
    [ "gnrc_pkt_len", "group__net__gnrc__pkt.html#gae5eca86f5855affdc6739e325c85f62e", null ],
    [ "gnrc_pkt_len_upto", "group__net__gnrc__pkt.html#gaf7fd495c70fb6cffec13b2eb4887446b", null ],
    [ "gnrc_pktsnip_search_type", "group__net__gnrc__pkt.html#ga75f0a9cb9c250366cc37808931b3c0f5", null ]
];