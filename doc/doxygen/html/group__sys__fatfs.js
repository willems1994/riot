var group__sys__fatfs =
[
    [ "fatfs.h", "fatfs_8h.html", null ],
    [ "fatfs_desc", "structfatfs__desc.html", [
      [ "abs_path_str_buff", "structfatfs__desc.html#a19227baa09030724decbd9592a1cddbb", null ],
      [ "fat_fs", "structfatfs__desc.html#a675bddc8be0f418e4c1576ccbd7856fb", null ],
      [ "vol_idx", "structfatfs__desc.html#a0927d8c81590df080227069e1f416809", null ]
    ] ],
    [ "fatfs_file_desc", "structfatfs__file__desc.html", [
      [ "file", "structfatfs__file__desc.html#ac75efbf8d4f52e939f3dfb44446ca4f9", null ],
      [ "fname", "structfatfs__file__desc.html#a40aa8e97154e12c295f05c59f565fa5c", null ]
    ] ],
    [ "EPOCH_YEAR_OFFSET", "group__sys__fatfs.html#gac60b25fabfee168b71723862d745cfa1", null ],
    [ "FATFS_MAX_ABS_PATH_SIZE", "group__sys__fatfs.html#gac2705d10dbe276b4b760ead6ef7f20e5", null ],
    [ "FATFS_MAX_VOL_STR_LEN", "group__sys__fatfs.html#ga936f8b2bea013417ca1c0826e8183781", null ],
    [ "FATFS_MOUNT_OPT", "group__sys__fatfs.html#ga7ef4d049a02926687d3e3ff7ec2da937", null ],
    [ "FATFS_YEAR_OFFSET", "group__sys__fatfs.html#ga91e6a88e878df2c8d1ecd3065d92dfa7", null ],
    [ "fatfs_desc_t", "group__sys__fatfs.html#ga55339a4dcebd9f1be581de4638b41670", null ],
    [ "fatfs_file_desc_t", "group__sys__fatfs.html#gaab14929b4866418e088e4283df9a0313", null ],
    [ "fatfs_file_system", "group__sys__fatfs.html#ga9a619a8314ba35fc986b101ddbb973dc", null ]
];