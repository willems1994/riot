var pic32_clicker_2include_2board_8h =
[
    [ "EIC_IRQ", "pic32-clicker_2include_2board_8h.html#aecac83223fd46af5303cb487d2a9eec1", null ],
    [ "LED1_MASK", "pic32-clicker_2include_2board_8h.html#a669ed6e073140d069b30442bf4c08842", null ],
    [ "LED1_OFF", "pic32-clicker_2include_2board_8h.html#a343fd2d3ce61b84f88ddfaea32c67c58", null ],
    [ "LED1_ON", "pic32-clicker_2include_2board_8h.html#aadd4c7ae0cd4e9bbb17f7055dd51fe08", null ],
    [ "LED1_PIN", "pic32-clicker_2include_2board_8h.html#a318aa17e5d40e2132d2c7f6269ce7f51", null ],
    [ "LED1_TOGGLE", "pic32-clicker_2include_2board_8h.html#a267fdbba1d750146b73da35c1731fd17", null ],
    [ "LED2_MASK", "pic32-clicker_2include_2board_8h.html#a40f0f4b5ae7ea50d341105ddc740101e", null ],
    [ "LED2_OFF", "pic32-clicker_2include_2board_8h.html#ac6468b1df4dfabcca0bb142044d6f976", null ],
    [ "LED2_ON", "pic32-clicker_2include_2board_8h.html#ab55f588eb2c5177d3f7806e60d379fba", null ],
    [ "LED2_PIN", "pic32-clicker_2include_2board_8h.html#af6f84078113b55354d20585131b386f7", null ],
    [ "LED2_TOGGLE", "pic32-clicker_2include_2board_8h.html#acd16785845ce7004334b91a98707f8eb", null ],
    [ "TICKS_PER_US", "pic32-clicker_2include_2board_8h.html#a74d0b3592618206b4fae5e7ed4d63f6a", null ],
    [ "board_init", "pic32-clicker_2include_2board_8h.html#a916f2adc2080b4fe88034086d107a8dc", null ]
];