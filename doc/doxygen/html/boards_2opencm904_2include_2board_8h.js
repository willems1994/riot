var boards_2opencm904_2include_2board_8h =
[
    [ "BTN0_MODE", "boards_2opencm904_2include_2board_8h.html#a904884feea8e03c30c9a85ca6d7a8e5c", null ],
    [ "BTN0_PIN", "boards_2opencm904_2include_2board_8h.html#aab5c3eca54046333af52593b9e360270", null ],
    [ "DXL_DIR_PIN", "boards_2opencm904_2include_2board_8h.html#a96c1b1f174eb046793a44d2246563e5a", null ],
    [ "LED0_MASK", "boards_2opencm904_2include_2board_8h.html#abfabde35a3e8b7ab50146a5e223bfead", null ],
    [ "LED0_OFF", "boards_2opencm904_2include_2board_8h.html#aef2e39b9fd343d0e437fac0463f3e02d", null ],
    [ "LED0_ON", "boards_2opencm904_2include_2board_8h.html#a1e48dc02333bfc2e6ad3e5bd466eecd4", null ],
    [ "LED0_PIN", "boards_2opencm904_2include_2board_8h.html#a3fc522fda449d6edde717d6a857429cf", null ],
    [ "LED0_TOGGLE", "boards_2opencm904_2include_2board_8h.html#aebc6389533d9fc8dcbe4d2129a4d5a45", null ],
    [ "LED_PORT", "boards_2opencm904_2include_2board_8h.html#a663daa01e565aee93c6f20c5845b90b4", null ],
    [ "STDIO_UART_BAUDRATE", "boards_2opencm904_2include_2board_8h.html#ab2f12549369d69d3db8f38d49c8ca507", null ],
    [ "STDIO_UART_DEV", "boards_2opencm904_2include_2board_8h.html#a81935d479349dc2ce0a416bcb0e6beda", null ],
    [ "XTIMER_BACKOFF", "boards_2opencm904_2include_2board_8h.html#a370b9e9a079c4cb4f54fd947b67b9f41", null ],
    [ "XTIMER_WIDTH", "boards_2opencm904_2include_2board_8h.html#afea1be2406d45b8fbb1dca1a318ac2dc", null ],
    [ "board_init", "boards_2opencm904_2include_2board_8h.html#a916f2adc2080b4fe88034086d107a8dc", null ]
];