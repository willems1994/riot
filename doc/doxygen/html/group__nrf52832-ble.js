var group__nrf52832_ble =
[
    [ "ble-core.h", "ble-core_8h.html", null ],
    [ "ble_advertising_init", "group__nrf52832-ble.html#ga7f8b91d415d9530a1e7b54e6578786c0", null ],
    [ "ble_advertising_start", "group__nrf52832-ble.html#gad40dbc7d063e789101f63d9239506e55", null ],
    [ "ble_get_mac", "group__nrf52832-ble.html#ga29e49f1ce9a75785a6aad1e3dc0e1ad1", null ],
    [ "ble_stack_init", "group__nrf52832-ble.html#gac20292bb47b59093357dcee5bc1650a5", null ]
];