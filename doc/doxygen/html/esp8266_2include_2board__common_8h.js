var esp8266_2include_2board__common_8h =
[
    [ "MTD_0", "esp8266_2include_2board__common_8h.html#a0f992c379bd87e59320727deb561bbbd", null ],
    [ "SPIFFS_ALIGNED_OBJECT_INDEX_TABLES", "esp8266_2include_2board__common_8h.html#afdc47aa571d5b518828f894260a0d61b", null ],
    [ "SPIFFS_CACHE", "esp8266_2include_2board__common_8h.html#af23e950a724dcece1082fa62106fcbd0", null ],
    [ "SPIFFS_HAL_CALLBACK_EXTRA", "esp8266_2include_2board__common_8h.html#a5344be1b6fbd49daec8866db48adcd8a", null ],
    [ "SPIFFS_READ_ONLY", "esp8266_2include_2board__common_8h.html#a71836a4b19e137303a1cf15800f658e3", null ],
    [ "SPIFFS_SINGLETON", "esp8266_2include_2board__common_8h.html#a76aad051735d4001dec8d09f1be4dcb0", null ],
    [ "STDIO_UART_BAUDRATE", "esp8266_2include_2board__common_8h.html#ab2f12549369d69d3db8f38d49c8ca507", null ],
    [ "board_init", "esp8266_2include_2board__common_8h.html#a916f2adc2080b4fe88034086d107a8dc", null ],
    [ "board_print_config", "esp8266_2include_2board__common_8h.html#a0d425c6a6195af2f7e5da2c72147d0b0", null ],
    [ "mtd0", "esp8266_2include_2board__common_8h.html#aff5f5efe3abd184592b11d520918dc44", null ]
];