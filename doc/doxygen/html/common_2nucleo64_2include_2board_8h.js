var common_2nucleo64_2include_2board_8h =
[
    [ "BTN0_MODE", "group__boards__common__nucleo64.html#ga904884feea8e03c30c9a85ca6d7a8e5c", null ],
    [ "BTN0_PIN", "group__boards__common__nucleo64.html#gaab5c3eca54046333af52593b9e360270", null ],
    [ "LED0_MASK", "group__boards__common__nucleo64.html#gabfabde35a3e8b7ab50146a5e223bfead", null ],
    [ "LED0_OFF", "group__boards__common__nucleo64.html#gaef2e39b9fd343d0e437fac0463f3e02d", null ],
    [ "LED0_ON", "group__boards__common__nucleo64.html#ga1e48dc02333bfc2e6ad3e5bd466eecd4", null ],
    [ "LED0_PIN", "group__boards__common__nucleo64.html#ga3fc522fda449d6edde717d6a857429cf", null ],
    [ "LED0_PORT", "group__boards__common__nucleo64.html#ga76914453bb5cda4ebca204e091e8f55c", null ],
    [ "LED0_TOGGLE", "group__boards__common__nucleo64.html#gaebc6389533d9fc8dcbe4d2129a4d5a45", null ]
];