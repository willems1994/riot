var group__net__skald__eddystone =
[
    [ "skald/eddystone.h", "skald_2eddystone_8h.html", null ],
    [ "skald_eddystone_uid_t", "structskald__eddystone__uid__t.html", [
      [ "EDDYSTONE_NAMESPACE_LEN", "structskald__eddystone__uid__t.html#ad3c77c70e2202e82a432fe3b421d3af2", null ],
      [ "instance", "structskald__eddystone__uid__t.html#a790cf78f08b9f25c166c9c481b833b55", null ]
    ] ],
    [ "skald_eddystone_uid_adv", "group__net__skald__eddystone.html#ga56f34edb1900f623a4a05b16ec411f2d", null ],
    [ "skald_eddystone_url_adv", "group__net__skald__eddystone.html#ga6db3ecbee7807f7ef9e3d76375a0e837", null ]
];