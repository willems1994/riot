var group__net__gnrc__nettype =
[
    [ "nettype.h", "nettype_8h.html", null ],
    [ "gnrc_nettype_t", "group__net__gnrc__nettype.html#ga2582fbb16a318806983c225a69460902", [
      [ "GNRC_NETTYPE_IOVEC", "group__net__gnrc__nettype.html#gga2582fbb16a318806983c225a69460902a23df3f5a298ba2fdaedf3404beb508ec", null ],
      [ "GNRC_NETTYPE_NETIF", "group__net__gnrc__nettype.html#gga2582fbb16a318806983c225a69460902a591a497a7b4a31427c44c7b24baf17c3", null ],
      [ "GNRC_NETTYPE_UNDEF", "group__net__gnrc__nettype.html#gga2582fbb16a318806983c225a69460902aadb666d279898cdecd1dcc0cc18799f1", null ],
      [ "GNRC_NETTYPE_SIXLOWPAN", "group__net__gnrc__nettype.html#gga2582fbb16a318806983c225a69460902ae3db3bc6ba0636854f94574d7eba0a99", null ],
      [ "GNRC_NETTYPE_IPV6", "group__net__gnrc__nettype.html#gga2582fbb16a318806983c225a69460902a5b4cb265411204c95e4a4996dcafe380", null ],
      [ "GNRC_NETTYPE_ICMPV6", "group__net__gnrc__nettype.html#gga2582fbb16a318806983c225a69460902adbb6f5119b7c8f58e11d3ca5c4abd2d6", null ],
      [ "GNRC_NETTYPE_UDP", "group__net__gnrc__nettype.html#gga2582fbb16a318806983c225a69460902a73162332c559b1a558e5d076e28d53ad", null ],
      [ "GNRC_NETTYPE_NUMOF", "group__net__gnrc__nettype.html#gga2582fbb16a318806983c225a69460902af8ddf1a8140c718cdd3e1e7cb40c0e5c", null ]
    ] ],
    [ "GNRC_NETTYPE_ICMPV6", "group__net__gnrc__nettype.html#gga2582fbb16a318806983c225a69460902adbb6f5119b7c8f58e11d3ca5c4abd2d6", null ],
    [ "gnrc_nettype_from_ethertype", "group__net__gnrc__nettype.html#ga2d2c19e8f4647c18815104fc3e70584f", null ],
    [ "gnrc_nettype_from_protnum", "group__net__gnrc__nettype.html#gaa706f61051016219e5a9e1b0f6f71abf", null ],
    [ "gnrc_nettype_to_ethertype", "group__net__gnrc__nettype.html#gabb0f1234440fcbc13f350310d2ff04c7", null ],
    [ "gnrc_nettype_to_protnum", "group__net__gnrc__nettype.html#ga33ec8c250360b80274a130278faf2843", null ]
];