var structcoap__pkt__t =
[
    [ "hdr", "structcoap__pkt__t.html#aa1ffc04c491c11cb0972bd95cb3b3bd8", null ],
    [ "options", "structcoap__pkt__t.html#a54b1774608b1f2cb867e21c81e979907", null ],
    [ "options_len", "structcoap__pkt__t.html#abb6e6d0d60c2154b84b3a61e0cddab56", null ],
    [ "payload", "structcoap__pkt__t.html#a296b66b050d0be92094daf2aba8d730a", null ],
    [ "payload_len", "structcoap__pkt__t.html#a42765c97fe9d014c311b6b733827a17b", null ],
    [ "token", "structcoap__pkt__t.html#a2e7247a6cb35b2d855f26fc132624d01", null ]
];