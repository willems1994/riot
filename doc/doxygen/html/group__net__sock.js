var group__net__sock =
[
    [ "DNS sock API", "group__net__sock__dns.html", "group__net__sock__dns" ],
    [ "Raw IPv4/IPv6 sock API", "group__net__sock__ip.html", "group__net__sock__ip" ],
    [ "TCP sock API", "group__net__sock__tcp.html", "group__net__sock__tcp" ],
    [ "UDP sock API", "group__net__sock__udp.html", "group__net__sock__udp" ],
    [ "sock utility functions", "group__net__sock__util.html", "group__net__sock__util" ],
    [ "sock.h", "sock_8h.html", null ],
    [ "sock_ip_ep_t", "structsock__ip__ep__t.html", [
      [ "addr", "structsock__ip__ep__t.html#a793d4e3fef21c2a694f339f79d9c16fc", null ],
      [ "family", "structsock__ip__ep__t.html#ae7ae802357d4ec15b34860c8d85a8bf1", null ],
      [ "ipv4", "structsock__ip__ep__t.html#aeaf0f01115440fd3565c8f7c2e304096", null ],
      [ "ipv4_u32", "structsock__ip__ep__t.html#a12825c6d9d56ae275517f27aae3acaf9", null ],
      [ "ipv6", "structsock__ip__ep__t.html#ab8868cf7ab0dd8478b464f6ad260a8ee", null ],
      [ "netif", "structsock__ip__ep__t.html#a30b82677263d30a8d04dbbae5a4dce45", null ]
    ] ],
    [ "_sock_tl_ep", "struct__sock__tl__ep.html", [
      [ "addr", "struct__sock__tl__ep.html#a296289fd087648c0e06e943d25c4a11a", null ],
      [ "family", "struct__sock__tl__ep.html#a39cc1104d239f1b620e0714a1c33b247", null ],
      [ "ipv4", "struct__sock__tl__ep.html#a962ad49f2dd126b6329db542df577fa7", null ],
      [ "ipv4_u32", "struct__sock__tl__ep.html#a4a3ba084a16d9729f04200f3e158e927", null ],
      [ "ipv6", "struct__sock__tl__ep.html#ac57a885cc2ffcebc1759e3ac4c60cec9", null ],
      [ "netif", "struct__sock__tl__ep.html#a4f667124619a9590a0471585f71b2213", null ],
      [ "port", "struct__sock__tl__ep.html#a00b5dbff09ee8e87806fff6280966140", null ]
    ] ],
    [ "SOCK_ADDR_ANY_NETIF", "group__net__sock.html#ga062f5eb3763541ec0ab6e261447b01ca", null ],
    [ "SOCK_FLAGS_REUSE_EP", "group__net__sock.html#ga2289814aa41ddb66a19f444bd0fee52e", null ],
    [ "SOCK_HAS_IPV6", "group__net__sock.html#ga858ed875d71d514276bb7a30fd050d87", null ],
    [ "SOCK_IPV4_EP_ANY", "group__net__sock.html#gaca8b50e1b4fabde0f5073a665faf5e07", null ],
    [ "SOCK_IPV6_EP_ANY", "group__net__sock.html#ga3f10e5b714c03824d6dc4fff5d372b8f", null ],
    [ "SOCK_NO_TIMEOUT", "group__net__sock.html#gaf0c954b49c306f6c125d25ddba9f352e", null ]
];