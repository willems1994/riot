var boards_2stm32f429i_disc1_2include_2board_8h =
[
    [ "BTN0_MODE", "boards_2stm32f429i-disc1_2include_2board_8h.html#a904884feea8e03c30c9a85ca6d7a8e5c", null ],
    [ "BTN0_PIN", "boards_2stm32f429i-disc1_2include_2board_8h.html#aab5c3eca54046333af52593b9e360270", null ],
    [ "LED0_MASK", "boards_2stm32f429i-disc1_2include_2board_8h.html#abfabde35a3e8b7ab50146a5e223bfead", null ],
    [ "LED0_OFF", "boards_2stm32f429i-disc1_2include_2board_8h.html#aef2e39b9fd343d0e437fac0463f3e02d", null ],
    [ "LED0_ON", "boards_2stm32f429i-disc1_2include_2board_8h.html#a1e48dc02333bfc2e6ad3e5bd466eecd4", null ],
    [ "LED0_PIN", "boards_2stm32f429i-disc1_2include_2board_8h.html#a3fc522fda449d6edde717d6a857429cf", null ],
    [ "LED0_TOGGLE", "boards_2stm32f429i-disc1_2include_2board_8h.html#aebc6389533d9fc8dcbe4d2129a4d5a45", null ],
    [ "LED1_MASK", "boards_2stm32f429i-disc1_2include_2board_8h.html#a669ed6e073140d069b30442bf4c08842", null ],
    [ "LED1_OFF", "boards_2stm32f429i-disc1_2include_2board_8h.html#a343fd2d3ce61b84f88ddfaea32c67c58", null ],
    [ "LED1_ON", "boards_2stm32f429i-disc1_2include_2board_8h.html#aadd4c7ae0cd4e9bbb17f7055dd51fe08", null ],
    [ "LED1_PIN", "boards_2stm32f429i-disc1_2include_2board_8h.html#a318aa17e5d40e2132d2c7f6269ce7f51", null ],
    [ "LED1_TOGGLE", "boards_2stm32f429i-disc1_2include_2board_8h.html#a267fdbba1d750146b73da35c1731fd17", null ],
    [ "board_init", "boards_2stm32f429i-disc1_2include_2board_8h.html#a916f2adc2080b4fe88034086d107a8dc", null ]
];