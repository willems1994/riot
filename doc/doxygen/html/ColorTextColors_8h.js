var ColorTextColors_8h =
[
    [ "BGDEFAULT", "ColorTextColors_8h.html#aed168756b7a469d79e37b79d91c7b7fb", null ],
    [ "BGGREEN", "ColorTextColors_8h.html#a898cbb3b8ecef2510cb6152b1f96d4c8", null ],
    [ "BGRED", "ColorTextColors_8h.html#a95e0a3871c4ee628ff78494e0c4e459a", null ],
    [ "CDEFAULT", "ColorTextColors_8h.html#a08f5dbe80bad9154a7c0c4e0d1695a9f", null ],
    [ "CGREEN", "ColorTextColors_8h.html#aa5341f160639e0bc96a8134b4c33e45f", null ],
    [ "CRED", "ColorTextColors_8h.html#ab93994c82cc32f5aaa86dc9fe39b3942", null ],
    [ "LINEFILL", "ColorTextColors_8h.html#a4e70f0645cd2d12b766d1cc9988357db", null ],
    [ "SBOLD", "ColorTextColors_8h.html#ae2840542b3d120252d3f773781b9d769", null ],
    [ "SDEFAULT", "ColorTextColors_8h.html#a38e3789ea401c350e35004f3580c1ce8", null ]
];