var boards_2nucleo_f410rb_2include_2periph__conf_8h =
[
    [ "ADC_CONFIG", "group__boards__nucleo-f410rb.html#gabe2b0ad66fab5aa34c6404c2ebe77ec5", null ],
    [ "ADC_NUMOF", "group__boards__nucleo-f410rb.html#ga2f0c741db24aa2ccded869ba53f6a302", null ],
    [ "CLOCK_AHB", "group__boards__nucleo-f410rb.html#gaad59105b6bb2f74e0ca52f5d2be8b8e1", null ],
    [ "CLOCK_AHB_DIV", "group__boards__nucleo-f410rb.html#ga11f66224742678d401efba36fb4d9164", null ],
    [ "CLOCK_APB1", "group__boards__nucleo-f410rb.html#ga7c07f079a96c4bf2c1c0727cc73a8efd", null ],
    [ "CLOCK_APB1_DIV", "group__boards__nucleo-f410rb.html#ga2cad8e54e6cdecca5c8a58a411ef5a93", null ],
    [ "CLOCK_APB2", "group__boards__nucleo-f410rb.html#gab9db63572275f73c0933ab6733daf159", null ],
    [ "CLOCK_APB2_DIV", "group__boards__nucleo-f410rb.html#ga34259b3a8aae08bd77bab9cecdf1398e", null ],
    [ "CLOCK_CORECLOCK", "group__boards__nucleo-f410rb.html#gafc465f12242e68f6c3695caa3ba0a169", null ],
    [ "CLOCK_HSE", "group__boards__nucleo-f410rb.html#ga19d32ef5403d838f9398b9706618cb40", null ],
    [ "CLOCK_LSE", "group__boards__nucleo-f410rb.html#ga727373fed6afe243f41b211f7e66b285", null ],
    [ "CLOCK_PLL_M", "group__boards__nucleo-f410rb.html#ga5963e9d857a94bce3662fa83cc41b683", null ],
    [ "CLOCK_PLL_N", "group__boards__nucleo-f410rb.html#ga1c3484818170fe048c55eaac9d56f46c", null ],
    [ "CLOCK_PLL_P", "group__boards__nucleo-f410rb.html#ga160e6d888eff96d8853812e91d12df50", null ],
    [ "CLOCK_PLL_Q", "group__boards__nucleo-f410rb.html#ga23ccdd51ab0cfa878079b30c696ad532", null ],
    [ "I2C_0_ISR", "group__boards__nucleo-f410rb.html#ga5655b75a493bbd17b560958d66369152", null ],
    [ "I2C_NUMOF", "group__boards__nucleo-f410rb.html#gabce62e16a6e3b3205801fed93c51692d", null ],
    [ "SPI_NUMOF", "group__boards__nucleo-f410rb.html#gab35a2b79568128efef74adf1ba1910a8", null ],
    [ "TIMER_0_ISR", "group__boards__nucleo-f410rb.html#ga4c490d334538c05373718609ca5fe2d4", null ],
    [ "TIMER_NUMOF", "group__boards__nucleo-f410rb.html#ga6e2f66f5b6f5c835dd11f9766c4ed897", null ],
    [ "UART_0_DMA_ISR", "group__boards__nucleo-f410rb.html#ga639d73a9b4925c2970acf86d88df3d49", null ],
    [ "UART_0_ISR", "group__boards__nucleo-f410rb.html#ga713e03d19734d793baee3d1cc25c2dbb", null ],
    [ "UART_1_DMA_ISR", "group__boards__nucleo-f410rb.html#gadf387a613ea902458db462f67f43dfcb", null ],
    [ "UART_1_ISR", "group__boards__nucleo-f410rb.html#gaf9358264b5cbce69dddad098a8600aae", null ],
    [ "UART_2_DMA_ISR", "group__boards__nucleo-f410rb.html#gaeb88de936556ee08e00c78860eadfd0f", null ],
    [ "UART_2_ISR", "group__boards__nucleo-f410rb.html#gafdfcd079b784102f3ceb94a04d9c9250", null ],
    [ "UART_NUMOF", "group__boards__nucleo-f410rb.html#ga850405f2aaa352ad264346531f0e6230", null ],
    [ "i2c_config", "group__boards__nucleo-f410rb.html#gaa9dcbfbe7aa5baf027d834e5bca62a47", null ],
    [ "spi_config", "group__boards__nucleo-f410rb.html#ga873188d7292e07499dcde9674b1e849c", null ],
    [ "spi_divtable", "group__boards__nucleo-f410rb.html#gae3b1d06e940a46447dea0987400fdc04", null ],
    [ "timer_config", "group__boards__nucleo-f410rb.html#ga2dd41f782d2c67052e4dc7d37cef89b1", null ],
    [ "uart_config", "group__boards__nucleo-f410rb.html#ga1643cfc64589407fb96b4cbf908689a5", null ]
];