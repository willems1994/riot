var group__cpu__efm32 =
[
    [ "Silicon Labs EFM32GG family", "group__cpu__efm32gg.html", null ],
    [ "Silicon Labs EFM32LG family", "group__cpu__efm32lg.html", null ],
    [ "Silicon Labs EFM32PG12B family", "group__cpu__efm32pg12b.html", null ],
    [ "Silicon Labs EFM32PG1B family", "group__cpu__efm32pg1b.html", null ],
    [ "Silicon Labs EFR32MG12P family", "group__cpu__efr32mg12p.html", null ],
    [ "Silicon Labs EFR32MG1P family", "group__cpu__efr32mg1p.html", null ],
    [ "efm32/include/cpu_conf.h", "efm32_2include_2cpu__conf_8h.html", null ],
    [ "efm32/include/periph_cpu.h", "efm32_2include_2periph__cpu_8h.html", null ],
    [ "CPU_DEFAULT_IRQ_PRIO", "group__cpu__efm32.html#ga811633719ff60ee247e64b333d4b8675", null ],
    [ "FLASHPAGE_SIZE", "group__cpu__efm32.html#gafce96cb577e50c76434ba92363ca20e8", null ]
];