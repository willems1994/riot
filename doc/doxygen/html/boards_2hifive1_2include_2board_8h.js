var boards_2hifive1_2include_2board_8h =
[
    [ "LED0_OFF", "group__boards__hifive.html#gaef2e39b9fd343d0e437fac0463f3e02d", null ],
    [ "LED0_ON", "group__boards__hifive.html#ga1e48dc02333bfc2e6ad3e5bd466eecd4", null ],
    [ "LED0_PIN", "group__boards__hifive.html#ga3fc522fda449d6edde717d6a857429cf", null ],
    [ "LED0_TOGGLE", "group__boards__hifive.html#gaebc6389533d9fc8dcbe4d2129a4d5a45", null ],
    [ "LED1_OFF", "group__boards__hifive.html#ga343fd2d3ce61b84f88ddfaea32c67c58", null ],
    [ "LED1_ON", "group__boards__hifive.html#gaadd4c7ae0cd4e9bbb17f7055dd51fe08", null ],
    [ "LED1_PIN", "group__boards__hifive.html#ga318aa17e5d40e2132d2c7f6269ce7f51", null ],
    [ "LED1_TOGGLE", "group__boards__hifive.html#ga267fdbba1d750146b73da35c1731fd17", null ],
    [ "LED2_OFF", "group__boards__hifive.html#gac6468b1df4dfabcca0bb142044d6f976", null ],
    [ "LED2_ON", "group__boards__hifive.html#gab55f588eb2c5177d3f7806e60d379fba", null ],
    [ "LED2_PIN", "group__boards__hifive.html#gaf6f84078113b55354d20585131b386f7", null ],
    [ "LED2_TOGGLE", "group__boards__hifive.html#gacd16785845ce7004334b91a98707f8eb", null ],
    [ "board_init", "group__boards__hifive.html#ga916f2adc2080b4fe88034086d107a8dc", null ],
    [ "board_init_clock", "group__boards__hifive.html#gaf49f994beb1f6db82155b83fa779f187", null ]
];