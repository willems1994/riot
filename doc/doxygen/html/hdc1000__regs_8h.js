var hdc1000__regs_8h =
[
    [ "HDC1000_BTST_LOW", "hdc1000__regs_8h.html#a4724fb08ac1272d8723237503e630d10", null ],
    [ "HDC1000_CONFIG", "hdc1000__regs_8h.html#a2d6089d9a96e0582ae593ed72cd792a2", null ],
    [ "HDC1000_DEVICE_ID", "hdc1000__regs_8h.html#a7aff9bb1c707a3e100217a833eda4b7a", null ],
    [ "HDC1000_DID_VALUE", "hdc1000__regs_8h.html#a0fa435c1ae10979f39361800f60a2aee", null ],
    [ "HDC1000_HEAT", "hdc1000__regs_8h.html#ac7d1ece43c25cd29dc04ef45abbefbcc", null ],
    [ "HDC1000_HRES11", "hdc1000__regs_8h.html#a4f1836d547dca732f0cfb6b429a8c608", null ],
    [ "HDC1000_HRES14", "hdc1000__regs_8h.html#a63c66f7088995aeade2865dae13026ed", null ],
    [ "HDC1000_HRES8", "hdc1000__regs_8h.html#ab3689cab1b1d509b6b21f646c3b2b236", null ],
    [ "HDC1000_HRES_MSK", "hdc1000__regs_8h.html#a21b7f5c432c78f3a708e4f3c171468a0", null ],
    [ "HDC1000_HUMIDITY", "hdc1000__regs_8h.html#a839fa76b44137633d579c82c10ae61a7", null ],
    [ "HDC1000_MANUFACTURER_ID", "hdc1000__regs_8h.html#a0810bdbb341df582bef8f82e7a31f18a", null ],
    [ "HDC1000_MID_VALUE", "hdc1000__regs_8h.html#a5b0474786c8ed6e8be09806a293854f5", null ],
    [ "HDC1000_RST", "hdc1000__regs_8h.html#afa18d288858d91d171ad21eba6bd9c20", null ],
    [ "HDC1000_SEQ_MOD", "hdc1000__regs_8h.html#a510c5790d6317ad03a836a34f3db3efe", null ],
    [ "HDC1000_SID1", "hdc1000__regs_8h.html#a6855fa775b500c126510f107783e9700", null ],
    [ "HDC1000_SID2", "hdc1000__regs_8h.html#a0868ab85a50b9a8897ab9c9a3a78d3ee", null ],
    [ "HDC1000_SID3", "hdc1000__regs_8h.html#a82692af18d7ae62fe3b373cf5f79deb2", null ],
    [ "HDC1000_TEMPERATURE", "hdc1000__regs_8h.html#a8ef80362010e0b245f1d8d2c7d82d275", null ],
    [ "HDC1000_TRES11", "hdc1000__regs_8h.html#af2521daf104331cff086e1f5c6110a6c", null ],
    [ "HDC1000_TRES14", "hdc1000__regs_8h.html#a98568c387e673daaf5fe90b7d7dfe77e", null ],
    [ "HDC1000_TRES_MSK", "hdc1000__regs_8h.html#ab8671622109a15be5a482a781ceb85c5", null ]
];