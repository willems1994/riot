var sodaq_explorer_2include_2board_8h =
[
    [ "BTN0_MODE", "sodaq-explorer_2include_2board_8h.html#a904884feea8e03c30c9a85ca6d7a8e5c", null ],
    [ "BTN0_PIN", "sodaq-explorer_2include_2board_8h.html#aab5c3eca54046333af52593b9e360270", null ],
    [ "LED0_MASK", "sodaq-explorer_2include_2board_8h.html#abfabde35a3e8b7ab50146a5e223bfead", null ],
    [ "LED0_OFF", "sodaq-explorer_2include_2board_8h.html#aef2e39b9fd343d0e437fac0463f3e02d", null ],
    [ "LED0_ON", "sodaq-explorer_2include_2board_8h.html#a1e48dc02333bfc2e6ad3e5bd466eecd4", null ],
    [ "LED0_PIN", "sodaq-explorer_2include_2board_8h.html#a3fc522fda449d6edde717d6a857429cf", null ],
    [ "LED0_PORT", "sodaq-explorer_2include_2board_8h.html#a76914453bb5cda4ebca204e091e8f55c", null ],
    [ "LED0_TOGGLE", "sodaq-explorer_2include_2board_8h.html#aebc6389533d9fc8dcbe4d2129a4d5a45", null ],
    [ "RN2XX3_PARAM_PIN_RESET", "sodaq-explorer_2include_2board_8h.html#a95132c8ab6b45d81453beaddc688dfbb", null ],
    [ "RN2XX3_PARAM_UART", "sodaq-explorer_2include_2board_8h.html#ae19e18135f60f4e597e3c86ef5edd74b", null ],
    [ "XTIMER_WIDTH", "sodaq-explorer_2include_2board_8h.html#afea1be2406d45b8fbb1dca1a318ac2dc", null ],
    [ "board_init", "sodaq-explorer_2include_2board_8h.html#a916f2adc2080b4fe88034086d107a8dc", null ]
];