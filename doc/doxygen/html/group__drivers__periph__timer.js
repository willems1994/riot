var group__drivers__periph__timer =
[
    [ "drivers/include/periph/timer.h", "drivers_2include_2periph_2timer_8h.html", null ],
    [ "timer_isr_ctx_t", "structtimer__isr__ctx__t.html", [
      [ "arg", "structtimer__isr__ctx__t.html#a60e83c0293c251fc5821c59f210cc1f4", null ],
      [ "cb", "structtimer__isr__ctx__t.html#a97512a7e1cdd590d3534abe2e6d4d1bf", null ]
    ] ],
    [ "TIMER_DEV", "group__drivers__periph__timer.html#gac66133744b2b503740e6324e2efa644e", null ],
    [ "TIMER_UNDEF", "group__drivers__periph__timer.html#gaa0c84c20a11f7659548d09b0c786dd23", null ],
    [ "tim_t", "group__drivers__periph__timer.html#ga9edf7bd1cdc783c7a23cb740c9ba15dc", null ],
    [ "timer_cb_t", "group__drivers__periph__timer.html#ga2afe9f4ce8c01fc4253a17ffebc552cc", null ],
    [ "timer_clear", "group__drivers__periph__timer.html#ga3ef6a48915b57707513ff17c0463a277", null ],
    [ "timer_init", "group__drivers__periph__timer.html#ga0850d6b0f76643c40d4701b6c6658bd8", null ],
    [ "timer_read", "group__drivers__periph__timer.html#ga02217bf29c1e76c795a0b197408b91b7", null ],
    [ "timer_set", "group__drivers__periph__timer.html#gaccff2ca33cca64411015f808369cb919", null ],
    [ "timer_set_absolute", "group__drivers__periph__timer.html#ga2b440fa3b789669204f659de9238d752", null ],
    [ "timer_start", "group__drivers__periph__timer.html#gafe70538411bdcd344b2f663b1c6073b0", null ],
    [ "timer_stop", "group__drivers__periph__timer.html#ga995bb5527183eacd8bb4b835be143613", null ]
];