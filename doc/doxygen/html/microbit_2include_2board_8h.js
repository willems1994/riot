var microbit_2include_2board_8h =
[
    [ "BTN0_MODE", "microbit_2include_2board_8h.html#a904884feea8e03c30c9a85ca6d7a8e5c", null ],
    [ "BTN0_PIN", "microbit_2include_2board_8h.html#aab5c3eca54046333af52593b9e360270", null ],
    [ "BTN1_MODE", "microbit_2include_2board_8h.html#ac792994d9b2d95b9761a05597ddea744", null ],
    [ "BTN1_PIN", "microbit_2include_2board_8h.html#aeb7f5e51d2c7c61ec0a5d5c29591e9b3", null ],
    [ "MAG3110_PARAM_ADDR", "microbit_2include_2board_8h.html#a2917c5952f13de9c5666f26fb7bde648", null ],
    [ "MAG3110_PARAM_I2C", "microbit_2include_2board_8h.html#a846ecd57404d37b1caf5332eb2c8c446", null ],
    [ "MICROBIT_LED_COL1", "microbit_2include_2board_8h.html#a9b673b83c676546fe73919bcc85cb594", null ],
    [ "MICROBIT_LED_COL2", "microbit_2include_2board_8h.html#ae0b7af161c855cf67781b188e272794c", null ],
    [ "MICROBIT_LED_COL3", "microbit_2include_2board_8h.html#a10f71561c71b9381342cafd1abf3dea3", null ],
    [ "MICROBIT_LED_COL4", "microbit_2include_2board_8h.html#a02d9fd14188a0dabd38701ba8f6fc48a", null ],
    [ "MICROBIT_LED_COL5", "microbit_2include_2board_8h.html#a1cb1ac0c2946f56f62aa4412c35203c3", null ],
    [ "MICROBIT_LED_COL6", "microbit_2include_2board_8h.html#a8fdb9e52478b4d92f3c94f17047adef0", null ],
    [ "MICROBIT_LED_COL7", "microbit_2include_2board_8h.html#adeef8a48866881895ec1d45083b1f73f", null ],
    [ "MICROBIT_LED_COL8", "microbit_2include_2board_8h.html#aa8956fc563b1e4a9ad3c530bbf698edc", null ],
    [ "MICROBIT_LED_COL9", "microbit_2include_2board_8h.html#aaa7a36cb3388606c97b8bc2020f51d25", null ],
    [ "MICROBIT_LED_ROW1", "microbit_2include_2board_8h.html#aa06560975d10e4a401c4fa150a4f2613", null ],
    [ "MICROBIT_LED_ROW2", "microbit_2include_2board_8h.html#a7cdce2e015eb00fd1b9f6dd91d86188c", null ],
    [ "MICROBIT_LED_ROW3", "microbit_2include_2board_8h.html#adefa786188f801790be1cc2fa5b261ff", null ],
    [ "MMA8X5X_PARAM_ADDR", "microbit_2include_2board_8h.html#a704a3a7b5b9b5aa680a1dafe7157c02c", null ],
    [ "MMA8X5X_PARAM_I2C", "microbit_2include_2board_8h.html#a564cf142e7fae24c8c4384231d514918", null ],
    [ "MMA8X5X_PARAM_TYPE", "microbit_2include_2board_8h.html#a55e51c2536cce092718faf9cf20f7f83", null ],
    [ "XTIMER_BACKOFF", "microbit_2include_2board_8h.html#a370b9e9a079c4cb4f54fd947b67b9f41", null ],
    [ "XTIMER_WIDTH", "microbit_2include_2board_8h.html#afea1be2406d45b8fbb1dca1a318ac2dc", null ],
    [ "board_init", "microbit_2include_2board_8h.html#a916f2adc2080b4fe88034086d107a8dc", null ]
];