var boards_2openmote_cc2538_2include_2periph__conf_8h =
[
    [ "ADC_NUMOF", "boards_2openmote-cc2538_2include_2periph__conf_8h.html#a2f0c741db24aa2ccded869ba53f6a302", null ],
    [ "CLOCK_CORECLOCK", "boards_2openmote-cc2538_2include_2periph__conf_8h.html#afc465f12242e68f6c3695caa3ba0a169", null ],
    [ "I2C_IRQ_PRIO", "boards_2openmote-cc2538_2include_2periph__conf_8h.html#a7a0bc389843ed85946f608482ee17929", null ],
    [ "I2C_NUMOF", "boards_2openmote-cc2538_2include_2periph__conf_8h.html#abce62e16a6e3b3205801fed93c51692d", null ],
    [ "RADIO_IRQ_PRIO", "boards_2openmote-cc2538_2include_2periph__conf_8h.html#abc130ad4aa6c216bc9b810e36d5d63f9", null ],
    [ "SOC_ADC_ADCCON3_EREF", "boards_2openmote-cc2538_2include_2periph__conf_8h.html#a8622110f4312bd12718d9969714501e8", null ],
    [ "SPI_NUMOF", "boards_2openmote-cc2538_2include_2periph__conf_8h.html#ab35a2b79568128efef74adf1ba1910a8", null ],
    [ "TIMER_IRQ_PRIO", "boards_2openmote-cc2538_2include_2periph__conf_8h.html#adbe3eb289332c7b7bf848191d3a65923", null ],
    [ "TIMER_NUMOF", "boards_2openmote-cc2538_2include_2periph__conf_8h.html#a6e2f66f5b6f5c835dd11f9766c4ed897", null ],
    [ "UART_0_ISR", "boards_2openmote-cc2538_2include_2periph__conf_8h.html#a713e03d19734d793baee3d1cc25c2dbb", null ],
    [ "UART_NUMOF", "boards_2openmote-cc2538_2include_2periph__conf_8h.html#a850405f2aaa352ad264346531f0e6230", null ],
    [ "adc_config", "boards_2openmote-cc2538_2include_2periph__conf_8h.html#a160c7b1c3bc13c7cb5ac4ed375f4e21d", null ],
    [ "i2c_config", "boards_2openmote-cc2538_2include_2periph__conf_8h.html#aa9dcbfbe7aa5baf027d834e5bca62a47", null ],
    [ "spi_config", "boards_2openmote-cc2538_2include_2periph__conf_8h.html#a873188d7292e07499dcde9674b1e849c", null ],
    [ "timer_config", "boards_2openmote-cc2538_2include_2periph__conf_8h.html#a2dd41f782d2c67052e4dc7d37cef89b1", null ],
    [ "uart_config", "boards_2openmote-cc2538_2include_2periph__conf_8h.html#a1643cfc64589407fb96b4cbf908689a5", null ]
];