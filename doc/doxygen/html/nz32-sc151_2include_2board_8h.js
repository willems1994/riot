var nz32_sc151_2include_2board_8h =
[
    [ "LED0_MASK", "nz32-sc151_2include_2board_8h.html#abfabde35a3e8b7ab50146a5e223bfead", null ],
    [ "LED0_OFF", "nz32-sc151_2include_2board_8h.html#aef2e39b9fd343d0e437fac0463f3e02d", null ],
    [ "LED0_ON", "nz32-sc151_2include_2board_8h.html#a1e48dc02333bfc2e6ad3e5bd466eecd4", null ],
    [ "LED0_PIN", "nz32-sc151_2include_2board_8h.html#a3fc522fda449d6edde717d6a857429cf", null ],
    [ "LED0_TOGGLE", "nz32-sc151_2include_2board_8h.html#aebc6389533d9fc8dcbe4d2129a4d5a45", null ],
    [ "board_init", "nz32-sc151_2include_2board_8h.html#a916f2adc2080b4fe88034086d107a8dc", null ]
];