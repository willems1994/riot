var group__net__gnrc__sock =
[
    [ "gnrc_sock_internal.h", "gnrc__sock__internal_8h.html", null ],
    [ "sock_types.h", "sock__types_8h.html", null ],
    [ "gnrc_sock_reg", "structgnrc__sock__reg.html", [
      [ "entry", "structgnrc__sock__reg.html#a8d16208dcdddc30988a97cd96a024fd2", null ],
      [ "mbox", "structgnrc__sock__reg.html#a936a208fd08628a446e3775288fdd4d0", null ],
      [ "mbox_queue", "structgnrc__sock__reg.html#a0000d2f89cffe5795c85b2614302e4e9", null ]
    ] ],
    [ "sock_ip", "structsock__ip.html", [
      [ "flags", "structsock__ip.html#a1217cd6c584015ef565a4082355c1983", null ],
      [ "local", "structsock__ip.html#a452935139c80a2a699f728c9176cbb60", null ],
      [ "reg", "structsock__ip.html#a1ce7783972a5a795829eae9bcb6d315c", null ],
      [ "remote", "structsock__ip.html#ab1548e96a4ac10872e57bd76609861f3", null ]
    ] ],
    [ "sock_udp", "structsock__udp.html", [
      [ "flags", "structsock__udp.html#a603e1765c62b4402ea422e461cbd4423", null ],
      [ "local", "structsock__udp.html#a4527c938cf3d949f79bbe7b4f9bcfc57", null ],
      [ "reg", "structsock__udp.html#a17361c96a916c14584884d515713e114", null ],
      [ "remote", "structsock__udp.html#a29fde26b43b782d23a454bd75f1da1e9", null ]
    ] ],
    [ "GNRC_SOCK_DYN_PORTRANGE_ERR", "group__net__gnrc__sock.html#ga35a76218705c511962dbc26ea4b4c0d4", null ],
    [ "GNRC_SOCK_DYN_PORTRANGE_MAX", "group__net__gnrc__sock.html#ga07323eb4a2bf30cca04cba662c8c0436", null ],
    [ "GNRC_SOCK_DYN_PORTRANGE_MIN", "group__net__gnrc__sock.html#ga4b781c28930aee57210196299f9a73aa", null ],
    [ "GNRC_SOCK_DYN_PORTRANGE_NUM", "group__net__gnrc__sock.html#ga40b8e38475486103799cb08b5886ec5a", null ],
    [ "GNRC_SOCK_DYN_PORTRANGE_OFF", "group__net__gnrc__sock.html#ga5889b0920d48c79e6e9e77ff8e309ee4", null ],
    [ "SOCK_MBOX_SIZE", "group__net__gnrc__sock.html#ga98d12bf5688195c5754d20cc93581ee8", null ],
    [ "gnrc_sock_reg_t", "group__net__gnrc__sock.html#ga485dd5086fa39d1b981119133a9b0ae6", null ],
    [ "gnrc_af_not_supported", "group__net__gnrc__sock.html#gaa69a591d4be68618a69df17610086aad", null ],
    [ "gnrc_ep_addr_any", "group__net__gnrc__sock.html#ga8ba9598591e07f49dae165b04119a5fd", null ],
    [ "gnrc_ep_set", "group__net__gnrc__sock.html#gaafc7cbf3e1bbe97d0599177dff8bff4c", null ],
    [ "gnrc_sock_create", "group__net__gnrc__sock.html#gaedd1d80e6b1250eeb32fddfdf23a7968", null ],
    [ "gnrc_sock_recv", "group__net__gnrc__sock.html#ga9f9583cff00c0ea57694097880c4f750", null ],
    [ "gnrc_sock_send", "group__net__gnrc__sock.html#gaa66a462ccc7944a7d4a14dd652005cc3", null ]
];