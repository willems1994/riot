var structmtd__spi__nor__t =
[
    [ "addr_width", "structmtd__spi__nor__t.html#af3375ee883b3291e7d4d5525367c9749", null ],
    [ "base", "structmtd__spi__nor__t.html#a5e7566a79757a8b3ae3bc010cce63d6e", null ],
    [ "clk", "structmtd__spi__nor__t.html#a8fedcf89207b178a20b913ce797f6bc4", null ],
    [ "cs", "structmtd__spi__nor__t.html#a093928ce64653beb41fcdf612749b794", null ],
    [ "flag", "structmtd__spi__nor__t.html#a5cd102d2ec5ae3247454d43faecf84a9", null ],
    [ "jedec_id", "structmtd__spi__nor__t.html#a4af66c3f88d6b33431237515c48dc774", null ],
    [ "mode", "structmtd__spi__nor__t.html#a63b56128b191014f033989272d9142ab", null ],
    [ "opcode", "structmtd__spi__nor__t.html#a8eb32909423b96bcdacdd8f9c9715861", null ],
    [ "page_addr_mask", "structmtd__spi__nor__t.html#a0602ceba18303c236cb39bfb516d9f35", null ],
    [ "page_addr_shift", "structmtd__spi__nor__t.html#ac5e8c0762d54a9cb9b71b8864eba9e26", null ],
    [ "sec_addr_mask", "structmtd__spi__nor__t.html#abc5fddace93fba81caaed121bf866fac", null ],
    [ "sec_addr_shift", "structmtd__spi__nor__t.html#a7093bbebb387f2ba9b7fcb11630592a7", null ],
    [ "spi", "structmtd__spi__nor__t.html#ae42db35746739424e2c8608f9bb50e70", null ]
];