var group__cpu__lpc2387 =
[
    [ "lpc2387/include/cpu_conf.h", "lpc2387_2include_2cpu__conf_8h.html", null ],
    [ "lpc2387/include/periph_cpu.h", "lpc2387_2include_2periph__cpu_8h.html", null ],
    [ "cpu_print_last_instruction", "group__cpu__lpc2387.html#ga8a02f8177d64e6e010565ca8f9263b4f", null ],
    [ "install_irq", "group__cpu__lpc2387.html#gac0f1e1357620da12703b0f25891fcc1e", null ],
    [ "lpc2387_pclk_scale", "group__cpu__lpc2387.html#gabe9f744242a17c93af9443f84bf74a0a", null ],
    [ "__stack_start", "group__cpu__lpc2387.html#ga4e868ecdedbe3eec729d18c41e534047", null ]
];