var structmsp__usart__t =
[
    [ "BR0", "structmsp__usart__t.html#a48a65d0e14785845ff2a901d115ccdc7", null ],
    [ "BR1", "structmsp__usart__t.html#a8290b75c17af1b5a1c77ecd05857185a", null ],
    [ "CTL", "structmsp__usart__t.html#a979c4c80b59fbf912ed2874a8d0e1de1", null ],
    [ "MCTL", "structmsp__usart__t.html#a1985ac6e6a3e89987952cf3d45ee4847", null ],
    [ "RCTL", "structmsp__usart__t.html#aa8c904356b7bd82ef61e0b5be6ce8a73", null ],
    [ "RXBUF", "structmsp__usart__t.html#a44f385691f0a0e46568747c8fea32877", null ],
    [ "TCTL", "structmsp__usart__t.html#a126457919910aecf30badf3966032f9c", null ],
    [ "TXBUF", "structmsp__usart__t.html#ad7a24b4ffb7cc35f1a94d55661bc7d58", null ]
];