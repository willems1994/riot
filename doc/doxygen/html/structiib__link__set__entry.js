var structiib__link__set__entry =
[
    [ "address_list_head", "structiib__link__set__entry.html#af23296477f1a1e361593a7bf9ac3dcca", null ],
    [ "exp_time", "structiib__link__set__entry.html#acb944fdc9fb9ea7e2ef1bc2e0b73488f", null ],
    [ "heard_time", "structiib__link__set__entry.html#a84a14abeff388d8da659de1e72c15e40", null ],
    [ "last_status", "structiib__link__set__entry.html#a541af68c5fb7c2d72370bc3203e0a347", null ],
    [ "lost", "structiib__link__set__entry.html#ab09bf494bfb61b9608a05abd5a70e65e", null ],
    [ "metric_in", "structiib__link__set__entry.html#ae059a961e69e33833a150b8b5d214c53", null ],
    [ "metric_out", "structiib__link__set__entry.html#a245468e9ec80812e4cbf604ee3a88df6", null ],
    [ "nb_elt", "structiib__link__set__entry.html#abc40b085bbce324396352963256e3d56", null ],
    [ "next", "structiib__link__set__entry.html#a9c51930ba81cd99a4c6ca32646ac4976", null ],
    [ "pending", "structiib__link__set__entry.html#aeb8c8cbf0bde9e47de6280c533429e05", null ],
    [ "sym_time", "structiib__link__set__entry.html#a6e4b39f64eb891ae6296d87a4d2df20a", null ]
];