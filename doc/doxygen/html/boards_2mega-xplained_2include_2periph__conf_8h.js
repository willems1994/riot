var boards_2mega_xplained_2include_2periph__conf_8h =
[
    [ "ADC_NUMOF", "boards_2mega-xplained_2include_2periph__conf_8h.html#a2f0c741db24aa2ccded869ba53f6a302", null ],
    [ "CLOCK_CORECLOCK", "boards_2mega-xplained_2include_2periph__conf_8h.html#afc465f12242e68f6c3695caa3ba0a169", null ],
    [ "I2C_BUS_SPEED", "boards_2mega-xplained_2include_2periph__conf_8h.html#adc86c2905952ae6cdfc595c6cda292d5", null ],
    [ "I2C_NUMOF", "boards_2mega-xplained_2include_2periph__conf_8h.html#abce62e16a6e3b3205801fed93c51692d", null ],
    [ "SPI_NUMOF", "boards_2mega-xplained_2include_2periph__conf_8h.html#ab35a2b79568128efef74adf1ba1910a8", null ],
    [ "TIMER_0", "boards_2mega-xplained_2include_2periph__conf_8h.html#acc9ff9df4a9674a1ce9ba08fc4a4679e", null ],
    [ "TIMER_0_FLAG", "boards_2mega-xplained_2include_2periph__conf_8h.html#af545ab222937696d716fb72704ae6d24", null ],
    [ "TIMER_0_ISRA", "boards_2mega-xplained_2include_2periph__conf_8h.html#a129b4396002353144b8fda00eed52ebf", null ],
    [ "TIMER_0_ISRB", "boards_2mega-xplained_2include_2periph__conf_8h.html#a031b76a3e31c7516ba8ce7470aa48db4", null ],
    [ "TIMER_0_MASK", "boards_2mega-xplained_2include_2periph__conf_8h.html#a0a48dcdd556f07abb4eef03f16778b03", null ],
    [ "TIMER_1", "boards_2mega-xplained_2include_2periph__conf_8h.html#ac62c99c2a9289891c1b83052242cca49", null ],
    [ "TIMER_1_FLAG", "boards_2mega-xplained_2include_2periph__conf_8h.html#a083db0549b9f7416dff3d8e937a7eaa9", null ],
    [ "TIMER_1_ISRA", "boards_2mega-xplained_2include_2periph__conf_8h.html#a10c0c20f45b4237b7aa71662ca12e2af", null ],
    [ "TIMER_1_ISRB", "boards_2mega-xplained_2include_2periph__conf_8h.html#a9720057b4c9f7af9f81ed3c659ea79e8", null ],
    [ "TIMER_1_MASK", "boards_2mega-xplained_2include_2periph__conf_8h.html#a47a524bd523c18d8ff4552cb56812c8f", null ],
    [ "TIMER_NUMOF", "boards_2mega-xplained_2include_2periph__conf_8h.html#a6e2f66f5b6f5c835dd11f9766c4ed897", null ],
    [ "UART_0", "boards_2mega-xplained_2include_2periph__conf_8h.html#a8df7ac6593afa170af7861eeb0157345", null ],
    [ "UART_0_ISR", "boards_2mega-xplained_2include_2periph__conf_8h.html#a713e03d19734d793baee3d1cc25c2dbb", null ],
    [ "UART_1", "boards_2mega-xplained_2include_2periph__conf_8h.html#acf39c144799b38bdc54e9db980b1b29e", null ],
    [ "UART_1_ISR", "boards_2mega-xplained_2include_2periph__conf_8h.html#af9358264b5cbce69dddad098a8600aae", null ],
    [ "UART_NUMOF", "boards_2mega-xplained_2include_2periph__conf_8h.html#a850405f2aaa352ad264346531f0e6230", null ]
];