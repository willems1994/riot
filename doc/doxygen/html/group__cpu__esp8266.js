var group__cpu__esp8266 =
[
    [ "ESP8266 SDK interface", "group__cpu__esp8266__sdk.html", "group__cpu__esp8266__sdk" ],
    [ "cpu/esp8266/include/common.h", "cpu_2esp8266_2include_2common_8h.html", null ],
    [ "esp8266/include/cpu.h", "esp8266_2include_2cpu_8h.html", null ],
    [ "esp8266/include/cpu_conf.h", "esp8266_2include_2cpu__conf_8h.html", null ],
    [ "exceptions.h", "exceptions_8h.html", null ],
    [ "gpio_common.h", "gpio__common_8h.html", null ],
    [ "irq_arch.h", "irq__arch_8h.html", null ],
    [ "esp8266/include/periph_cpu.h", "esp8266_2include_2periph__cpu_8h.html", null ],
    [ "syscalls.h", "syscalls_8h.html", null ],
    [ "thread_arch.h", "thread__arch_8h.html", null ],
    [ "tools.h", "tools_8h.html", null ],
    [ "xtensa_conf.h", "xtensa__conf_8h.html", null ]
];