var group__drivers__pulse__counter =
[
    [ "pulse_counter.h", "pulse__counter_8h.html", null ],
    [ "pulse_counter_params_t", "structpulse__counter__params__t.html", [
      [ "gpio", "structpulse__counter__params__t.html#aa7e097531e7b6ec2af996c5d0c3d8979", null ],
      [ "gpio_flank", "structpulse__counter__params__t.html#ad857e110e5da403b6b412d5acab720c7", null ]
    ] ],
    [ "pulse_counter_t", "structpulse__counter__t.html", [
      [ "pulse_count", "structpulse__counter__t.html#a254b91b19d4ef4aacaab95b9a7121feb", null ]
    ] ],
    [ "pulse_counter_init", "group__drivers__pulse__counter.html#ga54733af1995a24a49a6419ae7a6b7d7a", null ],
    [ "pulse_counter_read_with_reset", "group__drivers__pulse__counter.html#gadd8f5dab5fbf97293ff29b837afeb8f6", null ],
    [ "pulse_counter_read_without_reset", "group__drivers__pulse__counter.html#ga8d87506be159027aac70556c1dc50c63", null ],
    [ "pulse_counter_reset", "group__drivers__pulse__counter.html#gaaad1f05228696abcb6fad40528183d3b", null ]
];