var atmega328p_2include_2periph__cpu_8h =
[
    [ "CPU_ATMEGA_EXT_INTS", "atmega328p_2include_2periph__cpu_8h.html#a33b21f2a315e57318668814f00727415", null ],
    [ "EEPROM_SIZE", "atmega328p_2include_2periph__cpu_8h.html#ae3ef7bba113f663df6996f286b632a3f", null ],
    [ "GPIO_PIN", "atmega328p_2include_2periph__cpu_8h.html#ae29846b3ecd19a0b7c44ff80a37ae7c1", null ],
    [ "I2C_PIN_MASK", "atmega328p_2include_2periph__cpu_8h.html#a00d0bff01337d7e458f49061c4e8bb98", null ],
    [ "I2C_PORT_REG", "atmega328p_2include_2periph__cpu_8h.html#aa3fa75e27eb4ad89cba2748dc486ca86", null ],
    [ "PORT_B", "atmega328p_2include_2periph__cpu_8h.html#a0411cd49bb5b71852cecd93bcbf0ca2da16ada472d473fbd0207b99e9e4d68f4a", null ],
    [ "PORT_C", "atmega328p_2include_2periph__cpu_8h.html#a0411cd49bb5b71852cecd93bcbf0ca2da627cc690c37f97527dd2f07aa22092d9", null ],
    [ "PORT_D", "atmega328p_2include_2periph__cpu_8h.html#a0411cd49bb5b71852cecd93bcbf0ca2daf7242fe75227a46a190645663f91ce69", null ]
];