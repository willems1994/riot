var cc2650stk_2include_2board_8h =
[
    [ "BTN0_MODE", "cc2650stk_2include_2board_8h.html#a904884feea8e03c30c9a85ca6d7a8e5c", null ],
    [ "BTN0_PIN", "cc2650stk_2include_2board_8h.html#aab5c3eca54046333af52593b9e360270", null ],
    [ "BTN1_MODE", "cc2650stk_2include_2board_8h.html#ac792994d9b2d95b9761a05597ddea744", null ],
    [ "BTN1_PIN", "cc2650stk_2include_2board_8h.html#aeb7f5e51d2c7c61ec0a5d5c29591e9b3", null ],
    [ "LED0_OFF", "cc2650stk_2include_2board_8h.html#aef2e39b9fd343d0e437fac0463f3e02d", null ],
    [ "LED0_ON", "cc2650stk_2include_2board_8h.html#a1e48dc02333bfc2e6ad3e5bd466eecd4", null ],
    [ "LED0_PIN", "cc2650stk_2include_2board_8h.html#a3fc522fda449d6edde717d6a857429cf", null ],
    [ "LED0_TOGGLE", "cc2650stk_2include_2board_8h.html#aebc6389533d9fc8dcbe4d2129a4d5a45", null ],
    [ "LED1_OFF", "cc2650stk_2include_2board_8h.html#a343fd2d3ce61b84f88ddfaea32c67c58", null ],
    [ "LED1_ON", "cc2650stk_2include_2board_8h.html#aadd4c7ae0cd4e9bbb17f7055dd51fe08", null ],
    [ "LED1_PIN", "cc2650stk_2include_2board_8h.html#a318aa17e5d40e2132d2c7f6269ce7f51", null ],
    [ "LED1_TOGGLE", "cc2650stk_2include_2board_8h.html#a267fdbba1d750146b73da35c1731fd17", null ],
    [ "XTIMER_BACKOFF", "cc2650stk_2include_2board_8h.html#a370b9e9a079c4cb4f54fd947b67b9f41", null ],
    [ "XTIMER_ISR_BACKOFF", "cc2650stk_2include_2board_8h.html#aa1be564fc21297d7c1c8be267cbd36f6", null ],
    [ "XTIMER_WIDTH", "cc2650stk_2include_2board_8h.html#afea1be2406d45b8fbb1dca1a318ac2dc", null ],
    [ "board_init", "cc2650stk_2include_2board_8h.html#a916f2adc2080b4fe88034086d107a8dc", null ]
];