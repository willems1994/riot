var group__drivers__mtd__native =
[
    [ "mtd_native.h", "mtd__native_8h.html", null ],
    [ "mtd_native_dev", "structmtd__native__dev.html", [
      [ "dev", "structmtd__native__dev.html#a4fce8de5ef6a9723611f86c160a96049", null ],
      [ "fname", "structmtd__native__dev.html#a2e93014dbbba17219605b2b627f58400", null ]
    ] ],
    [ "mtd_native_dev_t", "group__drivers__mtd__native.html#ga6fdc3611427737a53ac9b15c67360cf0", null ],
    [ "native_flash_driver", "group__drivers__mtd__native.html#ga7f03d3fae971552a8a753118f64af34d", null ]
];