var group__sys__arduino__api =
[
    [ "arduino.hpp", "arduino_8hpp.html", null ],
    [ "serialport.hpp", "serialport_8hpp.html", [
      [ "INPUT", "group__sys__arduino__api.html#gga3d01b665f7e924f5f97fa41f5879c7f7ae310c909d76b003d016bef8bdf16936a", null ],
      [ "OUTPUT", "group__sys__arduino__api.html#gga3d01b665f7e924f5f97fa41f5879c7f7a2ab08d3e103968f5f4f26b66a52e99d6", null ],
      [ "INPUT_PULLUP", "group__sys__arduino__api.html#gga3d01b665f7e924f5f97fa41f5879c7f7a851c2ac60276ada62e8d9ba216c7a487", null ],
      [ "LOW", "group__sys__arduino__api.html#gga49b1d57ca8b026018a74c3dcb2779740a6a226f4143ca3b18999551694cdb72a8", null ],
      [ "HIGH", "group__sys__arduino__api.html#gga49b1d57ca8b026018a74c3dcb2779740a0c3a1dacf94061154b3ee354359c5893", null ]
    ] ],
    [ "analogRead", "group__sys__arduino__api.html#ga6a005b7bdfeeca9d1d61b38929f34c58", null ],
    [ "delay", "group__sys__arduino__api.html#ga6e3106f365121b6d5c36bedcc2ce2473", null ],
    [ "digitalRead", "group__sys__arduino__api.html#gae1e3d979645a535a8ce49f935392d8bc", null ],
    [ "digitalWrite", "group__sys__arduino__api.html#gad04318e746cb94b26e94e9aacca0b207", null ],
    [ "pinMode", "group__sys__arduino__api.html#gac7bdb53335528ad073ca13eb2b1bdc00", null ],
    [ "Serial", "group__sys__arduino__api.html#gafddb03e428e69583abe4bd0e60f63ed7", null ]
];