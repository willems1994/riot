var boards_2arduino_mkrzero_2include_2board_8h =
[
    [ "CARD_DETECT_PIN", "boards_2arduino-mkrzero_2include_2board_8h.html#a599933528ed62f08141075b6546dca5f", null ],
    [ "LED0_MASK", "boards_2arduino-mkrzero_2include_2board_8h.html#abfabde35a3e8b7ab50146a5e223bfead", null ],
    [ "LED0_OFF", "boards_2arduino-mkrzero_2include_2board_8h.html#aef2e39b9fd343d0e437fac0463f3e02d", null ],
    [ "LED0_ON", "boards_2arduino-mkrzero_2include_2board_8h.html#a1e48dc02333bfc2e6ad3e5bd466eecd4", null ],
    [ "LED0_PIN", "boards_2arduino-mkrzero_2include_2board_8h.html#a3fc522fda449d6edde717d6a857429cf", null ],
    [ "LED0_TOGGLE", "boards_2arduino-mkrzero_2include_2board_8h.html#aebc6389533d9fc8dcbe4d2129a4d5a45", null ],
    [ "LED_PORT", "boards_2arduino-mkrzero_2include_2board_8h.html#a663daa01e565aee93c6f20c5845b90b4", null ],
    [ "SDCARD_SPI_PARAM_CLK", "boards_2arduino-mkrzero_2include_2board_8h.html#a310f30aa4d25f868559b683ac025ad8a", null ],
    [ "SDCARD_SPI_PARAM_CS", "boards_2arduino-mkrzero_2include_2board_8h.html#a98bbf84fda3506835da403b773e5bedf", null ],
    [ "SDCARD_SPI_PARAM_MISO", "boards_2arduino-mkrzero_2include_2board_8h.html#aafff7922492a49535a39a7f55b87fd59", null ],
    [ "SDCARD_SPI_PARAM_MOSI", "boards_2arduino-mkrzero_2include_2board_8h.html#aaf353c6fca5fc34012a4a0ce7fd1270b", null ],
    [ "SDCARD_SPI_PARAM_POWER", "boards_2arduino-mkrzero_2include_2board_8h.html#a1b39ba3680695d6a0197bf66295fb7ec", null ],
    [ "SDCARD_SPI_PARAM_POWER_AH", "boards_2arduino-mkrzero_2include_2board_8h.html#ac519e2e5309f85391ecd5b5f8394ae77", null ],
    [ "SDCARD_SPI_PARAM_SPI", "boards_2arduino-mkrzero_2include_2board_8h.html#a41cbf87bcb5db3d76e7bce9db6718437", null ]
];