var ethos_8h =
[
    [ "ETHOS_ESC_CHAR", "group__drivers__ethos.html#ga9bd86104008297873108bde98d624775", null ],
    [ "ETHOS_FRAME_DELIMITER", "group__drivers__ethos.html#ga66fc07093d1d6202a35b5ffbddc15592", null ],
    [ "ETHOS_FRAME_TYPE_DATA", "group__drivers__ethos.html#ga4b642eebc0ea14227661c9ce11bbc547", null ],
    [ "ETHOS_FRAME_TYPE_HELLO", "group__drivers__ethos.html#gad841d59a5d0fe4fb8081b86f31c037f7", null ],
    [ "ETHOS_FRAME_TYPE_HELLO_REPLY", "group__drivers__ethos.html#ga36792022b48ef7c37af077ade587a1ae", null ],
    [ "ETHOS_FRAME_TYPE_TEXT", "group__drivers__ethos.html#gadaabdad5add96f2db19244a130f76418", null ],
    [ "line_state_t", "group__drivers__ethos.html#ga55f4253a9885dff9bf3ab4e3131b5e45", [
      [ "WAIT_FRAMESTART", "group__drivers__ethos.html#gga55f4253a9885dff9bf3ab4e3131b5e45a6f298e93f843e74fd0c792baf1946b8b", null ],
      [ "IN_FRAME", "group__drivers__ethos.html#gga55f4253a9885dff9bf3ab4e3131b5e45a1f7b785b4904afe9d3db41381be4f1c7", null ],
      [ "IN_ESCAPE", "group__drivers__ethos.html#gga55f4253a9885dff9bf3ab4e3131b5e45a06a95289c6f7d2a909063f4c6f153171", null ]
    ] ],
    [ "ethos_send_frame", "group__drivers__ethos.html#gadb9bc46ab8a39cc605210402c12fa2b8", null ],
    [ "ethos_setup", "group__drivers__ethos.html#ga5e025d81f8e7863ca7e7d009f1a0eb07", null ]
];