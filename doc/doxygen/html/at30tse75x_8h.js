var at30tse75x_8h =
[
    [ "AT30TSE75X_BUS_FREE_TIME_US", "group__drivers__at30tse75x.html#gaa84a9eacfbe8ba71eb78874409a7b614", null ],
    [ "AT30TSE75X_CMD__GENERAL_CALL_RELATCH", "group__drivers__at30tse75x.html#ga3a79a8d5a241ef2695da7f7c3bebb470", null ],
    [ "AT30TSE75X_CMD__GENERAL_CALL_RESET", "group__drivers__at30tse75x.html#ga0e523b73096e3d0681b30eebbcd5bbb2", null ],
    [ "AT30TSE75X_CMD__RESTORE_FROM_NVRAM", "group__drivers__at30tse75x.html#gaa60544c435461dbc378be839096e1da5", null ],
    [ "AT30TSE75X_CMD__SAVE_TO_NVRAM", "group__drivers__at30tse75x.html#ga1c1b9552138b2066ed0f7f765633b29c", null ],
    [ "AT30TSE75X_CONFIG__ALARM_MODE_BIT", "group__drivers__at30tse75x.html#gabf7504df577d314b99cf87e4e0745e5e", null ],
    [ "AT30TSE75X_CONFIG__ALERT_POL_BIT", "group__drivers__at30tse75x.html#ga3d022e71ab7a80d340ee84f82fea0a8d", null ],
    [ "AT30TSE75X_CONFIG__FTQ_MASK", "group__drivers__at30tse75x.html#ga798d017eadd47077061e1036ea9d59fe", null ],
    [ "AT30TSE75X_CONFIG__FTQ_SHIFT", "group__drivers__at30tse75x.html#ga37b2a4cd7fcaa00ea39533817329507a", null ],
    [ "AT30TSE75X_CONFIG__OS_BIT", "group__drivers__at30tse75x.html#ga33a0a2c7732143e26dc922a6cabc63d7", null ],
    [ "AT30TSE75X_CONFIG__RESOLUTION_MASK", "group__drivers__at30tse75x.html#ga89765063ad04b6247de1968662d3c40f", null ],
    [ "AT30TSE75X_CONFIG__RESOLUTION_SHIFT", "group__drivers__at30tse75x.html#ga0b131e8d560cdee08718f28ed3ddf956", null ],
    [ "AT30TSE75X_CONFIG__SHUTDOWN_BIT", "group__drivers__at30tse75x.html#ga8cf0c1efbf2c96a951791698cc7baf2c", null ],
    [ "AT30TSE75X_EEPROM_ADDR", "group__drivers__at30tse75x.html#ga97068f74f39125e04ffa3c00e63384c6", null ],
    [ "AT30TSE75X_FRACTIONAL_BASE", "group__drivers__at30tse75x.html#ga78346fc6d30d37cde24ae09c6ab8e1a7", null ],
    [ "AT30TSE75X_FRACTIONAL_MASK", "group__drivers__at30tse75x.html#ga1b1fe32e4cd1506d045ca4b6bbfd8eda", null ],
    [ "AT30TSE75X_FRACTIONAL_SHIFT", "group__drivers__at30tse75x.html#ga9a430b60b6509a9e6bd5340f9dddee93", null ],
    [ "AT30TSE75X_INTEGER_MASK", "group__drivers__at30tse75x.html#ga239d799715a54adec9bb35c532b8a86e", null ],
    [ "AT30TSE75X_INTEGER_SHIFT", "group__drivers__at30tse75x.html#gacd359a30b7f5744e1e48037de0bcbb53", null ],
    [ "AT30TSE75X_REG__CONFIG", "group__drivers__at30tse75x.html#ga6e55f9844a939d683d3ab42355502d57", null ],
    [ "AT30TSE75X_REG__LIMIT_HIGH", "group__drivers__at30tse75x.html#ga4747405f3185fa48126fcde8370eee1c", null ],
    [ "AT30TSE75X_REG__LIMIT_LOW", "group__drivers__at30tse75x.html#gaaef717bedc40289f9b852d52386e9eed", null ],
    [ "AT30TSE75X_REG__NV_CONFIG", "group__drivers__at30tse75x.html#ga5d2be21fe1a1de27bcffaef89958c926", null ],
    [ "AT30TSE75X_REG__NV_LIMIR_HIGH", "group__drivers__at30tse75x.html#ga136fb32d06552a88a8aedfa448539967", null ],
    [ "AT30TSE75X_REG__NV_LIMIT_LOW", "group__drivers__at30tse75x.html#gae8848f4648c1ee63d3f2ae5bfcdf0401", null ],
    [ "AT30TSE75X_REG__TEMPERATURE", "group__drivers__at30tse75x.html#ga09acfe0ae724f8d96dfe1a800400e311", null ],
    [ "AT30TSE75X_TEMP_ADDR", "group__drivers__at30tse75x.html#ga600826104cf879dc4b3531875981f657", null ],
    [ "at30tse75x_alarm_polatity_t", "group__drivers__at30tse75x.html#ga5f702e73082112cd7dae9a2e336fe1de", [
      [ "AT30TSE75X_ALARM_ACTIVE_LOW", "group__drivers__at30tse75x.html#gga5f702e73082112cd7dae9a2e336fe1dea86150310cb567168a02a3731ae4f3e7b", null ],
      [ "AT30TSE75X_ALARM_ACTIVE_HIGH", "group__drivers__at30tse75x.html#gga5f702e73082112cd7dae9a2e336fe1dea7dd19d0355c841c67b024cbd11bae3ab", null ]
    ] ],
    [ "at30tse75x_fault_tolerance_t", "group__drivers__at30tse75x.html#ga995ed7ef8bd331621851f376d8bfc0b0", [
      [ "AT30TSE75X_ALARM_AFTER_1", "group__drivers__at30tse75x.html#gga995ed7ef8bd331621851f376d8bfc0b0a6793552f181dc97483f24ee92b95eeeb", null ],
      [ "AT30TSE75X_ALARM_AFTER_2", "group__drivers__at30tse75x.html#gga995ed7ef8bd331621851f376d8bfc0b0adae9c2fb13ca3aca0931502c65adc148", null ],
      [ "AT30TSE75X_ALARM_AFTER_4", "group__drivers__at30tse75x.html#gga995ed7ef8bd331621851f376d8bfc0b0abacc0a982eac72575eb14edef6b40dab", null ],
      [ "AT30TSE75X_ALARM_AFTER_6", "group__drivers__at30tse75x.html#gga995ed7ef8bd331621851f376d8bfc0b0a34965ec1c1b02e06eb0e96fbe6b5bda3", null ]
    ] ],
    [ "at30tse75x_mode_t", "group__drivers__at30tse75x.html#gaf2e8b3e16188ace5b23d750b48a6f605", [
      [ "AT30TSE75X_MODE_COMPARATOR", "group__drivers__at30tse75x.html#ggaf2e8b3e16188ace5b23d750b48a6f605a1cc7fd62746e51890545e5e735c1263d", null ],
      [ "AT30TSE75X_MODE_INTERRUPT", "group__drivers__at30tse75x.html#ggaf2e8b3e16188ace5b23d750b48a6f605a1372b57bda0ef8760b575f62e2af7fea", null ],
      [ "AT30TSE75X_MODE_ONE_SHOT", "group__drivers__at30tse75x.html#ggaf2e8b3e16188ace5b23d750b48a6f605a1c61ea07820bcf2e57d2158093afc14b", null ]
    ] ],
    [ "at30tse75x_resolution_t", "group__drivers__at30tse75x.html#gafeadcd21593d24469ca9a993ccc61b53", [
      [ "AT30TSE75X_RESOLUTION_9BIT", "group__drivers__at30tse75x.html#ggafeadcd21593d24469ca9a993ccc61b53a39dde28a56f937664ecf8acd46fdc7a0", null ],
      [ "AT30TSE75X_RESOLUTION_10BIT", "group__drivers__at30tse75x.html#ggafeadcd21593d24469ca9a993ccc61b53af025336399ef70e6b7d1df080fe303bb", null ],
      [ "AT30TSE75X_RESOLUTION_11BIT", "group__drivers__at30tse75x.html#ggafeadcd21593d24469ca9a993ccc61b53a6b6c443c684f6361a37efc22f4d4e921", null ],
      [ "AT30TSE75X_RESOLUTION_12BIT", "group__drivers__at30tse75x.html#ggafeadcd21593d24469ca9a993ccc61b53a72e4cf4e2de00c52e275d66bda9cccc0", null ]
    ] ],
    [ "at30tse75x_get_config", "group__drivers__at30tse75x.html#gab3cc192d34877673efaf0445867ee722", null ],
    [ "at30tse75x_get_temperature", "group__drivers__at30tse75x.html#ga5139c4b3f6a4c138fd96179f39b0224d", null ],
    [ "at30tse75x_init", "group__drivers__at30tse75x.html#gae202f1f561b097db8bbf516ac49d1cb0", null ],
    [ "at30tse75x_restore_config", "group__drivers__at30tse75x.html#gae222355cd32dba4d627a9b9d382db514", null ],
    [ "at30tse75x_save_config", "group__drivers__at30tse75x.html#ga7ed65a164f928fa9da0fa5bd5d0e7277", null ],
    [ "at30tse75x_set_alarm_polarity", "group__drivers__at30tse75x.html#gad12b8bcf52cdd799befc018ad24491a1", null ],
    [ "at30tse75x_set_config", "group__drivers__at30tse75x.html#ga6eb10dbbe138e0507bf54a8470f9bb72", null ],
    [ "at30tse75x_set_fault_tolerance", "group__drivers__at30tse75x.html#gaccc2c0e7b0f6857c813cacdc5c3db0b7", null ],
    [ "at30tse75x_set_limit_high", "group__drivers__at30tse75x.html#gaf35d6c719afa9784168e970dbb5e9b46", null ],
    [ "at30tse75x_set_limit_low", "group__drivers__at30tse75x.html#ga349afd43999be83ffd8c08a7d83bc97b", null ],
    [ "at30tse75x_set_mode", "group__drivers__at30tse75x.html#ga84ddf449aab7ec99473bc206f9d563c3", null ],
    [ "at30tse75x_set_resolution", "group__drivers__at30tse75x.html#gab7e84f48c09566945c3c0e62e90f7248", null ]
];