var group__drivers__mtd__sdcard =
[
    [ "mtd_sdcard.h", "mtd__sdcard_8h.html", null ],
    [ "mtd_sdcard_t", "structmtd__sdcard__t.html", [
      [ "base", "structmtd__sdcard__t.html#a5031f36eda4f6a2d654e525fde1e63f6", null ],
      [ "params", "structmtd__sdcard__t.html#a074fdf961968bd58642c821c23ab26c8", null ],
      [ "sd_card", "structmtd__sdcard__t.html#a9a4ad97f8f094b7dd7805394513be394", null ]
    ] ],
    [ "MTD_SDCARD_SKIP_ERASE", "group__drivers__mtd__sdcard.html#ga5531695a2ca0b3a62e3820e36b26c27b", null ],
    [ "mtd_sdcard_driver", "group__drivers__mtd__sdcard.html#gae9ab7f348989c05ad636dfd5c5024ebc", null ]
];