var structcan =
[
    [ "af", "structcan.html#aafa77df536ce8151811e7c59cc1e1871", null ],
    [ "candev", "structcan.html#a85b10fb35e23e87ee7b6243894d07f8a", null ],
    [ "conf", "structcan.html#a7cfeb7401dcd71b2eda8532edd36c8af", null ],
    [ "isr_flags", "structcan.html#a5ac58679913a20a72978123034d7aedf", null ],
    [ "rx_fifo", "structcan.html#a3f6ce109c1c5647dde9c670fc954e639", null ],
    [ "rx_pin", "structcan.html#a76a2d8cf476fc3e17f9174c51754e5fb", null ],
    [ "tx_mailbox", "structcan.html#a1b316a4220fb242b76de916b8fb19c45", null ],
    [ "tx_pin", "structcan.html#a9229d4e0a30fb6adfbd75af4e7b2d237", null ]
];