var structnetdev__ieee802154__t =
[
    [ "chan", "structnetdev__ieee802154__t.html#a8b7b2c7732c22a9f3740555f6b3333c2", null ],
    [ "flags", "structnetdev__ieee802154__t.html#a9696c92033a8f663bc2e11239924ec35", null ],
    [ "long_addr", "structnetdev__ieee802154__t.html#a0b59bd3613a1db42dcdc0315a7092378", null ],
    [ "netdev", "structnetdev__ieee802154__t.html#a6840d8f87cf087deb58b1ff7334ea622", null ],
    [ "pan", "structnetdev__ieee802154__t.html#a2d809f49131c9d2e6f7f67370ada6dfe", null ],
    [ "seq", "structnetdev__ieee802154__t.html#a203eea596910c7f3f446a63f3d02127f", null ],
    [ "short_addr", "structnetdev__ieee802154__t.html#a3a8b2b97d33e54cae8c5fb8decf49480", null ]
];