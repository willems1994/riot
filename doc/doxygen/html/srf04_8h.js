var srf04_8h =
[
    [ "SRF04_OK", "group__drivers__srf04.html#gga14061dd054afd472999a4f4f6cdf3923a63ddce164b6f69ef490dd74916912753", null ],
    [ "SRF04_ERR_INVALID", "group__drivers__srf04.html#gga14061dd054afd472999a4f4f6cdf3923a003a25e7dcff32442773c5018a941df7", null ],
    [ "SRF04_ERR_MEASURING", "group__drivers__srf04.html#gga14061dd054afd472999a4f4f6cdf3923a5e34ff890f2daba39236cdb695c0075b", null ],
    [ "SRF04_ERR_GPIO", "group__drivers__srf04.html#gga14061dd054afd472999a4f4f6cdf3923a9ba19fb2f8572b39350b7be588296679", null ],
    [ "srf04_get_distance", "group__drivers__srf04.html#gaa437bcf6f4f16be0543760a6670e9e0a", null ],
    [ "srf04_init", "group__drivers__srf04.html#gad24fd83c4e3aa5ec320ead31e68c8299", null ],
    [ "srf04_read", "group__drivers__srf04.html#ga3901854d25bcf9ce952e03b8760041d6", null ],
    [ "srf04_trigger", "group__drivers__srf04.html#ga2a634ea644e25adaf7fb3afc1e0963aa", null ]
];