var group__net__gnrc__pktbuf =
[
    [ "Packet", "group__net__gnrc__pkt.html", "group__net__gnrc__pkt" ],
    [ "pktbuf.h", "pktbuf_8h.html", null ],
    [ "GNRC_PKTBUF_SIZE", "group__net__gnrc__pktbuf.html#ga83a14b0588bb8adc93b565c4ebc5ec56", null ],
    [ "gnrc_pktbuf_add", "group__net__gnrc__pktbuf.html#ga658aed0ce2b31d784e32849eb0f60d27", null ],
    [ "gnrc_pktbuf_duplicate_upto", "group__net__gnrc__pktbuf.html#gaa4f5c5797d70aa279c75742e3421124e", null ],
    [ "gnrc_pktbuf_get_iovec", "group__net__gnrc__pktbuf.html#ga81791c69631ebb03494d93cae48b4bfb", null ],
    [ "gnrc_pktbuf_hold", "group__net__gnrc__pktbuf.html#gac679dd478531a503852093f4c044c65c", null ],
    [ "gnrc_pktbuf_init", "group__net__gnrc__pktbuf.html#gaab1dff47c0eecdad43f511425e1341e7", null ],
    [ "gnrc_pktbuf_is_empty", "group__net__gnrc__pktbuf.html#ga3c89a6668a1fae81ff8a37f011fadd12", null ],
    [ "gnrc_pktbuf_is_sane", "group__net__gnrc__pktbuf.html#ga382dff5b57c2954c7dffbf3b37b4eee3", null ],
    [ "gnrc_pktbuf_mark", "group__net__gnrc__pktbuf.html#ga3d7c5274101df3fa6eff69edf46f347f", null ],
    [ "gnrc_pktbuf_realloc_data", "group__net__gnrc__pktbuf.html#ga3d9a8b1307f8971680299b2f86ac4a3e", null ],
    [ "gnrc_pktbuf_release", "group__net__gnrc__pktbuf.html#gaed632f849e2ae54b8ec22990967ca38a", null ],
    [ "gnrc_pktbuf_release_error", "group__net__gnrc__pktbuf.html#ga39c4489933328ddabcf87d5940df9807", null ],
    [ "gnrc_pktbuf_remove_snip", "group__net__gnrc__pktbuf.html#gafbb055c2d0ef263bf0ed38b392761719", null ],
    [ "gnrc_pktbuf_replace_snip", "group__net__gnrc__pktbuf.html#ga2bd457bcdcaac91fbe6b7ab5723ce68f", null ],
    [ "gnrc_pktbuf_start_write", "group__net__gnrc__pktbuf.html#ga640418467294ae3d408c109ab27bd617", null ],
    [ "gnrc_pktbuf_stats", "group__net__gnrc__pktbuf.html#gaf4e7cccaa0d94f9ccaefdceab382d396", null ]
];