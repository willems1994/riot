var irq__arch_8h =
[
    [ "critical_enter", "irq__arch_8h.html#a7abb705243ee9376d1298135decb6837", null ],
    [ "critical_exit", "irq__arch_8h.html#a19f972cb9d5c72a54f02eb0d95c347d6", null ],
    [ "irq_isr_enter", "irq__arch_8h.html#adff90dc1d9e58d6a3f4ac2fd65b04f7f", null ],
    [ "irq_isr_exit", "irq__arch_8h.html#a7ed497c5be8b190a104049d038f3293b", null ],
    [ "irq_interrupt_nesting", "irq__arch_8h.html#af00c70dea9bc3103a3777abdc69729a1", null ]
];