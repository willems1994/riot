var saul__reg_8h =
[
    [ "saul_reg_t", "group__sys__saul__reg.html#gace9930d1aa84c5856a998c4e68f3f791", null ],
    [ "saul_reg_add", "group__sys__saul__reg.html#gaca103e10c65ee97d26b50bae032087ea", null ],
    [ "saul_reg_find_name", "group__sys__saul__reg.html#gad882ba47657b5721a76573a42bdd33d5", null ],
    [ "saul_reg_find_nth", "group__sys__saul__reg.html#ga24a911a5ce3165b0d2c3591de911d583", null ],
    [ "saul_reg_find_type", "group__sys__saul__reg.html#ga0adb4c2fe6dedb54f586e88f25e2041a", null ],
    [ "saul_reg_read", "group__sys__saul__reg.html#gae307c1e37be5eec30cd62ac65cf08192", null ],
    [ "saul_reg_rm", "group__sys__saul__reg.html#ga860a4f9194397ad962a5547b32374874", null ],
    [ "saul_reg_write", "group__sys__saul__reg.html#ga1f07f9a71039815dfc36ede25fccb0a8", null ],
    [ "saul_reg", "group__sys__saul__reg.html#ga813859c105124f5f7f9ab660889029e8", null ]
];