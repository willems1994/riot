var sodaq_autonomo_2include_2board_8h =
[
    [ "LED0_MASK", "sodaq-autonomo_2include_2board_8h.html#abfabde35a3e8b7ab50146a5e223bfead", null ],
    [ "LED0_OFF", "sodaq-autonomo_2include_2board_8h.html#aef2e39b9fd343d0e437fac0463f3e02d", null ],
    [ "LED0_ON", "sodaq-autonomo_2include_2board_8h.html#a1e48dc02333bfc2e6ad3e5bd466eecd4", null ],
    [ "LED0_PIN", "sodaq-autonomo_2include_2board_8h.html#a3fc522fda449d6edde717d6a857429cf", null ],
    [ "LED0_TOGGLE", "sodaq-autonomo_2include_2board_8h.html#aebc6389533d9fc8dcbe4d2129a4d5a45", null ],
    [ "LED_PORT", "sodaq-autonomo_2include_2board_8h.html#a663daa01e565aee93c6f20c5845b90b4", null ],
    [ "XTIMER", "sodaq-autonomo_2include_2board_8h.html#a5493328d801f3a1540ea42b62fbb72f1", null ],
    [ "XTIMER_CHAN", "sodaq-autonomo_2include_2board_8h.html#a8b747b85d4d5f2e1be910cdbc72a01de", null ],
    [ "board_init", "sodaq-autonomo_2include_2board_8h.html#a916f2adc2080b4fe88034086d107a8dc", null ]
];