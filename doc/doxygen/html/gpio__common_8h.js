var gpio__common_8h =
[
    [ "_gpio_pin_usage_t", "gpio__common_8h.html#ab9db7083d286820e3512ad73a434c477", [
      [ "_GPIO", "gpio__common_8h.html#ab9db7083d286820e3512ad73a434c477afcbb35fb1a160b237d2c9fb71fb0ec59", null ],
      [ "_I2C", "gpio__common_8h.html#ab9db7083d286820e3512ad73a434c477a0ac404c7b8c632d260cf878083c89dfb", null ],
      [ "_PWM", "gpio__common_8h.html#ab9db7083d286820e3512ad73a434c477aca02f1f0a8f5d3ec805ac75def0f84a0", null ],
      [ "_SPI", "gpio__common_8h.html#ab9db7083d286820e3512ad73a434c477aaedd000faf92f9f0319527b56116a6b5", null ],
      [ "_SPIF", "gpio__common_8h.html#ab9db7083d286820e3512ad73a434c477a809c93e983b2facaeb7225e4c0070edb", null ],
      [ "_UART", "gpio__common_8h.html#ab9db7083d286820e3512ad73a434c477a81edaa33040c79af51a328da4c4866e9", null ]
    ] ],
    [ "_gpio_pin_usage", "gpio__common_8h.html#a593534088c8ff014fab546df39a0e626", null ],
    [ "_gpio_to_iomux", "gpio__common_8h.html#a34cb569a20861bc1547ce0f7abf64921", null ],
    [ "_iomux_to_gpio", "gpio__common_8h.html#adbdc289712110c4841a46b40314a3bc3", null ]
];