var tsl2561_8h =
[
    [ "TSL2561_ADDR_FLOAT", "group__drivers__tsl2561.html#gac7cc83a3f07e3073fe2c17deb84e8cfc", null ],
    [ "TSL2561_ADDR_HIGH", "group__drivers__tsl2561.html#ga165fdc75d9a1583616bc4b0527080869", null ],
    [ "TSL2561_ADDR_LOW", "group__drivers__tsl2561.html#gaffb49bd7afa3145eb9b988cc3bdd7ff4", null ],
    [ "TSL2561_BADDEV", "group__drivers__tsl2561.html#gaa06c229e9f4558a873227ade541a525c", null ],
    [ "TSL2561_GAIN_16X", "group__drivers__tsl2561.html#ga6219dd0517092a6373b872dbdf2048e3", null ],
    [ "TSL2561_GAIN_1X", "group__drivers__tsl2561.html#ga8eaca9622ca726354e35cf49ca7cb9d2", null ],
    [ "TSL2561_INTEGRATIONTIME_101MS", "group__drivers__tsl2561.html#ga4bedca93c34f4e256be090b23cb630b4", null ],
    [ "TSL2561_INTEGRATIONTIME_13MS", "group__drivers__tsl2561.html#ga811282fea43ee5ff081789ec02506e62", null ],
    [ "TSL2561_INTEGRATIONTIME_402MS", "group__drivers__tsl2561.html#ga7d909f235c38212a515af985d3e534fa", null ],
    [ "TSL2561_INTEGRATIONTIME_NA", "group__drivers__tsl2561.html#ga2d8801132c3160383f8b73132081c816", null ],
    [ "TSL2561_NOI2C", "group__drivers__tsl2561.html#gaadb8a1f5efd8e5f3a78d95d381d4a5d5", null ],
    [ "TSL2561_OK", "group__drivers__tsl2561.html#ga0a11f100b4c61de6511fef520e759d18", null ],
    [ "tsl2561_init", "group__drivers__tsl2561.html#ga35236b4ecbf7935554f847e4e07d803a", null ],
    [ "tsl2561_read_illuminance", "group__drivers__tsl2561.html#gae6c28514b64828d1ff49bdbbb6e192ce", null ]
];