var group__cpu__mips32r2__common =
[
    [ "eic_irq.h", "eic__irq_8h.html", null ],
    [ "EIC_IRQ_TIMER", "group__cpu__mips32r2__common.html#gaf55632dd6d7ee7d01ac00724baf4205c", null ],
    [ "eic_irq_ack", "group__cpu__mips32r2__common.html#ga84864dabc2bc48282a20bbb373cc2709", null ],
    [ "eic_irq_configure", "group__cpu__mips32r2__common.html#ga164fdaea933e09430b8c3481bd877d65", null ],
    [ "eic_irq_disable", "group__cpu__mips32r2__common.html#ga06e65db8198e7b12f35c057addc22870", null ],
    [ "eic_irq_enable", "group__cpu__mips32r2__common.html#ga9db939e543b6e3f535d74bbc87d7fc04", null ]
];