var teensy31_2include_2board_8h =
[
    [ "DISABLE_WDOG", "teensy31_2include_2board_8h.html#a93c87c5f0e67ed3de661632f8b54d803", null ],
    [ "LED0_BIT", "teensy31_2include_2board_8h.html#aad3eba5c40740281860f62c5a84527eb", null ],
    [ "LED0_OFF", "teensy31_2include_2board_8h.html#aef2e39b9fd343d0e437fac0463f3e02d", null ],
    [ "LED0_ON", "teensy31_2include_2board_8h.html#a1e48dc02333bfc2e6ad3e5bd466eecd4", null ],
    [ "LED0_PIN", "teensy31_2include_2board_8h.html#a3fc522fda449d6edde717d6a857429cf", null ],
    [ "LED0_TOGGLE", "teensy31_2include_2board_8h.html#aebc6389533d9fc8dcbe4d2129a4d5a45", null ],
    [ "LED_PORT", "teensy31_2include_2board_8h.html#a663daa01e565aee93c6f20c5845b90b4", null ],
    [ "LPTIMER_CLKSRC", "teensy31_2include_2board_8h.html#a9f184bcd17e87770b5f0bdbb32d43447", null ],
    [ "XTIMER_BACKOFF", "teensy31_2include_2board_8h.html#a370b9e9a079c4cb4f54fd947b67b9f41", null ],
    [ "XTIMER_CHAN", "teensy31_2include_2board_8h.html#a8b747b85d4d5f2e1be910cdbc72a01de", null ],
    [ "XTIMER_DEV", "teensy31_2include_2board_8h.html#a5e48bb301c732e044b08f336fb851d5e", null ],
    [ "XTIMER_ISR_BACKOFF", "teensy31_2include_2board_8h.html#aa1be564fc21297d7c1c8be267cbd36f6", null ],
    [ "XTIMER_OVERHEAD", "teensy31_2include_2board_8h.html#a6692048ab318695696ecf54dc4cc04be", null ],
    [ "board_init", "teensy31_2include_2board_8h.html#a916f2adc2080b4fe88034086d107a8dc", null ]
];