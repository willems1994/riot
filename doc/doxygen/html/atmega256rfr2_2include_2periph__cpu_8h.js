var atmega256rfr2_2include_2periph__cpu_8h =
[
    [ "CPU_ATMEGA_EXT_INTS", "atmega256rfr2_2include_2periph__cpu_8h.html#a33b21f2a315e57318668814f00727415", null ],
    [ "CPUID_LEN", "atmega256rfr2_2include_2periph__cpu_8h.html#a1943715eaeaa63e28b7b4e207f655fca", null ],
    [ "EEPROM_SIZE", "atmega256rfr2_2include_2periph__cpu_8h.html#ae3ef7bba113f663df6996f286b632a3f", null ],
    [ "GPIO_UNDEF", "atmega256rfr2_2include_2periph__cpu_8h.html#a3969ce1e494a72d3c2925b10ddeb4604", null ],
    [ "I2C_PIN_MASK", "atmega256rfr2_2include_2periph__cpu_8h.html#a00d0bff01337d7e458f49061c4e8bb98", null ],
    [ "I2C_PORT_REG", "atmega256rfr2_2include_2periph__cpu_8h.html#aa3fa75e27eb4ad89cba2748dc486ca86", null ],
    [ "PORT_B", "atmega256rfr2_2include_2periph__cpu_8h.html#a726ca809ffd3d67ab4b8476646f26635a16ada472d473fbd0207b99e9e4d68f4a", null ],
    [ "PORT_D", "atmega256rfr2_2include_2periph__cpu_8h.html#a726ca809ffd3d67ab4b8476646f26635af7242fe75227a46a190645663f91ce69", null ],
    [ "PORT_E", "atmega256rfr2_2include_2periph__cpu_8h.html#a726ca809ffd3d67ab4b8476646f26635abad63f022d1fa37a66f87dc31a78f6a9", null ],
    [ "PORT_F", "atmega256rfr2_2include_2periph__cpu_8h.html#a726ca809ffd3d67ab4b8476646f26635aa3760b302740c7d09c93ec7a634f837c", null ],
    [ "PORT_G", "atmega256rfr2_2include_2periph__cpu_8h.html#a726ca809ffd3d67ab4b8476646f26635a48afb424254d52e7d97a7c1428f5aafa", null ]
];