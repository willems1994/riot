var gnrc__sock__internal_8h =
[
    [ "GNRC_SOCK_DYN_PORTRANGE_ERR", "group__net__gnrc__sock.html#ga35a76218705c511962dbc26ea4b4c0d4", null ],
    [ "GNRC_SOCK_DYN_PORTRANGE_MAX", "group__net__gnrc__sock.html#ga07323eb4a2bf30cca04cba662c8c0436", null ],
    [ "GNRC_SOCK_DYN_PORTRANGE_MIN", "group__net__gnrc__sock.html#ga4b781c28930aee57210196299f9a73aa", null ],
    [ "GNRC_SOCK_DYN_PORTRANGE_NUM", "group__net__gnrc__sock.html#ga40b8e38475486103799cb08b5886ec5a", null ],
    [ "GNRC_SOCK_DYN_PORTRANGE_OFF", "group__net__gnrc__sock.html#ga5889b0920d48c79e6e9e77ff8e309ee4", null ],
    [ "gnrc_af_not_supported", "group__net__gnrc__sock.html#gaa69a591d4be68618a69df17610086aad", null ],
    [ "gnrc_ep_addr_any", "group__net__gnrc__sock.html#ga8ba9598591e07f49dae165b04119a5fd", null ],
    [ "gnrc_ep_set", "group__net__gnrc__sock.html#gaafc7cbf3e1bbe97d0599177dff8bff4c", null ],
    [ "gnrc_sock_create", "group__net__gnrc__sock.html#gaedd1d80e6b1250eeb32fddfdf23a7968", null ],
    [ "gnrc_sock_recv", "group__net__gnrc__sock.html#ga9f9583cff00c0ea57694097880c4f750", null ],
    [ "gnrc_sock_send", "group__net__gnrc__sock.html#gaa66a462ccc7944a7d4a14dd652005cc3", null ]
];