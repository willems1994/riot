var portrange_8h =
[
    [ "IANA_DYNAMIC_PORTRANGE_MAX", "group__net__iana__portrange.html#ga4776804382c105a56b6d812b86ff3e58", null ],
    [ "IANA_DYNAMIC_PORTRANGE_MIN", "group__net__iana__portrange.html#gabf43d0446593d2bfe147c51aed39067b", null ],
    [ "IANA_SYSTEM_PORTRANGE_MAX", "group__net__iana__portrange.html#ga91a574688c462f80ed1f4f27a71d610d", null ],
    [ "IANA_SYSTEM_PORTRANGE_MIN", "group__net__iana__portrange.html#ga02ac2e64b581869401e578c0fd9c7cf7", null ],
    [ "IANA_USER_PORTRANGE_MAX", "group__net__iana__portrange.html#gac1728e797dac23205129ea5abf4ed53c", null ],
    [ "IANA_USER_PORTRANGE_MIN", "group__net__iana__portrange.html#ga4af38d76cdb9c4c797aae7c00338f5b0", null ]
];