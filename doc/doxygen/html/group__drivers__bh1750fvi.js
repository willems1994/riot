var group__drivers__bh1750fvi =
[
    [ "bh1750fvi_internal.h", "bh1750fvi__internal_8h.html", null ],
    [ "bh1750fvi.h", "bh1750fvi_8h.html", null ],
    [ "bh1750fvi_t", "structbh1750fvi__t.html", [
      [ "addr", "structbh1750fvi__t.html#ac8dc78f4ea503c1c7515037ea51334bb", null ],
      [ "i2c", "structbh1750fvi__t.html#ab06caa033e6c0f11abda722b44446161", null ]
    ] ],
    [ "bh1750fvi_params_t", "structbh1750fvi__params__t.html", [
      [ "addr", "structbh1750fvi__params__t.html#aebb7302abd34dba6140c6180a7e02b8d", null ],
      [ "i2c", "structbh1750fvi__params__t.html#ac3ab80ca0eb0c60631f6e1f982c8f611", null ]
    ] ],
    [ "BH1750FVI_ADDR_PIN_HIGH", "group__drivers__bh1750fvi.html#ga8ae157a86c49f74434760c0c541527ea", null ],
    [ "BH1750FVI_ADDR_PIN_LOW", "group__drivers__bh1750fvi.html#gaa8e5f443326ad899c4937b7c7a58f250", null ],
    [ "BH1750FVI_DEFAULT_ADDR", "group__drivers__bh1750fvi.html#ga573f635279c7f403e54b937d95e20867", null ],
    [ "BH1750FVI_I2C_MAX_CLK", "group__drivers__bh1750fvi.html#ga72d162eb583391f9618449bda2ea621d", [
      [ "BH1750FVI_OK", "group__drivers__bh1750fvi.html#gga92c4bd66c8074c3d126a069275a0a80ca6a7eb967db7afedb8b3f4dad6b255185", null ],
      [ "BH1750FVI_ERR_I2C", "group__drivers__bh1750fvi.html#gga92c4bd66c8074c3d126a069275a0a80ca91579767285407bfbfb2f43c8f11760e", null ]
    ] ],
    [ "bh1750fvi_init", "group__drivers__bh1750fvi.html#gab0f3fead02957de35c99a2d880d7751b", null ],
    [ "bh1750fvi_sample", "group__drivers__bh1750fvi.html#ga638d26e128e8be081585c701b7169156", null ]
];