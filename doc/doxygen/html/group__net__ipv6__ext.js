var group__net__ipv6__ext =
[
    [ "IPv6 routing header extension", "group__net__ipv6__ext__rh.html", "group__net__ipv6__ext__rh" ],
    [ "ipv6/ext.h", "ipv6_2ext_8h.html", null ],
    [ "ipv6_ext_t", "structipv6__ext__t.html", [
      [ "len", "structipv6__ext__t.html#a54a59702db977b7eca4b601ad08e0c43", null ],
      [ "nh", "structipv6__ext__t.html#a41518ff63dbd6eb2002e6c51b6372043", null ]
    ] ],
    [ "IPV6_EXT_LEN_UNIT", "group__net__ipv6__ext.html#ga216a95e7c0821ddc609cd69cc6dc2a06", null ],
    [ "ipv6_ext_get_next", "group__net__ipv6__ext.html#gaaef6304711848f44621fe84b921237f5", null ]
];