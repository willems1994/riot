var structvfs__file__ops =
[
    [ "close", "structvfs__file__ops.html#a0bfc1733cc6123a44b5646ee5f40bf36", null ],
    [ "fcntl", "structvfs__file__ops.html#a05f92600f5de7b030a60ba727c7b0e6c", null ],
    [ "fstat", "structvfs__file__ops.html#a321c2d8e606f21ec64b3056d49dcba59", null ],
    [ "lseek", "structvfs__file__ops.html#a4dcd5c291b62718bcdcbca9aefa8f57b", null ],
    [ "open", "structvfs__file__ops.html#a52216a6ed298ae07b7b15c1b5edb6f03", null ],
    [ "read", "structvfs__file__ops.html#a97e827ff776e771423c643961bf5e5fe", null ],
    [ "write", "structvfs__file__ops.html#a9e4fab79a71451339efd10db396d2e48", null ]
];