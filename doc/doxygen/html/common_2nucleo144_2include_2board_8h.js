var common_2nucleo144_2include_2board_8h =
[
    [ "AUTO_INIT_LED0", "group__boards__common__nucleo144.html#ga7bc4f2c5d94e1d36e2482b6dede4b85e", null ],
    [ "BTN0_MODE", "group__boards__common__nucleo144.html#ga904884feea8e03c30c9a85ca6d7a8e5c", null ],
    [ "BTN0_PIN", "group__boards__common__nucleo144.html#gaab5c3eca54046333af52593b9e360270", null ],
    [ "LED0_MASK", "group__boards__common__nucleo144.html#gabfabde35a3e8b7ab50146a5e223bfead", null ],
    [ "LED0_OFF", "group__boards__common__nucleo144.html#gaef2e39b9fd343d0e437fac0463f3e02d", null ],
    [ "LED0_ON", "group__boards__common__nucleo144.html#ga1e48dc02333bfc2e6ad3e5bd466eecd4", null ],
    [ "LED0_PIN", "group__boards__common__nucleo144.html#ga3fc522fda449d6edde717d6a857429cf", null ],
    [ "LED0_PORT", "group__boards__common__nucleo144.html#ga76914453bb5cda4ebca204e091e8f55c", null ],
    [ "LED0_TOGGLE", "group__boards__common__nucleo144.html#gaebc6389533d9fc8dcbe4d2129a4d5a45", null ],
    [ "LED1_MASK", "group__boards__common__nucleo144.html#ga669ed6e073140d069b30442bf4c08842", null ],
    [ "LED1_OFF", "group__boards__common__nucleo144.html#ga343fd2d3ce61b84f88ddfaea32c67c58", null ],
    [ "LED1_ON", "group__boards__common__nucleo144.html#gaadd4c7ae0cd4e9bbb17f7055dd51fe08", null ],
    [ "LED1_PIN", "group__boards__common__nucleo144.html#ga318aa17e5d40e2132d2c7f6269ce7f51", null ],
    [ "LED1_TOGGLE", "group__boards__common__nucleo144.html#ga267fdbba1d750146b73da35c1731fd17", null ],
    [ "LED2_MASK", "group__boards__common__nucleo144.html#ga40f0f4b5ae7ea50d341105ddc740101e", null ],
    [ "LED2_OFF", "group__boards__common__nucleo144.html#gac6468b1df4dfabcca0bb142044d6f976", null ],
    [ "LED2_ON", "group__boards__common__nucleo144.html#gab55f588eb2c5177d3f7806e60d379fba", null ],
    [ "LED2_PIN", "group__boards__common__nucleo144.html#gaf6f84078113b55354d20585131b386f7", null ],
    [ "LED2_TOGGLE", "group__boards__common__nucleo144.html#gacd16785845ce7004334b91a98707f8eb", null ]
];