var fxos8700_8h =
[
    [ "FXOS8700_USE_ACC_RAW_VALUES", "group__drivers__fxos8700.html#ga03cf07f8923bb6a9ac5d6563582836ba", null ],
    [ "FXOS8700_OK", "group__drivers__fxos8700.html#ggaa94c5fdcac6a4b26211b612c3edc04ffaef084482114b7d2788279eea0c5639d8", null ],
    [ "FXOS8700_ADDRERR", "group__drivers__fxos8700.html#ggaa94c5fdcac6a4b26211b612c3edc04ffa594f7358f9465b6201e1ea9a262bcb7b", null ],
    [ "FXOS8700_NOBUS", "group__drivers__fxos8700.html#ggaa94c5fdcac6a4b26211b612c3edc04ffaa8dfc3d083f59eabba16eeac5b468081", null ],
    [ "FXOS8700_NODEV", "group__drivers__fxos8700.html#ggaa94c5fdcac6a4b26211b612c3edc04ffa755e7cbceb14b2fb14f916e63b866773", null ],
    [ "FXOS8700_BUSERR", "group__drivers__fxos8700.html#ggaa94c5fdcac6a4b26211b612c3edc04ffae51939cc2100583094063797233f8c24", null ],
    [ "fxos8700_init", "group__drivers__fxos8700.html#gac022ca08d06fd59b6d7cf6f765c71c60", null ],
    [ "fxos8700_read", "group__drivers__fxos8700.html#ga09e5b1d85b4d7177e78e34e239d1e3f3", null ],
    [ "fxos8700_read_cached", "group__drivers__fxos8700.html#ga7114bda84b0cc418c9cf478c76a4a2e8", null ],
    [ "fxos8700_set_active", "group__drivers__fxos8700.html#ga4928ed1db97707e3585ece52f685ac51", null ],
    [ "fxos8700_set_idle", "group__drivers__fxos8700.html#gad5db20e30adffe39910ce91e044691b6", null ]
];