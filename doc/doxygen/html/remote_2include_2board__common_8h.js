var remote_2include_2board__common_8h =
[
    [ "CCA_BACKDOOR_ACTIVE_LEVEL", "group__boards__common__remote.html#ga9699faa8c7f0c45617c11f30e651dd17", null ],
    [ "CCA_BACKDOOR_ENABLE", "group__boards__common__remote.html#ga34e13c3e9c96acc4f91f84346a02b3a1", null ],
    [ "CCA_BACKDOOR_PORT_A_PIN", "group__boards__common__remote.html#gacc8334f56c93b33d2fd4bf9522a54c9a", null ],
    [ "LED3_OFF", "group__boards__common__remote.html#ga0a18a6978d9c62c42afc3917d2f258b8", null ],
    [ "LED3_ON", "group__boards__common__remote.html#gac69b87cb5151087202d5a354c87eb492", null ],
    [ "LED3_TOGGLE", "group__boards__common__remote.html#ga0702de219624c9cb1bdb4ea9b25ee62d", null ],
    [ "LED4_OFF", "group__boards__common__remote.html#ga48d3711a619866c5ca8fe8135968c25f", null ],
    [ "LED4_ON", "group__boards__common__remote.html#ga109660ec7c41cf167d83833c660f7c14", null ],
    [ "LED4_TOGGLE", "group__boards__common__remote.html#ga91a29c04e6b07e38d2beec0db1148140", null ],
    [ "LED_ALL_OFF", "group__boards__common__remote.html#ga9e504ad35b99eb31e11e00ce4620c2ac", null ],
    [ "LED_ALL_ON", "group__boards__common__remote.html#gaec51b25c3ba8debee2631856919cb928", null ],
    [ "UPDATE_CCA", "group__boards__common__remote.html#ga530224dc27cca3aa1574e67390a604f8", null ],
    [ "XTIMER_BACKOFF", "group__boards__common__remote.html#ga370b9e9a079c4cb4f54fd947b67b9f41", null ],
    [ "XTIMER_ISR_BACKOFF", "group__boards__common__remote.html#gaa1be564fc21297d7c1c8be267cbd36f6", null ],
    [ "XTIMER_WIDTH", "group__boards__common__remote.html#gafea1be2406d45b8fbb1dca1a318ac2dc", null ],
    [ "board_init", "group__boards__common__remote.html#ga916f2adc2080b4fe88034086d107a8dc", null ]
];