var boards_2sensebox__samd21_2include_2board_8h =
[
    [ "BMX280_PARAM_I2C_ADDR", "boards_2sensebox__samd21_2include_2board_8h.html#acedef26bde47d8c9f0bafc767141ad6b", null ],
    [ "BTN0_MODE", "boards_2sensebox__samd21_2include_2board_8h.html#a904884feea8e03c30c9a85ca6d7a8e5c", null ],
    [ "BTN0_PIN", "boards_2sensebox__samd21_2include_2board_8h.html#aab5c3eca54046333af52593b9e360270", null ],
    [ "BTN0_PORT", "boards_2sensebox__samd21_2include_2board_8h.html#a9a61388c9e491aec2e44cc03956bb299", null ],
    [ "HDC1000_PARAM_ADDR", "boards_2sensebox__samd21_2include_2board_8h.html#a123b89c93359ec27194b9c12073e803b", null ],
    [ "I2C_DISABLE", "boards_2sensebox__samd21_2include_2board_8h.html#aa314cf7ad54bb5b7aac40e022704c3bb", null ],
    [ "I2C_EN_MASK", "boards_2sensebox__samd21_2include_2board_8h.html#a54ddfbda9b8a04ea27c3436d0c362beb", null ],
    [ "I2C_EN_MODE", "boards_2sensebox__samd21_2include_2board_8h.html#a273c6347eabcd6a01d9305ac2de49f1b", null ],
    [ "I2C_EN_PIN", "boards_2sensebox__samd21_2include_2board_8h.html#a04857b497eb19ccce40b2c398961d1dd", null ],
    [ "I2C_EN_PORT", "boards_2sensebox__samd21_2include_2board_8h.html#aefcc1a190bb7d67ade8dcc37550edf0c", null ],
    [ "I2C_ENABLE", "boards_2sensebox__samd21_2include_2board_8h.html#aaa50f9e9a9ced9480b2195bb63ebbaca", null ],
    [ "LED0_MASK", "boards_2sensebox__samd21_2include_2board_8h.html#abfabde35a3e8b7ab50146a5e223bfead", null ],
    [ "LED0_OFF", "boards_2sensebox__samd21_2include_2board_8h.html#aef2e39b9fd343d0e437fac0463f3e02d", null ],
    [ "LED0_ON", "boards_2sensebox__samd21_2include_2board_8h.html#a1e48dc02333bfc2e6ad3e5bd466eecd4", null ],
    [ "LED0_PIN", "boards_2sensebox__samd21_2include_2board_8h.html#a3fc522fda449d6edde717d6a857429cf", null ],
    [ "LED0_TOGGLE", "boards_2sensebox__samd21_2include_2board_8h.html#aebc6389533d9fc8dcbe4d2129a4d5a45", null ],
    [ "LED1_MASK", "boards_2sensebox__samd21_2include_2board_8h.html#a669ed6e073140d069b30442bf4c08842", null ],
    [ "LED1_OFF", "boards_2sensebox__samd21_2include_2board_8h.html#a343fd2d3ce61b84f88ddfaea32c67c58", null ],
    [ "LED1_ON", "boards_2sensebox__samd21_2include_2board_8h.html#aadd4c7ae0cd4e9bbb17f7055dd51fe08", null ],
    [ "LED1_PIN", "boards_2sensebox__samd21_2include_2board_8h.html#a318aa17e5d40e2132d2c7f6269ce7f51", null ],
    [ "LED1_TOGGLE", "boards_2sensebox__samd21_2include_2board_8h.html#a267fdbba1d750146b73da35c1731fd17", null ],
    [ "LED_PORT", "boards_2sensebox__samd21_2include_2board_8h.html#a663daa01e565aee93c6f20c5845b90b4", null ],
    [ "SX127X_PARAM_DIO0", "boards_2sensebox__samd21_2include_2board_8h.html#ae0de0cec38e4d8f96ae68e4340028bce", null ],
    [ "SX127X_PARAM_DIO1", "boards_2sensebox__samd21_2include_2board_8h.html#abbd59e89ccaa2f06a3c2d399ec9b1084", null ],
    [ "SX127X_PARAM_DIO2", "boards_2sensebox__samd21_2include_2board_8h.html#a9b87315a4259c499509b06d6f469bba8", null ],
    [ "SX127X_PARAM_DIO3", "boards_2sensebox__samd21_2include_2board_8h.html#ac917eea6062e10a46800eed21dd3c59b", null ],
    [ "SX127X_PARAM_DIO_MULTI", "boards_2sensebox__samd21_2include_2board_8h.html#ab611a1ebfa4d95120c6030486b1efd01", null ],
    [ "SX127X_PARAM_PASELECT", "boards_2sensebox__samd21_2include_2board_8h.html#a23245c840fc63b266a868b24b6747a51", null ],
    [ "SX127X_PARAM_RESET", "boards_2sensebox__samd21_2include_2board_8h.html#ae05bb210c4d80e12824dc022bf5c438d", null ],
    [ "SX127X_PARAM_SPI", "boards_2sensebox__samd21_2include_2board_8h.html#a046a1997a85c68028a155d867af3b596", null ],
    [ "SX127X_PARAM_SPI_NSS", "boards_2sensebox__samd21_2include_2board_8h.html#a0de61f26012008e2bbb3feeab7584395", null ],
    [ "TSL2561_PARAM_ADDR", "boards_2sensebox__samd21_2include_2board_8h.html#ae898d358740c3f0952f457c5a8aa2ee3", null ],
    [ "WAIT_FOR_SPI_RESET", "boards_2sensebox__samd21_2include_2board_8h.html#a91036f4b00b35e3a7c8a777ae08d770a", null ],
    [ "XBEE1_CS_PIN", "boards_2sensebox__samd21_2include_2board_8h.html#a688b650d00eb17980da4e86e8565aafd", null ],
    [ "XBEE1_DISABLE", "boards_2sensebox__samd21_2include_2board_8h.html#ae19c3d47f6a6fa08f8329c5d21fff59b", null ],
    [ "XBEE1_EN_MASK", "boards_2sensebox__samd21_2include_2board_8h.html#a29a21d9cc03bff355c26473fb444a4df", null ],
    [ "XBEE1_EN_MODE", "boards_2sensebox__samd21_2include_2board_8h.html#a64511bdb414986388b07302be73717b9", null ],
    [ "XBEE1_EN_PIN", "boards_2sensebox__samd21_2include_2board_8h.html#aa8c2a8ce1786abf1130f1d309f05189b", null ],
    [ "XBEE1_EN_PORT", "boards_2sensebox__samd21_2include_2board_8h.html#a7f75218457631f7d5a544ed576902f45", null ],
    [ "XBEE1_ENABLE", "boards_2sensebox__samd21_2include_2board_8h.html#a706f594ec6c6c472e0d10104405ed632", null ],
    [ "XBEE1_INT_PIN", "boards_2sensebox__samd21_2include_2board_8h.html#a46ca2cfc171f23891adc56a69775c9ec", null ],
    [ "XBEE2_CS_PIN", "boards_2sensebox__samd21_2include_2board_8h.html#ab2e06b4fb3d5d325e8a8c956fe1ea805", null ],
    [ "XBEE2_DISABLE", "boards_2sensebox__samd21_2include_2board_8h.html#a1f82da04472c1b7238517d1af374331f", null ],
    [ "XBEE2_EN_MASK", "boards_2sensebox__samd21_2include_2board_8h.html#a55dff2a06532cb261f994ba2838fe7c8", null ],
    [ "XBEE2_EN_MODE", "boards_2sensebox__samd21_2include_2board_8h.html#a20a474ea2039da224e15ae99991c4c9b", null ],
    [ "XBEE2_EN_PIN", "boards_2sensebox__samd21_2include_2board_8h.html#ae101a4bed5fea55ffb63b18ba77ce652", null ],
    [ "XBEE2_EN_PORT", "boards_2sensebox__samd21_2include_2board_8h.html#af9d3fea4b9cfb5d00538beac30a1c382", null ],
    [ "XBEE2_ENABLE", "boards_2sensebox__samd21_2include_2board_8h.html#a8c3a85625c98f0e5df705665a0766bd4", null ],
    [ "XBEE2_INT_PIN", "boards_2sensebox__samd21_2include_2board_8h.html#a278f96a8e5a8d6b40b83ac1fa027740d", null ],
    [ "board_init", "boards_2sensebox__samd21_2include_2board_8h.html#a916f2adc2080b4fe88034086d107a8dc", null ]
];