var group__drivers__veml6070 =
[
    [ "veml6070.h", "veml6070_8h.html", null ],
    [ "veml6070_params_t", "structveml6070__params__t.html", [
      [ "i2c_dev", "structveml6070__params__t.html#a4d6da03be1609c9b5db9278da1ad016c", null ],
      [ "itime", "structveml6070__params__t.html#adc5d43958597fa4463cd1b9348d21274", null ]
    ] ],
    [ "veml6070_t", "structveml6070__t.html", [
      [ "params", "structveml6070__t.html#a71922501e327374c07865b7675585e25", null ]
    ] ],
    [ "veml6070_itime_t", "group__drivers__veml6070.html#ga9e0572b796e0c20755e3f3f9a989c950", [
      [ "VEML6070_OK", "group__drivers__veml6070.html#ggab98c672c5aaffb98a9189cd99e58ff23a997bc5b246995ce54442fb156d703017", null ],
      [ "VEML6070_ERR_I2C", "group__drivers__veml6070.html#ggab98c672c5aaffb98a9189cd99e58ff23a1b908290653c3148e0298c8451dbf9f9", null ]
    ] ],
    [ "veml6070_integrationtime", "group__drivers__veml6070.html#ga0c39b45f995df3f279a39d3147391a6d", [
      [ "VEML6070_HALF_T", "group__drivers__veml6070.html#gga0c39b45f995df3f279a39d3147391a6daa5461ceadd78fd0e1e9a3ef7c82618fd", null ],
      [ "VEML6070_1_T", "group__drivers__veml6070.html#gga0c39b45f995df3f279a39d3147391a6da35cf2848ca374313d127d9988c2dcde9", null ],
      [ "VEML6070_2_T", "group__drivers__veml6070.html#gga0c39b45f995df3f279a39d3147391a6dadbe1ecf895c02ac7e08f035d68d9b904", null ],
      [ "VEML6070_4_T", "group__drivers__veml6070.html#gga0c39b45f995df3f279a39d3147391a6da3a74b6dd4ecfdc3f6901ab9d249413f7", null ]
    ] ],
    [ "veml6070_init", "group__drivers__veml6070.html#ga0c19cad88dd63de4640a5fb13d318b01", null ],
    [ "veml6070_read_uv", "group__drivers__veml6070.html#ga419bd7c7880c43578237686baa7e095f", null ]
];