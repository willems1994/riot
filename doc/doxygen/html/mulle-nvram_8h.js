var mulle_nvram_8h =
[
    [ "MULLE_NVRAM_MAGIC_EXPECTED", "mulle-nvram_8h.html#a4d758ea8daf755e112889dfadf78082a", null ],
    [ "mulle_nvram_address_t", "mulle-nvram_8h.html#a89a0525f07d9c38b52923929977f7674", null ],
    [ "mulle_nvram_address", "mulle-nvram_8h.html#aca2dafa6e517af7f783caf5a90521b14", [
      [ "MULLE_NVRAM_MAGIC", "mulle-nvram_8h.html#aca2dafa6e517af7f783caf5a90521b14af9a418c7c97dd2a93a162c02f1566740", null ],
      [ "MULLE_NVRAM_BOOT_COUNT", "mulle-nvram_8h.html#aca2dafa6e517af7f783caf5a90521b14a59a3f019d1834487f211d70cd21239cc", null ]
    ] ],
    [ "mulle_nvram", "mulle-nvram_8h.html#ac84722e3c8293a532651944ca5df4ef5", null ]
];