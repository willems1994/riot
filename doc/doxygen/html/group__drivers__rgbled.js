var group__drivers__rgbled =
[
    [ "rgbled.h", "rgbled_8h.html", null ],
    [ "rgbled_t", "structrgbled__t.html", [
      [ "channel_b", "structrgbled__t.html#a7d396fd3926c532a97bd394d60cb02bc", null ],
      [ "channel_g", "structrgbled__t.html#a11549edd1ff8ab238c89430c9f405046", null ],
      [ "channel_r", "structrgbled__t.html#aea60dcc77ba78e41352255fc28dcb493", null ],
      [ "device", "structrgbled__t.html#a756d862adafef9992244db29608af5ed", null ]
    ] ],
    [ "rgbled_init", "group__drivers__rgbled.html#gaad3deb3b9c27901d4afe4b6c8c2e1faf", null ],
    [ "rgbled_set", "group__drivers__rgbled.html#gaf79536bf5ad21f97774c41eb81cf31f8", null ]
];