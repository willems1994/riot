var dns_8h =
[
    [ "DNS_CLASS_IN", "group__net__sock__dns.html#ga9cc84b120d830f184e7249db81ed1736", null ],
    [ "DNS_TYPE_A", "group__net__sock__dns.html#gafe9f7ff23f3feba2776dcfa366f53c3f", null ],
    [ "DNS_TYPE_AAAA", "group__net__sock__dns.html#gacfb2eb201eadb88b54ae795a01fd34cd", null ],
    [ "SOCK_DNS_MAX_NAME_LEN", "group__net__sock__dns.html#gaa901dd315beac9e625a262ed562ebdc4", null ],
    [ "SOCK_DNS_PORT", "group__net__sock__dns.html#gaa36647fbd8431024ef6c647b719b64f8", null ],
    [ "SOCK_DNS_QUERYBUF_LEN", "group__net__sock__dns.html#ga7735be57ba382368f3c626df080ec3a7", null ],
    [ "SOCK_DNS_RETRIES", "group__net__sock__dns.html#gabaac7a8268c9fb890f45c4c3276a22df", null ],
    [ "sock_dns_query", "group__net__sock__dns.html#ga508b47ec287a3846ca8f8fc2c0bbe9bd", null ],
    [ "sock_dns_server", "group__net__sock__dns.html#ga99ea302f97897ac95d47f1cb83b2ac91", null ]
];