var boards_2teensy31_2include_2periph__conf_8h =
[
    [ "CLOCK_BUSCLOCK", "boards_2teensy31_2include_2periph__conf_8h.html#a47b8fb0bde3602f542629a3a73f3cb96", null ],
    [ "CLOCK_CORECLOCK", "boards_2teensy31_2include_2periph__conf_8h.html#afc465f12242e68f6c3695caa3ba0a169", null ],
    [ "LPTMR_CONFIG", "boards_2teensy31_2include_2periph__conf_8h.html#a403e041dff680770a6206018e406f72c", null ],
    [ "LPTMR_NUMOF", "boards_2teensy31_2include_2periph__conf_8h.html#af404dfea11a245ccd77baec388c2da5a", null ],
    [ "PIT_BASECLOCK", "boards_2teensy31_2include_2periph__conf_8h.html#a422423244be42d00a0fbc27cdc1798f5", null ],
    [ "PIT_CONFIG", "boards_2teensy31_2include_2periph__conf_8h.html#a4d3c916098ef9d754a85e9d65d6fb34e", null ],
    [ "PIT_ISR_0", "boards_2teensy31_2include_2periph__conf_8h.html#aa1c7a21c9d38b2e3cfaf660b70e59272", null ],
    [ "PIT_ISR_1", "boards_2teensy31_2include_2periph__conf_8h.html#ab1fb8843db54b7373933bfe8b817a930", null ],
    [ "PIT_NUMOF", "boards_2teensy31_2include_2periph__conf_8h.html#a5f68f194b32c0deea003fa8a56a848e5", null ],
    [ "PWM_NUMOF", "boards_2teensy31_2include_2periph__conf_8h.html#a44adbd579bb180f3cfe8ec78932eb7a1", null ],
    [ "TIMER_NUMOF", "boards_2teensy31_2include_2periph__conf_8h.html#a6e2f66f5b6f5c835dd11f9766c4ed897", null ],
    [ "UART_0_ISR", "boards_2teensy31_2include_2periph__conf_8h.html#a713e03d19734d793baee3d1cc25c2dbb", null ],
    [ "UART_1_ISR", "boards_2teensy31_2include_2periph__conf_8h.html#af9358264b5cbce69dddad098a8600aae", null ],
    [ "UART_NUMOF", "boards_2teensy31_2include_2periph__conf_8h.html#a850405f2aaa352ad264346531f0e6230", null ],
    [ "clock_config", "boards_2teensy31_2include_2periph__conf_8h.html#a021b7e40d51887bc813ae47548c898d2", null ],
    [ "pwm_config", "boards_2teensy31_2include_2periph__conf_8h.html#a5177e7e865ac2ae63f7d56c8b3078707", null ],
    [ "uart_config", "boards_2teensy31_2include_2periph__conf_8h.html#a1643cfc64589407fb96b4cbf908689a5", null ]
];