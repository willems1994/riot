var tftp_8h =
[
    [ "GNRC_TFTP_DEFAULT_DST_PORT", "group__net__gnrc__tftp.html#gab410b107753e8a5f5a3a6299edcd123c", null ],
    [ "GNRC_TFTP_DEFAULT_SRC_PORT", "group__net__gnrc__tftp.html#ga1e0338d598227fa63fd063a824888ea9", null ],
    [ "GNRC_TFTP_DEFAULT_TIMEOUT", "group__net__gnrc__tftp.html#gab909805796fdeb717418e90e6b3165d8", null ],
    [ "GNRC_TFTP_MAX_FILENAME_LEN", "group__net__gnrc__tftp.html#ga566bbcd68447eeb0ea8c4d167708c5b1", null ],
    [ "GNRC_TFTP_MAX_RETRIES", "group__net__gnrc__tftp.html#gaaba1643864728edf37f3478c8a2083c6", null ],
    [ "GNRC_TFTP_MAX_TRANSFER_UNIT", "group__net__gnrc__tftp.html#gab0bbd1583f3ca0419af6a6d5bcd00576", null ],
    [ "tftp_data_cb_t", "group__net__gnrc__tftp.html#ga5e1c0a6d8acf5d2af25dc4f174e6335e", null ],
    [ "tftp_start_cb_t", "group__net__gnrc__tftp.html#ga7da4f294948037790c836f9fa9fccee8", null ],
    [ "tftp_stop_cb_t", "group__net__gnrc__tftp.html#gaee4b33a7d17d2f103e78ff92d7637e1e", null ],
    [ "tftp_action_t", "group__net__gnrc__tftp.html#ga2b0b9861399cda4872de6460b9d9669f", [
      [ "TFTP_READ", "group__net__gnrc__tftp.html#gga2b0b9861399cda4872de6460b9d9669fa8dd5da5fffe16d15b5d5fb2a47f053ab", null ],
      [ "TFTP_WRITE", "group__net__gnrc__tftp.html#gga2b0b9861399cda4872de6460b9d9669fa38f972c40657d027c9860a3791dd5ef4", null ]
    ] ],
    [ "tftp_event_t", "group__net__gnrc__tftp.html#ga485d4ee0da165ca7754e4dabc3e5752b", [
      [ "TFTP_SUCCESS", "group__net__gnrc__tftp.html#gga485d4ee0da165ca7754e4dabc3e5752ba19431fa451dcd2d49c696c11cddefdf1", null ],
      [ "TFTP_PEER_ERROR", "group__net__gnrc__tftp.html#gga485d4ee0da165ca7754e4dabc3e5752bab3ad1ee97107dc85cff49eb283774ba1", null ],
      [ "TFTP_INTERN_ERROR", "group__net__gnrc__tftp.html#gga485d4ee0da165ca7754e4dabc3e5752ba3be4d255194af797789010fd7f43196b", null ]
    ] ],
    [ "tftp_mode_t", "group__net__gnrc__tftp.html#ga26696983fe99b65d348b58ea61c2f2ff", [
      [ "TTM_ASCII", "group__net__gnrc__tftp.html#gga26696983fe99b65d348b58ea61c2f2ffa8c81d840b8265dbc84687be2ace5e23a", null ],
      [ "TTM_OCTET", "group__net__gnrc__tftp.html#gga26696983fe99b65d348b58ea61c2f2ffa459e2f7af9c47ad82ec0a25151b8c27a", null ],
      [ "TTM_MAIL", "group__net__gnrc__tftp.html#gga26696983fe99b65d348b58ea61c2f2ffa31e762b3049978bc4070d487455b4455", null ]
    ] ],
    [ "gnrc_tftp_client_read", "group__net__gnrc__tftp.html#ga8e55a352b02987a4468aeeb792067e13", null ],
    [ "gnrc_tftp_client_write", "group__net__gnrc__tftp.html#gaf272d70010dad0b678e1f2ad6b92af67", null ],
    [ "gnrc_tftp_server", "group__net__gnrc__tftp.html#ga8fb129322546e3f17147a10b950417cc", null ],
    [ "gnrc_tftp_server_stop", "group__net__gnrc__tftp.html#ga5b4809942a45d0c462d52c4aed39c7a1", null ]
];