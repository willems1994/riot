var esp8266_2include_2periph__conf__common_8h =
[
    [ "ADC_NUMOF", "esp8266_2include_2periph__conf__common_8h.html#a2f0c741db24aa2ccded869ba53f6a302", null ],
    [ "BIT", "esp8266_2include_2periph__conf__common_8h.html#aa9374d9bd55e09857c4bf40e2f294435", null ],
    [ "PWM0_CHANNEL_GPIOS", "esp8266_2include_2periph__conf__common_8h.html#ab975840349750834a41c5833199e4e83", null ],
    [ "PWM0_DEV", "esp8266_2include_2periph__conf__common_8h.html#a7dd74c7b0990d0693dd2bb9b0107896c", null ],
    [ "PWM_CHANNEL_NUM_MAX", "esp8266_2include_2periph__conf__common_8h.html#a8b967c03fbfe9d5b391c689b9b060b55", null ],
    [ "PWM_NUMOF", "esp8266_2include_2periph__conf__common_8h.html#a44adbd579bb180f3cfe8ec78932eb7a1", null ],
    [ "SPI0_CS0_GPIO", "esp8266_2include_2periph__conf__common_8h.html#a5bb9ecc9962123128bafca210f3dd846", null ],
    [ "SPI0_DEV", "esp8266_2include_2periph__conf__common_8h.html#a90a82746e60135d1b41da479b7c4a7f5", null ],
    [ "SPI0_MISO_GPIO", "esp8266_2include_2periph__conf__common_8h.html#a0b6d031e8ecc4110c5c282e124c5ef6d", null ],
    [ "SPI0_MOSI_GPIO", "esp8266_2include_2periph__conf__common_8h.html#ac3debf68a711390c2a725d77d8b0f54d", null ],
    [ "SPI0_SCK_GPIO", "esp8266_2include_2periph__conf__common_8h.html#a1fcebfb3a562b1ea8e08ab0244c4d9e3", null ],
    [ "SPI_DEV", "esp8266_2include_2periph__conf__common_8h.html#afb9420809bc7722e41488a090b53eaf9", null ],
    [ "SPI_NUMOF", "esp8266_2include_2periph__conf__common_8h.html#ab35a2b79568128efef74adf1ba1910a8", null ],
    [ "TIMER_CHANNELS", "esp8266_2include_2periph__conf__common_8h.html#a84bd471496f2a93c5a4ea1cf490bf83e", null ],
    [ "TIMER_NUMOF", "esp8266_2include_2periph__conf__common_8h.html#a6e2f66f5b6f5c835dd11f9766c4ed897", null ],
    [ "UART0_RXD", "esp8266_2include_2periph__conf__common_8h.html#a28fc91fb6e973e5f0be9e44e5edd5449", null ],
    [ "UART0_TXD", "esp8266_2include_2periph__conf__common_8h.html#add71a59fa12734b27043cabf5546d176", null ],
    [ "UART_NUMOF", "esp8266_2include_2periph__conf__common_8h.html#a850405f2aaa352ad264346531f0e6230", null ]
];