var group__drivers__periph__uart =
[
    [ "uart.h", "uart_8h.html", null ],
    [ "uart_isr_ctx_t", "structuart__isr__ctx__t.html", [
      [ "arg", "structuart__isr__ctx__t.html#ab8839a548b2ddbb4fca16f9f252c41aa", null ],
      [ "rx_cb", "structuart__isr__ctx__t.html#aa60adcebe7216a0f79af3a3082a4bf55", null ]
    ] ],
    [ "UART_DEV", "group__drivers__periph__uart.html#gafc5afd63560d27731d2517b3005f3294", null ],
    [ "UART_UNDEF", "group__drivers__periph__uart.html#ga3604c25a8762ee60bf570df4a3150f5c", null ],
    [ "uart_rx_cb_t", "group__drivers__periph__uart.html#gac4baa58903938817c0b1690a41463df0", null ],
    [ "uart_t", "group__drivers__periph__uart.html#gafe5f2dd69c8a911b2ec737e7ea5fb33f", [
      [ "UART_OK", "group__drivers__periph__uart.html#ggaaa91b962ffd8aca2f3c24e886a222419a525187963c0aaa465f2a35b30b894d07", null ],
      [ "UART_NODEV", "group__drivers__periph__uart.html#ggaaa91b962ffd8aca2f3c24e886a222419a63d9c59561054598701f1217e03a2525", null ],
      [ "UART_NOBAUD", "group__drivers__periph__uart.html#ggaaa91b962ffd8aca2f3c24e886a222419a3f800cbd6c1f44ed8afd089e07d4edaa", null ],
      [ "UART_INTERR", "group__drivers__periph__uart.html#ggaaa91b962ffd8aca2f3c24e886a222419a6608baae54141f9335c1c735c5bca620", null ],
      [ "UART_NOMODE", "group__drivers__periph__uart.html#ggaaa91b962ffd8aca2f3c24e886a222419a941021a8f99c1bb1ffbcb511ff4ce3f2", null ]
    ] ],
    [ "uart_init", "group__drivers__periph__uart.html#ga99f3d00aa682d4f892a86cb803f08dff", null ],
    [ "uart_poweroff", "group__drivers__periph__uart.html#ga066a6312f829046f201ff41ecc6960ea", null ],
    [ "uart_poweron", "group__drivers__periph__uart.html#ga97ca086aeee451fcad48db4518ba325a", null ],
    [ "uart_write", "group__drivers__periph__uart.html#gae2360fb0e880c387cfc02dd0cbd1c113", null ]
];