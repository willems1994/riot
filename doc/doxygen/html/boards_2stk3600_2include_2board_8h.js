var boards_2stk3600_2include_2board_8h =
[
    [ "BC_PIN", "group__boards__stk3600.html#ga68576b8b2674e98cc147af8367fb58fe", null ],
    [ "LED0_OFF", "group__boards__stk3600.html#gaef2e39b9fd343d0e437fac0463f3e02d", null ],
    [ "LED0_ON", "group__boards__stk3600.html#ga1e48dc02333bfc2e6ad3e5bd466eecd4", null ],
    [ "LED0_PIN", "group__boards__stk3600.html#ga3fc522fda449d6edde717d6a857429cf", null ],
    [ "LED0_TOGGLE", "group__boards__stk3600.html#gaebc6389533d9fc8dcbe4d2129a4d5a45", null ],
    [ "LED1_OFF", "group__boards__stk3600.html#ga343fd2d3ce61b84f88ddfaea32c67c58", null ],
    [ "LED1_ON", "group__boards__stk3600.html#gaadd4c7ae0cd4e9bbb17f7055dd51fe08", null ],
    [ "LED1_PIN", "group__boards__stk3600.html#ga318aa17e5d40e2132d2c7f6269ce7f51", null ],
    [ "LED1_TOGGLE", "group__boards__stk3600.html#ga267fdbba1d750146b73da35c1731fd17", null ],
    [ "PB0_PIN", "group__boards__stk3600.html#gaea39132b18ce668ed815d13c3153323b", null ],
    [ "PB1_PIN", "group__boards__stk3600.html#gaa33c970f051ff2f38a217f39f00599c1", null ],
    [ "XTIMER_HZ", "group__boards__stk3600.html#gaf68fde6b7d5b362834e6a8d382c6c0d7", null ],
    [ "XTIMER_WIDTH", "group__boards__stk3600.html#gafea1be2406d45b8fbb1dca1a318ac2dc", null ],
    [ "board_init", "group__boards__stk3600.html#ga916f2adc2080b4fe88034086d107a8dc", null ]
];