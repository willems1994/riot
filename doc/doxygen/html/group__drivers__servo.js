var group__drivers__servo =
[
    [ "servo.h", "servo_8h.html", null ],
    [ "servo_t", "structservo__t.html", [
      [ "channel", "structservo__t.html#a173b24561d92f333b0e7ffe37fd3b75c", null ],
      [ "device", "structservo__t.html#a4dc21135681ca829da9d6e87784d395a", null ],
      [ "max", "structservo__t.html#a5d3da1097abdb6dd17f06336e7ef401d", null ],
      [ "min", "structservo__t.html#a03e1990854d40a59aec90ce24c5015f8", null ],
      [ "scale_den", "structservo__t.html#a81e50607d16a70cb1124a25f7c00e130", null ],
      [ "scale_nom", "structservo__t.html#a06fd800ddb6927f81812b8ae61c3721d", null ]
    ] ],
    [ "servo_init", "group__drivers__servo.html#gad326f97ac3642e01ad0a3054c4151913", null ],
    [ "servo_set", "group__drivers__servo.html#ga849dd70c359fc70e372216f1a22fd653", null ]
];