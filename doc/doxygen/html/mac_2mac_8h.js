var mac_2mac_8h =
[
    [ "GNRC_MAC_DISPATCH_BUFFER_SIZE", "group__net__gnrc__mac.html#ga4313abc56acfcad934b8df8ebf78739b", null ],
    [ "GNRC_MAC_ENABLE_DUTYCYCLE_RECORD", "group__net__gnrc__mac.html#gafebdd1a1361fd8dbeb16a1cdab252642", null ],
    [ "GNRC_MAC_NEIGHBOR_COUNT", "group__net__gnrc__mac.html#gad25a455d33568e20da589999191ec62c", null ],
    [ "GNRC_MAC_RX_QUEUE_SIZE", "group__net__gnrc__mac.html#gab2daec18fcad7063afe76cd166bbca96", null ],
    [ "GNRC_MAC_TX_QUEUE_SIZE", "group__net__gnrc__mac.html#ga4cd6b740957a77ba5caaf7a76c89293d", null ]
];