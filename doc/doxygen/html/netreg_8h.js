var netreg_8h =
[
    [ "GNRC_NETREG_DEMUX_CTX_ALL", "group__net__gnrc__netreg.html#ga1a6359efa13b3bfed70af55deacde7ae", null ],
    [ "GNRC_NETREG_ENTRY_INIT_CB", "group__net__gnrc__netreg.html#ga76e61a7cd58dbfd5898ce4d94149a8bc", null ],
    [ "GNRC_NETREG_ENTRY_INIT_MBOX", "group__net__gnrc__netreg.html#gaec3b578250b0c3ee059576629338a71d", null ],
    [ "GNRC_NETREG_ENTRY_INIT_PID", "group__net__gnrc__netreg.html#gaed56e891d9a47cba3dcf80f927623137", null ],
    [ "gnrc_netreg_entry_cb_t", "group__net__gnrc__netreg.html#ga5a1dc197a877ff181b5da6c1c1604e14", null ],
    [ "gnrc_netreg_entry_t", "group__net__gnrc__netreg.html#ga8dbe8b5d7eddad31671428c30d003f6c", null ],
    [ "gnrc_netreg_type_t", "group__net__gnrc__netreg.html#ga7f3ef84f4d2a3cb8e1d2b5c3cb7ec31c", [
      [ "GNRC_NETREG_TYPE_DEFAULT", "group__net__gnrc__netreg.html#gga7f3ef84f4d2a3cb8e1d2b5c3cb7ec31ca052076705778900f3a5b37d7ba306b27", null ],
      [ "GNRC_NETREG_TYPE_MBOX", "group__net__gnrc__netreg.html#gga7f3ef84f4d2a3cb8e1d2b5c3cb7ec31ca5238dfc0ad573628f7610e2bd319593c", null ],
      [ "GNRC_NETREG_TYPE_CB", "group__net__gnrc__netreg.html#gga7f3ef84f4d2a3cb8e1d2b5c3cb7ec31cab220add399befee1db1eef1b75579e9a", null ]
    ] ],
    [ "gnrc_netreg_calc_csum", "group__net__gnrc__netreg.html#ga1db6d7fa7ad6315e70e7621cadfe2123", null ],
    [ "gnrc_netreg_entry_init_cb", "group__net__gnrc__netreg.html#gae316e62857c64393605a8fc0fb1b7546", null ],
    [ "gnrc_netreg_entry_init_mbox", "group__net__gnrc__netreg.html#ga3947a387f2e024dc99cb0f8ca0beda8b", null ],
    [ "gnrc_netreg_entry_init_pid", "group__net__gnrc__netreg.html#ga806d3268f56159bb73fcbfcc92c5cd2f", null ],
    [ "gnrc_netreg_getnext", "group__net__gnrc__netreg.html#ga9c62fc1398543c757eaea0956a97b51a", null ],
    [ "gnrc_netreg_init", "group__net__gnrc__netreg.html#gaa0dafe42cd1f165ba11e694caf4e9b6e", null ],
    [ "gnrc_netreg_lookup", "group__net__gnrc__netreg.html#gae6c8a22d969ee61d23fc926b27bc76e8", null ],
    [ "gnrc_netreg_num", "group__net__gnrc__netreg.html#ga47ba14b395e125b1966215296d400bf0", null ],
    [ "gnrc_netreg_register", "group__net__gnrc__netreg.html#gafb6a9d0b840aae07c3c1617932835ef1", null ],
    [ "gnrc_netreg_unregister", "group__net__gnrc__netreg.html#gad68b86ef84f5b1c6391067bdd8b16f2b", null ]
];