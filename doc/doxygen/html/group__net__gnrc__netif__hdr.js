var group__net__gnrc__netif__hdr =
[
    [ "gnrc/netif/hdr.h", "gnrc_2netif_2hdr_8h.html", null ],
    [ "gnrc_netif_hdr_t", "structgnrc__netif__hdr__t.html", [
      [ "dst_l2addr_len", "structgnrc__netif__hdr__t.html#a563ac266e69071c4854e6e1939d6cf8f", null ],
      [ "flags", "structgnrc__netif__hdr__t.html#abbf718ba7223770b72eb8fbd39dd85f6", null ],
      [ "if_pid", "structgnrc__netif__hdr__t.html#a27177e629f5187ce530661a053716198", null ],
      [ "lqi", "structgnrc__netif__hdr__t.html#afcef28c36923802cd092f42e9f60e29c", null ],
      [ "rssi", "structgnrc__netif__hdr__t.html#a27fb1e471d3deee75af21e9275969587", null ],
      [ "src_l2addr_len", "structgnrc__netif__hdr__t.html#aa6fea1446fddd0f2d3c9f15654c38b91", null ]
    ] ],
    [ "GNRC_NETIF_HDR_FLAGS_BROADCAST", "group__net__gnrc__netif__hdr.html#ga20cd0f0071d5efac972f7a5954438b52", null ],
    [ "GNRC_NETIF_HDR_FLAGS_MORE_DATA", "group__net__gnrc__netif__hdr.html#gaf472166125197f3937a046c74a616081", null ],
    [ "GNRC_NETIF_HDR_FLAGS_MULTICAST", "group__net__gnrc__netif__hdr.html#ga47429487a2fa85881a0eaa8c4bd5b948", null ],
    [ "GNRC_NETIF_HDR_L2ADDR_MAX_LEN", "group__net__gnrc__netif__hdr.html#gaefe50831ad4ae5980edb0abc70fa3392", null ],
    [ "GNRC_NETIF_HDR_L2ADDR_PRINT_LEN", "group__net__gnrc__netif__hdr.html#gaffd8f1d5e824c8379931b8ac9da1472d", null ],
    [ "gnrc_netif_hdr_build", "group__net__gnrc__netif__hdr.html#ga751e7ebc315a8b729379eb1c5d672168", null ],
    [ "gnrc_netif_hdr_get_dst_addr", "group__net__gnrc__netif__hdr.html#gac8447f18dca8ba362b788b38b2333ea8", null ],
    [ "gnrc_netif_hdr_get_dstaddr", "group__net__gnrc__netif__hdr.html#ga22d869e083065cae988a1815cab3da24", null ],
    [ "gnrc_netif_hdr_get_flag", "group__net__gnrc__netif__hdr.html#ga0b0c5a77402e579b8ef8b9f5d493906e", null ],
    [ "gnrc_netif_hdr_get_src_addr", "group__net__gnrc__netif__hdr.html#gaae99832a0efea4a5a2501a8fc9469f21", null ],
    [ "gnrc_netif_hdr_get_srcaddr", "group__net__gnrc__netif__hdr.html#ga04f1a2efa801939013d95e6f1e9cb3f6", null ],
    [ "gnrc_netif_hdr_init", "group__net__gnrc__netif__hdr.html#ga3a0838c7d1e42045a85aae4897b20ba6", null ],
    [ "gnrc_netif_hdr_print", "group__net__gnrc__netif__hdr.html#gad410e937d22d546590d6d5ffe510fff3", null ],
    [ "gnrc_netif_hdr_set_dst_addr", "group__net__gnrc__netif__hdr.html#ga3c078c74cbc5436b6e06955a3c10de39", null ],
    [ "gnrc_netif_hdr_set_src_addr", "group__net__gnrc__netif__hdr.html#ga8437aa492d26458ef98ad1baab017290", null ],
    [ "gnrc_netif_hdr_sizeof", "group__net__gnrc__netif__hdr.html#gaee87acdca58ed85cd7dfb4600e19dce7", null ]
];