var adcxx1c__regs_8h =
[
    [ "ADCXX1C_ALERT_STATUS_ADDR", "adcxx1c__regs_8h.html#a334ef6100991a4ea410f718f0fd5cd61", null ],
    [ "ADCXX1C_CONF_ADDR", "adcxx1c__regs_8h.html#aa089849816d99f8fba1a411dc88492df", null ],
    [ "ADCXX1C_CONF_ALERT_FLAG_EN", "adcxx1c__regs_8h.html#ac3943b3f318f6ae86b11263d07f957ac", null ],
    [ "ADCXX1C_CONF_ALERT_PIN_EN", "adcxx1c__regs_8h.html#ad47272bfa643726099a0f924e90b7556", null ],
    [ "ADCXX1C_CONV_RES_ADDR", "adcxx1c__regs_8h.html#a310c600c5a21df35e520cdb6ade999ce", null ],
    [ "ADCXX1C_HIGH_LIMIT_ADDR", "adcxx1c__regs_8h.html#a16bf7dd5ef901eef6cac419fff22f6c2", null ],
    [ "ADCXX1C_HIGHEST_CONV_ADDR", "adcxx1c__regs_8h.html#adc326e8daab8a87e51f284a57ba1e5e4", null ],
    [ "ADCXX1C_HYSTERESIS_ADDR", "adcxx1c__regs_8h.html#a3001274b5c5384e0f815075ae14e22b9", null ],
    [ "ADCXX1C_LOW_LIMIT_ADDR", "adcxx1c__regs_8h.html#aaf1eb1f46bce75d0fce89a0c92ad885b", null ],
    [ "ADCXX1C_LOWEST_CONV_ADDR", "adcxx1c__regs_8h.html#aaaf0031d219c0915bc405067eac0879d", null ]
];