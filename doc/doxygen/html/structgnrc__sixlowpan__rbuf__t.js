var structgnrc__sixlowpan__rbuf__t =
[
    [ "current_size", "structgnrc__sixlowpan__rbuf__t.html#a06d766e5d8001be6b0aa9c90df82334a", null ],
    [ "dst", "structgnrc__sixlowpan__rbuf__t.html#a4b82543fc94b9e87df281f3c8fad8cc0", null ],
    [ "dst_len", "structgnrc__sixlowpan__rbuf__t.html#a155577c1b887455b8382648cbf29feba", null ],
    [ "pkt", "structgnrc__sixlowpan__rbuf__t.html#ac3ff6108595526ca390a9fb8fdd80d7b", null ],
    [ "src", "structgnrc__sixlowpan__rbuf__t.html#a733375dc7fd2c3a4d0cce2c6ad530408", null ],
    [ "src_len", "structgnrc__sixlowpan__rbuf__t.html#a32b14fda2ab92cd418f77683b3038f70", null ],
    [ "tag", "structgnrc__sixlowpan__rbuf__t.html#aef37195a65e38ed76caefbfd50160909", null ]
];