var group__sys__checksum =
[
    [ "CRC16 (lightweight)", "group__sys__checksum__ucrc16.html", "group__sys__checksum__ucrc16" ],
    [ "CRC16-CCITT", "group__sys__checksum__crc16__ccitt.html", "group__sys__checksum__crc16__ccitt" ],
    [ "Fletcher16", "group__sys__checksum__fletcher16.html", "group__sys__checksum__fletcher16" ],
    [ "Fletcher32", "group__sys__checksum__fletcher32.html", "group__sys__checksum__fletcher32" ]
];