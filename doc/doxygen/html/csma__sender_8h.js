var csma__sender_8h =
[
    [ "CSMA_SENDER_BACKOFF_PERIOD_UNIT", "group__net__csma__sender.html#gafe23231c335816313b8557c56b32a0d0", null ],
    [ "CSMA_SENDER_MAX_BACKOFFS_DEFAULT", "group__net__csma__sender.html#gae0929f308854a8817c2009784de7e43b", null ],
    [ "CSMA_SENDER_MAX_BE_DEFAULT", "group__net__csma__sender.html#ga17f80cc3ae898e83de83d1864dd9c02e", null ],
    [ "CSMA_SENDER_MIN_BE_DEFAULT", "group__net__csma__sender.html#gad8bf13605362c316cbb37224abf65290", null ],
    [ "csma_sender_cca_send", "group__net__csma__sender.html#ga9e80dfc661a85d05360877ea137644dc", null ],
    [ "csma_sender_csma_ca_send", "group__net__csma__sender.html#gad096783a38c6d2666cb0ff777396584b", null ],
    [ "CSMA_SENDER_CONF_DEFAULT", "group__net__csma__sender.html#gadaa5ba030a03ed9adba39220f8743cc6", null ]
];