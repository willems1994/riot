var nrf52_2include_2periph__cpu_8h =
[
    [ "i2c_conf_t", "structi2c__conf__t.html", "structi2c__conf__t" ],
    [ "pwm_conf_t", "structpwm__conf__t.html", "structpwm__conf__t" ],
    [ "ADC_NUMOF", "nrf52_2include_2periph__cpu_8h.html#a2f0c741db24aa2ccded869ba53f6a302", null ],
    [ "CLOCK_CORECLOCK", "nrf52_2include_2periph__cpu_8h.html#afc465f12242e68f6c3695caa3ba0a169", null ],
    [ "HAVE_ADC_RES_T", "nrf52_2include_2periph__cpu_8h.html#ad67a2397ca0e713d34a0f2edf8e62456", null ],
    [ "HAVE_I2C_SPEED_T", "nrf52_2include_2periph__cpu_8h.html#a1229320c595db94034f9fec9efcd46a3", null ],
    [ "HAVE_PWM_MODE_T", "nrf52_2include_2periph__cpu_8h.html#ad3f4b49485a68fa28fac2ab7ef9a9a7f", null ],
    [ "PERIPH_CLOCK", "nrf52_2include_2periph__cpu_8h.html#ab27691583b1a6b3095e4426e8523e954", null ],
    [ "PERIPH_I2C_NEED_READ_REG", "nrf52_2include_2periph__cpu_8h.html#a5ee5980de0aba9d9d03729b400287755", null ],
    [ "PERIPH_I2C_NEED_WRITE_REG", "nrf52_2include_2periph__cpu_8h.html#a826c22d1182510f6ff3129f831b1107b", null ],
    [ "PWM_CHANNELS", "nrf52_2include_2periph__cpu_8h.html#a312588cf680d50931666643dea45bb13", null ],
    [ "PWM_MODE", "nrf52_2include_2periph__cpu_8h.html#a802bbf8c474672e9ff02395d2aac91a4", null ],
    [ "SPI_MISOSEL", "nrf52_2include_2periph__cpu_8h.html#afd9ba8acf11b29577c084506eb84c85b", null ],
    [ "SPI_MOSISEL", "nrf52_2include_2periph__cpu_8h.html#ad23a7b64dc665092d7f87314b1ce7e18", null ],
    [ "SPI_SCKSEL", "nrf52_2include_2periph__cpu_8h.html#a69c3487da96722898ad0abf562158743", null ],
    [ "UART_IRQN", "nrf52_2include_2periph__cpu_8h.html#a422e90dfbad8b08208b33cf489f5ec4c", null ],
    [ "NRF52_AIN0", "nrf52_2include_2periph__cpu_8h.html#abe974f22edeef8189603ba75fd90ff3ca7d8a6d6505b0654ad7af44d549392516", null ],
    [ "NRF52_AIN1", "nrf52_2include_2periph__cpu_8h.html#abe974f22edeef8189603ba75fd90ff3ca7f12fce52b36e7bebd1a90b69d6bbd2c", null ],
    [ "NRF52_AIN2", "nrf52_2include_2periph__cpu_8h.html#abe974f22edeef8189603ba75fd90ff3ca4b47df322eb44f6aeaac38c3157332c1", null ],
    [ "NRF52_AIN3", "nrf52_2include_2periph__cpu_8h.html#abe974f22edeef8189603ba75fd90ff3caa28b23ba837e9e7508c5837ab06dd5a9", null ],
    [ "NRF52_AIN4", "nrf52_2include_2periph__cpu_8h.html#abe974f22edeef8189603ba75fd90ff3ca8c76437be012f9778c81dfe0dfd96f5a", null ],
    [ "NRF52_AIN5", "nrf52_2include_2periph__cpu_8h.html#abe974f22edeef8189603ba75fd90ff3cadcd0f69a94f72ecaefe111cc556a17ab", null ],
    [ "NRF52_AIN6", "nrf52_2include_2periph__cpu_8h.html#abe974f22edeef8189603ba75fd90ff3ca671ed4777b8a34db7e33edcacae6c705", null ],
    [ "NRF52_AIN7", "nrf52_2include_2periph__cpu_8h.html#abe974f22edeef8189603ba75fd90ff3cacac5953d119e4b3d32814a599400b7e2", null ],
    [ "NRF52_VDD", "nrf52_2include_2periph__cpu_8h.html#abe974f22edeef8189603ba75fd90ff3caa75e405cfd0772c9ed403068fe380bde", null ],
    [ "adc_res_t", "nrf52_2include_2periph__cpu_8h.html#ae4d48fdca21097fd8b34324f33ae4020", [
      [ "ADC_RES_6BIT", "group__drivers__periph__adc.html#ggae4d48fdca21097fd8b34324f33ae4020ad8993dbcb448737453754f2b371903ac", null ],
      [ "ADC_RES_7BIT", "group__drivers__periph__adc.html#ggae4d48fdca21097fd8b34324f33ae4020a44e4b27c8a6788cb325078cf4dbac501", null ],
      [ "ADC_RES_8BIT", "group__drivers__periph__adc.html#ggae4d48fdca21097fd8b34324f33ae4020a97a7b6a04d04ff2aa5215e7ff99b9639", null ],
      [ "ADC_RES_9BIT", "group__drivers__periph__adc.html#ggae4d48fdca21097fd8b34324f33ae4020a672b0464e1f5c2b971877ac50a1b99c7", null ],
      [ "ADC_RES_10BIT", "group__drivers__periph__adc.html#ggae4d48fdca21097fd8b34324f33ae4020ae21291741a3a61209b20161449066b5a", null ],
      [ "ADC_RES_12BIT", "group__drivers__periph__adc.html#ggae4d48fdca21097fd8b34324f33ae4020aed92ce547f7c8a6561c3a161066a5fe4", null ],
      [ "ADC_RES_14BIT", "group__drivers__periph__adc.html#ggae4d48fdca21097fd8b34324f33ae4020ac0ae241188655074aac9911748e0bc59", null ],
      [ "ADC_RES_16BIT", "group__drivers__periph__adc.html#ggae4d48fdca21097fd8b34324f33ae4020a272f6369b4ad8f3fa155ac13a2b5bfda", null ],
      [ "ADC_RES_10BIT", "group__drivers__periph__adc.html#ggae4d48fdca21097fd8b34324f33ae4020ae21291741a3a61209b20161449066b5a", null ],
      [ "ADC_RES_6BIT", "group__drivers__periph__adc.html#ggae4d48fdca21097fd8b34324f33ae4020ad8993dbcb448737453754f2b371903ac", null ],
      [ "ADC_RES_8BIT", "group__drivers__periph__adc.html#ggae4d48fdca21097fd8b34324f33ae4020a97a7b6a04d04ff2aa5215e7ff99b9639", null ],
      [ "ADC_RES_10BIT", "group__drivers__periph__adc.html#ggae4d48fdca21097fd8b34324f33ae4020ae21291741a3a61209b20161449066b5a", null ],
      [ "ADC_RES_12BIT", "group__drivers__periph__adc.html#ggae4d48fdca21097fd8b34324f33ae4020aed92ce547f7c8a6561c3a161066a5fe4", null ],
      [ "ADC_RES_14BIT", "group__drivers__periph__adc.html#ggae4d48fdca21097fd8b34324f33ae4020ac0ae241188655074aac9911748e0bc59", null ],
      [ "ADC_RES_16BIT", "group__drivers__periph__adc.html#ggae4d48fdca21097fd8b34324f33ae4020a272f6369b4ad8f3fa155ac13a2b5bfda", null ],
      [ "ADC_RES_6BIT", "group__drivers__periph__adc.html#ggae4d48fdca21097fd8b34324f33ae4020ad8993dbcb448737453754f2b371903ac", null ],
      [ "ADC_RES_8BIT", "group__drivers__periph__adc.html#ggae4d48fdca21097fd8b34324f33ae4020a97a7b6a04d04ff2aa5215e7ff99b9639", null ],
      [ "ADC_RES_10BIT", "group__drivers__periph__adc.html#ggae4d48fdca21097fd8b34324f33ae4020ae21291741a3a61209b20161449066b5a", null ],
      [ "ADC_RES_12BIT", "group__drivers__periph__adc.html#ggae4d48fdca21097fd8b34324f33ae4020aed92ce547f7c8a6561c3a161066a5fe4", null ],
      [ "ADC_RES_14BIT", "group__drivers__periph__adc.html#ggae4d48fdca21097fd8b34324f33ae4020ac0ae241188655074aac9911748e0bc59", null ],
      [ "ADC_RES_16BIT", "group__drivers__periph__adc.html#ggae4d48fdca21097fd8b34324f33ae4020a272f6369b4ad8f3fa155ac13a2b5bfda", null ],
      [ "ADC_RES_6BIT", "group__drivers__periph__adc.html#ggae4d48fdca21097fd8b34324f33ae4020ad8993dbcb448737453754f2b371903ac", null ],
      [ "ADC_RES_8BIT", "group__drivers__periph__adc.html#ggae4d48fdca21097fd8b34324f33ae4020a97a7b6a04d04ff2aa5215e7ff99b9639", null ],
      [ "ADC_RES_10BIT", "group__drivers__periph__adc.html#ggae4d48fdca21097fd8b34324f33ae4020ae21291741a3a61209b20161449066b5a", null ],
      [ "ADC_RES_12BIT", "group__drivers__periph__adc.html#ggae4d48fdca21097fd8b34324f33ae4020aed92ce547f7c8a6561c3a161066a5fe4", null ],
      [ "ADC_RES_14BIT", "group__drivers__periph__adc.html#ggae4d48fdca21097fd8b34324f33ae4020ac0ae241188655074aac9911748e0bc59", null ],
      [ "ADC_RES_16BIT", "group__drivers__periph__adc.html#ggae4d48fdca21097fd8b34324f33ae4020a272f6369b4ad8f3fa155ac13a2b5bfda", null ],
      [ "ADC_RES_6BIT", "group__drivers__periph__adc.html#ggae4d48fdca21097fd8b34324f33ae4020ad8993dbcb448737453754f2b371903ac", null ],
      [ "ADC_RES_8BIT", "group__drivers__periph__adc.html#ggae4d48fdca21097fd8b34324f33ae4020a97a7b6a04d04ff2aa5215e7ff99b9639", null ],
      [ "ADC_RES_10BIT", "group__drivers__periph__adc.html#ggae4d48fdca21097fd8b34324f33ae4020ae21291741a3a61209b20161449066b5a", null ],
      [ "ADC_RES_12BIT", "group__drivers__periph__adc.html#ggae4d48fdca21097fd8b34324f33ae4020aed92ce547f7c8a6561c3a161066a5fe4", null ],
      [ "ADC_RES_14BIT", "group__drivers__periph__adc.html#ggae4d48fdca21097fd8b34324f33ae4020ac0ae241188655074aac9911748e0bc59", null ],
      [ "ADC_RES_16BIT", "group__drivers__periph__adc.html#ggae4d48fdca21097fd8b34324f33ae4020a272f6369b4ad8f3fa155ac13a2b5bfda", null ],
      [ "ADC_RES_6BIT", "group__drivers__periph__adc.html#ggae4d48fdca21097fd8b34324f33ae4020ad8993dbcb448737453754f2b371903ac", null ],
      [ "ADC_RES_8BIT", "group__drivers__periph__adc.html#ggae4d48fdca21097fd8b34324f33ae4020a97a7b6a04d04ff2aa5215e7ff99b9639", null ],
      [ "ADC_RES_10BIT", "group__drivers__periph__adc.html#ggae4d48fdca21097fd8b34324f33ae4020ae21291741a3a61209b20161449066b5a", null ],
      [ "ADC_RES_12BIT", "group__drivers__periph__adc.html#ggae4d48fdca21097fd8b34324f33ae4020aed92ce547f7c8a6561c3a161066a5fe4", null ],
      [ "ADC_RES_14BIT", "group__drivers__periph__adc.html#ggae4d48fdca21097fd8b34324f33ae4020ac0ae241188655074aac9911748e0bc59", null ],
      [ "ADC_RES_16BIT", "group__drivers__periph__adc.html#ggae4d48fdca21097fd8b34324f33ae4020a272f6369b4ad8f3fa155ac13a2b5bfda", null ],
      [ "ADC_RES_6BIT", "group__drivers__periph__adc.html#ggae4d48fdca21097fd8b34324f33ae4020ad8993dbcb448737453754f2b371903ac", null ],
      [ "ADC_RES_8BIT", "group__drivers__periph__adc.html#ggae4d48fdca21097fd8b34324f33ae4020a97a7b6a04d04ff2aa5215e7ff99b9639", null ],
      [ "ADC_RES_10BIT", "group__drivers__periph__adc.html#ggae4d48fdca21097fd8b34324f33ae4020ae21291741a3a61209b20161449066b5a", null ],
      [ "ADC_RES_12BIT", "group__drivers__periph__adc.html#ggae4d48fdca21097fd8b34324f33ae4020aed92ce547f7c8a6561c3a161066a5fe4", null ],
      [ "ADC_RES_14BIT", "group__drivers__periph__adc.html#ggae4d48fdca21097fd8b34324f33ae4020ac0ae241188655074aac9911748e0bc59", null ],
      [ "ADC_RES_16BIT", "group__drivers__periph__adc.html#ggae4d48fdca21097fd8b34324f33ae4020a272f6369b4ad8f3fa155ac13a2b5bfda", null ],
      [ "ADC_RES_6BIT", "group__drivers__periph__adc.html#ggae4d48fdca21097fd8b34324f33ae4020ad8993dbcb448737453754f2b371903ac", null ],
      [ "ADC_RES_8BIT", "group__drivers__periph__adc.html#ggae4d48fdca21097fd8b34324f33ae4020a97a7b6a04d04ff2aa5215e7ff99b9639", null ],
      [ "ADC_RES_10BIT", "group__drivers__periph__adc.html#ggae4d48fdca21097fd8b34324f33ae4020ae21291741a3a61209b20161449066b5a", null ],
      [ "ADC_RES_12BIT", "group__drivers__periph__adc.html#ggae4d48fdca21097fd8b34324f33ae4020aed92ce547f7c8a6561c3a161066a5fe4", null ],
      [ "ADC_RES_14BIT", "group__drivers__periph__adc.html#ggae4d48fdca21097fd8b34324f33ae4020ac0ae241188655074aac9911748e0bc59", null ],
      [ "ADC_RES_16BIT", "group__drivers__periph__adc.html#ggae4d48fdca21097fd8b34324f33ae4020a272f6369b4ad8f3fa155ac13a2b5bfda", null ],
      [ "ADC_RES_6BIT", "group__drivers__periph__adc.html#ggae4d48fdca21097fd8b34324f33ae4020ad8993dbcb448737453754f2b371903ac", null ],
      [ "ADC_RES_8BIT", "group__drivers__periph__adc.html#ggae4d48fdca21097fd8b34324f33ae4020a97a7b6a04d04ff2aa5215e7ff99b9639", null ],
      [ "ADC_RES_10BIT", "group__drivers__periph__adc.html#ggae4d48fdca21097fd8b34324f33ae4020ae21291741a3a61209b20161449066b5a", null ],
      [ "ADC_RES_12BIT", "group__drivers__periph__adc.html#ggae4d48fdca21097fd8b34324f33ae4020aed92ce547f7c8a6561c3a161066a5fe4", null ],
      [ "ADC_RES_14BIT", "group__drivers__periph__adc.html#ggae4d48fdca21097fd8b34324f33ae4020ac0ae241188655074aac9911748e0bc59", null ],
      [ "ADC_RES_16BIT", "group__drivers__periph__adc.html#ggae4d48fdca21097fd8b34324f33ae4020a272f6369b4ad8f3fa155ac13a2b5bfda", null ]
    ] ],
    [ "i2c_speed_t", "nrf52_2include_2periph__cpu_8h.html#a6e6a870f98abb8cffa95373b69fb8243", [
      [ "I2C_SPEED_LOW", "group__drivers__periph__i2c.html#gga6e6a870f98abb8cffa95373b69fb8243a6b350d376580872bb53bdfc4ff41d9b0", null ],
      [ "I2C_SPEED_NORMAL", "group__drivers__periph__i2c.html#gga6e6a870f98abb8cffa95373b69fb8243a0826bf5711e82ba26b4ada6104260583", null ],
      [ "I2C_SPEED_FAST", "group__drivers__periph__i2c.html#gga6e6a870f98abb8cffa95373b69fb8243a8dc6cae939d20d56a78d123365caa4ca", null ],
      [ "I2C_SPEED_FAST_PLUS", "group__drivers__periph__i2c.html#gga6e6a870f98abb8cffa95373b69fb8243a42e708fe61f237d88f6cf53f32e17e8e", null ],
      [ "I2C_SPEED_HIGH", "group__drivers__periph__i2c.html#gga6e6a870f98abb8cffa95373b69fb8243a745e792241485f11092fabd600fd6b48", null ],
      [ "I2C_SPEED_LOW", "group__drivers__periph__i2c.html#gga6e6a870f98abb8cffa95373b69fb8243a6b350d376580872bb53bdfc4ff41d9b0", null ],
      [ "I2C_SPEED_NORMAL", "group__drivers__periph__i2c.html#gga6e6a870f98abb8cffa95373b69fb8243a0826bf5711e82ba26b4ada6104260583", null ],
      [ "I2C_SPEED_FAST", "group__drivers__periph__i2c.html#gga6e6a870f98abb8cffa95373b69fb8243a8dc6cae939d20d56a78d123365caa4ca", null ],
      [ "I2C_SPEED_FAST_PLUS", "group__drivers__periph__i2c.html#gga6e6a870f98abb8cffa95373b69fb8243a42e708fe61f237d88f6cf53f32e17e8e", null ],
      [ "I2C_SPEED_HIGH", "group__drivers__periph__i2c.html#gga6e6a870f98abb8cffa95373b69fb8243a745e792241485f11092fabd600fd6b48", null ],
      [ "I2C_SPEED_LOW", "group__drivers__periph__i2c.html#gga6e6a870f98abb8cffa95373b69fb8243a6b350d376580872bb53bdfc4ff41d9b0", null ],
      [ "I2C_SPEED_NORMAL", "group__drivers__periph__i2c.html#gga6e6a870f98abb8cffa95373b69fb8243a0826bf5711e82ba26b4ada6104260583", null ],
      [ "I2C_SPEED_FAST", "group__drivers__periph__i2c.html#gga6e6a870f98abb8cffa95373b69fb8243a8dc6cae939d20d56a78d123365caa4ca", null ],
      [ "I2C_SPEED_FAST_PLUS", "group__drivers__periph__i2c.html#gga6e6a870f98abb8cffa95373b69fb8243a42e708fe61f237d88f6cf53f32e17e8e", null ],
      [ "I2C_SPEED_HIGH", "group__drivers__periph__i2c.html#gga6e6a870f98abb8cffa95373b69fb8243a745e792241485f11092fabd600fd6b48", null ],
      [ "I2C_SPEED_LOW", "group__drivers__periph__i2c.html#gga6e6a870f98abb8cffa95373b69fb8243a6b350d376580872bb53bdfc4ff41d9b0", null ],
      [ "I2C_SPEED_NORMAL", "group__drivers__periph__i2c.html#gga6e6a870f98abb8cffa95373b69fb8243a0826bf5711e82ba26b4ada6104260583", null ],
      [ "I2C_SPEED_FAST", "group__drivers__periph__i2c.html#gga6e6a870f98abb8cffa95373b69fb8243a8dc6cae939d20d56a78d123365caa4ca", null ],
      [ "I2C_SPEED_FAST_PLUS", "group__drivers__periph__i2c.html#gga6e6a870f98abb8cffa95373b69fb8243a42e708fe61f237d88f6cf53f32e17e8e", null ],
      [ "I2C_SPEED_HIGH", "group__drivers__periph__i2c.html#gga6e6a870f98abb8cffa95373b69fb8243a745e792241485f11092fabd600fd6b48", null ],
      [ "I2C_SPEED_NORMAL", "group__drivers__periph__i2c.html#gga6e6a870f98abb8cffa95373b69fb8243a0826bf5711e82ba26b4ada6104260583", null ],
      [ "I2C_SPEED_FAST", "group__drivers__periph__i2c.html#gga6e6a870f98abb8cffa95373b69fb8243a8dc6cae939d20d56a78d123365caa4ca", null ],
      [ "I2C_SPEED_LOW", "group__drivers__periph__i2c.html#gga6e6a870f98abb8cffa95373b69fb8243a6b350d376580872bb53bdfc4ff41d9b0", null ],
      [ "I2C_SPEED_NORMAL", "group__drivers__periph__i2c.html#gga6e6a870f98abb8cffa95373b69fb8243a0826bf5711e82ba26b4ada6104260583", null ],
      [ "I2C_SPEED_FAST", "group__drivers__periph__i2c.html#gga6e6a870f98abb8cffa95373b69fb8243a8dc6cae939d20d56a78d123365caa4ca", null ],
      [ "I2C_SPEED_FAST_PLUS", "group__drivers__periph__i2c.html#gga6e6a870f98abb8cffa95373b69fb8243a42e708fe61f237d88f6cf53f32e17e8e", null ],
      [ "I2C_SPEED_HIGH", "group__drivers__periph__i2c.html#gga6e6a870f98abb8cffa95373b69fb8243a745e792241485f11092fabd600fd6b48", null ]
    ] ],
    [ "pwm_mode_t", "nrf52_2include_2periph__cpu_8h.html#a562b5946a0edd6f5eebb63db7d154d56", [
      [ "PWM_LEFT", "group__drivers__periph__pwm.html#gga562b5946a0edd6f5eebb63db7d154d56ae55d6c4908a2d9ee818e546ebabd35ba", null ],
      [ "PWM_RIGHT", "group__drivers__periph__pwm.html#gga562b5946a0edd6f5eebb63db7d154d56aec8c768bb78a2a239515679e8e2da73d", null ],
      [ "PWM_CENTER", "group__drivers__periph__pwm.html#gga562b5946a0edd6f5eebb63db7d154d56aa702c2d37cded311e8770035242e2ed3", null ],
      [ "PWM_CENTER_INV", "group__drivers__periph__pwm.html#gga562b5946a0edd6f5eebb63db7d154d56a6ee64b338aef03e37588787db9e72526", null ],
      [ "PWM_LEFT", "group__drivers__periph__pwm.html#gga562b5946a0edd6f5eebb63db7d154d56ae55d6c4908a2d9ee818e546ebabd35ba", null ],
      [ "PWM_RIGHT", "group__drivers__periph__pwm.html#gga562b5946a0edd6f5eebb63db7d154d56aec8c768bb78a2a239515679e8e2da73d", null ],
      [ "PWM_CENTER", "group__drivers__periph__pwm.html#gga562b5946a0edd6f5eebb63db7d154d56aa702c2d37cded311e8770035242e2ed3", null ]
    ] ]
];