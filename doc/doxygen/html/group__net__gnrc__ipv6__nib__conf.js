var group__net__gnrc__ipv6__nib__conf =
[
    [ "ipv6/nib/conf.h", "ipv6_2nib_2conf_8h.html", null ],
    [ "GNRC_IPV6_NIB_ABR_NUMOF", "group__net__gnrc__ipv6__nib__conf.html#ga3034a888fac2257faad135df998a77c3", null ],
    [ "GNRC_IPV6_NIB_CONF_6LBR", "group__net__gnrc__ipv6__nib__conf.html#gaae0836aa6e9d9af3b3a465c704764a7d", null ],
    [ "GNRC_IPV6_NIB_CONF_6LN", "group__net__gnrc__ipv6__nib__conf.html#ga262861f7bedb8ec92e41645ea2fd8618", null ],
    [ "GNRC_IPV6_NIB_CONF_6LR", "group__net__gnrc__ipv6__nib__conf.html#ga33488e18c924ee07257e923978d00156", null ],
    [ "GNRC_IPV6_NIB_CONF_ADV_ROUTER", "group__net__gnrc__ipv6__nib__conf.html#gaf7d23fde27944d67998eb8ce1add6313", null ],
    [ "GNRC_IPV6_NIB_CONF_ARSM", "group__net__gnrc__ipv6__nib__conf.html#ga1d53291e6a89b3b94f13fe814c0c5bf8", null ],
    [ "GNRC_IPV6_NIB_CONF_DC", "group__net__gnrc__ipv6__nib__conf.html#gad4e438be232c04d850ed7a5f52936de6", null ],
    [ "GNRC_IPV6_NIB_CONF_DNS", "group__net__gnrc__ipv6__nib__conf.html#ga2c7f1cdb084990c976edc0aee3ed21fe", null ],
    [ "GNRC_IPV6_NIB_CONF_MULTIHOP_DAD", "group__net__gnrc__ipv6__nib__conf.html#ga13a9ce57d8ab5fb04f3eb2226b422c90", null ],
    [ "GNRC_IPV6_NIB_CONF_MULTIHOP_P6C", "group__net__gnrc__ipv6__nib__conf.html#ga5564169f60995daa34fb17af5d471fff", null ],
    [ "GNRC_IPV6_NIB_CONF_NO_RTR_SOL", "group__net__gnrc__ipv6__nib__conf.html#ga610772595efb78421ac860d85f5059c8", null ],
    [ "GNRC_IPV6_NIB_CONF_QUEUE_PKT", "group__net__gnrc__ipv6__nib__conf.html#gab703a80f339cf89a041cd14e0f27314f", null ],
    [ "GNRC_IPV6_NIB_CONF_REACH_TIME_RESET", "group__net__gnrc__ipv6__nib__conf.html#ga1a7b302a397b7efe735226fadf797e3f", null ],
    [ "GNRC_IPV6_NIB_CONF_REDIRECT", "group__net__gnrc__ipv6__nib__conf.html#ga4840ac470de98297ed77bd38b56f09a7", null ],
    [ "GNRC_IPV6_NIB_CONF_ROUTER", "group__net__gnrc__ipv6__nib__conf.html#ga3b71a95f44b285aaf973cb23bff92201", null ],
    [ "GNRC_IPV6_NIB_CONF_SLAAC", "group__net__gnrc__ipv6__nib__conf.html#gaa47a6efaa52f16c15e9858994a395ce5", null ],
    [ "GNRC_IPV6_NIB_DEFAULT_ROUTER_NUMOF", "group__net__gnrc__ipv6__nib__conf.html#ga4e9f6e1284255cda0e59d8da3fb4fa59", null ],
    [ "GNRC_IPV6_NIB_L2ADDR_MAX_LEN", "group__net__gnrc__ipv6__nib__conf.html#ga98164bf9afdadab1f553b00c5f39766a", null ],
    [ "GNRC_IPV6_NIB_NUMOF", "group__net__gnrc__ipv6__nib__conf.html#gad549499055dcce8399ebf30f6cb5073c", null ],
    [ "GNRC_IPV6_NIB_OFFL_NUMOF", "group__net__gnrc__ipv6__nib__conf.html#gae319bc32b0670b25529b746b9aa019b1", null ]
];