var group__drivers__mtd__spi__nor =
[
    [ "mtd_spi_nor.h", "mtd__spi__nor_8h.html", null ],
    [ "mtd_spi_nor_opcode_t", "structmtd__spi__nor__opcode__t.html", [
      [ "block_erase", "structmtd__spi__nor__opcode__t.html#ae46f800c7b42ad747c00fad9bc436252", null ],
      [ "block_erase_32k", "structmtd__spi__nor__opcode__t.html#a5916a6c57d52e6d74f71a939c38a1f6d", null ],
      [ "chip_erase", "structmtd__spi__nor__opcode__t.html#a901ead7b51c7cf93f41e7d469fc595bd", null ],
      [ "page_program", "structmtd__spi__nor__opcode__t.html#a8af08f0967f056abf4ce3a4cb68082cc", null ],
      [ "rdid", "structmtd__spi__nor__opcode__t.html#af51dbf25ff0b397c32f32e6b43e35f9d", null ],
      [ "rdsr", "structmtd__spi__nor__opcode__t.html#afc83f703e757f0bd0542ac3ca2b25e2c", null ],
      [ "read", "structmtd__spi__nor__opcode__t.html#a8f30ed1fee594d3d9e0c22d6790e54d2", null ],
      [ "read_fast", "structmtd__spi__nor__opcode__t.html#a4c10632a87fe45794c676d1564ffd68d", null ],
      [ "sector_erase", "structmtd__spi__nor__opcode__t.html#aea528e7adb614cdda7cf3772d76aa9d8", null ],
      [ "sleep", "structmtd__spi__nor__opcode__t.html#ae48b6a09752f9867fa6c40b70a91bc5c", null ],
      [ "wake", "structmtd__spi__nor__opcode__t.html#ac13b15fcf2592c5121453a870b32d779", null ],
      [ "wren", "structmtd__spi__nor__opcode__t.html#af56bcd77e3cfb43279eb4643a98d177a", null ],
      [ "wrsr", "structmtd__spi__nor__opcode__t.html#a54eb3fb37a5b86fe96b094c428be5e10", null ]
    ] ],
    [ "mtd_jedec_id_t", "structmtd__jedec__id__t.html", [
      [ "bank", "structmtd__jedec__id__t.html#a8526c01842b574c90266fd284e447ff4", null ],
      [ "device", "structmtd__jedec__id__t.html#a5fbeb3242181db80f8b9e65726358b8d", null ],
      [ "manuf", "structmtd__jedec__id__t.html#a9a2a181e0ce764044787ab7366d56adf", null ]
    ] ],
    [ "mtd_spi_nor_t", "structmtd__spi__nor__t.html", [
      [ "addr_width", "structmtd__spi__nor__t.html#af3375ee883b3291e7d4d5525367c9749", null ],
      [ "base", "structmtd__spi__nor__t.html#a5e7566a79757a8b3ae3bc010cce63d6e", null ],
      [ "clk", "structmtd__spi__nor__t.html#a8fedcf89207b178a20b913ce797f6bc4", null ],
      [ "cs", "structmtd__spi__nor__t.html#a093928ce64653beb41fcdf612749b794", null ],
      [ "flag", "structmtd__spi__nor__t.html#a5cd102d2ec5ae3247454d43faecf84a9", null ],
      [ "jedec_id", "structmtd__spi__nor__t.html#a4af66c3f88d6b33431237515c48dc774", null ],
      [ "mode", "structmtd__spi__nor__t.html#a63b56128b191014f033989272d9142ab", null ],
      [ "opcode", "structmtd__spi__nor__t.html#a8eb32909423b96bcdacdd8f9c9715861", null ],
      [ "page_addr_mask", "structmtd__spi__nor__t.html#a0602ceba18303c236cb39bfb516d9f35", null ],
      [ "page_addr_shift", "structmtd__spi__nor__t.html#ac5e8c0762d54a9cb9b71b8864eba9e26", null ],
      [ "sec_addr_mask", "structmtd__spi__nor__t.html#abc5fddace93fba81caaed121bf866fac", null ],
      [ "sec_addr_shift", "structmtd__spi__nor__t.html#a7093bbebb387f2ba9b7fcb11630592a7", null ],
      [ "spi", "structmtd__spi__nor__t.html#ae42db35746739424e2c8608f9bb50e70", null ]
    ] ],
    [ "JEDEC_NEXT_BANK", "group__drivers__mtd__spi__nor.html#gaea62da6fc7567e17f64b838d0e94a0c6", null ],
    [ "SPI_NOR_F_SECT_32K", "group__drivers__mtd__spi__nor.html#ga9dc56c1141d961001a1f64fff4bfa6f7", null ],
    [ "SPI_NOR_F_SECT_4K", "group__drivers__mtd__spi__nor.html#ga4c5a7b9da43ceef5c8f3f173cc9813e2", null ],
    [ "mtd_spi_nor_driver", "group__drivers__mtd__spi__nor.html#gac22c586a8b05f92d01bbbcfffaec552b", null ],
    [ "mtd_spi_nor_opcode_default", "group__drivers__mtd__spi__nor.html#ga3125f86f0fccaee059286f3e24fb20b4", null ],
    [ "mtd_spi_nor_opcode_default_4bytes", "group__drivers__mtd__spi__nor.html#gae5b39cdad8b563b2d6e3844cb868ace8", null ]
];