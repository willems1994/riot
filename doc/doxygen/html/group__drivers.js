var group__drivers =
[
    [ "AT (Hayes) command set library", "group__drivers__at.html", "group__drivers__at" ],
    [ "Actuator Device Drivers", "group__drivers__actuators.html", "group__drivers__actuators" ],
    [ "Network Device Drivers", "group__drivers__netdev.html", "group__drivers__netdev" ],
    [ "Peripheral Driver Interface", "group__drivers__periph.html", "group__drivers__periph" ],
    [ "Sensor Device Drivers", "group__drivers__sensors.html", "group__drivers__sensors" ],
    [ "Soft Peripheral Driver Interface", "group__drivers__soft__periph.html", "group__drivers__soft__periph" ],
    [ "Storage Device Drivers", "group__drivers__storage.html", "group__drivers__storage" ],
    [ "[S]ensor [A]ctuator [U]ber [L]ayer", "group__drivers__saul.html", "group__drivers__saul" ]
];