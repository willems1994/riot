var io1__xplained__internals_8h =
[
    [ "IO1_GPIO1_PIN", "io1__xplained__internals_8h.html#ae3905530c260269a8de18faedfc45e2e", null ],
    [ "IO1_GPIO2_PIN", "io1__xplained__internals_8h.html#a73105666720690aec9b8e7db2e4c1197", null ],
    [ "IO1_LED_PIN", "io1__xplained__internals_8h.html#aad430a8d9cf321fc07c43f0a9f4ece51", null ],
    [ "IO1_LIGHT_ADC_LINE", "io1__xplained__internals_8h.html#a0b5a9fc195b066f3862bb7e4ad7df486", null ],
    [ "IO1_LIGHT_ADC_RES", "io1__xplained__internals_8h.html#a55f05a0b5e1795ae0a59bc8b6a6e0b7e", null ],
    [ "IO1_SDCARD_SPI_PARAM_CLK", "io1__xplained__internals_8h.html#a014185630fb0195f153a5158cf98d4f6", null ],
    [ "IO1_SDCARD_SPI_PARAM_CS", "io1__xplained__internals_8h.html#a7e6cbf07b001338a7597e877a79b36d6", null ],
    [ "IO1_SDCARD_SPI_PARAM_DETECT", "io1__xplained__internals_8h.html#a10414214452fa9ab4e322a8a8f0ddce7", null ],
    [ "IO1_SDCARD_SPI_PARAM_MISO", "io1__xplained__internals_8h.html#a4031b2e9b272d0159de9ae2f5af878ec", null ],
    [ "IO1_SDCARD_SPI_PARAM_MOSI", "io1__xplained__internals_8h.html#ad3e8da12f0e67af8e4159fe3ad331fc1", null ],
    [ "IO1_SDCARD_SPI_PARAM_POWER", "io1__xplained__internals_8h.html#a04809b3eac02564fe9a21bc5abae0737", null ],
    [ "IO1_SDCARD_SPI_PARAM_POWER_AH", "io1__xplained__internals_8h.html#a56031ff0cc2e3c520cf75d8d22fba132", null ],
    [ "IO1_SDCARD_SPI_PARAM_SPI", "io1__xplained__internals_8h.html#a2116924eb9c99734e7dd50a11d59dc00", null ],
    [ "IO1_TEMPERATURE_BASE_ADDR", "io1__xplained__internals_8h.html#afcd1c97a848026981ae3e2e6576d9471", null ],
    [ "IO1_TEMPERATURE_DEFAULT_ADDR", "io1__xplained__internals_8h.html#a7662a18c6f3cdf48155baa7eea12f315", null ]
];