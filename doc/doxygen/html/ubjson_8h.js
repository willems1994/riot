var ubjson_8h =
[
    [ "ubjson_cookie_t", "group__sys__ubjson.html#ga357ee85fb5adeb3e26b2645ef7e1e165", null ],
    [ "ubjson_read_callback_t", "group__sys__ubjson.html#ga03c6a3e3d3b55322798f92b53d9d9d4d", null ],
    [ "ubjson_read_t", "group__sys__ubjson.html#ga01ae5c60e45427448376cc7455c85514", null ],
    [ "ubjson_write_t", "group__sys__ubjson.html#ga65f5b3d2ea03d533d23512d4aa8e0e1f", null ],
    [ "ubjson_int32_type_t", "group__sys__ubjson.html#ga7f09721040bc53020791b7cb6de33426", [
      [ "UBJSON_INT32_INT8", "group__sys__ubjson.html#gga7f09721040bc53020791b7cb6de33426aa930bc10cbe8a68f20a6aceb7a788c90", null ],
      [ "UBJSON_INT32_UINT8", "group__sys__ubjson.html#gga7f09721040bc53020791b7cb6de33426a9d2bc23ed6c572228029ead6f3b3a6d7", null ],
      [ "UBJSON_INT32_INT16", "group__sys__ubjson.html#gga7f09721040bc53020791b7cb6de33426a3a9ae14c042d6df28c3f72fc20fe6125", null ],
      [ "UBJSON_INT32_INT32", "group__sys__ubjson.html#gga7f09721040bc53020791b7cb6de33426a307dcf8027b63b384d3e0255ddf50fd6", null ]
    ] ],
    [ "ubjson_read_callback_result_t", "group__sys__ubjson.html#ga6fa01f6c3406134b5f1c5e02753f98ae", [
      [ "UBJSON_OKAY", "group__sys__ubjson.html#gga6fa01f6c3406134b5f1c5e02753f98aeabf7074498fb8565ec892509a8aef59da", null ],
      [ "UBJSON_ABORTED", "group__sys__ubjson.html#gga6fa01f6c3406134b5f1c5e02753f98aea479707b559c7b77d223361bb8aa4452e", null ],
      [ "UBJSON_INVALID_DATA", "group__sys__ubjson.html#gga6fa01f6c3406134b5f1c5e02753f98aea6061942ed0a2b9abe27ad2ee0ad8852f", null ],
      [ "UBJSON_PREMATURELY_ENDED", "group__sys__ubjson.html#gga6fa01f6c3406134b5f1c5e02753f98aea837f3deb04fadd9ec3b0c9af03589c91", null ],
      [ "UBJSON_SIZE_ERROR", "group__sys__ubjson.html#gga6fa01f6c3406134b5f1c5e02753f98aeaaee35b00798bbea67405fbbc071ad87d", null ]
    ] ],
    [ "ubjson_type_t", "group__sys__ubjson.html#ga3863c33b19602aa200d67e545692ce6d", [
      [ "UBJSON_ABSENT", "group__sys__ubjson.html#gga3863c33b19602aa200d67e545692ce6da9d8684513ea6f3a6a82c54939f430627", null ],
      [ "UBJSON_TYPE_NULL", "group__sys__ubjson.html#gga3863c33b19602aa200d67e545692ce6da10d4ebf108470152591e4cc457c7a7ef", null ],
      [ "UBJSON_TYPE_NOOP", "group__sys__ubjson.html#gga3863c33b19602aa200d67e545692ce6dacc0b4d47fccf258d2625930a5573851d", null ],
      [ "UBJSON_TYPE_BOOL", "group__sys__ubjson.html#gga3863c33b19602aa200d67e545692ce6da183557358c9cf366dedee5eb2b781e80", null ],
      [ "UBJSON_TYPE_INT32", "group__sys__ubjson.html#gga3863c33b19602aa200d67e545692ce6da047c9589c29f5ff4489a24c0acf129a5", null ],
      [ "UBJSON_TYPE_INT64", "group__sys__ubjson.html#gga3863c33b19602aa200d67e545692ce6daff9ea5dabb6f6ff3156a31b964e89ef8", null ],
      [ "UBJSON_TYPE_FLOAT", "group__sys__ubjson.html#gga3863c33b19602aa200d67e545692ce6da0f565014b6c19236bd8afb2c16085437", null ],
      [ "UBJSON_TYPE_DOUBLE", "group__sys__ubjson.html#gga3863c33b19602aa200d67e545692ce6dafb6736473d4badc3af8a62b2457e4d7f", null ],
      [ "UBJSON_TYPE_STRING", "group__sys__ubjson.html#gga3863c33b19602aa200d67e545692ce6da686676759979cf14de5f23cdc2963ec0", null ],
      [ "UBJSON_ENTER_ARRAY", "group__sys__ubjson.html#gga3863c33b19602aa200d67e545692ce6da0d0b8420f90dc1480efcf4fda07f899f", null ],
      [ "UBJSON_ENTER_OBJECT", "group__sys__ubjson.html#gga3863c33b19602aa200d67e545692ce6daadd458c3113f7bb42012c4cd8986fcdc", null ],
      [ "UBJSON_INDEX", "group__sys__ubjson.html#gga3863c33b19602aa200d67e545692ce6dac0933bc1dc21b951f7d1356ae98cfc38", null ],
      [ "UBJSON_KEY", "group__sys__ubjson.html#gga3863c33b19602aa200d67e545692ce6daacf28ffda0d8683a5a6970f2cb4aaeb6", null ]
    ] ],
    [ "ubjson_close_array", "group__sys__ubjson.html#gac90e8a51d051ef8cc675029422c35a79", null ],
    [ "ubjson_close_object", "group__sys__ubjson.html#gad9ba7ab11f2121aa44e4aa44f878694f", null ],
    [ "ubjson_get_bool", "group__sys__ubjson.html#gade2b8a93b9facdd0d01396c09f22e45c", null ],
    [ "ubjson_get_double", "group__sys__ubjson.html#ga5e01348fe1252d2ac901d15eafd74ac6", null ],
    [ "ubjson_get_float", "group__sys__ubjson.html#gaf26af3545ce1a40d8bdccc302f3e67b9", null ],
    [ "ubjson_get_i32", "group__sys__ubjson.html#ga716fe7841f266c16fb4b0970d0a1c01a", null ],
    [ "ubjson_get_i64", "group__sys__ubjson.html#ga9cbdc33a906956426eb04005f827f810", null ],
    [ "ubjson_get_string", "group__sys__ubjson.html#ga8f7496bbd502b1ade5a045761c3277f0", null ],
    [ "ubjson_open_array", "group__sys__ubjson.html#gaec989615aee234e8a7adc8e18cb1b985", null ],
    [ "ubjson_open_array_len", "group__sys__ubjson.html#gaa045e5cc0fad278e2ef6f0d6052e75c8", null ],
    [ "ubjson_open_object", "group__sys__ubjson.html#gaef42cba95ac7fbddfce3d8ca14ca3cf0", null ],
    [ "ubjson_open_object_len", "group__sys__ubjson.html#ga9c21e54a1d584ad0e1a66fe06c416c58", null ],
    [ "ubjson_peek_value", "group__sys__ubjson.html#ga71b84effba8475db4f51b87ef4dae15d", null ],
    [ "ubjson_read", "group__sys__ubjson.html#gaab1e79f701d7461335babbcc02bbc872", null ],
    [ "ubjson_read_array", "group__sys__ubjson.html#ga329a764a5f47786bc58ba2415694ad11", null ],
    [ "ubjson_read_next", "group__sys__ubjson.html#gac33e599836f7f872c956dcfe1b66f83d", null ],
    [ "ubjson_read_object", "group__sys__ubjson.html#ga7c4cedd978735e516b8a5c4040c22658", null ],
    [ "ubjson_write_bool", "group__sys__ubjson.html#ga04b6a9d5a70f5cd85c012a4d9cb6d529", null ],
    [ "ubjson_write_double", "group__sys__ubjson.html#ga765e83627d5b66b0795315d4e2983e51", null ],
    [ "ubjson_write_float", "group__sys__ubjson.html#gac64dbeedbe76068450f7fb52170db4e9", null ],
    [ "ubjson_write_i32", "group__sys__ubjson.html#ga6f34dffb6fe2cf034c4279e2a9f50962", null ],
    [ "ubjson_write_i64", "group__sys__ubjson.html#ga1951b009f1cec4db4641d87946342a0a", null ],
    [ "ubjson_write_init", "group__sys__ubjson.html#ga1d4922823d072e090f8048ddde39fa13", null ],
    [ "ubjson_write_key", "group__sys__ubjson.html#ga3f5b2b8a351c176f72f0a33f58fa8606", null ],
    [ "ubjson_write_noop", "group__sys__ubjson.html#gad3315058a936d8fce79b97a01980d107", null ],
    [ "ubjson_write_null", "group__sys__ubjson.html#ga6fc2fb26db9c125ae1a59e7f2ac8eeee", null ],
    [ "ubjson_write_string", "group__sys__ubjson.html#ga47724872bfe4ed6f064cb9d43fea163e", null ]
];