var group__net__sock__dns =
[
    [ "dns.h", "dns_8h.html", null ],
    [ "sock_dns_hdr_t", "structsock__dns__hdr__t.html", [
      [ "ancount", "structsock__dns__hdr__t.html#afe6519a857c52f0ca8860a3b0fc8545c", null ],
      [ "arcount", "structsock__dns__hdr__t.html#a0bc113bb131ddb322e49178c379f1764", null ],
      [ "flags", "structsock__dns__hdr__t.html#a53a05848e5dd7d6008d4d558c1065bc1", null ],
      [ "id", "structsock__dns__hdr__t.html#a36398e02b3e8a6fa7cbedb2e3c7bf761", null ],
      [ "nscount", "structsock__dns__hdr__t.html#af976b790bd91cf64ba79c5c4a485e83c", null ],
      [ "payload", "structsock__dns__hdr__t.html#a919001b94286fd224a57f399fd21d2df", null ],
      [ "qdcount", "structsock__dns__hdr__t.html#a14e5591b5ab4d2b1ebd6ac8c3403a365", null ]
    ] ],
    [ "sock_dns_query", "group__net__sock__dns.html#ga508b47ec287a3846ca8f8fc2c0bbe9bd", null ],
    [ "sock_dns_server", "group__net__sock__dns.html#ga99ea302f97897ac95d47f1cb83b2ac91", null ]
];