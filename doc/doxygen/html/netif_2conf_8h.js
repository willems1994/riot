var netif_2conf_8h =
[
    [ "GNRC_NETIF_DEFAULT_HL", "netif_2conf_8h.html#a6e2497fa5a5eebf334f10f37144639c6", null ],
    [ "GNRC_NETIF_IPV6_ADDRS_NUMOF", "netif_2conf_8h.html#a0d2713931d504ecd972747894c3b08ef", null ],
    [ "GNRC_NETIF_IPV6_GROUPS_NUMOF", "netif_2conf_8h.html#a5dec745d3d45a5bdd2363120680b102e", null ],
    [ "GNRC_NETIF_IPV6_RTR_ADDR", "netif_2conf_8h.html#a959ec307828d41868ed382ac0390c403", null ],
    [ "GNRC_NETIF_L2ADDR_MAXLEN", "netif_2conf_8h.html#a907d9c7ff80e5dbd6f338f02bf276947", null ],
    [ "GNRC_NETIF_NUMOF", "netif_2conf_8h.html#a7378e5401a9ec3e70ff9d7e36be667d5", null ],
    [ "GNRC_NETIF_PRIO", "netif_2conf_8h.html#a436e6a2270c976fa2f614592cc633d5f", null ],
    [ "GNRC_NETIF_RPL_ADDR", "netif_2conf_8h.html#af05f390c2a9a3781b50946449fb2563e", null ]
];