var jc42__internal_8h =
[
    [ "JC42_BUS_FREE_TIME_US", "jc42__internal_8h.html#abdb692aedbd3e2f7fbc10fe5eeb2238a", null ],
    [ "JC42_REG_CAP", "jc42__internal_8h.html#a864b3693d1f17b385a51bbf606d84064", null ],
    [ "JC42_REG_CONFIG", "jc42__internal_8h.html#a4edc98a3e1c2a070d54e2f8107a32f4f", null ],
    [ "JC42_REG_DEVICEID", "jc42__internal_8h.html#ad4f458b18452926c6f52103f1f7f0cfd", null ],
    [ "JC42_REG_MANID", "jc42__internal_8h.html#a8b6aa769593bcf82f1053677a6877439", null ],
    [ "JC42_REG_TEMP", "jc42__internal_8h.html#ae9cfebf77570ea5e96e223e2cc35a4c1", null ],
    [ "JC42_REG_TEMP_CRITICAL", "jc42__internal_8h.html#a71c094100db5562c3af6631333dc9e45", null ],
    [ "JC42_REG_TEMP_LOWER", "jc42__internal_8h.html#a6049987f783717aee2aa51d5a61ae739", null ],
    [ "JC42_REG_TEMP_UPPER", "jc42__internal_8h.html#abfcf0c2cd30ae80afccab78960e81d0b", null ]
];