var native_2include_2cpu__conf_8h =
[
    [ "ISR_STACKSIZE", "native_2include_2cpu__conf_8h.html#aa9a78f0a8d308d2d145cfe671c36ec8e", null ],
    [ "NATIVE_ETH_PROTO", "native_2include_2cpu__conf_8h.html#ab29c7e3d94314067d72388e2cdd9d339", null ],
    [ "THREAD_EXTRA_STACKSIZE_PRINTF", "native_2include_2cpu__conf_8h.html#af30305c0f413c7da1e7445de2b5ea5a5", null ],
    [ "THREAD_EXTRA_STACKSIZE_PRINTF_FLOAT", "native_2include_2cpu__conf_8h.html#aef8779ee69990dfa8a5ca5d96420c3ff", null ],
    [ "THREAD_STACKSIZE_DEFAULT", "native_2include_2cpu__conf_8h.html#a713ebddc00581f4d415095cdbfd8791f", null ],
    [ "THREAD_STACKSIZE_IDLE", "native_2include_2cpu__conf_8h.html#ac4f62f762a057d594ae0ee4522dd14c2", null ],
    [ "THREAD_STACKSIZE_MINIMUM", "native_2include_2cpu__conf_8h.html#ab771adde08a437e30fc39b67d6ca65ad", null ]
];