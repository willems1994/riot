var netopt_8h =
[
    [ "netopt_enable_t", "group__net__netopt.html#ga09337825cca1fd15dfefc0e31a56a86c", [
      [ "NETOPT_DISABLE", "group__net__netopt.html#gga09337825cca1fd15dfefc0e31a56a86ca4e083ab230f781d839f6363cc3721cbf", null ],
      [ "NETOPT_ENABLE", "group__net__netopt.html#gga09337825cca1fd15dfefc0e31a56a86cad34b92c49a140a9d52529ffe47352a3e", null ]
    ] ],
    [ "netopt_rf_testmode_t", "group__net__netopt.html#ga569d25644270a8b1971ed7214ae20b7b", [
      [ "NETOPT_RF_TESTMODE_IDLE", "group__net__netopt.html#gga569d25644270a8b1971ed7214ae20b7ba46f0e4fd9252bba978c4ac08e099492d", null ],
      [ "NETOPT_RF_TESTMODE_CRX", "group__net__netopt.html#gga569d25644270a8b1971ed7214ae20b7baf5d799c61b42db1bc613c1fc5e581c3f", null ],
      [ "NETOPT_RF_TESTMODE_CTX_CW", "group__net__netopt.html#gga569d25644270a8b1971ed7214ae20b7babf1d8cd1039acc1da6d558583a3272f9", null ],
      [ "NETOPT_RF_TESTMODE_CTX_PRBS9", "group__net__netopt.html#gga569d25644270a8b1971ed7214ae20b7ba05ad0934f663fc70bfd3497f700aac46", null ]
    ] ],
    [ "netopt_state_t", "group__net__netopt.html#ga796283228e6d45c83479d4c9cc83374f", [
      [ "NETOPT_STATE_OFF", "group__net__netopt.html#gga796283228e6d45c83479d4c9cc83374fa292fc54096f976946d8782886df70750", null ],
      [ "NETOPT_STATE_SLEEP", "group__net__netopt.html#gga796283228e6d45c83479d4c9cc83374faea8a32448f35879bfe141cbd27e64cf9", null ],
      [ "NETOPT_STATE_IDLE", "group__net__netopt.html#gga796283228e6d45c83479d4c9cc83374fa20cc9c3569b99decbc9f2b0f68d25546", null ],
      [ "NETOPT_STATE_RX", "group__net__netopt.html#gga796283228e6d45c83479d4c9cc83374fa6b8d6e7ebe4110a5efde18489ac2fd1e", null ],
      [ "NETOPT_STATE_TX", "group__net__netopt.html#gga796283228e6d45c83479d4c9cc83374fa96392582c88b822bbfdaf7cbc8a86859", null ],
      [ "NETOPT_STATE_RESET", "group__net__netopt.html#gga796283228e6d45c83479d4c9cc83374faea483cf8f8a2d40c8ab97eb03041d6d9", null ],
      [ "NETOPT_STATE_STANDBY", "group__net__netopt.html#gga796283228e6d45c83479d4c9cc83374fa8d6ceec9374ee9650ed1275d2167b09a", null ]
    ] ],
    [ "netopt_t", "group__net__netopt.html#ga19e30424c1ab107c9c84dc0cb29d9906", [
      [ "NETOPT_CHANNEL", "group__net__netopt.html#gga19e30424c1ab107c9c84dc0cb29d9906ad584cc4e9560a9d6dd802e05456cb927", null ],
      [ "NETOPT_IS_CHANNEL_CLR", "group__net__netopt.html#gga19e30424c1ab107c9c84dc0cb29d9906ad8948312373baf9930a2c5753357e190", null ],
      [ "NETOPT_ADDRESS", "group__net__netopt.html#gga19e30424c1ab107c9c84dc0cb29d9906a211ad84f5ea3987ed591deeaf1fd4758", null ],
      [ "NETOPT_ADDRESS_LONG", "group__net__netopt.html#gga19e30424c1ab107c9c84dc0cb29d9906a3f2a397a9a0dbf9673b3501566b05093", null ],
      [ "NETOPT_ADDR_LEN", "group__net__netopt.html#gga19e30424c1ab107c9c84dc0cb29d9906a4213c9495ff4bed3e0d076e348d86c24", null ],
      [ "NETOPT_SRC_LEN", "group__net__netopt.html#gga19e30424c1ab107c9c84dc0cb29d9906a8f08c78b2f53f3ae29f5054cb408d2e2", null ],
      [ "NETOPT_NID", "group__net__netopt.html#gga19e30424c1ab107c9c84dc0cb29d9906a551909804a12132f831ad941db9e6711", null ],
      [ "NETOPT_HOP_LIMIT", "group__net__netopt.html#gga19e30424c1ab107c9c84dc0cb29d9906af61b835d13854e8ced557203302c6e66", null ],
      [ "NETOPT_IPV6_IID", "group__net__netopt.html#gga19e30424c1ab107c9c84dc0cb29d9906a74f1c2aac469e1132c28cb902007ecfe", null ],
      [ "NETOPT_IPV6_ADDR", "group__net__netopt.html#gga19e30424c1ab107c9c84dc0cb29d9906a98b1f68f4205abe1e6c0d1b2da831316", null ],
      [ "NETOPT_IPV6_ADDR_REMOVE", "group__net__netopt.html#gga19e30424c1ab107c9c84dc0cb29d9906adcc19b251a0b90d4fdd09fa366313827", null ],
      [ "NETOPT_IPV6_ADDR_FLAGS", "group__net__netopt.html#gga19e30424c1ab107c9c84dc0cb29d9906aeb0b0602a5e969ee8ca4be0117f76a4e", null ],
      [ "NETOPT_IPV6_GROUP", "group__net__netopt.html#gga19e30424c1ab107c9c84dc0cb29d9906a1629374a72d350af9559d8ec248fdd68", null ],
      [ "NETOPT_IPV6_GROUP_LEAVE", "group__net__netopt.html#gga19e30424c1ab107c9c84dc0cb29d9906a477ff59b5dbd5674eeffeaffe18cc127", null ],
      [ "NETOPT_IPV6_FORWARDING", "group__net__netopt.html#gga19e30424c1ab107c9c84dc0cb29d9906a8291a167f0e739623dfb3158704d249c", null ],
      [ "NETOPT_IPV6_SND_RTR_ADV", "group__net__netopt.html#gga19e30424c1ab107c9c84dc0cb29d9906ae790325fc7cb8733c92834be26a4e40e", null ],
      [ "NETOPT_TX_POWER", "group__net__netopt.html#gga19e30424c1ab107c9c84dc0cb29d9906a8dced0cf56140e6bb79c71b99d57e31b", null ],
      [ "NETOPT_MAX_PACKET_SIZE", "group__net__netopt.html#gga19e30424c1ab107c9c84dc0cb29d9906a4e7628bc2c06235aae569968f17a6817", null ],
      [ "NETOPT_PRELOADING", "group__net__netopt.html#gga19e30424c1ab107c9c84dc0cb29d9906ab9a0177309e4537d2f14f58d41abde11", null ],
      [ "NETOPT_PROMISCUOUSMODE", "group__net__netopt.html#gga19e30424c1ab107c9c84dc0cb29d9906a69724f3f722e5c4e0c5569ba02778ad8", null ],
      [ "NETOPT_AUTOACK", "group__net__netopt.html#gga19e30424c1ab107c9c84dc0cb29d9906abcbe52f2967202691a8effaf2cc5a964", null ],
      [ "NETOPT_ACK_PENDING", "group__net__netopt.html#gga19e30424c1ab107c9c84dc0cb29d9906ac1e9d9c1f84ba00547b39a3267cde1fd", null ],
      [ "NETOPT_ACK_REQ", "group__net__netopt.html#gga19e30424c1ab107c9c84dc0cb29d9906aa24b591ed7595ffabb167c07b272df09", null ],
      [ "NETOPT_RETRANS", "group__net__netopt.html#gga19e30424c1ab107c9c84dc0cb29d9906a043d7cc3f6e6db041140aae25e5036f5", null ],
      [ "NETOPT_PROTO", "group__net__netopt.html#gga19e30424c1ab107c9c84dc0cb29d9906a34b6c7131e28f33ce908963a902cf793", null ],
      [ "NETOPT_STATE", "group__net__netopt.html#gga19e30424c1ab107c9c84dc0cb29d9906a8e2ba1ca0480826ca5da1406ff5ac0ea", null ],
      [ "NETOPT_RAWMODE", "group__net__netopt.html#gga19e30424c1ab107c9c84dc0cb29d9906a333771e8925e8f5a107adb638268cb6f", null ],
      [ "NETOPT_RX_START_IRQ", "group__net__netopt.html#gga19e30424c1ab107c9c84dc0cb29d9906adc806ef6fd448193159f88750075e147", null ],
      [ "NETOPT_RX_END_IRQ", "group__net__netopt.html#gga19e30424c1ab107c9c84dc0cb29d9906abfe1af955e6e95dc8955164b38db76ec", null ],
      [ "NETOPT_TX_START_IRQ", "group__net__netopt.html#gga19e30424c1ab107c9c84dc0cb29d9906a7dd78e9c81406010b963b185e44da124", null ],
      [ "NETOPT_TX_END_IRQ", "group__net__netopt.html#gga19e30424c1ab107c9c84dc0cb29d9906acac9393f99a963580d459b764988aa2b", null ],
      [ "NETOPT_AUTOCCA", "group__net__netopt.html#gga19e30424c1ab107c9c84dc0cb29d9906a3c84d6406a0e0c6929e15db79b3dfc8b", null ],
      [ "NETOPT_LINK_CONNECTED", "group__net__netopt.html#gga19e30424c1ab107c9c84dc0cb29d9906a1d2d6200cdb87b1fd03fe2d0e592f41d", null ],
      [ "NETOPT_CSMA", "group__net__netopt.html#gga19e30424c1ab107c9c84dc0cb29d9906a566a086347f022dbd8962485bd245a10", null ],
      [ "NETOPT_CSMA_RETRIES", "group__net__netopt.html#gga19e30424c1ab107c9c84dc0cb29d9906a0649ce2ec6ed00ac09b8dd835a66c7a2", null ],
      [ "NETOPT_CSMA_MAXBE", "group__net__netopt.html#gga19e30424c1ab107c9c84dc0cb29d9906a46672f5b118198e63fec82969698c1ef", null ],
      [ "NETOPT_CSMA_MINBE", "group__net__netopt.html#gga19e30424c1ab107c9c84dc0cb29d9906a87d96a0e4caf4280a4086ac034b27cfa", null ],
      [ "NETOPT_MAC_NO_SLEEP", "group__net__netopt.html#gga19e30424c1ab107c9c84dc0cb29d9906a9f44a8acf20b766e82c3a152bca0a83a", null ],
      [ "NETOPT_IS_WIRED", "group__net__netopt.html#gga19e30424c1ab107c9c84dc0cb29d9906a63df059db83000fa6088f08690127a02", null ],
      [ "NETOPT_DEVICE_TYPE", "group__net__netopt.html#gga19e30424c1ab107c9c84dc0cb29d9906a22423b24454d2cc25a395fd398505bf4", null ],
      [ "NETOPT_CHANNEL_PAGE", "group__net__netopt.html#gga19e30424c1ab107c9c84dc0cb29d9906a2aed4618f7cee3f4511f13d93b51c590", null ],
      [ "NETOPT_CCA_THRESHOLD", "group__net__netopt.html#gga19e30424c1ab107c9c84dc0cb29d9906a9e859e23c3785b9b3739e5a7be6b057b", null ],
      [ "NETOPT_CCA_MODE", "group__net__netopt.html#gga19e30424c1ab107c9c84dc0cb29d9906a88c5d8f2807ffa20328ecd85eda79aef", null ],
      [ "NETOPT_STATS", "group__net__netopt.html#gga19e30424c1ab107c9c84dc0cb29d9906a830267e5ff9dbe2ce5bc95bb0c24b095", null ],
      [ "NETOPT_ENCRYPTION", "group__net__netopt.html#gga19e30424c1ab107c9c84dc0cb29d9906a9f0f42ca5ac9333212640417cbe4c9d9", null ],
      [ "NETOPT_ENCRYPTION_KEY", "group__net__netopt.html#gga19e30424c1ab107c9c84dc0cb29d9906aad4f8cd8d611d0f3d181c40ca27f7197", null ],
      [ "NETOPT_RF_TESTMODE", "group__net__netopt.html#gga19e30424c1ab107c9c84dc0cb29d9906a63f1b24fd3a18d699ec36b131222a98a", null ],
      [ "NETOPT_L2FILTER", "group__net__netopt.html#gga19e30424c1ab107c9c84dc0cb29d9906a0a2e1dd5ac031c45230a0acc797ad91d", null ],
      [ "NETOPT_L2FILTER_RM", "group__net__netopt.html#gga19e30424c1ab107c9c84dc0cb29d9906a5ee9f15518667c6dc405a6eca2475b28", null ],
      [ "NETOPT_LAST_ED_LEVEL", "group__net__netopt.html#gga19e30424c1ab107c9c84dc0cb29d9906a7475afa2b2fe9f20a33ad9f801ee53d0", null ],
      [ "NETOPT_PREAMBLE_LENGTH", "group__net__netopt.html#gga19e30424c1ab107c9c84dc0cb29d9906ad7e6f46992f3f041b1180dec7158e060", null ],
      [ "NETOPT_INTEGRITY_CHECK", "group__net__netopt.html#gga19e30424c1ab107c9c84dc0cb29d9906a4cb92da70908a9ccf2ad3c0a1c85de35", null ],
      [ "NETOPT_CHANNEL_FREQUENCY", "group__net__netopt.html#gga19e30424c1ab107c9c84dc0cb29d9906aea3d0fbea46b0728ae1a3cfd86064992", null ],
      [ "NETOPT_CHANNEL_HOP", "group__net__netopt.html#gga19e30424c1ab107c9c84dc0cb29d9906a31e414cfb8bd50ecd5d6ddeab9cadf6e", null ],
      [ "NETOPT_CHANNEL_HOP_PERIOD", "group__net__netopt.html#gga19e30424c1ab107c9c84dc0cb29d9906a02726ef3f1bf1dab3841368e9e72699a", null ],
      [ "NETOPT_SINGLE_RECEIVE", "group__net__netopt.html#gga19e30424c1ab107c9c84dc0cb29d9906ab366b4f0e6d37b3df7cd68905f2a5889", null ],
      [ "NETOPT_RX_TIMEOUT", "group__net__netopt.html#gga19e30424c1ab107c9c84dc0cb29d9906ac31405b2cd948195d73bb201820e4d1a", null ],
      [ "NETOPT_TX_TIMEOUT", "group__net__netopt.html#gga19e30424c1ab107c9c84dc0cb29d9906adb6fb42576acc86ca27809b02c446eaf", null ],
      [ "NETOPT_BANDWIDTH", "group__net__netopt.html#gga19e30424c1ab107c9c84dc0cb29d9906a7f9c71560337bd53982f02a99b958251", null ],
      [ "NETOPT_SPREADING_FACTOR", "group__net__netopt.html#gga19e30424c1ab107c9c84dc0cb29d9906acd582c00d5ec8f68340f8e0d44eac1cb", null ],
      [ "NETOPT_CODING_RATE", "group__net__netopt.html#gga19e30424c1ab107c9c84dc0cb29d9906a6550170a27bdf7086ea67159c4eb86cf", null ],
      [ "NETOPT_FIXED_HEADER", "group__net__netopt.html#gga19e30424c1ab107c9c84dc0cb29d9906ac379693f5b3766a90dbbb5e7aa56da77", null ],
      [ "NETOPT_IQ_INVERT", "group__net__netopt.html#gga19e30424c1ab107c9c84dc0cb29d9906a5d8206442e2f03186517eebd7e0b9dbc", null ],
      [ "NETOPT_6LO_IPHC", "group__net__netopt.html#gga19e30424c1ab107c9c84dc0cb29d9906ad6e0a0b19603825a18a3e0125667bc32", null ],
      [ "NETOPT_TX_RETRIES_NEEDED", "group__net__netopt.html#gga19e30424c1ab107c9c84dc0cb29d9906a860b21d7335e516435b0762e246bdfb5", null ],
      [ "NETOPT_BLE_CTX", "group__net__netopt.html#gga19e30424c1ab107c9c84dc0cb29d9906af41b0238e6569bb499d7ec5dd779c577", null ],
      [ "NETOPT_CHECKSUM", "group__net__netopt.html#gga19e30424c1ab107c9c84dc0cb29d9906a29b14f878f161d5a2205bb595c73764a", null ],
      [ "NETOPT_PHY_BUSY", "group__net__netopt.html#gga19e30424c1ab107c9c84dc0cb29d9906ac4e7e285395cd684280f35b88ea4ee66", null ],
      [ "NETOPT_NUMOF", "group__net__netopt.html#gga19e30424c1ab107c9c84dc0cb29d9906ac16b269650b10e74d7046c95f11cae6c", null ]
    ] ],
    [ "netopt2str", "group__net__netopt.html#ga927a5d2656d7f1794b8e347b17899992", null ]
];