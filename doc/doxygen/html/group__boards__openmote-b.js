var group__boards__openmote_b =
[
    [ "openmote-b/include/board.h", "openmote-b_2include_2board_8h.html", null ],
    [ "boards/openmote-b/include/periph_conf.h", "boards_2openmote-b_2include_2periph__conf_8h.html", null ],
    [ "CCA_BACKDOOR_ACTIVE_LEVEL", "group__boards__openmote-b.html#ga9699faa8c7f0c45617c11f30e651dd17", null ],
    [ "CCA_BACKDOOR_PORT_A_PIN", "group__boards__openmote-b.html#gacc8334f56c93b33d2fd4bf9522a54c9a", null ],
    [ "RF_SWITCH_2_4_GHZ_PIN", "group__boards__openmote-b.html#gaea610e1b03baa5ce42c501110265b5aa", null ],
    [ "RF_SWITCH_SUB_GHZ_PIN", "group__boards__openmote-b.html#gafabb06960a7876f5a1683d9dd7e064c1", null ],
    [ "board_init", "group__boards__openmote-b.html#ga916f2adc2080b4fe88034086d107a8dc", null ]
];