var ezr32wg_2include_2periph__cpu_8h =
[
    [ "timer_conf_t", "structtimer__conf__t.html", "structtimer__conf__t" ],
    [ "uart_conf_t", "structuart__conf__t.html", "structuart__conf__t" ],
    [ "CPUID_ADDR", "ezr32wg_2include_2periph__cpu_8h.html#a0d3fc6a1a30b894ce55d50e7eac993b2", null ],
    [ "CPUID_LEN", "ezr32wg_2include_2periph__cpu_8h.html#a1943715eaeaa63e28b7b4e207f655fca", null ],
    [ "GPIO_PIN", "ezr32wg_2include_2periph__cpu_8h.html#ae29846b3ecd19a0b7c44ff80a37ae7c1", null ],
    [ "GPIO_UNDEF", "ezr32wg_2include_2periph__cpu_8h.html#a3969ce1e494a72d3c2925b10ddeb4604", null ],
    [ "HAVE_GPIO_T", "ezr32wg_2include_2periph__cpu_8h.html#a759f553fbddd2915b49e50c967661fb1", null ],
    [ "HAVE_TIMER_T", "ezr32wg_2include_2periph__cpu_8h.html#abdb64cdcca05bcadf7585dc2ff16b910", null ],
    [ "TIMER_UNDEF", "ezr32wg_2include_2periph__cpu_8h.html#aa0c84c20a11f7659548d09b0c786dd23", null ],
    [ "gpio_t", "ezr32wg_2include_2periph__cpu_8h.html#a8e0133bf7f8e7484ac0088145cd051ec", null ],
    [ "tim_t", "ezr32wg_2include_2periph__cpu_8h.html#a9f2cd02a5d0bc04dee408dce50d11c19", null ],
    [ "PA", "ezr32wg_2include_2periph__cpu_8h.html#a550cb4231d535863c55643bbd2d3c6baae3d8a811f3bf7196f361be4104db68db", null ],
    [ "PB", "ezr32wg_2include_2periph__cpu_8h.html#a550cb4231d535863c55643bbd2d3c6baa8b7dd81ba2f0d15957795457d92ce139", null ],
    [ "PC", "ezr32wg_2include_2periph__cpu_8h.html#a550cb4231d535863c55643bbd2d3c6baaa2c62b62b658ac45e83749e9e9c1cb46", null ],
    [ "PD", "ezr32wg_2include_2periph__cpu_8h.html#a550cb4231d535863c55643bbd2d3c6baaefbc069e0ac4cd293f3ba527bec2befe", null ],
    [ "PE", "ezr32wg_2include_2periph__cpu_8h.html#a550cb4231d535863c55643bbd2d3c6baa84a0e8421125bc6ef85e43bab494f68c", null ],
    [ "PF", "ezr32wg_2include_2periph__cpu_8h.html#a550cb4231d535863c55643bbd2d3c6baac1b9838c8d86ff9cdb39757140ebc8ae", null ]
];