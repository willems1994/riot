var group__cpu__native =
[
    [ "async_read.h", "async__read_8h.html", null ],
    [ "native/include/cpu_conf.h", "native_2include_2cpu__conf_8h.html", null ],
    [ "cpu/native/include/periph_conf.h", "cpu_2native_2include_2periph__conf_8h.html", null ],
    [ "native/include/periph_cpu.h", "native_2include_2periph__cpu_8h.html", null ],
    [ "tty_uart.h", "tty__uart_8h.html", null ],
    [ "_native_callback_t", "group__cpu__native.html#gae998f4b95e49817bcc66a28111a96e0c", null ],
    [ "cpu_print_last_instruction", "group__cpu__native.html#ga8a02f8177d64e6e010565ca8f9263b4f", null ],
    [ "register_interrupt", "group__cpu__native.html#gad426267a904c6f947ee8db422b96fd11", null ],
    [ "unregister_interrupt", "group__cpu__native.html#ga4cd6d8121a16284903171d6ef3c48ce5", null ]
];