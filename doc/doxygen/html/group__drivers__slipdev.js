var group__drivers__slipdev =
[
    [ "slipdev.h", "slipdev_8h.html", null ],
    [ "slipdev_params_t", "structslipdev__params__t.html", [
      [ "baudrate", "structslipdev__params__t.html#a1720a84845187420341516cb66cc77e3", null ],
      [ "uart", "structslipdev__params__t.html#a85fef6f627f2780feac21dec246c03ef", null ]
    ] ],
    [ "slipdev_t", "structslipdev__t.html", [
      [ "config", "structslipdev__t.html#a15d2ec6969624dc0ff2b675ba015d61a", null ],
      [ "inbuf", "structslipdev__t.html#a06700351f5c784bc34a77e6ddbb0d562", null ],
      [ "inesc", "structslipdev__t.html#a916ca0ee3eecbde8c3d11a2abcf188c0", null ],
      [ "netdev", "structslipdev__t.html#affca7a08df49efc813500002e1ea3707", null ],
      [ "rxmem", "structslipdev__t.html#a435561f83a0a540602ec368932a59346", null ]
    ] ],
    [ "SLIPDEV_BUFSIZE", "group__drivers__slipdev.html#ga624acfdd29f33414adeb1e0d281489d7", null ],
    [ "slipdev_setup", "group__drivers__slipdev.html#ga6e5a3ef2e4a0bf8c0dd5aebc76b6546a", null ]
];