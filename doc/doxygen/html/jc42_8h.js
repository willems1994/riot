var jc42_8h =
[
    [ "JC42_NODEV", "group__drivers__jc42.html#ga8376ded1babc33ad16909ccdadd46cdb", null ],
    [ "JC42_NOI2C", "group__drivers__jc42.html#gaadbbe956b37b0f2adf01fcdd9e9a8434", null ],
    [ "JC42_OK", "group__drivers__jc42.html#ga9e26fd5b83c09f06620c15dd80c309b0", null ],
    [ "jc42_get_config", "group__drivers__jc42.html#ga53686c1b5a166c972395a598bee34b59", null ],
    [ "jc42_get_temperature", "group__drivers__jc42.html#gaa2e971c679119a6d613100ca66d0c479", null ],
    [ "jc42_init", "group__drivers__jc42.html#ga58e04228cdfd753865bda4f689c9785e", null ],
    [ "jc42_set_config", "group__drivers__jc42.html#gade80f8e01f202066151692420d60c28d", null ],
    [ "jc42_temperature_saul_driver", "group__drivers__jc42.html#gaf73f4f9b40a391ea9b87d00d26f4d8d1", null ]
];