var eddystone_8h =
[
    [ "EDDYSTONE_EID", "group__net__eddystone.html#ga9fd3c6b743b599aba645ac58c200238b", null ],
    [ "EDDYSTONE_INSTANCE_LEN", "group__net__eddystone.html#ga5f699ac1dcbe8fc92ac6725ac77c15a6", null ],
    [ "EDDYSTONE_NAMESPACE_LEN", "group__net__eddystone.html#gacfdad191264eb7c884ff1bd395e15c35", null ],
    [ "EDDYSTONE_TLM", "group__net__eddystone.html#gac4306e70db52f133ae4cb2d14e7e14c1", null ],
    [ "EDDYSTONE_UID", "group__net__eddystone.html#gaad645c625213edb163b38845f5ccecf2", null ],
    [ "EDDYSTONE_URL", "group__net__eddystone.html#gabbfbc9b7cab8574879cfa66d00325ef7", null ],
    [ "EDDYSTONE_URL_HTTP", "group__net__eddystone.html#gafbc807dd8dd8ae67e7565278187e0546", null ],
    [ "EDDYSTONE_URL_HTTP_WWW", "group__net__eddystone.html#ga34919677d777451ac6343b15b7aa9f2d", null ],
    [ "EDDYSTONE_URL_HTTPS", "group__net__eddystone.html#ga32b64372d8c482ba63b625509d2dbce3", null ],
    [ "EDDYSTONE_URL_HTTPS_WWWW", "group__net__eddystone.html#gac0b3d34fb8373e041d9ca1742d5a20c2", null ]
];