var structkw2xrf__t =
[
    [ "buf", "structkw2xrf__t.html#ab621d509d48c7e73eb0bea07d6c290cf", null ],
    [ "idle_state", "structkw2xrf__t.html#aa15d6a02a57799077114f9551e621993", null ],
    [ "netdev", "structkw2xrf__t.html#a01df592c62e03442c93634c92e715f3f", null ],
    [ "params", "structkw2xrf__t.html#aaa82ce2c1a0e82bad99b5a95f59b8a3a", null ],
    [ "pending_tx", "structkw2xrf__t.html#a6777e9ef9148432c12da100ae3ea57f1", null ],
    [ "state", "structkw2xrf__t.html#a0937d2c4f1f993d57f90257995bf0c25", null ],
    [ "thread", "structkw2xrf__t.html#aaedd49b8324c94893b54e02a83dffc87", null ],
    [ "tx_frame_len", "structkw2xrf__t.html#ae0a48e350dd499a111ae7bdb08840004", null ],
    [ "tx_power", "structkw2xrf__t.html#a6f7e3092d7de1c2dcc8af3e732bd4b38", null ]
];