var group__drivers__lsm6dsl =
[
    [ "lsm6dsl.h", "lsm6dsl_8h.html", null ],
    [ "lsm6dsl_internal.h", "lsm6dsl__internal_8h.html", null ],
    [ "lsm6dsl_params_t", "structlsm6dsl__params__t.html", [
      [ "acc_decimation", "structlsm6dsl__params__t.html#ac02fbc3804ffc5df60c21adf4656d94f", null ],
      [ "acc_fs", "structlsm6dsl__params__t.html#a850a07a1a89e5fdd803fdccfd609dac1", null ],
      [ "acc_odr", "structlsm6dsl__params__t.html#aab0d3e44f67056ca59e00431bad9a0c2", null ],
      [ "addr", "structlsm6dsl__params__t.html#ac3a48eb13f1425e421d7973511bc0d9b", null ],
      [ "gyro_decimation", "structlsm6dsl__params__t.html#a71cd12e8a3c79faac2d898c1f1d5e71f", null ],
      [ "gyro_fs", "structlsm6dsl__params__t.html#a6af69c602cce85fefb13f315ea364973", null ],
      [ "gyro_odr", "structlsm6dsl__params__t.html#ac5a407c2eb20fb0664bffa55bc8ce2d6", null ],
      [ "i2c", "structlsm6dsl__params__t.html#aae2f2c729661f508d425612b62131cbd", null ]
    ] ],
    [ "lsm6dsl_t", "structlsm6dsl__t.html", [
      [ "params", "structlsm6dsl__t.html#ae420e39995325ba3c20dbfaba0ba7211", null ]
    ] ],
    [ "lsm6dsl_3d_data_t", "structlsm6dsl__3d__data__t.html", [
      [ "x", "structlsm6dsl__3d__data__t.html#a1669a3aec03b716b0dc9e5dd73fbcf41", null ],
      [ "y", "structlsm6dsl__3d__data__t.html#a3d57d70454ab05e9d0721290d32ed071", null ],
      [ "z", "structlsm6dsl__3d__data__t.html#a093d9699538853e26ecce824d839ba4d", null ],
      [ "LSM6DSL_OK", "group__drivers__lsm6dsl.html#gga7ada2556f3c386db5f31969fb6d4d002ac8eebcb422d42a13c6291c97a4accd52", null ],
      [ "LSM6DSL_ERROR_BUS", "group__drivers__lsm6dsl.html#gga7ada2556f3c386db5f31969fb6d4d002ae126dac30a10cd3afde59e6e453e08f1", null ],
      [ "LSM6DSL_ERROR_CNF", "group__drivers__lsm6dsl.html#gga7ada2556f3c386db5f31969fb6d4d002a8d078715dbcff4ad488bb70f88b1ff72", null ],
      [ "LSM6DSL_ERROR_DEV", "group__drivers__lsm6dsl.html#gga7ada2556f3c386db5f31969fb6d4d002a50f4a911a9f48ccd9a1ea711db515e7a", null ]
    ] ],
    [ "lsm6dsl_acc_power_down", "group__drivers__lsm6dsl.html#gae57cdd77ca3ece2605f125c535044b8f", null ],
    [ "lsm6dsl_acc_power_up", "group__drivers__lsm6dsl.html#ga6f72c9c0cd23b5e8179a88f644f48e9e", null ],
    [ "lsm6dsl_gyro_power_down", "group__drivers__lsm6dsl.html#ga7cecd2389e4fec5824b95821cd2cdfd6", null ],
    [ "lsm6dsl_gyro_power_up", "group__drivers__lsm6dsl.html#gab4e43fc2eadf7ed26400e1aa79b4811b", null ],
    [ "lsm6dsl_init", "group__drivers__lsm6dsl.html#ga54f68deba967897728c8586045e0e62f", null ],
    [ "lsm6dsl_read_acc", "group__drivers__lsm6dsl.html#ga6246a96d8b72b16937eb4478e430d1d7", null ],
    [ "lsm6dsl_read_gyro", "group__drivers__lsm6dsl.html#ga339148e1cec0b0dc13005ec17a986b84", null ],
    [ "lsm6dsl_read_temp", "group__drivers__lsm6dsl.html#gacf7bb1d6ed136178b86a74da1d39b17a", null ]
];