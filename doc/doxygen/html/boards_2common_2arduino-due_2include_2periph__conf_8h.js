var boards_2common_2arduino_due_2include_2periph__conf_8h =
[
    [ "CLOCK_CORECLOCK", "boards_2common_2arduino-due_2include_2periph__conf_8h.html#afc465f12242e68f6c3695caa3ba0a169", null ],
    [ "CLOCK_EXT_OSC", "boards_2common_2arduino-due_2include_2periph__conf_8h.html#aa37072c55ec68adf464962e59f7bf82f", null ],
    [ "CLOCK_FWS", "boards_2common_2arduino-due_2include_2periph__conf_8h.html#aa4e6512d6dd7ba94360c4024ad60c269", null ],
    [ "CLOCK_PLL_DIV", "boards_2common_2arduino-due_2include_2periph__conf_8h.html#ad7c20feb5ba29cd3c6479fa74db3bbb0", null ],
    [ "CLOCK_PLL_MUL", "boards_2common_2arduino-due_2include_2periph__conf_8h.html#a902a38d6dfe9bc80271132ace0d1ca69", null ],
    [ "PWM_CHAN_NUMOF", "boards_2common_2arduino-due_2include_2periph__conf_8h.html#a512721a9b01d749ca2277a33bb89a628", null ],
    [ "PWM_NUMOF", "boards_2common_2arduino-due_2include_2periph__conf_8h.html#a44adbd579bb180f3cfe8ec78932eb7a1", null ],
    [ "SPI_NUMOF", "boards_2common_2arduino-due_2include_2periph__conf_8h.html#ab35a2b79568128efef74adf1ba1910a8", null ],
    [ "TIMER_0_ISR", "boards_2common_2arduino-due_2include_2periph__conf_8h.html#a4c490d334538c05373718609ca5fe2d4", null ],
    [ "TIMER_1_ISR", "boards_2common_2arduino-due_2include_2periph__conf_8h.html#ab1f8037bcb60d4669f508c471f92bc17", null ],
    [ "TIMER_NUMOF", "boards_2common_2arduino-due_2include_2periph__conf_8h.html#a6e2f66f5b6f5c835dd11f9766c4ed897", null ],
    [ "UART_0_ISR", "boards_2common_2arduino-due_2include_2periph__conf_8h.html#a713e03d19734d793baee3d1cc25c2dbb", null ],
    [ "UART_1_ISR", "boards_2common_2arduino-due_2include_2periph__conf_8h.html#af9358264b5cbce69dddad098a8600aae", null ],
    [ "UART_2_ISR", "boards_2common_2arduino-due_2include_2periph__conf_8h.html#afdfcd079b784102f3ceb94a04d9c9250", null ],
    [ "UART_3_ISR", "boards_2common_2arduino-due_2include_2periph__conf_8h.html#aae13f589a3f29600569b784d83c85462", null ],
    [ "UART_NUMOF", "boards_2common_2arduino-due_2include_2periph__conf_8h.html#a850405f2aaa352ad264346531f0e6230", null ],
    [ "pwm_chan", "boards_2common_2arduino-due_2include_2periph__conf_8h.html#a2d4ef05f499098747a68c98008d40728", null ],
    [ "spi_config", "boards_2common_2arduino-due_2include_2periph__conf_8h.html#a873188d7292e07499dcde9674b1e849c", null ],
    [ "timer_config", "boards_2common_2arduino-due_2include_2periph__conf_8h.html#a2dd41f782d2c67052e4dc7d37cef89b1", null ],
    [ "uart_config", "boards_2common_2arduino-due_2include_2periph__conf_8h.html#a1643cfc64589407fb96b4cbf908689a5", null ]
];