var aem_8h =
[
    [ "AEM_DWT_CTRL", "group__boards__common__silabs__drivers__aem.html#ga4eceb3a7e48422c7039b54853d1c6727", null ],
    [ "AEM_ITM_LAR", "group__boards__common__silabs__drivers__aem.html#ga5c203153d7b7123033aed4ab9ddbfad7", null ],
    [ "AEM_ITM_TCR", "group__boards__common__silabs__drivers__aem.html#gaddb3d151d55b8bdc160edeb07e89bc0d", null ],
    [ "AEM_TPI_ACPR", "group__boards__common__silabs__drivers__aem.html#ga1433a7bf5c8b9b62dc58b55f8ad7147b", null ],
    [ "AEM_TPI_FFCR", "group__boards__common__silabs__drivers__aem.html#ga5d43c11f95538c247643fca01802d7a0", null ],
    [ "AEM_TPI_SPPR", "group__boards__common__silabs__drivers__aem.html#gaa83963f6f5a90e1ca92ae08887476fa3", null ],
    [ "aem_init", "group__boards__common__silabs__drivers__aem.html#ga68d8f756bbfe73f9d88cba6c43d8f459", null ]
];