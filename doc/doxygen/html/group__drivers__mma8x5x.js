var group__drivers__mma8x5x =
[
    [ "mma8x5x.h", "mma8x5x_8h.html", null ],
    [ "mma8x5x_regs.h", "mma8x5x__regs_8h.html", null ],
    [ "mma8x5x_params_t", "structmma8x5x__params__t.html", [
      [ "addr", "structmma8x5x__params__t.html#a590b6ae13b70ad33107d01e344187bf7", null ],
      [ "i2c", "structmma8x5x__params__t.html#a01182ac1b086b3404d14286ac101825a", null ],
      [ "offset", "structmma8x5x__params__t.html#a0ee5cb459788608146d5f70925e62a9c", null ],
      [ "range", "structmma8x5x__params__t.html#a6271d95f431417ef5b4e350ecf5c944c", null ],
      [ "rate", "structmma8x5x__params__t.html#a439f5825d9d471d2c60cbf6df65d45c4", null ],
      [ "type", "structmma8x5x__params__t.html#a7011afaeffe14ba06e423968464b524f", null ]
    ] ],
    [ "mma8x5x_t", "structmma8x5x__t.html", [
      [ "params", "structmma8x5x__t.html#a0e492c723bc601c13666c39b4852afbe", null ]
    ] ],
    [ "mma8x5x_data_t", "structmma8x5x__data__t.html", [
      [ "x", "structmma8x5x__data__t.html#aecce252edd1e7805a51c1f81acb6f71b", null ],
      [ "y", "structmma8x5x__data__t.html#a5724de837d1b730560449d112ac0b0af", null ],
      [ "z", "structmma8x5x__data__t.html#a0994991f355532e83c7200d0d3f783d1", null ]
    ] ],
    [ "MMA8X5X_I2C_ADDRESS", "group__drivers__mma8x5x.html#ga324510e2ef1ed451c24a60e80edb98e7", [
      [ "MMA8X5X_TYPE_MMA8652", "group__drivers__mma8x5x.html#gga9b29769343da78881f442e657eb62b6ea8b4bdab19cfd89f7b38ceda905c36d03", null ],
      [ "MMA8X5X_TYPE_MMA8653", "group__drivers__mma8x5x.html#gga9b29769343da78881f442e657eb62b6ea4ab9450711625dac59de585554469002", null ],
      [ "MMA8X5X_TYPE_MMA8451", "group__drivers__mma8x5x.html#gga9b29769343da78881f442e657eb62b6ea97d9ab0b80e152e04ec8da9654863a00", null ],
      [ "MMA8X5X_TYPE_MMA8452", "group__drivers__mma8x5x.html#gga9b29769343da78881f442e657eb62b6ead5d6d9c182fd6489d0c8a5e5fc93e0e3", null ],
      [ "MMA8X5X_TYPE_MMA8453", "group__drivers__mma8x5x.html#gga9b29769343da78881f442e657eb62b6ea07ff68f1d3906e1e99e832c4af0f1ff6", null ],
      [ "MMA8X5X_RATE_800HZ", "group__drivers__mma8x5x.html#gga1e8054db89feba236b2235d16d0bd1ada02d49b03436ba5dcd1934da40cc2c191", null ],
      [ "MMA8X5X_RATE_400HZ", "group__drivers__mma8x5x.html#gga1e8054db89feba236b2235d16d0bd1ada5b53c7c047eac0f032c8a001ffaa4484", null ],
      [ "MMA8X5X_RATE_200HZ", "group__drivers__mma8x5x.html#gga1e8054db89feba236b2235d16d0bd1ada06ec9aedd1bee45640ddd3990198604f", null ],
      [ "MMA8X5X_RATE_100HZ", "group__drivers__mma8x5x.html#gga1e8054db89feba236b2235d16d0bd1ada16452ae63c737b3114fb77c51ebb67b5", null ],
      [ "MMA8X5X_RATE_50HZ", "group__drivers__mma8x5x.html#gga1e8054db89feba236b2235d16d0bd1adae398f7f2b0b339bcd1078aa45a30b7b7", null ],
      [ "MMA8X5X_RATE_1HZ25", "group__drivers__mma8x5x.html#gga1e8054db89feba236b2235d16d0bd1ada2b8a3907cd3dea3883aaef7968a37cd5", null ],
      [ "MMA8X5X_RATE_6HZ25", "group__drivers__mma8x5x.html#gga1e8054db89feba236b2235d16d0bd1adaa4295382cbc423e4bdfcccfbc2c3b70e", null ],
      [ "MMA8X5X_RATE_1HZ56", "group__drivers__mma8x5x.html#gga1e8054db89feba236b2235d16d0bd1adaabee619c267883b38c06bac9c6ca8eee", null ],
      [ "MMA8X5X_RANGE_2G", "group__drivers__mma8x5x.html#gga4f79453548dc20926be4b5a000ecbd66ad902847a54c100aa3805199aae2e2aa4", null ],
      [ "MMA8X5X_RANGE_4G", "group__drivers__mma8x5x.html#gga4f79453548dc20926be4b5a000ecbd66a80c8b4470ac531bad63d36f95dd8597a", null ],
      [ "MMA8X5X_RANGE_8G", "group__drivers__mma8x5x.html#gga4f79453548dc20926be4b5a000ecbd66a9bade83ea5fc4953a7cb4ccc080a7c4f", null ],
      [ "MMA8X5X_OK", "group__drivers__mma8x5x.html#ggaf59064efe753e6928fd91539a27503d1a7076df545069fa71f10950ac3d0aa225", null ],
      [ "MMA8X5X_DATA_READY", "group__drivers__mma8x5x.html#ggaf59064efe753e6928fd91539a27503d1a689fa65f9c5f2a2f01964d1f649d6884", null ],
      [ "MMA8X5X_NOI2C", "group__drivers__mma8x5x.html#ggaf59064efe753e6928fd91539a27503d1a19a99cf8b7530019379a04d699128b69", null ],
      [ "MMA8X5X_NODEV", "group__drivers__mma8x5x.html#ggaf59064efe753e6928fd91539a27503d1af4a9a69c1fb65aa32c6f1872c5f65cae", null ],
      [ "MMA8X5X_NODATA", "group__drivers__mma8x5x.html#ggaf59064efe753e6928fd91539a27503d1a3caf45afb7d894745f6830bae0a95503", null ]
    ] ],
    [ "mma8x5x_ack_int", "group__drivers__mma8x5x.html#ga63fd284d31680237ccddea94028ca28d", null ],
    [ "mma8x5x_init", "group__drivers__mma8x5x.html#ga5420a9cd99c97f9b88848f99f5a5e2b6", null ],
    [ "mma8x5x_is_ready", "group__drivers__mma8x5x.html#gadbad9d29b3fbc68bd05c80fbdf2b58d9", null ],
    [ "mma8x5x_read", "group__drivers__mma8x5x.html#gaefe225136d5aafcc4a37b38ba7525287", null ],
    [ "mma8x5x_set_active", "group__drivers__mma8x5x.html#gaacb242963421e9544ae2e8af89c3815d", null ],
    [ "mma8x5x_set_motiondetect", "group__drivers__mma8x5x.html#gac996436ccb6c416c848812b4433856c0", null ],
    [ "mma8x5x_set_standby", "group__drivers__mma8x5x.html#gac4798e15272544651f7171d3dbc46c37", null ],
    [ "mma8x5x_set_user_offset", "group__drivers__mma8x5x.html#ga0129d9d93189e200a7ec5b979586d71c", null ]
];