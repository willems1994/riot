var stm32f4discovery_2include_2arduino__pinmap_8h =
[
    [ "ARDUINO_A0", "stm32f4discovery_2include_2arduino__pinmap_8h.html#a8c361b055851b7e04792fc31645daadf", null ],
    [ "ARDUINO_A1", "stm32f4discovery_2include_2arduino__pinmap_8h.html#a170a6c527949ee7b9c8ca6889f2b7235", null ],
    [ "ARDUINO_A2", "stm32f4discovery_2include_2arduino__pinmap_8h.html#a5740fce40ca2169045b09dd1a1f4d681", null ],
    [ "ARDUINO_A3", "stm32f4discovery_2include_2arduino__pinmap_8h.html#aa1d15a34aa68b68a4ee3d8b079547e5d", null ],
    [ "ARDUINO_PIN_0", "stm32f4discovery_2include_2arduino__pinmap_8h.html#acad741047fd7dca25a3dd8a62aa29462", null ],
    [ "ARDUINO_PIN_1", "stm32f4discovery_2include_2arduino__pinmap_8h.html#aceb50b34589a0d29d5c07b2846de8eaa", null ],
    [ "ARDUINO_PIN_2", "stm32f4discovery_2include_2arduino__pinmap_8h.html#a442a4d1d35251c8e39845582afc92f7a", null ],
    [ "ARDUINO_PIN_3", "stm32f4discovery_2include_2arduino__pinmap_8h.html#ae52d466203b1be7d2a8b52a05efa0c49", null ],
    [ "ARDUINO_PIN_4", "stm32f4discovery_2include_2arduino__pinmap_8h.html#a4c9ea65aa36101fde55e6b28f2ee99fc", null ],
    [ "ARDUINO_PIN_5", "stm32f4discovery_2include_2arduino__pinmap_8h.html#a9b264db3d6951e7b8fd115eede94f9f0", null ],
    [ "ARDUINO_PIN_6", "stm32f4discovery_2include_2arduino__pinmap_8h.html#a4610d5acda40575f42f367db84e7a90a", null ],
    [ "ARDUINO_PIN_7", "stm32f4discovery_2include_2arduino__pinmap_8h.html#a9ec207e1ec82310017a39485ac63548b", null ]
];