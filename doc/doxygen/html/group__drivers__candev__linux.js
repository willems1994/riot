var group__drivers__candev__linux =
[
    [ "candev_linux.h", "candev__linux_8h.html", null ],
    [ "candev_linux_conf", "structcandev__linux__conf.html", [
      [ "interface_name", "structcandev__linux__conf.html#ab592c3704362d1bbe847c4a7c3b715ed", null ]
    ] ],
    [ "candev_linux", "structcandev__linux.html", [
      [ "candev", "structcandev__linux.html#ad2a896c91e9240449d11c15be8c7ddcc", null ],
      [ "conf", "structcandev__linux.html#a925ebd5269648caea63be5d96c07bfc9", null ],
      [ "filters", "structcandev__linux.html#ae1694e97e69534fb2aae495f7fe4bb81", null ],
      [ "sock", "structcandev__linux.html#aa146edd57daea361eb74a46f35075ddf", null ]
    ] ],
    [ "CAN_MAX_SIZE_INTERFACE_NAME", "group__drivers__candev__linux.html#ga314fd0bfec6e2abeeb5a24a50779b35b", null ],
    [ "CANDEV_LINUX_DEFAULT_BITRATE", "group__drivers__candev__linux.html#ga1079abfb970db5efddd538a9283dd15a", null ],
    [ "CANDEV_LINUX_DEFAULT_SPT", "group__drivers__candev__linux.html#ga58f95c0cfa47bb38df75d7c330821b96", null ],
    [ "CANDEV_LINUX_MAX_FILTERS_RX", "group__drivers__candev__linux.html#gaec19105f9718a8946c0c350dd9a5d635", null ],
    [ "candev_linux_conf_t", "group__drivers__candev__linux.html#gae6ecc32cbb6e14f103b278b7b35c8d3c", null ],
    [ "candev_linux_t", "group__drivers__candev__linux.html#gaf7842275bc7de263c28f06cd656e7bcd", null ],
    [ "candev_linux_init", "group__drivers__candev__linux.html#ga657002a9dac43095c771b30b780531fb", null ],
    [ "candev_linux_conf", "group__drivers__candev__linux.html#gaf34db4c6d7ec22bf8218f54632687222", null ]
];