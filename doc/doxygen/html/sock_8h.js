var sock_8h =
[
    [ "SOCK_ADDR_ANY_NETIF", "group__net__sock.html#ga062f5eb3763541ec0ab6e261447b01ca", null ],
    [ "SOCK_FLAGS_REUSE_EP", "group__net__sock.html#ga2289814aa41ddb66a19f444bd0fee52e", null ],
    [ "SOCK_HAS_IPV6", "group__net__sock.html#ga858ed875d71d514276bb7a30fd050d87", null ],
    [ "SOCK_IPV4_EP_ANY", "group__net__sock.html#gaca8b50e1b4fabde0f5073a665faf5e07", null ],
    [ "SOCK_IPV6_EP_ANY", "group__net__sock.html#ga3f10e5b714c03824d6dc4fff5d372b8f", null ],
    [ "SOCK_NO_TIMEOUT", "group__net__sock.html#gaf0c954b49c306f6c125d25ddba9f352e", null ]
];