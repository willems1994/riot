var group__net__rdcli__config =
[
    [ "rdcli_config.h", "rdcli__config_8h.html", null ],
    [ "RDCLI_EP_PREFIX", "group__net__rdcli__config.html#ga0b8efb33324337820bd26b414d78ed8a", null ],
    [ "RDCLI_EP_SUFFIX_LEN", "group__net__rdcli__config.html#ga8a034f3b5f8571b94ff6d8d23d2aa07d", null ],
    [ "RDCLI_LT", "group__net__rdcli__config.html#ga302fe8496aa9934f4da61d3bfc2e5775", null ],
    [ "RDCLI_SERVER_ADDR", "group__net__rdcli__config.html#ga76bdf79d6dbf54e4cbc4962d0fc7cf46", null ],
    [ "RDCLI_SERVER_PORT", "group__net__rdcli__config.html#ga27e3883317fb6b097ca99aefa56ac647", null ],
    [ "RDCLI_STARTUP_DELAY", "group__net__rdcli__config.html#ga62ede1d46c3aaaae05875e2392599805", null ],
    [ "RDCLI_UPDATE_INTERVAL", "group__net__rdcli__config.html#gaa1f4443d0978f9aed8512f29fc742e4f", null ]
];