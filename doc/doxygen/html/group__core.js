var group__core =
[
    [ "IRQ Handling", "group__core__irq.html", "group__core__irq" ],
    [ "Kernel utilities", "group__core__util.html", "group__core__util" ],
    [ "Mailboxes", "group__core__mbox.html", "group__core__mbox" ],
    [ "Messaging / IPC", "group__core__msg.html", "group__core__msg" ],
    [ "Scheduler", "group__core__sched.html", "group__core__sched" ],
    [ "Startup and Configuration", "group__core__internal.html", "group__core__internal" ],
    [ "Synchronization", "group__core__sync.html", "group__core__sync" ],
    [ "Thread Flags", "group__core__thread__flags.html", "group__core__thread__flags" ],
    [ "Threading", "group__core__thread.html", "group__core__thread" ]
];