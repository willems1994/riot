var structisotp =
[
    [ "arg", "structisotp.html#a1fcb2185b1c4af3095ef414a3f871922", null ],
    [ "entry", "structisotp.html#ae1df16de6102eab09a2ccb9cdfdc2eb5", null ],
    [ "next", "structisotp.html#a64270bc3113f126b617e03340d2491f7", null ],
    [ "opt", "structisotp.html#a13e672e58564db4cdb50cee1ddf5c8e7", null ],
    [ "rx", "structisotp.html#ab2c6aebc10b7fe15ffd304a1ece696f1", null ],
    [ "rx_timer", "structisotp.html#a00cd6c898f07f3cf870decf5f675f0d4", null ],
    [ "rxfc", "structisotp.html#a34d978c59a2513842b615f317ea72583", null ],
    [ "tx", "structisotp.html#ac52468dfc9800a08318bbb7f4b22cefb", null ],
    [ "tx_gap", "structisotp.html#ab47b289c77d2030e284c59d427891c70", null ],
    [ "tx_timer", "structisotp.html#ab54e220ed9f1aa342cf1e872fd45e643", null ],
    [ "tx_wft", "structisotp.html#a1e0c91051cc85d4fe422bae7725768e5", null ],
    [ "txfc", "structisotp.html#a7910213baa33ca2d6147465ccd59019d", null ]
];