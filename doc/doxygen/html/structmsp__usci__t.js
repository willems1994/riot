var structmsp__usci__t =
[
    [ "ABCTL", "structmsp__usci__t.html#a9d40e320ac6aeff282c3dea727ec81e3", null ],
    [ "ABR0", "structmsp__usci__t.html#a9a166394bd60f50db7b0ce46aafac029", null ],
    [ "ABR1", "structmsp__usci__t.html#a61f8baf871bcddc46cb53878aaf40734", null ],
    [ "ACTL0", "structmsp__usci__t.html#ac9656381733da5d9ad80a266bf64d69d", null ],
    [ "ACTL1", "structmsp__usci__t.html#a24cc2c16e0de88f8fdd9ebee4dedba0f", null ],
    [ "AMCTL", "structmsp__usci__t.html#a944af9ab2828a65acae89daaeb94c359", null ],
    [ "ARXBUF", "structmsp__usci__t.html#a702dcaa8cad377ee96a16a2db59c9ee2", null ],
    [ "ASTAT", "structmsp__usci__t.html#a0b2c3aa40ecc32084f53f892958114ee", null ],
    [ "ATXBUF", "structmsp__usci__t.html#a2eedfed5af5106dcdbb47f45fe392737", null ],
    [ "BBR0", "structmsp__usci__t.html#a4790620ac56d047214cb1ea00a94badd", null ],
    [ "BBR1", "structmsp__usci__t.html#a9e43fcb18a77f57496bd7ce9caa8aa98", null ],
    [ "BCTL0", "structmsp__usci__t.html#a67c246880863a831f44bcec947a3f653", null ],
    [ "BCTL1", "structmsp__usci__t.html#a593bf9e1229f7b7e37083638e7712615", null ],
    [ "BI2CIE", "structmsp__usci__t.html#ac83cbd502e328fc9ffe1df5cdb16a9b1", null ],
    [ "BRXBUF", "structmsp__usci__t.html#a50ce731daeddbcf4388131d6abda288b", null ],
    [ "BSTAT", "structmsp__usci__t.html#a422f777b7cb6ea3cd240edf776c4afa7", null ],
    [ "BTXBUF", "structmsp__usci__t.html#a7b0c186744e394b8103c43e803e25f97", null ],
    [ "IRRCTL", "structmsp__usci__t.html#a1483442d3cc4542c2f4396e54d381876", null ],
    [ "IRTCTL", "structmsp__usci__t.html#ad37a7d413892d281f6ae98cfc6aa53b7", null ]
];