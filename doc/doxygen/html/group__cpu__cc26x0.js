var group__cpu__cc26x0 =
[
    [ "TI CC26x0 definitions", "group__cpu__cc26x0__definitions.html", "group__cpu__cc26x0__definitions" ],
    [ "cc26x0/include/cpu_conf.h", "cc26x0_2include_2cpu__conf_8h.html", null ],
    [ "cc26x0/include/periph_cpu.h", "cc26x0_2include_2periph__cpu_8h.html", null ],
    [ "CPU_DEFAULT_IRQ_PRIO", "group__cpu__cc26x0.html#ga811633719ff60ee247e64b333d4b8675", null ]
];